-- MySQL dump 10.16  Distrib 10.1.35-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: aht_landing_medical_clinic
-- ------------------------------------------------------
-- Server version	10.1.35-MariaDB-1~xenial

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_ak_params`
--

DROP TABLE IF EXISTS `wp_ak_params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_ak_params` (
  `tag` varchar(255) NOT NULL,
  `data` longtext,
  PRIMARY KEY (`tag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_ak_params`
--

LOCK TABLES `wp_ak_params` WRITE;
/*!40000 ALTER TABLE `wp_ak_params` DISABLE KEYS */;
INSERT INTO `wp_ak_params` VALUES ('update_version','3.1.0');
/*!40000 ALTER TABLE `wp_ak_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_ak_profiles`
--

DROP TABLE IF EXISTS `wp_ak_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_ak_profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `configuration` longtext,
  `filters` longtext,
  `quickicon` tinyint(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_ak_profiles`
--

LOCK TABLES `wp_ak_profiles` WRITE;
/*!40000 ALTER TABLE `wp_ak_profiles` DISABLE KEYS */;
INSERT INTO `wp_ak_profiles` VALUES (1,'Default Backup Profile','###AES128###hC/6OsHC37aKfIC5NpDYOfzpCrrNCenm7fWzITDk2nJ3g7Jh/qYJxupgUEBfkOT2pxufOq12FGdKnUZ99S5EdqhDVuL/b4j+y0aCPoNRBzmCWpJ8UWlqFrwMH4ePiElAP2FY3oHe/JwHCgjeRwRqKVg/Mqp2NmuiZmQkCu63cxmO8deySqmgMTxkAkaL2p+ecTNGg+3W/VAJZek0uWDEUBaO1HnyGUl7RmOrV19t2G5mJ/cgDFerKIuge/QtpLlWl0BRCL26kTQ56OH4VKfw4Ma63BXN5uVM8v/X0oLSJ70W6cJCg0WWl9eUU+/Wv2arZm4+b/87B+CSVlh0KfuWT++PZlceifDYfI8TarmH4mhJ+uj77o+2oXU1/jPHymgsLQ9zN4w8jb0eXnVo+OLHoCkgUpBSrhhJ8YhjZ847WdBFdtyBZXeCkLOhz2J1Y0eTXHx5syJB3V96riTbx2S+cdcqlKe2Z7myYoxT8E4GCYu5xvz4mQr0zMWVyrXkDSNEs1wk5YU848edtxxfkFoJZWuQHpGrLfGX27UMiAZPtEU0sig9zeYp9W6xURpVI7wqEdrxNeguiitzyIDWX8bsHks9W2PRiQ8ntmMEhItb9NSUiP5pcTaiInGYDCcNufFF0TgWHUp+Dumwv7yoYtEnLReZSgXSC0sOUs019FcEQJwFQ/w7QmMhudqKOfxypKe1OpvziYF/eh2t4qY7LYj3GeKOnO877XgHUAJKcELWsZuqr1praqagpd/lFYTCu5n0A5D7TQcIdZTxTzR9GO+JxWtlvJVnjXF7QW+vHbaa+YRxftT2qDdF1eZVrL5zJQ1OuNvD+erVqFc2FSkxjnxuEJDcsHWxU12+QcH5zKVwzovdlaUUWi++XVggGv3wOcDAFrLZKHo4N9LZB+BUfDv7hp5t64m6DKYadVE/iYSat+7aeuM+fnaflyg5O7/wjoR8IE9HO9ldhOl0gKw36CCG9/4DgIv1OY55VwqFn1MAsugEX7dTAgYvlZtG/KKa49QJE0OHWf6XeiV7QZfo2FyAsUu0ThLbb1ZwygcIfLxXWLaXdS+1p0O13fcuiPi7ky6eTQU3ymNUTL7SmEZY4knyFzI34q1P1o/FwBkIPNR7L04C/ZJbJYHGH6SkjhEByAKuo8hFRONrPHn0fzRp3R6zXl46sAuUpU7E8PR3O6kd5l8dbZH/x7sS8XLqwiciKBFaMGP9s+TGG/F45Z/fdzTzE5zTZvAWlmZ7BN6fUTf70i6Qd8+v2e8oOhabhHKLue69cMJ5tryXP13jGa6lIjnhFmRSilMsShYCMkFYb98pU6Wx/2gDox9n1VCrHXe5jwVpYvVJ8GeiSQbX+x/iX6mwU4fZl/HCUGxE9gqQ03BZsg5OjsowFIKMobzEjhfRFiMGZsMj/pAQCMEQ2V0uSIBORYQz4XsRbmpCA/fj1s0I+m7sm6VhZrGkZYTBTkpMbynLFeWIfOs75Uxia8Jw7m+58FeHIiFFWtqknetO+P5gSsuQpbnAylhSH1NbbufDxlKnIjT12C6wePrc1f3gvgtBEZgdiG6HCUwzbeiMFywnF489cA8zCBhmgsPT1B8kTwBwMw9Hd9Z43N7Dl79N37nfwhOcZLHRdq9p/AaZlYqPmAfLflmiusZzqwkHkVhQx7ETan7LT4+hulQYv8xegsDTkk2punTi0oJVfoogNHJpqX1tN2++Af1qjL9lk6LIxdwbnahihL0+6ceObEtBAQR1z7R8NW79QrLvyiEP9gSBrufoD1scx2oPrnZfTw6Tco2RoLcNkoxBLdrqTY3zhXFGzHKE2pQydeC9Z2lBvOmSd3nW++dAfm0cJtq5NjMtSwJQhFYyvxldw3o1tsPQuqfQgKdotDo6ZVQN2JPHiVflayxUonEJHiBArX8h/qy6TD+aCg7hm2sGBsF5ZPOOdsJif2NiZsKblSkbSw139IqM/q2XCyCU3IWcfhxOJP+rj0VJ33/OcKsAv4H83X5d7kzgJRMfdPidPL56qhHEgUKsiI+DxvK/iMyJCOu6hpzppmpz7dcdn/F4H5UC5LzkfM51dDOPI4Oht4hNx22GraatFUGS83apxISDAx88dVgZ5nAGy7Na7iz88Im6OXmT5VbGTPIl/p7CabNMXuCrnwZzOWAdrreXDg2W5AA9nceGCbiQKRgdAHhKvs9+ILO7ebzOuaXPc4WRizt5rvoovVLRHSubWYXqm7LtJar+uh8YY4bnGqTEbAmxQWI61suON0U33BLskds+UFK3Jb/QDc1T7RUnOUSiV+HSuLfhMcbhJDVWNorC2NjdylJ/x0Nlmc9mUWfEoiWz5fqo4rZN5uUTfAkAHU+W9mnIv2PWaOC7sh+seJO34tVxRUXx7mwZalDHue5jYQUtcgH5FuGALXc7apI1iSAKQf3FwTexoe/z2sjg6eDSG0+U58RCSF27BvnZAoJXWQZaUVJc9OH2Ngs7gN/XTSjATHBytqqdy9l+O+IQOQw+niko1TJT7qgbRPoiFCtige4duY4q7mlLQki3r5X+0FSW7se37Dys7n+/wxxRUvMA6kkdYAR01N8IpreVCHDYZeraxRIKZ4t532kgC9r9kqhW9duD0eCIk0zjXDvWYDkefdNKdY+9TeRvLIquo5wxV6jJJbEPoZ2ZFWO9M3NKUFNUM2b/50TABtkyI4lvbD6vvnkr/kRD3ofMHkozt5+Nu9mDVh3z2XZjtaDHXJmaiqSWHDsWrHVowrZm1kdtrfHtrEpQSVY2gdqdPXnJm9ewhQDgjxYsxgcAAA==','',1);
/*!40000 ALTER TABLE `wp_ak_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_ak_stats`
--

DROP TABLE IF EXISTS `wp_ak_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_ak_stats` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `comment` longtext,
  `backupstart` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `backupend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('run','fail','complete') NOT NULL DEFAULT 'run',
  `origin` varchar(30) NOT NULL DEFAULT 'backend',
  `type` varchar(30) NOT NULL DEFAULT 'full',
  `profile_id` bigint(20) NOT NULL DEFAULT '1',
  `archivename` longtext,
  `absolute_path` longtext,
  `multipart` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(255) DEFAULT NULL,
  `backupid` varchar(255) DEFAULT NULL,
  `filesexist` tinyint(3) NOT NULL DEFAULT '1',
  `remote_filename` varchar(1000) DEFAULT NULL,
  `total_size` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_fullstatus` (`filesexist`,`status`) USING BTREE,
  KEY `idx_stale` (`status`,`origin`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_ak_stats`
--

LOCK TABLES `wp_ak_stats` WRITE;
/*!40000 ALTER TABLE `wp_ak_stats` DISABLE KEYS */;
INSERT INTO `wp_ak_stats` VALUES (1,'admin','','2019-02-14 11:10:02','2019-02-14 11:11:51','complete','backend','full',1,'site-localhost-20190214-111002utc.jpa','D:/xampp5/htdocs/aht/landing-pages/charity/wp-content/plugins/akeebabackupwp/app/backups/site-localhost-20190214-111002utc.jpa',1,'backend','id1',1,NULL,29814183);
/*!40000 ALTER TABLE `wp_ak_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_ak_storage`
--

DROP TABLE IF EXISTS `wp_ak_storage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_ak_storage` (
  `tag` varchar(255) NOT NULL,
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data` longtext,
  PRIMARY KEY (`tag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_ak_storage`
--

LOCK TABLES `wp_ak_storage` WRITE;
/*!40000 ALTER TABLE `wp_ak_storage` DISABLE KEYS */;
INSERT INTO `wp_ak_storage` VALUES ('liveupdate','2019-02-15 13:37:37','{\"stuck\":0,\"software\":\"Akeeba Backup for WordPress\",\"version\":\"3.4.0\",\"link\":\"http:\\/\\/cdn.akeebabackup.com\\/downloads\\/backupwp\\/3.4.0\\/akeebabackupwp-3.4.0-core.zip\",\"date\":\"2019-02-12\",\"releasenotes\":\"<h3>What\'s new<\\/h3><p>    <strong>Removed support for PrestaShop<\\/strong>. The release of PrestaShop 1.7.5, which is essentially a completely different application, means that <a href=\\\"https:\\/\\/github.com\\/akeeba\\/angie\\/issues\\/63\\\">we can no longer make transferred sites work properly<\\/a>. As a result we had to discontinue support for PrestaShop altogether.<\\/p><p>    <strong>Better WordPress .htaccess support<\\/strong>. Our restoration script handles many more cases for transferring a WordPress site between hosts and folders. Please note that at this point it\'s not possible to handle .htaccess changes correctly when transferring from the domain root to a subdirectory of the same or a different (sub)domain. Our troubleshooter includes instructions for manually handling these cases \\u2013 essentially removing .htaccess content that\'s not core WordPress and regenerating it manually through the plugins of the restored site.<\\/p><h3>PHP versions supported<\\/h3><p>    We only officially support using our software with PHP 5.6, 7.1, 7.2 or 7.3. We strongly advise you to run the latest available version of PHP on a branch currently maintained by the PHP project for security and performance reasons. Older versions of PHP have known major security issues which are being actively exploited to hack sites and they have stopped receiving security updates, leaving you exposed to these issues. Moreover, they are slower, therefore consuming more server resources to perform the same tasks.<\\/p><p>    Kindly note that our policy is to officially support only the PHP versions which are not yet End Of Life per the official PHP project with a voluntarily extension of support for 6 to 9 months after they become End of Life. After that time we stop providing any support for these obsolete versions of PHP without any further notice.<\\/p><h3>Changelog<\\/h3><h4>Bug fixes<\\/h4><ul>\\t<li>[HIGH] Google Storage JSON API could not download files when the path or filename contained spaces<\\/li>\\t<li>[HIGH] Google Storage would create large files with %2F in the filename instead of using subdirectories (the Google API documentation was, unfortunately, instructing us to do something wrong)<\\/li>\\t<li>[LOW] Fixed styling in ALICE page<\\/li>\\t<li>[LOW] Google Storage would not work on hosts which disable parse_ini_string()<\\/li>\\t<li>[LOW] Some character combinations in configuration values (e.g. $) could get changed or removed upon saving<\\/li>\\t<li>[MEDIUM] Integrated restoration with JPS archives wasn\'t allowed<\\/li>\\t<li>[MEDIUM] Restoring with the FTP or Hybrid file write mode didn\'t work properly<\\/li><\\/ul><h4>New features<\\/h4><ul>\\t<li>WordPress restoration: Handling of more .htaccess use cases<\\/li><\\/ul><h4>Removed features<\\/h4><ul>\\t<li>Removed support for PrestaShop<\\/li><\\/ul>\",\"infourl\":\"https:\\/\\/www.akeebabackup.com\\/download\\/backupwp\\/3-4-0.html\",\"md5\":\"50bcc02e315d47d2ab74a4b63266aa70\",\"sha1\":\"b7df0c460b8d4d4d978ca291b49899b929fc161c\",\"sha256\":\"360d5f14ac2f6f523af83b69c49dafb12f214e508dcabf95c2534cbe40c8d3d3\",\"sha384\":\"a3c56277faaf032c9b9d14bb683ce95c6a05b9dd17af1ab7a8f5b84e61a3beeb358652e5c5b7587583f14dd02da9010b\",\"sha512\":\"e61dcebff16885ec94396309b38ae0de88303fc478e19c1ae9ef5c807485a4cd64930c79faac694d084448ffb8f0695aca09a52aa5d7462e7432b4c6f5374067\",\"platforms\":\"classicpress\\/1.0,php\\/5.4,php\\/5.5,php\\/5.6,php\\/7.0,php\\/7.1,php\\/7.2,php\\/7.3,wordpress\\/3.8+\",\"loadedUpdate\":1,\"stability\":\"stable\"}'),('liveupdate_lastcheck','2019-02-15 13:37:36','1550212656');
/*!40000 ALTER TABLE `wp_ak_storage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_ak_users`
--

DROP TABLE IF EXISTS `wp_ak_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_ak_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `parameters` longtext,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_ak_users`
--

LOCK TABLES `wp_ak_users` WRITE;
/*!40000 ALTER TABLE `wp_ak_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_ak_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_akeeba_common`
--

DROP TABLE IF EXISTS `wp_akeeba_common`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_akeeba_common` (
  `key` varchar(190) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_akeeba_common`
--

LOCK TABLES `wp_akeeba_common` WRITE;
/*!40000 ALTER TABLE `wp_akeeba_common` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_akeeba_common` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`) USING BTREE,
  KEY `comment_id` (`comment_id`) USING BTREE,
  KEY `meta_key` (`meta_key`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`) USING BTREE,
  KEY `comment_post_ID` (`comment_post_ID`) USING BTREE,
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`) USING BTREE,
  KEY `comment_date_gmt` (`comment_date_gmt`) USING BTREE,
  KEY `comment_parent` (`comment_parent`) USING BTREE,
  KEY `comment_author_email` (`comment_author_email`(10)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`) USING BTREE,
  KEY `link_visible` (`link_visible`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`) USING BTREE,
  UNIQUE KEY `option_name` (`option_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=608 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` VALUES (1,'siteurl','http://landing.wizworks.io/medical-clinic/','yes'),(2,'home','http://landing.wizworks.io/medical-clinic/','yes'),(3,'blogname','Medical_clinic','yes'),(4,'blogdescription','Just another WordPress site','yes'),(5,'users_can_register','0','yes'),(6,'admin_email','example@example.com','yes'),(7,'start_of_week','1','yes'),(8,'use_balanceTags','0','yes'),(9,'use_smilies','1','yes'),(10,'require_name_email','1','yes'),(11,'comments_notify','1','yes'),(12,'posts_per_rss','10','yes'),(13,'rss_use_excerpt','0','yes'),(14,'mailserver_url','mail.example.com','yes'),(15,'mailserver_login','login@example.com','yes'),(16,'mailserver_pass','password','yes'),(17,'mailserver_port','110','yes'),(18,'default_category','1','yes'),(19,'default_comment_status','open','yes'),(20,'default_ping_status','open','yes'),(21,'default_pingback_flag','1','yes'),(22,'posts_per_page','10','yes'),(23,'date_format','F j, Y','yes'),(24,'time_format','g:i a','yes'),(25,'links_updated_date_format','F j, Y g:i a','yes'),(26,'comment_moderation','0','yes'),(27,'moderation_notify','1','yes'),(28,'permalink_structure','','yes'),(29,'rewrite_rules','','yes'),(30,'hack_file','0','yes'),(31,'blog_charset','UTF-8','yes'),(32,'moderation_keys','','no'),(33,'active_plugins','a:1:{i:0;s:36:\"contact-form-7/wp-contact-form-7.php\";}','yes'),(34,'category_base','','yes'),(35,'ping_sites','http://rpc.pingomatic.com/','yes'),(36,'comment_max_links','2','yes'),(37,'gmt_offset','0','yes'),(38,'default_email_category','1','yes'),(39,'recently_edited','','no'),(40,'template','medical_clinic','yes'),(41,'stylesheet','medical_clinic','yes'),(42,'comment_whitelist','1','yes'),(43,'blacklist_keys','','no'),(44,'comment_registration','0','yes'),(45,'html_type','text/html','yes'),(46,'use_trackback','0','yes'),(47,'default_role','subscriber','yes'),(48,'db_version','43764','yes'),(49,'uploads_use_yearmonth_folders','1','yes'),(50,'upload_path','','yes'),(51,'blog_public','1','yes'),(52,'default_link_category','2','yes'),(53,'show_on_front','page','yes'),(54,'tag_base','','yes'),(55,'show_avatars','1','yes'),(56,'avatar_rating','G','yes'),(57,'upload_url_path','','yes'),(58,'thumbnail_size_w','180','yes'),(59,'thumbnail_size_h','179','yes'),(60,'thumbnail_crop','1','yes'),(61,'medium_size_w','846','yes'),(62,'medium_size_h','451','yes'),(63,'avatar_default','mystery','yes'),(64,'large_size_w','1024','yes'),(65,'large_size_h','1024','yes'),(66,'image_default_link_type','','yes'),(67,'image_default_size','','yes'),(68,'image_default_align','','yes'),(69,'close_comments_for_old_posts','0','yes'),(70,'close_comments_days_old','14','yes'),(71,'thread_comments','1','yes'),(72,'thread_comments_depth','5','yes'),(73,'page_comments','0','yes'),(74,'comments_per_page','50','yes'),(75,'default_comments_page','newest','yes'),(76,'comment_order','asc','yes'),(77,'sticky_posts','a:0:{}','yes'),(78,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),(79,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),(80,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes'),(81,'uninstall_plugins','a:1:{s:33:\"akeebabackupwp/akeebabackupwp.php\";a:2:{i:0;s:14:\"AkeebaBackupWP\";i:1;s:9:\"uninstall\";}}','no'),(82,'timezone_string','','yes'),(83,'page_for_posts','123','yes'),(84,'page_on_front','2','yes'),(85,'default_post_format','0','yes'),(86,'link_manager_enabled','0','yes'),(87,'finished_splitting_shared_terms','1','yes'),(88,'site_icon','0','yes'),(89,'medium_large_size_w','768','yes'),(90,'medium_large_size_h','0','yes'),(91,'wp_page_for_privacy_policy','3','yes'),(92,'show_comments_cookies_opt_in','0','yes'),(93,'initial_db_version','43764','yes'),(94,'wp_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),(95,'fresh_site','0','yes'),(96,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),(97,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),(98,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),(99,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),(100,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),(101,'sidebars_widgets','a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}','yes'),(102,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(103,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(104,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(105,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(106,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(107,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(108,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(109,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(110,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(111,'cron','a:7:{i:1560933376;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1560942018;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1560962176;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1561004340;a:1:{s:29:\"mc4wp_refresh_mailchimp_lists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1561005489;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1561005490;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes'),(112,'theme_mods_twentynineteen','a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1551410133;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}}','yes'),(123,'can_compress_scripts','1','no'),(137,'current_theme','medical_clinic','yes'),(138,'theme_mods_makeupartist','a:7:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:4:\"logo\";s:78:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/logo.png\";s:16:\"background_image\";s:76:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/bg.jpg\";s:15:\"background_size\";s:5:\"cover\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1550033884;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),(139,'theme_switched','','yes'),(141,'wpcf7','a:2:{s:7:\"version\";s:5:\"5.1.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1550032751;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}','yes'),(142,'recently_activated','a:0:{}','yes'),(143,'widget_makeupartists_latest_tweet','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(144,'widget_makeupartists_facebook_likebox','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(145,'widget_makeupartists_instagram_feed','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(146,'widget_makeupartists_about_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(147,'widget_mc4wp_form_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),(148,'mc4wp_version','4.3.3','yes'),(151,'mc4wp_flash_messages','a:0:{}','no'),(152,'mc4wp','a:4:{s:7:\"api_key\";s:37:\"e3c623f71aedd9077b9b00f96ac0ffb3-us13\";s:20:\"allow_usage_tracking\";i:0;s:15:\"debug_log_level\";s:7:\"warning\";s:18:\"first_activated_on\";i:1550032787;}','yes'),(153,'mc4wp_default_form_id','6','yes'),(154,'mc4wp_form_stylesheets','a:0:{}','yes'),(157,'mc4wp_mailchimp_list_ids','a:5:{i:0;s:10:\"3eaf265522\";i:1;s:10:\"40a0127437\";i:2;s:10:\"5aa392c949\";i:3;s:10:\"bdc2fcba4f\";i:4;s:10:\"f6840a0d51\";}','no'),(158,'mc4wp_mailchimp_list_5aa392c949','O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"5aa392c949\";s:6:\"web_id\";i:227941;s:4:\"name\";s:9:\"Test List\";s:16:\"subscriber_count\";i:1;s:12:\"merge_fields\";a:2:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:9:\"Le Truong\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}','no'),(159,'mc4wp_mailchimp_list_bdc2fcba4f','O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"bdc2fcba4f\";s:6:\"web_id\";i:354489;s:4:\"name\";s:10:\"newsletter\";s:16:\"subscriber_count\";i:1;s:12:\"merge_fields\";a:3:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:10:\"First Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"FNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:2;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:10:\"Full State\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}','no'),(160,'mc4wp_mailchimp_list_3eaf265522','O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"3eaf265522\";s:6:\"web_id\";i:354533;s:4:\"name\";s:9:\"User - EN\";s:16:\"subscriber_count\";i:8;s:12:\"merge_fields\";a:3:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:10:\"First Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"FNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:2;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:7:\"User EN\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}','no'),(161,'mc4wp_mailchimp_list_f6840a0d51','O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"f6840a0d51\";s:6:\"web_id\";i:354541;s:4:\"name\";s:9:\"User - DE\";s:16:\"subscriber_count\";i:1;s:12:\"merge_fields\";a:3:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:10:\"First Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"FNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:2;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:7:\"User De\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}','no'),(162,'mc4wp_mailchimp_list_40a0127437','O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"40a0127437\";s:6:\"web_id\";i:354537;s:4:\"name\";s:9:\"User - FR\";s:16:\"subscriber_count\";i:0;s:12:\"merge_fields\";a:3:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:10:\"First Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"FNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:2;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:7:\"User Fr\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}','no'),(169,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes'),(175,'theme_mods_charity','a:20:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"background_image\";s:76:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/bg.jpg\";s:4:\"logo\";s:78:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/logo.png\";s:13:\"google_font_h\";s:11:\"Baloo Tamma\";s:11:\"google_font\";s:0:\"\";s:20:\"newsletter_shortcode\";s:0:\"\";s:9:\"copyright\";s:127:\"<p class=\"text-center m-0\">(C) All Rights Reserved. Charity Theme, Designed &amp; Developed by <a href=\"#\">Template.net</a></p>\";s:16:\"banner_sub_title\";s:137:\"We are a group united by our passion to make a difference. We vow to help create change by making a better living condition for the poor.\";s:17:\"background_banner\";s:80:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/banner.png\";s:12:\"banner_title\";s:49:\"Forget what you can get and see what you can give\";s:17:\"about_col_1_title\";s:11:\"Our Mission\";s:20:\"donate_right_content\";s:265:\"<p>145 Amands Street, Beverly Hill Sights, NYC 2345</p>\n<p>Phone:<a href=\"tel:(800) 0123 4567 890\">(800) 0123 4567 890</a></p>\n<p>Email:<a href=\"mailto:barbershop@email.com\">barbershop@email.com</a></p>\n<p><a href=\"www.charitytheme.com\">www.charitytheme.com</a></p>\";s:13:\"donate_button\";s:10:\"Donate Now\";s:17:\"contact_shortcode\";s:39:\"[contact-form-7 id=\"5\" title=\"Contact\"]\";s:18:\"whatwedid_category\";s:1:\"3\";s:10:\"blogs_show\";s:3:\"yes\";s:14:\"blogs_category\";s:1:\"4\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1550291853;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),(274,'theme_mods_theme_file/charity','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1551409983;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),(289,'theme_mods_medical-clinic','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1551410129;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),(293,'theme_mods_medical_clinic','a:10:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:17:\"contact_shortcode\";s:39:\"[contact-form-7 id=\"5\" title=\"Contact\"]\";s:18:\"sub_title1_servies\";s:224:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque nec nam Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do sem et tortor. \";s:18:\"sub_title2_servies\";s:224:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque nec nam Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do sem et tortor. \";s:18:\"sub_title3_servies\";s:224:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque nec nam Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do sem et tortor. \";s:18:\"sub_title4_servies\";s:224:\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque nec nam Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do sem et tortor. \";s:11:\"google_font\";s:0:\"\";s:20:\"services_number_post\";s:1:\"4\";}','yes'),(350,'WPLANG','','yes'),(351,'new_admin_email','example@example.com','yes'),(383,'category_children','a:0:{}','yes'),(510,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:5:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.1\";s:7:\"version\";s:5:\"5.2.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:3;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.2.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.2-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.2\";s:7:\"version\";s:3:\"5.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:4;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.1.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.1.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1560931772;s:15:\"version_checked\";s:5:\"5.0.4\";s:12:\"translations\";a:0:{}}','no'),(513,'auto_core_update_notified','a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:19:\"example@example.com\";s:7:\"version\";s:5:\"5.0.4\";s:9:\"timestamp\";i:1554446826;}','no'),(604,'_site_transient_timeout_theme_roots','1560933573','no'),(605,'_site_transient_theme_roots','a:4:{s:14:\"medical_clinic\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}','no'),(606,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1560931775;s:7:\"checked\";a:4:{s:14:\"medical_clinic\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"1.2\";s:15:\"twentyseventeen\";s:3:\"2.0\";s:13:\"twentysixteen\";s:3:\"1.8\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.4\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.4.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.2\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.2.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:13:\"twentysixteen\";a:6:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.2.0.zip\";s:8:\"requires\";s:3:\"4.4\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}','no'),(607,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1560931776;s:7:\"checked\";a:3:{s:19:\"akismet/akismet.php\";s:3:\"4.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.1\";s:9:\"hello.php\";s:5:\"1.7.1\";}s:8:\"response\";a:3:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:9:\"hello.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:0:{}}','no');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`) USING BTREE,
  KEY `post_id` (`post_id`) USING BTREE,
  KEY `meta_key` (`meta_key`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=333 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` VALUES (1,2,'_wp_page_template','homepage.php'),(2,3,'_wp_page_template','default'),(3,5,'_form','<h2 class=\"font-weight-bold\">Need Assistance?Contact us now.</h2>\n<div class=\"form-row\">\n	<div class=\"form-group col\">\n		[text* your-name  class:form-control form-control-lg placeholder\"Your Name\"]\n	</div>\n</div>\n<div class=\"form-row\">\n	<div class=\"form-group col\">\n		[email your-email class:form-control form-control-lg placeholder \"Your Email\"]\n	</div>\n</div>\n<div class=\"form-row\">\n	<div class=\"form-group col\">\n		[text* your-phone  class:form-control form-control-lg placeholder\"Your Phone\"]\n	</div>\n</div>\n\n<div class=\"form-row\">\n	<div class=\"form-group  col\">\n		[textarea your-message class:form-control class:form-control-lg placeholder \"Type your message\"]\n	</div>\n</div>\n<div class=\"form-row\">\n	<div class=\"form-group col\">\n		[submit class:btn class:btn-primary class:btn-lg class:btn-block \"Submit Form\"]\n	</div>\n</div>'),(4,5,'_mail','a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:24:\"Charity \"[your-subject]\"\";s:6:\"sender\";s:29:\"Charity <example@example.com>\";s:9:\"recipient\";s:19:\"example@example.com\";s:4:\"body\";s:188:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Charity (http://localhost/aht/landing-pages/charity)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),(5,5,'_mail_2','a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"Charity \"[your-subject]\"\";s:6:\"sender\";s:29:\"Charity <example@example.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:130:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Charity (http://localhost/aht/landing-pages/charity)\";s:18:\"additional_headers\";s:29:\"Reply-To: example@example.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),(6,5,'_messages','a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),(7,5,'_additional_settings',''),(8,5,'_locale','en_US'),(9,6,'_mc4wp_settings','a:8:{s:15:\"required_fields\";s:5:\"EMAIL\";s:12:\"double_optin\";s:1:\"1\";s:15:\"update_existing\";s:1:\"0\";s:17:\"replace_interests\";s:1:\"1\";s:18:\"hide_after_success\";s:1:\"0\";s:8:\"redirect\";s:0:\"\";s:3:\"css\";s:1:\"0\";s:5:\"lists\";a:0:{}}'),(10,6,'text_subscribed','Thank you, your sign-up request was successful! Please check your email inbox to confirm.'),(11,6,'text_invalid_email','Please provide a valid email address.'),(12,6,'text_required_field_missing','Please fill in the required fields.'),(13,6,'text_already_subscribed','Given email address is already subscribed, thank you!'),(14,6,'text_error','Oops. Something went wrong. Please try again later.'),(15,6,'text_unsubscribed','You were successfully unsubscribed.'),(16,6,'text_not_subscribed','Given email address is not subscribed.'),(17,6,'text_no_lists_selected','Please select at least one list.'),(18,6,'text_updated','Thank you, your records have been updated!'),(19,2,'_edit_lock','1551501520:1'),(20,8,'_menu_item_type','post_type'),(21,8,'_menu_item_menu_item_parent','0'),(22,8,'_menu_item_object_id','2'),(23,8,'_menu_item_object','page'),(24,8,'_menu_item_target',''),(25,8,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),(26,8,'_menu_item_xfn',''),(27,8,'_menu_item_url',''),(29,9,'_menu_item_type','custom'),(30,9,'_menu_item_menu_item_parent','0'),(31,9,'_menu_item_object_id','9'),(32,9,'_menu_item_object','custom'),(33,9,'_menu_item_target',''),(34,9,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),(35,9,'_menu_item_xfn',''),(36,9,'_menu_item_url','#services'),(38,10,'_menu_item_type','custom'),(39,10,'_menu_item_menu_item_parent','0'),(40,10,'_menu_item_object_id','10'),(41,10,'_menu_item_object','custom'),(42,10,'_menu_item_target',''),(43,10,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),(44,10,'_menu_item_xfn',''),(45,10,'_menu_item_url','#projects'),(47,11,'_menu_item_type','custom'),(48,11,'_menu_item_menu_item_parent','0'),(49,11,'_menu_item_object_id','11'),(50,11,'_menu_item_object','custom'),(51,11,'_menu_item_target',''),(52,11,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),(53,11,'_menu_item_xfn',''),(54,11,'_menu_item_url','#team'),(65,13,'_menu_item_type','custom'),(66,13,'_menu_item_menu_item_parent','0'),(67,13,'_menu_item_object_id','13'),(68,13,'_menu_item_object','custom'),(69,13,'_menu_item_target',''),(70,13,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),(71,13,'_menu_item_xfn',''),(72,13,'_menu_item_url','#contact'),(159,52,'_edit_lock','1552044647:1'),(232,5,'_config_errors','a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),(241,82,'_edit_last','1'),(242,82,'_edit_lock','1552044089:1'),(243,82,'testimonial_position','(United Stated of America)'),(255,87,'_edit_last','1'),(256,87,'_edit_lock','1552043929:1'),(258,87,'testimonial_position',' (Australia)'),(259,89,'_edit_last','1'),(260,89,'_edit_lock','1552043911:1'),(262,89,'testimonial_position','(United Stated of America)'),(302,110,'_wp_attached_file','2019/02/item-carousel-2.jpg'),(303,110,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:430;s:6:\"height\";i:298;s:4:\"file\";s:27:\"2019/02/item-carousel-2.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"item-carousel-2-180x179.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"galley-img-4\";a:4:{s:4:\"file\";s:27:\"item-carousel-2-180x179.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(306,52,'_thumbnail_id','110'),(307,111,'_wp_attached_file','2019/03/avatar1.png'),(308,111,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:95;s:6:\"height\";i:95;s:4:\"file\";s:19:\"2019/03/avatar1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(309,89,'_thumbnail_id','111'),(310,112,'_wp_attached_file','2019/03/avatar2.png'),(311,112,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:95;s:6:\"height\";i:95;s:4:\"file\";s:19:\"2019/03/avatar2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(312,87,'_thumbnail_id','112'),(313,113,'_wp_attached_file','2019/03/avatar3.png'),(314,113,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:95;s:6:\"height\";i:95;s:4:\"file\";s:19:\"2019/03/avatar3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),(315,82,'_thumbnail_id','113'),(332,123,'_edit_lock','1557392677:1');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `post_name` (`post_name`(191)) USING BTREE,
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`) USING BTREE,
  KEY `post_parent` (`post_parent`) USING BTREE,
  KEY `post_author` (`post_author`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (2,1,'2019-02-13 04:36:14','2019-02-13 04:36:14','','Home','','publish','closed','open','','sample-page','','','2019-02-13 07:53:05','2019-02-13 07:53:05','',0,'http://localhost/aht/landing-pages/charity/?page_id=2',0,'page','',0),(3,1,'2019-02-13 04:36:14','2019-02-13 04:36:14','<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/aht/landing-pages/charity.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->','Privacy Policy','','draft','closed','open','','privacy-policy','','','2019-02-13 04:36:14','2019-02-13 04:36:14','',0,'http://localhost/aht/landing-pages/charity/?page_id=3',0,'page','',0),(5,1,'2019-02-13 04:39:11','2019-02-13 04:39:11','<h2 class=\"font-weight-bold\">Need Assistance?Contact us now.</h2>\r\n<div class=\"form-row\">\r\n	<div class=\"form-group col\">\r\n		[text* your-name  class:form-control form-control-lg placeholder\"Your Name\"]\r\n	</div>\r\n</div>\r\n<div class=\"form-row\">\r\n	<div class=\"form-group col\">\r\n		[email your-email class:form-control form-control-lg placeholder \"Your Email\"]\r\n	</div>\r\n</div>\r\n<div class=\"form-row\">\r\n	<div class=\"form-group col\">\r\n		[text* your-phone  class:form-control form-control-lg placeholder\"Your Phone\"]\r\n	</div>\r\n</div>\r\n\r\n<div class=\"form-row\">\r\n	<div class=\"form-group  col\">\r\n		[textarea your-message class:form-control class:form-control-lg placeholder \"Type your message\"]\r\n	</div>\r\n</div>\r\n<div class=\"form-row\">\r\n	<div class=\"form-group col\">\r\n		[submit class:btn class:btn-primary class:btn-lg class:btn-block \"Submit Form\"]\r\n	</div>\r\n</div>\n1\nCharity \"[your-subject]\"\nCharity <example@example.com>\nexample@example.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Charity (http://localhost/aht/landing-pages/charity)\nReply-To: [your-email]\n\n\n\n\nCharity \"[your-subject]\"\nCharity <example@example.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Charity (http://localhost/aht/landing-pages/charity)\nReply-To: example@example.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.','Contact','','publish','closed','closed','','contact-form-1','','','2019-03-02 03:36:29','2019-03-02 03:36:29','',0,'http://localhost/aht/landing-pages/charity/?post_type=wpcf7_contact_form&#038;p=5',0,'wpcf7_contact_form','',0),(6,1,'2019-02-13 04:39:59','2019-02-13 04:39:59','<div class=\"form-box\">\r\n  <div class=\"field-box\">\r\n    <label class=\"screen-reader-text\">>Email address: </label>\r\n    <input type=\"email\" name=\"EMAIL\" placeholder=\"Subscribe to Newsletter\" required />\r\n  </div>\r\n  <div class=\"action-box\">\r\n    <button type=\"submit\">Subscribe</button>\r\n  </div>\r\n</div>','','','publish','closed','closed','','6','','','2019-02-13 04:39:59','2019-02-13 04:39:59','',0,'http://localhost/aht/landing-pages/charity/mc4wp-form/6/',0,'mc4wp-form','',0),(7,1,'2019-02-13 04:43:03','2019-02-13 04:43:03','','Home','','inherit','closed','closed','','2-revision-v1','','','2019-02-13 04:43:03','2019-02-13 04:43:03','',2,'http://localhost/aht/landing-pages/charity/2019/02/13/2-revision-v1/',0,'revision','',0),(8,1,'2019-02-13 04:44:55','2019-02-13 04:44:55','','ABOUT US','','publish','closed','closed','','8','','','2019-03-14 10:57:49','2019-03-14 10:57:49','',0,'http://localhost/aht/landing-pages/charity/?p=8',1,'nav_menu_item','',0),(9,1,'2019-02-13 04:44:56','2019-02-13 04:44:56','','SERVICES','','publish','closed','closed','','about-us','','','2019-03-14 10:57:49','2019-03-14 10:57:49','',0,'http://localhost/aht/landing-pages/charity/?p=9',2,'nav_menu_item','',0),(10,1,'2019-02-13 04:44:56','2019-02-13 04:44:56','','BLOG','','publish','closed','closed','','donate','','','2019-03-14 10:57:49','2019-03-14 10:57:49','',0,'http://localhost/aht/landing-pages/charity/?p=10',3,'nav_menu_item','',0),(11,1,'2019-02-13 04:44:56','2019-02-13 04:44:56','','TESTIMONIALS','','publish','closed','closed','','what-we-did','','','2019-03-14 10:57:49','2019-03-14 10:57:49','',0,'http://localhost/aht/landing-pages/charity/?p=11',4,'nav_menu_item','',0),(13,1,'2019-02-13 04:44:56','2019-02-13 04:44:56','','CONTACT','','publish','closed','closed','','contact','','','2019-03-14 10:57:49','2019-03-14 10:57:49','',0,'http://localhost/aht/landing-pages/charity/?p=13',5,'nav_menu_item','',0),(52,1,'2019-02-15 02:26:28','2019-02-15 02:26:28','<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. </p>\n<!-- /wp:paragraph -->','Dedicated to provide and facilitate access to healthcare and enhance the health','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. ','publish','open','open','','500-girls-supported-before-during-and-after-emergencies-2','','','2019-03-08 11:20:05','2019-03-08 11:20:05','',0,'http://localhost/aht/landing-pages/charity/?p=52',0,'post','',0),(53,1,'2019-02-15 02:26:28','2019-02-15 02:26:28','<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. </p>\n<!-- /wp:paragraph -->','500+ Girls Supported Before, During and After Emergencies','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. ','inherit','closed','closed','','52-revision-v1','','','2019-02-15 02:26:28','2019-02-15 02:26:28','',52,'http://localhost/aht/landing-pages/charity/52-revision-v1/',0,'revision','',0),(69,1,'2019-02-16 04:39:28','2019-02-16 04:39:28','','Home','','inherit','closed','closed','','2-autosave-v1','','','2019-02-16 04:39:28','2019-02-16 04:39:28','',2,'http://landing.wizworks.io/medical-clinic/2-autosave-v1/',0,'revision','',0),(82,1,'2019-03-02 04:43:48','2019-03-02 04:43:48','<em class=\"text-blockquote\">Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em>','Sandra Bullock','','publish','closed','closed','','ddddddddd','','','2019-03-08 11:21:29','2019-03-08 11:21:29','',0,'http://landing.wizworks.io/medical-clinic/?post_type=testimonial&#038;p=82',0,'testimonial','',0),(83,1,'2019-03-02 04:43:48','2019-03-02 04:43:48','dddddddddddddddddd','ddddddddđ','','inherit','closed','closed','','82-revision-v1','','','2019-03-02 04:43:48','2019-03-02 04:43:48','',82,'http://landing.wizworks.io/medical-clinic/82-revision-v1/',0,'revision','',0),(87,1,'2019-03-02 06:34:51','2019-03-02 06:34:51','Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','Mathew Hyden','','publish','closed','closed','','sdfsdf','','','2019-03-08 11:21:10','2019-03-08 11:21:10','',0,'http://landing.wizworks.io/medical-clinic/?post_type=testimonial&#038;p=87',0,'testimonial','',0),(88,1,'2019-03-02 06:34:51','2019-03-02 06:34:51','sdfsdf','sdfsdf','','inherit','closed','closed','','87-revision-v1','','','2019-03-02 06:34:51','2019-03-02 06:34:51','',87,'http://landing.wizworks.io/medical-clinic/87-revision-v1/',0,'revision','',0),(89,1,'2019-03-02 06:35:44','2019-03-02 06:35:44','<em class=\"text-blockquote\">Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em>','Sandra Bullock','','publish','closed','closed','','sdfadas','','','2019-03-08 11:20:51','2019-03-08 11:20:51','',0,'http://landing.wizworks.io/medical-clinic/?post_type=testimonial&#038;p=89',0,'testimonial','',0),(90,1,'2019-03-02 06:35:44','2019-03-02 06:35:44','sdasd','sdfadas','','inherit','closed','closed','','89-revision-v1','','','2019-03-02 06:35:44','2019-03-02 06:35:44','',89,'http://landing.wizworks.io/medical-clinic/89-revision-v1/',0,'revision','',0),(91,1,'2019-03-02 07:08:22','2019-03-02 07:08:22','sdasdasdfasfa','sdfadas','','inherit','closed','closed','','89-revision-v1','','','2019-03-02 07:08:22','2019-03-02 07:08:22','',89,'http://landing.wizworks.io/medical-clinic/89-revision-v1/',0,'revision','',0),(92,1,'2019-03-02 07:24:39','2019-03-02 07:24:39','Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','sdfsdf','','inherit','closed','closed','','87-revision-v1','','','2019-03-02 07:24:39','2019-03-02 07:24:39','',87,'http://landing.wizworks.io/medical-clinic/87-revision-v1/',0,'revision','',0),(93,1,'2019-03-02 07:41:18','2019-03-02 07:41:18','<em class=\"text-blockquote\">Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em>','ddddddddđ','','inherit','closed','closed','','82-revision-v1','','','2019-03-02 07:41:18','2019-03-02 07:41:18','',82,'http://landing.wizworks.io/medical-clinic/82-revision-v1/',0,'revision','',0),(94,1,'2019-03-02 07:41:48','2019-03-02 07:41:48','<em class=\"text-blockquote\">Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em>','sdfadas','','inherit','closed','closed','','89-revision-v1','','','2019-03-02 07:41:48','2019-03-02 07:41:48','',89,'http://landing.wizworks.io/medical-clinic/89-revision-v1/',0,'revision','',0),(95,1,'2019-03-02 07:42:58','2019-03-02 07:42:58','<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. </p>\n<!-- /wp:paragraph -->','Dedicated to provide and facilitate access to healthcare and enhance the health','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. ','inherit','closed','closed','','52-revision-v1','','','2019-03-02 07:42:58','2019-03-02 07:42:58','',52,'http://landing.wizworks.io/medical-clinic/52-revision-v1/',0,'revision','',0),(96,1,'2019-03-02 07:48:50','2019-03-02 07:48:50','<em class=\"text-blockquote\">Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em>','Sandra Bullock','','inherit','closed','closed','','89-revision-v1','','','2019-03-02 07:48:50','2019-03-02 07:48:50','',89,'http://landing.wizworks.io/medical-clinic/89-revision-v1/',0,'revision','',0),(98,1,'2019-03-02 07:52:46','2019-03-02 07:52:46','Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','Mathew Hyden','','inherit','closed','closed','','87-revision-v1','','','2019-03-02 07:52:46','2019-03-02 07:52:46','',87,'http://landing.wizworks.io/medical-clinic/87-revision-v1/',0,'revision','',0),(100,1,'2019-03-02 07:53:39','2019-03-02 07:53:39','<em class=\"text-blockquote\">Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em>','Sandra Bullock','','inherit','closed','closed','','82-revision-v1','','','2019-03-02 07:53:39','2019-03-02 07:53:39','',82,'http://landing.wizworks.io/medical-clinic/82-revision-v1/',0,'revision','',0),(107,1,'2019-03-08 11:17:02','2019-03-08 11:17:02','<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. </p>\n<!-- /wp:paragraph -->','Dedicated to provide and facilitate access to healthcare and enhance the health','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis. ','inherit','closed','closed','','52-autosave-v1','','','2019-03-08 11:17:02','2019-03-08 11:17:02','',52,'http://landing.wizworks.io/medical-clinic/52-autosave-v1/',0,'revision','',0),(110,1,'2019-03-08 11:19:41','2019-03-08 11:19:41','','item-carousel-2','','inherit','open','closed','','item-carousel-2','','','2019-03-08 11:19:41','2019-03-08 11:19:41','',52,'http://landing.wizworks.io/medical-clinic/wp-content/uploads/2019/02/item-carousel-2.jpg',0,'attachment','image/jpeg',0),(111,1,'2019-03-08 11:20:48','2019-03-08 11:20:48','','avatar1','','inherit','open','closed','','avatar1','','','2019-03-08 11:20:48','2019-03-08 11:20:48','',89,'http://landing.wizworks.io/medical-clinic/wp-content/uploads/2019/03/avatar1.png',0,'attachment','image/png',0),(112,1,'2019-03-08 11:21:08','2019-03-08 11:21:08','','avatar2','','inherit','open','closed','','avatar2','','','2019-03-08 11:21:08','2019-03-08 11:21:08','',87,'http://landing.wizworks.io/medical-clinic/wp-content/uploads/2019/03/avatar2.png',0,'attachment','image/png',0),(113,1,'2019-03-08 11:21:26','2019-03-08 11:21:26','','avatar3','','inherit','open','closed','','avatar3','','','2019-03-08 11:21:26','2019-03-08 11:21:26','',82,'http://landing.wizworks.io/medical-clinic/wp-content/uploads/2019/03/avatar3.png',0,'attachment','image/png',0),(123,1,'2019-05-09 09:06:59','2019-05-09 09:06:59','','blog','','publish','closed','closed','','blog','','','2019-05-09 09:06:59','2019-05-09 09:06:59','',0,'http://landing.wizworks.io/medical-clinic/?page_id=123',0,'page','',0),(124,1,'2019-05-09 09:06:59','2019-05-09 09:06:59','','blog','','inherit','closed','closed','','123-revision-v1','','','2019-05-09 09:06:59','2019-05-09 09:06:59','',123,'http://landing.wizworks.io/medical-clinic/?p=124',0,'revision','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`) USING BTREE,
  KEY `term_taxonomy_id` (`term_taxonomy_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` VALUES (8,2,0),(9,2,0),(10,2,0),(11,2,0),(13,2,0),(52,5,0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`) USING BTREE,
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`) USING BTREE,
  KEY `taxonomy` (`taxonomy`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,0),(2,2,'nav_menu','',0,5),(5,5,'category','',0,1);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`) USING BTREE,
  KEY `term_id` (`term_id`) USING BTREE,
  KEY `meta_key` (`meta_key`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_termmeta`
--

LOCK TABLES `wp_termmeta` WRITE;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`) USING BTREE,
  KEY `slug` (`slug`(191)) USING BTREE,
  KEY `name` (`name`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Uncategorized','uncategorized',0),(2,'Main','main',0),(5,'section-team','section-team',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `meta_key` (`meta_key`(191)) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'nickname','admin'),(2,1,'first_name',''),(3,1,'last_name',''),(4,1,'description',''),(5,1,'rich_editing','true'),(6,1,'syntax_highlighting','true'),(7,1,'comment_shortcuts','false'),(8,1,'admin_color','fresh'),(9,1,'use_ssl','0'),(10,1,'show_admin_bar_front','true'),(11,1,'locale',''),(12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(13,1,'wp_user_level','10'),(14,1,'dismissed_wp_pointers','wp496_privacy'),(15,1,'show_welcome_panel','1'),(16,1,'session_tokens','a:3:{s:64:\"2fc9e34a24fb5c1ef4ad599138946465f768c16430b96e85a19abf13c62b7972\";a:4:{s:10:\"expiration\";i:1557565603;s:2:\"ip\";s:14:\"118.70.182.159\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36\";s:5:\"login\";i:1557392803;}s:64:\"65462d3ba32bf863711478b5add768142190b49836a5e2f1022bc58717b2e4f1\";a:4:{s:10:\"expiration\";i:1557567850;s:2:\"ip\";s:14:\"118.70.182.159\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36\";s:5:\"login\";i:1557395050;}s:64:\"1d64bee00a1c25eea9f652362e02f8342b6c1d787c9c6b895007806c9711bd1e\";a:4:{s:10:\"expiration\";i:1557647399;s:2:\"ip\";s:14:\"118.70.182.159\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36\";s:5:\"login\";i:1557474599;}}'),(17,1,'wp_dashboard_quick_press_last_post_id','125'),(18,1,'managenav-menuscolumnshidden','a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),(19,1,'metaboxhidden_nav-menus','a:4:{i:0;s:21:\"add-post-type-gallery\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";i:3;s:15:\"add-gallery_cat\";}'),(20,1,'nav_menu_recently_edited','2'),(21,1,'wp_user-settings','libraryContent=browse&editor=tinymce&hidetb=1'),(22,1,'wp_user-settings-time','1552013371'),(24,1,'AkeebaSession_6a4fc5daadc30f8b3eeb46bfe6e014d5','a:6:{s:9:\"insideCMS\";b:1;s:22:\"platformNameForUpdates\";s:9:\"wordpress\";s:25:\"platformVersionForUpdates\";s:5:\"5.0.3\";s:7:\"user_id\";i:1;s:13:\"newSecretWord\";N;s:7:\"profile\";i:1;}'),(26,1,'AkeebaSession_ba19a440619a16bbc773c5f1ed30bf68','a:1:{s:5:\"value\";s:128:\"dcc57c6e57371b7977de1a19ffd39eabace09e81414388c05b781caf2c65998c84562368639b07d51576c6e7d490150f84dfd0026154de2761f392ba96f8b372\";}'),(27,1,'AkeebaSession_id','8cb599fcc81b4be6e467611656e9106d'),(28,1,'community-events-location','a:1:{s:2:\"ip\";s:12:\"118.70.182.0\";}');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `user_login_key` (`user_login`) USING BTREE,
  KEY `user_nicename` (`user_nicename`) USING BTREE,
  KEY `user_email` (`user_email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (1,'admin','$P$BcIARQd9YRnk2Nbz3zj9V2uRLHvXni.','admin','datpv977@gmail.com','','2019-02-16 04:26:19','',0,'Datpv');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-15  6:37:28
