<?php
/**
 * Render listinfo in home page
 */

//Get config
$contactShow = get_theme_mod('contact_show', 'yes');
$bgLeftContact = get_theme_mod('bg_left_contact',get_template_directory_uri() . '/images/banner-form.jpg');
$imgLeftContact = get_theme_mod('img_left_contact',get_template_directory_uri() . '/images/checkin.png');
$diachi = get_theme_mod ('dia_chi','123 Street City State 45678');
$SDT = get_theme_mod ('SDT','1234 56 7890');
$Mail1 = get_theme_mod ('email_1','info@medicalclinicwebsite');
$Mail2 = get_theme_mod ('email_2','medicalclinicwebsite');
$contactShortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="5" title="Contact"]');
?>
<?php if ($contactShow === 'yes'):?>
<section class="section m-0" id="contact">
  <div class="content-contact container-fluid">
    <div class="row no-gutters">
      <div class="col-lg-5 gg-map">
        <!-- Google Maps - Settings on foote-->
        <div class="map" style="background-image: url(<?php echo $bgLeftContact?>);">
          <div class="row justify-content-end no-gutters">
            <div class="col-xl-6">
              <div class="info-contact">
                <div class="appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="300">
                  <div class="feature-box feature-box-style-2">
                    <div class="feature-box-icon"><img class="icons" src="<?php echo $imgLeftContact?>"/></div>
                    <div class="feature-box-info">
                      <p class="font-weight-bold"><?php echo $diachi?></p>
                      <p class="phone">Phone: <?php echo $SDT?></p>
                      <div class="mail"><a href="info@medicalclinicwebsite.com"><?php echo $Mail1 ?></a></div>
                      <div class="mail"><a href="medicalclinicwebsite.com"><?php echo $Mail2 ?>.com</a></div>
                      <div class="icon-mxh clear-fix">
                          <?php
                          if(function_exists('medical_clinic_social_link')){
                              medical_clinic_social_link();
                          }
                          ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="map-google">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14900.820008282746!2d105.7791938197754!3d20.984417999999994!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1550135934387" width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 form-ct col">
        <div class="row justify-content-start no-gutters">
          <div class="col col-xl-6">
            <div class="form-main">
               <?php echo do_shortcode($contactShortcode)?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif ?>