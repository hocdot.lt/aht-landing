<?php
/**
 * Render services in home
 */
//GET config
$servicesShow = get_theme_mod('services_show', 'yes');
$servicesTitle = get_theme_mod('title_services', 'Expert Care for Elderly Patients');
$servicesSubTitle = get_theme_mod('sub_title_services', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$servicesImages = get_theme_mod('background_right_services', get_template_directory_uri() . '/images/images2.jpg');
//item1
$servicesIcon1 = get_theme_mod('icon1_servies', get_template_directory_uri() . '/images/icon1.png');
$servicesTitle1 = get_theme_mod('title_icon1_servies', 'Dedicated Doctor for Elders');
$servicesSubTitleicon1 = get_theme_mod('sub_title1_servies', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
//item2
$servicesIcon2 = get_theme_mod('icon2_servies', get_template_directory_uri() . '/images/icon2.png');
$servicesTitle2 = get_theme_mod('title_icon2_servies', 'Special Equipments for Elders');
$servicesSubTitleicon2 = get_theme_mod('sub_title2_servies', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
//item3
$servicesIcon3 = get_theme_mod('icon3_servies', get_template_directory_uri() . '/images/icon3.png');
$servicesTitle3 = get_theme_mod('title_icon3_servies', 'Quality and Special Care for Elders');
$servicesSubTitleicon3 = get_theme_mod('sub_title3_servies', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
//item4
$servicesIcon4 = get_theme_mod('icon1_servies', get_template_directory_uri() . '/images/icon4.png');
$servicesTitle4 = get_theme_mod('title_icon4_servies', 'Dedicated Doctor for Elders');
$servicesSubTitleicon4 = get_theme_mod('sub_title4_servies', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
?>
<?php if ($servicesShow === 'yes'): ?>
    <section class="section border-0 m-0" id="services">
        <div class="header-service">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="appear-animation" data-appear-animation="fadeInUpShorter"
                             data-appear-animation-delay="200">
                            <h2 class="font-weight-bold mb-2"><?php echo $servicesTitle ?></h2>
                            <div class="desc-service">
                                <p><?php echo $servicesSubTitle ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-service">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="content-left-service">
                            <div class="appear-animation" data-appear-animation="fadeInLeftShorter"
                                 data-appear-animation-delay="300">
                                <div class="feature-box feature-box-style-2">
                                    <div class="feature-box-icon"><img class="icons" src="<?php echo $servicesIcon1 ?>">
                                    </div>
                                    <div class="feature-box-info">
                                        <h4 class="font-weight-bold text-4 mb-2"><?php echo $servicesTitle1 ?></h4>
                                        <p>
                                            <?php echo $servicesSubTitleicon1 ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="appear-animation" data-appear-animation="fadeInUpShorter">
                                <div class="feature-box feature-box-style-2">
                                    <div class="feature-box-icon"><img class="icons" src="<?php echo $servicesIcon2 ?>">
                                    </div>
                                    <div class="feature-box-info">
                                        <h4 class="font-weight-bold text-4 mb-2"><?php echo $servicesTitle2 ?></h4>
                                        <p>
                                            <?php echo $servicesSubTitleicon2 ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="appear-animation" data-appear-animation="fadeInRightShorter"
                                 data-appear-animation-delay="300">
                                <div class="feature-box feature-box-style-2">
                                    <div class="feature-box-icon"><img class="icons" src="<?php echo $servicesIcon3 ?>">
                                    </div>
                                    <div class="feature-box-info">
                                        <h4 class="font-weight-bold text-4 mb-2"><?php echo $servicesTitle3 ?></h4>
                                        <p>
                                            <?php echo $servicesSubTitleicon3 ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="content-right-service">
                            <div class="bg-img-right"><img class="img-right" src="<?php echo $servicesImages ?>"></div>
                            <div class="appear-animation" data-appear-animation="fadeInRightShorter"
                                 data-appear-animation-delay="300">
                                <div class="feature-box feature-box-style-2">
                                    <div class="feature-box-icon">
                                        <img class="icons" src="<?php echo $servicesIcon4 ?>">
                                    </div>
                                    <div class="feature-box-info">
                                        <h4 class="font-weight-bold text-4 mb-2"><?php echo $servicesTitle4 ?></h4>
                                        <p>
                                            <?php echo $servicesSubTitleicon4 ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>