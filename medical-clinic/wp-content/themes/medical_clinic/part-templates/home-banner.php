<?php
/**
 * Render banner in home page
 */

//Get config
$bannerShow = get_theme_mod('banner_show','yes');
$bannerTitle =  get_theme_mod('banner_title','Serving all people through exemplaryhealth care, education, research and community outreach');
$bannerSubTitle =  get_theme_mod('banner_sub_title','Not just better healthcare,but a better healthcare experience');
$bannerButtonText =  get_theme_mod('button_banner','Get Started');
$bannerButtonLink =  get_theme_mod('link_banner','#');
$bannerBg = get_theme_mod('background_banner_right',get_template_directory_uri() . '/images/bg-banner.jpg');
?>
<?php if($bannerShow === 'yes'): ?>
<section id="banner" style="background-image: url(<?php echo $bannerBg ?>)">
  <div class="container">
    <div class="row">
      <div class="col-xl-4">
        <div class="content-left-banner">
          <div class="text-banner">
               <?php if($bannerTitle): ?>
            <h1><?php echo $bannerTitle ?></h1>
            <?php endif; ?>
          </div>
          <div class="desc-banner">
          <?php if($bannerSubTitle):?>
            <p><?php echo $bannerSubTitle ?></p>
          <?php endif; ?>
          </div>
          <div class="btn-banner">
           <?php if($bannerButtonText): ?>
           <a href="<?php echo $bannerButtonLink ?>" class="btn" type="submit" ><?php echo $bannerButtonText ?></a>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>