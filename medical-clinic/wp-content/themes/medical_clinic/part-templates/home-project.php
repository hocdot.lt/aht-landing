<?php
/**
 * Render project in home page
 */

//Get config
$projectShow = get_theme_mod('project_show', 'yes');
$bgProject = get_theme_mod('background_project', get_template_directory_uri() . '/images/bg-slider.png');
$projectTitle = get_theme_mod('title_projects', 'Expert Care for Elderly Patients');
$projectsubTitle = get_theme_mod('sub_title_projects', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magnva aliqua. Pellentesque nec nam aliquam sem et tortor. ');
$projectCategory = get_theme_mod('projects_category', '');
$projectLimit = get_theme_mod('project_number_post', 5);

$args = array(
    'posts_per_page' => $projectLimit,
    'offset' => 0,
    'cat' => $projectCategory,
    'post_type' => 'post',
);
$posts_array = get_posts($args);
$i = 1;
?>
<?php if ($projectShow === 'yes'): ?>

    <div id="projects" style="background-image: url(<?php echo $bgProject ?>)">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 header-project">
                    <div class="appear-animation" data-appear-animation="fadeInUpShorter">
                        <h2 class="font-weight-bold mb-2"><?php echo $projectTitle ?></h2>
                    </div>
                    <p class="pb-3 mb-4 appear-animation" data-appear-animation="fadeInUpShorter"
                       data-appear-animation-delay="200">
                        <?php echo $projectsubTitle ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="pb-5 mb-5 content-project">
            <div class="appear-animation popup-gallery-ajax" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200">
                <div class="owl-carousel owl-theme mb-0"
                     data-plugin-options="{'autoplay': true, 'autoplayTimeout': 5000, 'autoplayHoverPause': true,  'center': true, 'loop': true, 'nav': false, 'dots': false, 'margin': 40}">
                    <?php foreach ($posts_array as $_post): setup_postdata( $_post ); ?>
                        <?php if ($i == 1): ?>
                            <?php $i = 5; ?>
                            <div class="portfolio-item">
                                <a href="<?php echo get_post_permalink($_post->ID) ?>" data-ajax-on-modal="">
                                  <span class="thumb-info thumb-info-lighten">
                                    <span class="thumb-info-wrapper">
                                        <img class="img-fluid border-radius-0" src=" <?php echo get_the_post_thumbnail_url($_post->ID); ?>" alt="<?php echo get_the_title($_post->ID) ?>"/>
                                        <span class="thumb-info-title">
                                            <span class="thumb-info-inner"><?php echo get_the_date('j M Y'); ?></span>
                                        </span>
                                    </span>
                                    </span>
                                </a>
                                <h3 class="title-thumb-info">
                                    <a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                                </h3>
                                <p class="text-thumb-info">
                                    <?php echo get_the_excerpt($_post->ID) ?>
                                </p>
                                <a class="link" href="<?php echo get_post_permalink($_post->ID) ?>">Continue Reading</a>
                            </div>
                        <?php endif; ?>
                    <?php endforeach;wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>