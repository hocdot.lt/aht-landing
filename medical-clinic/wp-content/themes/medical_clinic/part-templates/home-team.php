<?php
/**
 * Render team in home
 */
//GET config
$teamShow = get_theme_mod('team_show', 'yes');
$bgTeam = get_theme_mod('bg_team', get_template_directory_uri() . '/images/bg-slider.png');
$titleTeam = get_theme_mod('Title_Team', 'See what our customers have tosay about ourservices');
$SubtitleTeam = get_theme_mod('sub_title_team', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$teamLimit = get_theme_mod('services_number_post', 3);
$args = array(
    'posts_per_page' => $teamLimit,
    'offset' => 0,
    'post_type' => 'testimonial',
    'post_status' => 'publish',
    'order' => 'DESC',
);
$testimonial_array = get_posts($args);
?>
<?php if ($teamShow === 'yes'): ?>
    <div class="container pb-4" id="team" style="background-image: url(<?php echo $bgTeam ?>);">
        <div class="row pt-4 mt-4 mb-4">
            <div class="col-xl-4 appear-animation header-team" data-appear-animation="fadeInUpShorter">
                <h2 class="font-weight-bold"><?php echo $titleTeam ?></h2>
                <p><?php echo $SubtitleTeam ?></p>
            </div>
        </div>
        <div class="row pb-5 mb-5 appear-animation" data-appear-animation="fadeInUpShorter"
             data-appear-animation-delay="200">
            <?php foreach ($testimonial_array as $_post): setup_postdata($_post); ?>
                <div class="col-lg col-8">
                    <div class="testimonial">
                        <blockquote><em class="text-blockquote"> <?php echo wp_trim_words( get_the_excerpt($_post->ID), $num_words = 20, '...') ?></em></blockquote>
                    </div>
                    <div class="info-avatar">
                        <div class="process-step-circle">
                            <strong class="process-step-circle-content">
                                <img src="<?php echo get_the_post_thumbnail_url($_post->ID); ?>" alt="<?php echo get_the_title($_post->ID); ?>" />
                            </strong>
                        </div>
                        <div class="user-name">
                            <h3><?php echo get_the_title($_post->ID); ?></h3>
                            <p><?php echo get_post_meta($_post->ID, 'testimonial_position', true); ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach;
            wp_reset_postdata(); ?>
        </div>
    </div>
<?php endif ?>

