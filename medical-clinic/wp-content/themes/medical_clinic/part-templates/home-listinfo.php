<?php
/**
 * Render listinfo in home page
 */

//Get config
$listinfoShow = get_theme_mod('listinfo_show','yes');
//contentleft
$infoImages = get_theme_mod('images_left_info',get_template_directory_uri() . '/images/list-user.png');

//contentright
$infoTitle = get_theme_mod('title_right_info','Welcome to our Medical Clinic website');
$infosubTitle = get_theme_mod('sub_title_right','Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

//listbox
//box1
$infoIcon1 = get_theme_mod('icon_info',get_template_directory_uri() . '/images/oto.png');
$infoTitle1 = get_theme_mod('title_icon1','Ambulance');
//box2
$infoIcon2 = get_theme_mod('icon2_info',get_template_directory_uri() . '/images/user.png');
$infoTitle2 = get_theme_mod('title_icon2','DOCTORS');
//box3
$infoIcon3 = get_theme_mod('icon3_info',get_template_directory_uri() . '/images/phone.png');
$infoTitle3 = get_theme_mod('title_icon3','ANALYSIS');
//box4
$infoIcon4 = get_theme_mod('icon4_info',get_template_directory_uri() . '/images/lich.png');
$infoTitle4 = get_theme_mod('title_icon4','Hospital');
//box5
$infoIcon5 = get_theme_mod('icon5_info',get_template_directory_uri() . '/images/cap.png');
$infoTitle5 = get_theme_mod('title_icon5','MEdicines');
//box6
$infoIcon6 = get_theme_mod('icon6_info',get_template_directory_uri() . '/images/tivi.png');
$infoTitle6 = get_theme_mod('title_icon6','Diagram');
?>
<?php if($listinfoShow === 'yes'): ?>
<section id="listinfo">
  <div class="container-fluid container-list">
    <div class="row">
      <div class="col-lg-3 col-md-8">
        <div class="content-left-listinfo d-none d-lg-block">
          <div class="bg-list"><img src="<?php echo $infoImages ?>"></div>
        </div>
      </div>
      <div class="col-lg-5 col-md-8">
        <div class="content-right-listinfo">
          <div class="title-listinfo">
            <h3 class="title-listinfo-top"><?php echo $infoTitle ?></h3>
            <h3 class="title-listinfo-bottom">we provide a variety of services for individuals</h3>
          </div>
          <div class="desc-listinfo">
            <p><?php echo $infosubTitle ?></p>
          </div>
          <div class="content-listinfo">
            <div class="list-info list-info-1">
              <div class="row">
                <div class="box-item col-lg-2 col-sm">
                  <div class="item-info item1"><img src="<?php echo $infoIcon1 ?>"></div>
                  <div class="text-item-info"><a href="#"><?php echo $infoTitle1 ?></a></div>
                </div>
                <div class="box-item col-lg-2 col-sm">
                  <div class="item-info item2"><img src="<?php echo $infoIcon2 ?>"></div>
                  <div class="text-item-info"><a href="#"><?php echo $infoTitle2 ?></a></div>
                </div>
                <div class="box-item col-lg-2 col-sm">
                  <div class="item-info item3"><img src="<?php echo $infoIcon3 ?>"></div>
                  <div class="text-item-info"><a href="#"><?php echo $infoTitle3 ?></a></div>
                </div>
              </div>
            </div>
            <div class="list-info list-info-2">
              <div class="row">
                <div class="box-item col-lg-2 col-sm">
                  <div class="item-info item4"><img src="<?php echo $infoIcon4 ?>"></div>
                  <div class="text-item-info"><a href="#"><?php echo $infoTitle4 ?></a></div>
                </div>
                <div class="box-item col-lg-2 col-sm">
                  <div class="item-info item5"><img src="<?php echo $infoIcon5 ?>"></div>
                  <div class="text-item-info"><a href="#"><?php echo $infoTitle5 ?></a></div>
                </div>
                <div class="box-item col-lg-2 col-sm">
                  <div class="item-info item6"><img src="<?php echo $infoIcon6 ?>"></div>
                  <div class="text-item-info"><a href="#"><?php echo $infoTitle6 ?></a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>