<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php if (!function_exists('has_site_icon') || !has_site_icon()) : ?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon"
                  href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>"
                  type="image/x-icon"/>
        <?php endif; ?>
    <?php endif; ?>
    <?php wp_head(); ?>
</head>
<?php
$classAdminBar = is_admin_bar_showing() ? 'adminbar' : '';
?>
<body <?php body_class(); ?>>
<div id="page" class="hfeed site">

    <header class="header-transparent header-effect-shrink" id="header"
            data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
        <div class="header-body border-top-0 box-shadow-none">
            <div class="header-container container">
                <div class="header-row">
                    <div class="header-column">
                        <div class="header-row">
                            <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                <?php
                                $logo_header = get_theme_mod('logo', get_template_directory_uri() . '/images/logo.png');
                                if ($logo_header && $logo_header != ''):
                                    echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                else:
                                    echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                endif;
                                ?>
                            </a>
                            <div class="header-title">
                                <?php
                                $text1 = get_theme_mod('text_header', 'First Refuge');
                                $text2 = get_theme_mod('text_2_header', 'MEDICAL CLINIC');
                                ?>
                                <p class="title-top"><?php echo $text1 ?></p>
                                <p class="title-bootom"><?php echo $text2 ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="header-column justify-content-end">
                        <div class="header-row">
                            <div class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1">
                                <div class="header-nav-main header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                    <?php
                                    if (function_exists('render_header_menu')) {
                                        render_header_menu();
                                    }
                                    ?>
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                            data-target=".header-nav-main nav"><i class="fas fa-bars"></i></button>
                                    <!---->
                                    <div class="sidenav" id="mySidenav" onclick="closeNav()"><a class="closebtn" href="javascript:void(0)"
                                                                           onclick="closeNav()">×</a>
                                        <div class="showmenu">
                                            <?php
                                            if (has_nav_menu('primary')) {
                                                wp_nav_menu(array(
                                                        'theme_location' => 'primary',
                                                        'menu_class' => 'mega-menu',
                                                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                    )
                                                );
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <span class="onclick" style="font-size:30px;cursor:pointer" onclick="openNav()">
                  <div class="span"></div>
                  <div class="span"></div>
                  <div class="span"></div></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- include components/menu-->
    <!-- include components/page-title-->
    <div id="main" class="wrapper">
				
            

                    
        
       
