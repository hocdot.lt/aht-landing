<?php 
/**
Template Name: Home
*/
get_header();

get_template_part('part-templates/home-banner');
get_template_part('part-templates/home-listinfo');
get_template_part('part-templates/home-services');
get_template_part('part-templates/home-project');
get_template_part('part-templates/home-team');
get_template_part('part-templates/home-contact');

get_footer(); ?> 