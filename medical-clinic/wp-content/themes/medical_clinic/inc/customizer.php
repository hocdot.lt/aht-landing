<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action('customize_register', 'medical_clinic_customize_register');
function medical_clinic_customize_register($wp_customize)
{

    remove_theme_support('custom-header');

    require_once(get_template_directory() . '/inc/class/customizer-repeater-control.php');

    if (!class_exists('medical_clinic_Customize_Sidebar_Control')) {
        class medical_clinic_Customize_Sidebar_Control extends WP_Customize_Control
        {
            /**
             * Render the control's content.
             *
             * @since 3.4.0
             */
            public function render_content()
            {

                $output = '
			        <select>';
                foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) {
                    $output .= '<option value="' . esc_attr($sidebar['id']) . '"' . '>' .
                        ucwords($sidebar['name']) . '</option>';
                }
                $output .= '</select>';

                $output = str_replace('<select', '<select ' . $this->get_link(), $output);

                printf(
                    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                    $this->label,
                    $output
                );
                ?>
                <?php if ('' != $this->description) : ?>
                <div class="description customize-control-description">
                    <?php echo esc_html($this->description); ?>
                </div>
            <?php endif;
            }
        }
    }
    if (!class_exists('My_Dropdown_Category_Control')) {
        class My_Dropdown_Category_Control extends WP_Customize_Control
        {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content()
            {
                $dropdown_args = wp_parse_args($this->dropdown_args, array(
                    'taxonomy' => 'category',
                    'show_option_none' => ' ',
                    'selected' => $this->value(),
                    'show_option_all' => '',
                    'orderby' => 'id',
                    'order' => 'ASC',
                    'show_count' => 1,
                    'hide_empty' => 1,
                    'child_of' => 0,
                    'exclude' => '',
                    'hierarchical' => 1,
                    'depth' => 0,
                    'tab_index' => 0,
                    'hide_if_empty' => false,
                    'option_none_value' => 0,
                    'value_field' => 'term_id',
                ));

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories($dropdown_args);
                $dropdown1 = str_replace('<select', '<select ' . $this->get_link(), $dropdown);
                $dropdown1 = str_replace('cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                    $this->label, $dropdown1);
            }
        }
    }

    if (!class_exists('WP_Customize_Control'))
        return NULL;

    /**
     * Class to create a custom post type control
     */
    class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
    {
        private $posts = false;

        public function __construct($manager, $id, $args = array(), $options = array())
        {
            $postargs = array(
                'post_type' => 'service',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'orderby' => 'date',
                'order' => 'DESC',
            );
            $this->posts = get_posts($postargs);
            parent::__construct($manager, $id, $args);
        }

        /**
         * Render the content on the theme customizer page
         */
        public function render_content()
        {
            if (!empty($this->posts)) {
                ?>
                <label>
                    <span class="customize-service-dropdown"><?php echo esc_html($this->label); ?></span>
                    <select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
                        <?php
                        foreach ($this->posts as $post) {
                            printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
                        }
                        ?>
                    </select>
                </label>
                <?php
            }
        }
    }

    /**
     * Googe Font Select Custom Control
     */

    if (!class_exists('Google_font_Dropdown_Custom_Control')) {
        class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
        {

            private $fonts = false;

            public function __construct($manager, $id, $args = array(), $options = array())
            {
                $this->fonts = $this->get_fonts();
                parent::__construct($manager, $id, $args);
            }

            /**
             * Render the content of the dropdown
             *
             * Adding the font-family styling to the select so that the font renders
             * @return HTML
             */
            public function render_content()
            {
                if (!empty($this->fonts)) { ?>
                    <label>
                        <span class="customize-category-select-control"><?php echo esc_html($this->label); ?></span>
                        <select class="select-fonts" <?php $this->link(); ?>>
                            <?php
                            foreach ($this->fonts as $k => $v) {
                                printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
                            }
                            ?>
                        </select>
                    </label>
                <?php }
            }

            /**
             * Get the list of fonts
             *
             * @return string
             */
            function get_fonts()
            {
                $fonts = array(
                    'Baloo Tamma' => 'Baloo Tamma',
                    'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',
                    'Arial:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Arial',
                    'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',
                    'Old Standard TT:400,400i,700' => 'Old Standard TT',
                    'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
                    'Jura:300,400,500,600,700' => 'Jura',
                    'Playfair Display:400,700,400i' => 'Playfair Display',
                    'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
                    'Oswald:200,300,400,500,600,700' => 'Oswald',
                    'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
                    'Droid Sans:400,700' => 'Droid Sans',
                    'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
                    'Arvo:400,700,400i,700i' => 'Arvo',
                    'Lora:400,700,400i,700i' => 'Lora',
                    'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
                    'Oxygen:400,300,700' => 'Oxygen',
                    'PT Serif:400,700' => 'PT Serif',
                    'PT Sans:400,700,400i,700i' => 'PT Sans',
                    'PT Sans Narrow:400,700' => 'PT Sans Narrow',
                    'Cabin:400,700,400i' => 'Cabin',
                    'Fjalla One:400' => 'Fjalla One',
                    'Francois One:400' => 'Francois One',
                    'Josefin Sans:400,300,600,700' => 'Josefin Sans',
                    'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
                    'Arimo:400,700,400i,700i' => 'Arimo',
                    'Ubuntu:400,700,400i,700i' => 'Ubuntu',
                    'Bitter:400,700,400i' => 'Bitter',
                    'Droid Serif:400,700,400i,700i' => 'Droid Serif',
                    'Lobster:400' => 'Lobster',
                    'Roboto:400,400i,700,700i' => 'Roboto',
                    'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
                    'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
                    'Roboto Slab:100,300,400,700' => 'Roboto Slab',
                    'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
                    'Mandali:400' => 'Mandali',
                    'Vesper Libre:400,700' => 'Vesper Libre',
                    'NTR:400' => 'NTR',
                    'Dhurjati:400' => 'Dhurjati',
                    'Faster One:400' => 'Faster One',
                    'Mallanna:400' => 'Mallanna',
                    'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
                    'Galindo:400' => 'Galindo',
                    'Titan One:400' => 'Titan One',
                    'Abel:400' => 'Abel',
                    'Nunito:400,300,700' => 'Nunito',
                    'Poiret One:400' => 'Poiret One',
                    'Signika:400,300,600,700' => 'Signika',
                    'Muli:400,400i,300i,300' => 'Muli',
                    'Play:400,700' => 'Play',
                    'Bree Serif:400' => 'Bree Serif',
                    'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
                    'Cuprum:400,400i,700,700i' => 'Cuprum',
                    'Noto Serif:400,400i,700,700i' => 'Noto Serif',
                    'Pacifico:400' => 'Pacifico',
                    'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
                    'Asap:400,400i,700,700i' => 'Asap',
                    'Maven Pro:400,500,700' => 'Maven Pro',
                    'Dancing Script:400,700' => 'Dancing Script',
                    'Karla:400,700,400i,700i' => 'Karla',
                    'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
                    'Exo:400,300,400i,700,700i' => 'Exo',
                    'Varela Round:400' => 'Varela Round',
                    'Cabin Condensed:400,600,700' => 'Cabin Condensed',
                    'PT Sans Caption:400,700' => 'PT Sans Caption',
                    'Cinzel:400,700' => 'Cinzel',
                    'News Cycle:400,700' => 'News Cycle',
                    'Inconsolata:400,700' => 'Inconsolata',
                    'Architects Daughter:400' => 'Architects Daughter',
                    'Quicksand:400,700,300' => 'Quicksand',
                    'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
                    'Quicksand:400,700,300' => 'Quicksand',
                    'Monda:400,700' => 'Monda',
                    'Didact Gothic:400' => 'Didact Gothic',
                    'Coming Soon:400' => 'Coming Soon',
                    'Ropa Sans:400,400i' => 'Ropa Sans',
                    'Tinos:400,400i,700,700i' => 'Tinos',
                    'Glegoo:400,700' => 'Glegoo',
                    'Pontano Sans:400' => 'Pontano Sans',
                    'Fredoka One:400' => 'Fredoka One',
                    'Lobster Two:400,400i,700,700i' => 'Lobster Two',
                    'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
                    'Covered By Your Grace:400' => 'Covered By Your Grace',
                    'Changa One:400,400i' => 'Changa One',
                    'Marvel:400,400i,700,700i' => 'Marvel',
                    'BenchNine:400,700,300' => 'BenchNine',
                    'Orbitron:400,700,500' => 'Orbitron',
                    'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
                    'Bangers:400' => 'Bangers',
                    'Courgette:400' => 'Courgette',
                    '' => 'None',
                );
                return $fonts;
            }
        }
    }

    /**
     * TinyMCE Custom Control
     *
     */
    if (!class_exists('WP_TinyMCE_Custom_control')) {
        class WP_TinyMCE_Custom_control extends WP_Customize_Control
        {
            public $type = 'textarea';

            /**
             ** Render the content on the theme customizer page
             */
            public function render_content()
            { ?>
                <label>
                    <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
                    <?php
                    $settings = array(
                        'media_buttons' => true,
                        'quicktags' => true,
                        'textarea_rows' => 5
                    );
                    $this->filter_editor_setting_link();
                    wp_editor($this->value(), $this->id, $settings);
                    ?>
                </label>
                <?php
                do_action('admin_footer');
                do_action('admin_print_footer_scripts');
            }

            private function filter_editor_setting_link()
            {
                add_filter('the_editor', function ($output) {
                    return preg_replace('/<textarea/', '<textarea ' . $this->get_link(), $output, 1);
                });
            }
        }
    }

    /* Multi Input field */

    class Multi_Input_Custom_control extends WP_Customize_Control
    {
        public $type = 'multi_input';

        public function render_content()
        {
            ?>
            <label class="customize_multi_input">
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
                <p><?php echo wp_kses_post($this->description); ?></p>
                <input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>"
                       value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field"
                       data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
                <div class="customize_multi_fields">
                    <div class="set">
                        <input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
                        <a href="#" class="customize_multi_remove_field">X</a>
                    </div>
                </div>
                <a href="#"
                   class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'medical_clinic') ?></a>
            </label>
            <?php
        }
    }

    /**
     * Add Option Panel
     */

    // General
    $wp_customize->add_section('general_settings',
        array(
            'title' => esc_html__('General Settings', 'medical_clinic'),
            'priority' => 20,
        )
    );
    $wp_customize->add_setting('logo',
        array(
            'default' => get_template_directory_uri() . '/images/logo.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'logo',
        array(
            'label' => esc_html__('Logo', 'medical_clinic'),
            'description' => esc_html__('Upload Logo', 'medical_clinic'),
            'section' => 'general_settings',
        )
    ));

    $wp_customize->add_setting('google_font_h',
        array(
            'default' => 'Jura:300,400,500,600,700',
        )
    );
    $wp_customize->add_control(new Google_font_Dropdown_Custom_Control($wp_customize, 'google_font_h',
        array(
            'label' => 'Font Family Tab H',
            'section' => 'general_settings',
        )
    ));
    $wp_customize->add_control(new Google_font_Dropdown_Custom_Control($wp_customize, 'google_font_h',
        array(
            'label' => 'Font Family Tab H',
            'section' => 'general_settings',
        )
    ));
    $wp_customize->add_setting('google_font',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control(new Google_font_Dropdown_Custom_Control($wp_customize, 'google_font',
        array(
            'label' => 'Main Google Font',
            'section' => 'general_settings',
        )
    ));
    // text1
    $wp_customize->add_setting('text_header',
        array(
            'default' => 'First Refuge',
        )
    );
    $wp_customize->add_control('text_header',
        array(
            'label' => esc_html__('Text 1', 'medical_clinic'),
            'section' => 'general_settings',
            'type' => 'text',
        )
    );
    // text2
    $wp_customize->add_setting('text_2_header',
        array(
            'default' => esc_html__('MEDICAL CLINIC'),
        )
    );
    $wp_customize->add_control('text_2_header',
        array(
            'label' => esc_html__('Text 2', 'medical_clinic'),
            'section' => 'general_settings',
            'type' => 'text',
        )
    );

    // End Header

    // Panel Home Options

    $wp_customize->add_panel('home_page_setting', array(
        'priority' => 30,
        'capability' => 'edit_theme_options',
        'title' => __('Home Options', 'medical_clinic'),
        'description' => __('Several settings pertaining my theme', 'medical_clinic'),
    ));
    // Start Banner section
    $wp_customize->add_section('home_banner_settings',
        array(
            'title' => esc_html__('Banner Section', 'medical_clinic'),
            'priority' => 20,
            'panel' => 'home_page_setting',
        )
    );
    // title banner
    $wp_customize->add_setting('banner_title',
        array(
            'default' => 'Serving all people through exemplaryhealth care, education, research and community outreach',
        )
    );
    $wp_customize->add_control('banner_title',
        array(
            'label' => esc_html__('Title Banner', 'medical_clinic'),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // sub title banner
    $wp_customize->add_setting('banner_sub_title',
        array(
            'default' => esc_html__('Not just better healthcare,but a better healthcare experience'),
        )
    );
    $wp_customize->add_control('banner_sub_title',
        array(
            'label' => esc_html__('Sub Title Banner', 'medical_clinic'),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );
    // button banner
    $wp_customize->add_setting('button_banner',
        array(
            'default' => esc_html__('Get Started', 'medical_clinic'),

        )
    );
    $wp_customize->add_control('button_banner',
        array(
            'label' => esc_html__('Button banner', 'medical_clinic'),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting('link_banner',
        array(
            'default' => esc_html__('#', 'medical_clinic'),

        )
    );
    $wp_customize->add_control('link_banner',
        array(
            'label' => esc_html__('Button URL', 'medical_clinic'),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // background right banner
    $wp_customize->add_setting('background_banner_right',
        array(
            'default' => get_template_directory_uri() . '/images/bg-banner.jpg',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'background_banner_right',
        array(
            'label' => esc_html__('Background Banner', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'home_banner_settings',
        )
    ));
    //Show/Hide banner
    $wp_customize->add_setting('banner_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control('banner_show',
        array(
            'label' => esc_html__('Show Section', 'medical_clinic'),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );
    // End banner section


    // Start listinfo section
    $wp_customize->add_section('listinfo_settings',
        array(
            'title' => esc_html__('listinfo section', 'medical_clinic'),
            'priority' => 20,
            'panel' => 'home_page_setting',
        )
    );
    // listinfo

    $wp_customize->add_setting('images_left_info',
        array(
            'default' => get_template_directory_uri() . '/images/list-user.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'images_left_info',
        array(
            'label' => esc_html__('Background left info', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'listinfo_settings',
        )
    ));

    //content right info
    // title content right info
    $wp_customize->add_setting('title_right_info',
        array(
            'default' => 'Welcome to our Medical Clinic website',
        )
    );
    $wp_customize->add_control('title_right_info',
        array(
            'label' => esc_html__('Title', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'text',
        )
    );
    // sub title info
    $wp_customize->add_setting('sub_title_right',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control('sub_title_right',
        array(
            'label' => esc_html__('Sub Title', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'textarea',
        )
    );
    // list icon info
    //box1
    //icon 1
    $wp_customize->add_setting('icon_info',
        array(
            'default' => get_template_directory_uri() . '/images/oto.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon_info',
        array(
            'label' => esc_html__('icon 1', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'listinfo_settings',
        )
    ));
    //title icon 1
    $wp_customize->add_setting('title_icon1',
        array(
            'default' => 'Ambulance',
        )
    );
    $wp_customize->add_control('title_icon1',
        array(
            'label' => esc_html__('Title 1', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'text',
        )
    );
    //box2
    //icon2
    $wp_customize->add_setting('icon2_info',
        array(
            'default' => get_template_directory_uri() . '/images/user.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon2_info',
        array(
            'label' => esc_html__('icon 2', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'listinfo_settings',
        )
    ));
    //title icon 2
    $wp_customize->add_setting('title_icon2',
        array(
            'default' => 'DOCTORS',
        )
    );
    $wp_customize->add_control('title_icon2',
        array(
            'label' => esc_html__('Title 2', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'text',
        )
    );

    //box3
    //icon3
    $wp_customize->add_setting('icon3_info',
        array(
            'default' => get_template_directory_uri() . '/images/phone.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon3_info',
        array(
            'label' => esc_html__('icon 3', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'listinfo_settings',
        )
    ));
    //title icon 3
    $wp_customize->add_setting('title_icon3',
        array(
            'default' => 'ANALYSIS',
        )
    );
    $wp_customize->add_control('title_icon3',
        array(
            'label' => esc_html__('Title 3', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'text',
        )
    );
    //box4
    //icon4
    $wp_customize->add_setting('icon4_info',
        array(
            'default' => get_template_directory_uri() . '/images/lich.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon4_info',
        array(
            'label' => esc_html__('icon 4', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'listinfo_settings',
        )
    ));
    //title icon 4
    $wp_customize->add_setting('title_icon4',
        array(
            'default' => 'Hospital',
        )
    );
    $wp_customize->add_control('title_icon4',
        array(
            'label' => esc_html__('Title 4', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'text',
        )
    );
    //box5
    //icon5
    $wp_customize->add_setting('icon5_info',
        array(
            'default' => get_template_directory_uri() . '/images/cap.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon5_info',
        array(
            'label' => esc_html__('icon 5', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'listinfo_settings',
        )
    ));

    //title icon 5
    $wp_customize->add_setting('title_icon5',
        array(
            'default' => 'MEdicines',
        )
    );
    $wp_customize->add_control('title_icon5',
        array(
            'label' => esc_html__('Title 5', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'text',
        )
    );
    //box6
    //icon6
    $wp_customize->add_setting('icon6_info',
        array(
            'default' => get_template_directory_uri() . '/images/tivi.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon6_info',
        array(
            'label' => esc_html__('icon 6', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'listinfo_settings',
        )
    ));
    //title icon 6
    $wp_customize->add_setting('title_icon6',
        array(
            'default' => 'Diagram',
        )
    );
    $wp_customize->add_control('title_icon6',
        array(
            'label' => esc_html__('Title 6', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'text',
        )
    );

    //show/hide listinfo
    $wp_customize->add_setting('listinfo_show',
        array(
            'default' => 'yes',
        )
    );

    $wp_customize->add_control('listinfo_show',
        array(
            'label' => esc_html__('Show Section Info', 'medical_clinic'),
            'section' => 'listinfo_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );
    // End listinfo section

    // Start projects section
    $wp_customize->add_section('projects_settings',
        array(
            'title' => esc_html__('Projects Section', 'medical_clinic'),
            'priority' => 20,
            'panel' => 'home_page_setting',
        )
    );
    // title projects
    $wp_customize->add_setting('title_projects',
        array(
            'default' => 'Expert Care for Elderly Patients',
        )
    );
    $wp_customize->add_control('title_projects',
        array(
            'label' => esc_html__('Title', 'medical_clinic'),
            'section' => 'projects_settings',
            'type' => 'text',
        )
    );
    // sub title
    $wp_customize->add_setting('sub_title_projects',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control('sub_title_projects',
        array(
            'label' => esc_html__('Sub Title', 'medical_clinic'),
            'section' => 'projects_settings',
            'type' => 'textarea',
        )
    );
    //background projects_settings
    $wp_customize->add_setting('background_project',
        array(
            'default' => get_template_directory_uri() . '/images/bg-slider.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'background_project',
        array(
            'label' => esc_html__('Background Project', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'projects_settings',
        )
    ));
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach ($categories as $category) {
        if ($i == 0) {
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }
    $wp_customize->add_setting('projects_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('projects_category',
        array(
            'label' => esc_html__('Category Post', 'medical_clinic'),
            'section' => 'projects_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );
    //Number post
    $wp_customize->add_setting('project_number_post',
        array(
            'default' => esc_html__('5', 'medical_clinic'),
        )
    );
    $wp_customize->add_control('project_number_post',
        array(
            'label' => esc_html__('Number Post', 'medical_clinic'),
            'section' => 'projects_settings',
            'type' => 'text',
        )
    );
    //show/hide project
    $wp_customize->add_setting('project_show',
        array(
            'default' => 'yes',
        )
    );

    $wp_customize->add_control('project_show',
        array(
            'label' => esc_html__('Show Section Info', 'medical_clinic'),
            'section' => 'projects_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );

    // End projects section
    // Start services section
    $wp_customize->add_section('services_settings',
        array(
            'title' => esc_html__('services section', 'medical_clinic'),
            'priority' => 20,
            'panel' => 'home_page_setting',
        )
    );

    // title services
    $wp_customize->add_setting('title_services',
        array(
            'default' => 'Expert Care for Elderly Patients',
        )
    );
    $wp_customize->add_control('title_services',
        array(
            'label' => esc_html__('Title', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'text',
        )
    );
    // sub title
    $wp_customize->add_setting('sub_title_services',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control('sub_title_services',
        array(
            'label' => esc_html__('Sub Title', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'textarea',
        )
    );
    //images right services
    $wp_customize->add_setting('background_right_services',
        array(
            'default' => get_template_directory_uri() . '/images/images2.jpg',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'background_right_services',
        array(
            'label' => esc_html__('Images right', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'services_settings',
        )
    ));

    //post services
    //item1
    //icon1
    $wp_customize->add_setting('icon1_servies',
        array(
            'default' => get_template_directory_uri() . '/images/icon4.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon1_servies',
        array(
            'label' => esc_html__('icon 1', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'services_settings',
        )
    ));
    //title icon 1
    $wp_customize->add_setting('title_icon1_servies',
        array(
            'default' => 'Dedicated Doctor for Elders',
        )
    );
    $wp_customize->add_control('title_icon1_servies',
        array(
            'label' => esc_html__('Title 1', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'text',
        )
    );
    //sub title icon1
    $wp_customize->add_setting('sub_title1_servies',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control('sub_title1_servies',
        array(
            'label' => esc_html__('Sub Title 1', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'textarea',
        )
    );
//item1
//icon2
    $wp_customize->add_setting('icon2_servies',
        array(
            'default' => get_template_directory_uri() . '/images/icon2.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon2_servies',
        array(
            'label' => esc_html__('icon 2', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'services_settings',
        )
    ));
    //title icon 2
    $wp_customize->add_setting('title_icon2_servies',
        array(
            'default' => 'Special Equipments for Elders',
        )
    );
    $wp_customize->add_control('title_icon2_servies',
        array(
            'label' => esc_html__('Title 2', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'text',
        )
    );
    //sub title icon1
    $wp_customize->add_setting('sub_title2_servies',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control('sub_title2_servies',
        array(
            'label' => esc_html__('Sub Title 2', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'textarea',
        )
    );
    //item3
//icon3
    $wp_customize->add_setting('icon3_servies',
        array(
            'default' => get_template_directory_uri() . '/images/icon3.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon3_servies',
        array(
            'label' => esc_html__('icon 3', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'services_settings',
        )
    ));
    //title icon 3
    $wp_customize->add_setting('title_icon3_servies',
        array(
            'default' => 'Quality and Special Care for Elders',
        )
    );
    $wp_customize->add_control('title_icon3_servies',
        array(
            'label' => esc_html__('Title 3', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'text',
        )
    );
    //sub title icon3
    $wp_customize->add_setting('sub_title3_servies',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control('sub_title3_servies',
        array(
            'label' => esc_html__('Sub Title 3', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'textarea',
        )
    );
    //item4
//icon4
    $wp_customize->add_setting('icon4_servies',
        array(
            'default' => get_template_directory_uri() . '/images/icon4.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'icon4_servies',
        array(
            'label' => esc_html__('icon 4', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'services_settings',
        )
    ));
    //title icon 4
    $wp_customize->add_setting('title_icon4_servies',
        array(
            'default' => 'Quality and Special Care for Elders',
        )
    );
    $wp_customize->add_control('title_icon4_servies',
        array(
            'label' => esc_html__('Title 4', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'text',
        )
    );
    //sub title icon4
    $wp_customize->add_setting('sub_title4_servies',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control('sub_title4_servies',
        array(
            'label' => esc_html__('Sub Title 4', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting('services_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control('services_show',
        array(
            'label' => esc_html__('Show Section', 'medical_clinic'),
            'section' => 'services_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );
    // End services section
    // Start team section
    $wp_customize->add_section('team_settings',
        array(
            'title' => esc_html__('Testimonials section', 'medical_clinic'),
            'priority' => 20,
            'panel' => 'home_page_setting',
        )
    );
    //background
    $wp_customize->add_setting('bg_team',
        array(
            'default' => get_template_directory_uri() . '/images/bg-slider.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'bg_team',
        array(
            'label' => esc_html__('Column 1: Image', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'team_settings',
        )
    ));
    //title
    $wp_customize->add_setting('Title_Team',
        array(
            'default' => 'See what our customers have tosay about ourservices',
        )
    );
    $wp_customize->add_control('Title_Team',
        array(
            'label' => esc_html__('Title Section', 'medical_clinic'),
            'section' => 'team_settings',
            'type' => 'text',
        )
    );
    //sub title
    $wp_customize->add_setting('sub_title_team',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control('sub_title_team',
        array(
            'label' => esc_html__('Sub Title ', 'medical_clinic'),
            'section' => 'team_settings',
            'type' => 'textarea',
        )
    );
    //Number post
    $wp_customize->add_setting('services_number_post',
        array(
            'default' => esc_html__('3', 'medical_clinic'),
        )
    );
    $wp_customize->add_control('services_number_post',
        array(
            'label' => esc_html__('Number Post', 'medical_clinic'),
            'section' => 'team_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting('team_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control('team_show',
        array(
            'label' => esc_html__('Show Section', 'medical_clinic'),
            'section' => 'team_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );
    // End team section
    // Start contact section
    $wp_customize->add_section('contact_settings',
        array(
            'title' => esc_html__('contact section', 'medical_clinic'),
            'priority' => 20,
            'panel' => 'home_page_setting',
        )
    );
    $wp_customize->add_setting('bg_left_contact',
        array(
            'default' => get_template_directory_uri() . '/images/banner-form.jpg',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'bg_left_contact',
        array(
            'label' => esc_html__('Background Left: Image', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'contact_settings',
        )
    ));
    //sub title
    $wp_customize->add_setting('map_contact',
        array(
            'default' => esc_html__('<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14900.820008282746!2d105.7791938197754!3d20.984417999999994!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1550135934387" width="600" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>'),
        )
    );
    $wp_customize->add_control('map_contact',
        array(
            'label' => esc_html__('Map ', 'medical_clinic'),
            'section' => 'contact_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting('img_left_contact',
        array(
            'default' => get_template_directory_uri() . '/images/checkin.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'img_left_contact',
        array(
            'label' => esc_html__('Images left: Image', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'contact_settings',
        )
    ));
    //diachi
    $wp_customize->add_setting('dia_chi',
        array(
            'default' => '123 Street, City, State 45678',
        )
    );
    $wp_customize->add_control('dia_chi',
        array(
            'label' => esc_html__('Địa Chỉ', 'medical_clinic'),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    //sdt
    $wp_customize->add_setting('SDT',
        array(
            'default' => esc_html__('1234 56 7890'),
        )
    );
    $wp_customize->add_control('SDT',
        array(
            'label' => esc_html__('Số Điện Thoại ', 'medical_clinic'),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    //email1
    $wp_customize->add_setting('email_1',
        array(
            'default' => esc_html__('info@medicalclinicwebsite.com'),
        )
    );
    $wp_customize->add_control('email_1',
        array(
            'label' => esc_html__('Mail 1 ', 'medical_clinic'),
            'section' => 'contact_settings',
            'type' => 'email',
        )
    );
    //email2
    $wp_customize->add_setting('email_2',
        array(
            'default' => esc_html__('medicalclinicwebsite.com'),
        )
    );
    $wp_customize->add_control('email_2',
        array(
            'label' => esc_html__('Mail 2 ', 'medical_clinic'),
            'section' => 'contact_settings',
            'type' => 'email',
        )
    );
    //Contact shortcode
    $wp_customize->add_setting('contact_shortcode',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control('contact_shortcode',
        array(
            'label' => esc_html__('Contact shortcode', 'medical_clinic'),
            'section' => 'contact_settings',
            'type' => 'text'
        )
    );
    //shortcode
    $wp_customize->add_setting('contact_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control('contact_show',
        array(
            'label' => esc_html__('Show Section', 'medical_clinic'),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );
    // End contact section


    // End Home Options

    // Footer
    $wp_customize->add_section('footer_settings',
        array(
            'title' => esc_html__('Footer Settings', 'medical_clinic'),
            'priority' => 35,
        )
    );

    $wp_customize->add_setting('copyright',
        array(
            'default' => '<p class="text-center m-0">((C) 2019. All Rights Reserved. MedicalClinic. Designed & Developed by <a href="#">Template.net</a></p>',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control(new WP_TinyMCE_Custom_control($wp_customize, 'copyright',
        array(
            'label' => esc_html__('Copyright text', 'medical_clinic'),
            'section' => 'footer_settings',
        )
    ));
    $wp_customize->add_setting('img_left_contact',
        array(
            'default' => get_template_directory_uri() . '/images/checkin.png',
        )
    );
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'img_left_contact',
        array(
            'label' => esc_html__('Images left: Image', 'medical_clinic'),
            'description' => esc_html__('Upload Image', 'medical_clinic'),
            'section' => 'contact_settings',
        )
    ));
    //end footer

    // Social
    $wp_customize->add_section('social_settings',
        array(
            'title' => esc_html__('Social', 'medical_clinic'),
            'priority' => 35,
        )
    );
    $wp_customize->add_setting('show_social',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control('show_social',
        array(
            'label' => esc_html__('Show Social', 'medical_clinic'),
            'section' => 'social_settings',
            'type' => 'select',
            'choices' => array( // Optional.
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );

    $social_links = array(
        'facebook' => esc_html__('Facebook', 'medical_clinic'),
        'instagram' => esc_html__('Instagram', 'medical_clinic'),
        'twitter' => esc_html__('Twitter', 'medical_clinic'),
        'google' => esc_html__('Google', 'medical_clinic'),
        'linkedin' => esc_html__('Linkedin', 'medical_clinic')
    );
    foreach ($social_links as $key => $value) {
        $wp_customize->add_setting($key,
            array(
                'default' => '#',
            )
        );
        $wp_customize->add_control($key,
            array(
                'label' => esc_html($value),
                'section' => 'social_settings',
                'type' => 'text',
            )
        );
    }
    foreach ($social_links as $key => $value) {
        $wp_customize->add_setting('share_' . $key,
            array(
                'default' => 'yes',
            )
        );
        $wp_customize->add_control('share_' . $key,
            array(
                'label' => esc_html('Share ' . $value),
                'section' => 'social_settings',
                'type' => 'select',
                'choices' => array( // Optional.
                    'no' => esc_html__('No', 'medical_clinic'),
                    'yes' => esc_html__('Yes', 'medical_clinic'),
                )
            )
        );
    }

    // Blog
    $wp_customize->add_section('blog_page_settings',
        array(
            'title' => esc_html__('Blog Settings', 'medical_clinic'),
            'priority' => 35,
        )
    );
    $wp_customize->add_setting('blog_page_title',
        array(
            'default' => esc_html__('Blog Articles', 'medical_clinic'),
        )
    );
    $wp_customize->add_control('blog_page_title',
        array(
            'label' => esc_html__('Blog page Title', 'medical_clinic'),
            'section' => 'blog_page_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting('blog_single_related',
        array(
            'default' => 'yes',
        )
    );

    $wp_customize->add_control('blog_single_related',
        array(
            'label' => esc_html__('Related Posts', 'medical_clinic'),
            'section' => 'blog_page_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );

    $wp_customize->add_setting('show_loadmore_blog',
        array(
            'default' => 'no',
        )
    );
    $wp_customize->add_control('show_loadmore_blog',
        array(
            'label' => esc_html__('Show Loadmore', 'medical_clinic'),
            'section' => 'blog_page_settings',
            'type' => 'select',
            'choices' => array( // Optional.
                'no' => esc_html__('No', 'medical_clinic'),
                'yes' => esc_html__('Yes', 'medical_clinic'),
            )
        )
    );
}


/**
 * Theme options in the Customizer js
 * @package medical_clinic
 */

function editor_customizer_script()
{
    wp_enqueue_script('wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array('jquery'), false, true);
    wp_enqueue_script('customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array('jquery'), false, true);
}

add_action('customize_controls_enqueue_scripts', 'editor_customizer_script');

add_action('admin_menu', 'medical_clinic_customize_admin_menu_hide', 999);
function medical_clinic_customize_admin_menu_hide()
{
    global $submenu;

    // Remove Appearance - Customize Menu
    unset($submenu['themes.php'][6]);

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode(wp_unslash($_SERVER['REQUEST_URI'])),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="medical_clinic-theme-options"><span>' . esc_html__('medical_clinic', 'medical_clinic') . '</span>' . esc_html__('Theme Options', 'medical_clinic') . '</div>',
        '<div class="medical_clinic-theme-options"><span>' . esc_html__('medical_clinic', 'medical_clinic') . '</span>' . esc_html__('Theme Options', 'medical_clinic') . '</div>',
        'customize',
        esc_url($customize_url),
        ''
    );
}

function customizer_repeater_sanitize($input)
{
    $input_decoded = json_decode($input, true);

    if (!empty($input_decoded)) {
        foreach ($input_decoded as $boxk => $box) {
            foreach ($box as $key => $value) {

                $input_decoded[$boxk][$key] = wp_kses_post(force_balance_tags($value));

            }
        }
        return json_encode($input_decoded);
    }
    return $input;
}