// Commom Plugins
(function($) {

    'use strict';

    // Scroll to Top Button.
    if (typeof theme.PluginScrollToTop !== 'undefined') {
        theme.PluginScrollToTop.initialize();
    }

    // Tooltips
    if ($.isFunction($.fn['tooltip'])) {
        $('[data-tooltip]:not(.manual), [data-plugin-tooltip]:not(.manual)').tooltip();
    }

    // Popover
    if ($.isFunction($.fn['popover'])) {
        $(function() {
            $('[data-plugin-popover]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = theme.fn.getOptions($this.data('plugin-options'));
                if (pluginOptions)
                    opts = pluginOptions;

                $this.popover(opts);
            });
        });
    }

    // Validations
    if (typeof theme.PluginValidation !== 'undefined') {
        theme.PluginValidation.initialize();
    }

    // Match Height
    if ($.isFunction($.fn['matchHeight'])) {

        $('.match-height').matchHeight();

        // Featured Boxes
        $('.featured-boxes .featured-box').matchHeight();

        // Featured Box Full
        $('.featured-box-full').matchHeight();

    }

}).apply(this, [jQuery]);

// Animate
(function($) {

    'use strict';

    if ($.isFunction($.fn['themePluginAnimate'])) {

        $(function() {
            $('[data-appear-animation]').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = theme.fn.getOptions($this.data('plugin-options'));
                if (pluginOptions)
                    opts = pluginOptions;

                $this.themePluginAnimate(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Carousel
(function($) {

    'use strict';

    if ($.isFunction($.fn['themePluginCarousel'])) {

        $(function() {
            $('[data-plugin-carousel]:not(.manual), .owl-carousel:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = theme.fn.getOptions($this.data('plugin-options'));
                if (pluginOptions)
                    opts = pluginOptions;

                $this.themePluginCarousel(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Scrollable
(function($) {

    'use strict';

    if ( $.isFunction($.fn[ 'nanoScroller' ]) ) {

        $(function() {
            $('[data-plugin-scrollable]').each(function() {
                var $this = $( this ),
                    opts = {};

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions) {
                    opts = pluginOptions;
                }

                $this.themePluginScrollable(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Section Scroll
(function($) {

    'use strict';

    if ($.isFunction($.fn['themePluginSectionScroll'])) {

        $(function() {
            $('[data-plugin-section-scroll]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = theme.fn.getOptions($this.data('plugin-options'));
                if (pluginOptions)
                    opts = pluginOptions;

                $this.themePluginSectionScroll(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Sticky
(function($) {

    'use strict';

    if ($.isFunction($.fn['themePluginSticky'])) {

        $(function() {
            $('[data-plugin-sticky]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = theme.fn.getOptions($this.data('plugin-options'));
                if (pluginOptions)
                    opts = pluginOptions;

                $this.themePluginSticky(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Commom Partials
(function($) {

    'use strict';

    // Sticky Header
    if (typeof theme.StickyHeader !== 'undefined') {
        theme.StickyHeader.initialize();
    }

    // Nav Menu
    if (typeof theme.Nav !== 'undefined') {
        theme.Nav.initialize();
    }

    // Search
    if (typeof theme.Search !== 'undefined') {
        theme.Search.initialize();
    }

    // Newsletter
    if (typeof theme.Newsletter !== 'undefined') {
        theme.Newsletter.initialize();
    }

    // Account
    if (typeof theme.Account !== 'undefined') {
        theme.Account.initialize();
    }

}).apply(this, [jQuery]);

function openNav(){
    document.getElementById("mySidenav").style.width="100%";
}
function closeNav(){
    document.getElementById("mySidenav").style.width="0";
}

(function($){
    $(document).ready(function () {

    });
})(jQuery);
