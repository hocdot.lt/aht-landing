</div> <!-- End main-->

<footer class="mt-0" id="footer">
    <div class="footer-copyright">
        <div class="container py-2">
            <div class="nav-footer">
                <?php
                if (function_exists('render_header_menu')) {
                    render_header_menu();
                }
                ?>
            </div>
            <div class="desc-footer">
                <?php
                $copyright = get_theme_mod('copyright', '<p class="text-center m-0">((C) 2019. All Rights Reserved. MedicalClinic. Designed & Developed by <a href="#">Template.net</a></p>');;
                if ($copyright != '') : ?>
                    <?php echo wp_kses_post($copyright); ?>
                <?php endif; ?>


            </div>
            <div class="logo-footer">
                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                    <?php
                    $logo_header = get_theme_mod('logo', get_template_directory_uri() . '/images/logo.png');
                    if ($logo_header && $logo_header != ''):
                        echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                    else:
                        //bloginfo('name');
                    endif;
                    ?>
                </a>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</div> <!-- End page-->
</body>
</html>