<?php
/**
 * Template Name: Home
 */
get_header();

    if (function_exists('barlounge_banner_home')) {
        barlounge_banner_home();
    }
    if (function_exists('barlounge_about_home')) {
        barlounge_about_home();
    }
    if (function_exists('barlounge_reservation_home')) {
        barlounge_reservation_home();
    }
    if (function_exists('barlounge_services_home')) {
        barlounge_services_home();
    }
    if (function_exists('barlounge_gallery_home')) {
        //barlounge_gallery_home();
    }
    if (function_exists('barlounge_get_home_menu')) {
        barlounge_get_home_menu();
    }
    if (function_exists('barlounge_get_home_recent_blog')) {
        barlounge_get_home_recent_blog();
    }
    if (function_exists('barlounge_works_home')) {
        barlounge_works_home();
    }
    if (function_exists('barlounge_get_home_testimonials')) {
        barlounge_get_home_testimonials();
    }
    if (function_exists('barlounge_contact_section')) {
        barlounge_contact_section();
    }

get_footer(); ?>