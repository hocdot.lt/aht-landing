<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
$barlounge_sidebar_left = barlounge_get_sidebar_left();
$barlounge_sidebar_right = barlounge_get_sidebar_right();
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site ">
		<header id="masthead" class="site-header <?php if ( is_front_page() ) echo 'fixed-top'?>">
			<?php
				$bg_header_site = get_theme_mod('background_header_default',get_template_directory_uri() . '/images/bg-header-page.jpg');
			?>
			<?php if (is_front_page()) : ?>
			<div class="header-wrapper header-home">
			<?php else : ?>
			<div class="header-wrapper">
			<?php endif; ?>
				<div class="wrap-container">
					<div class="container">
						<div class="row">
							<div class="col-lg-2 col-md-6 col-6">
									<?php if (is_front_page()) : ?>
										<div class="logo-midde logo">
											<h1 class="header-logo">
										<?php else : ?>
											<div class="logo-midde logo">
											<h2 class="header-logo ">
											<?php endif; ?>
											<a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
												<?php 

												$logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');

												?>

												<?php
												if ($logo_header && $logo_header!=''):
													echo '<img class="logo-default"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
												else:
													//bloginfo('name');
												endif;
												?>
											</a>
											<?php if (is_front_page()) : ?>
										</h1>
									</div>
									<?php else : ?>
										</h2>
									</div>
									<?php endif; ?>
							</div>
							<div class="col-lg-6 col-md-2 col-2">
							<?php
								if(function_exists('barlounge_header_menu')){
									barlounge_header_menu();
								}
							?>	
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Menu -->
		</header>
        <div id="main" class="wrapper">
				
            

                    
        
       
