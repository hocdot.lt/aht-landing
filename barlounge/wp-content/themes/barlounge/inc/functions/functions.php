<?php
require_once(BARLOUNGE_FUNCTIONS . '/ajax_search/ajax-search.php');
// Compatible WPML (Add wpml option)
require_once(BARLOUNGE_FUNCTIONS . '/post_like_count.php');

if(!function_exists('barlounge_social_share')){
    function barlounge_social_share(){ ?>
	   <ul class="social-share">
			<?php if(get_theme_mod('share_facebook','yes') == 'yes'):?>
				<li><a target="_blank" class="fb" href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>" onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" ><i class="fab fa-facebook-square"></i></a></li>
			<?php endif;?>	
			<?php if(get_theme_mod('share_instagram','yes') == 'yes'):?>
				<li><a target="_blank" class="inta" href="https://api.instagram.com/?url=<?php the_permalink(); ?>"  onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" ><i class="fab fa-instagram"></i></a></li>
			<?php endif;?>
			<?php if(get_theme_mod('share_twitter','yes') == 'yes'):?>
				<li><a target="_blank" class="tw" href="https://twitter.com/share?url=<?php the_permalink(); ?>&amp;hashtags=seoiz" onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" ><i class="fab fa-twitter-square"></i></a></li>
			<?php endif;?>
			<?php if(get_theme_mod('share_pinterest','yes') == 'no'):?>
				<li><a target="_blank" class="pin" href="http://www.pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>"  onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" ><i class="fab fa-pinterest"></i></a></li>
			<?php endif;?>
			<?php if(get_theme_mod('share_google','yes') == 'yes'):?>
				<li><a target="_blank" class="gg" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" ><i class="fab fa-google-plus"></i></a></li>
			<?php endif;?>
			<?php if(get_theme_mod('share_linkedin','yes') == 'yes'):?>
				<li><a target="_blank" class="linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" onclick="window.open(this.href, 'mywin','left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;" ><i class="fab fa-linkedin"></i></a></li>
			<?php endif;?>
		</ul>
	<?php 
    }    
}

if(!function_exists('barlounge_social_link')){
    function barlounge_social_link(){
         if (get_theme_mod('facebook','#')!='' || get_theme_mod('instagram','#')!='' || get_theme_mod('twitter','#') !='' || get_theme_mod('google','#') !='' || get_theme_mod('linkedin','#') !='' ) : ?>
               <ul class="link-social">
                    <?php if(get_theme_mod('twitter','#') !=''):?>
                        <li><a target="_blank" class="tw" href="<?php echo esc_url(get_theme_mod('twitter'));?>" ><i class="fab fa-twitter-square"></i></a></li>
                    <?php endif;?>
                    <?php if(get_theme_mod('facebook','#')!=''):?>
                        <li><a target="_blank" class="fb" href="<?php echo esc_url(get_theme_mod('facebook'));?>" ><i class="fab fa-facebook-square"></i></a></li>
                    <?php endif;?>
                   <?php if(get_theme_mod('linkedin','#') !=''):?>
                       <li><a target="_blank" class="linkedin" href="<?php echo esc_url(get_theme_mod('linkedin'));?>" ><i class="fab fa-linkedin"></i></a></li>
                   <?php endif;?>
                   <?php if(get_theme_mod('pinterest','#') !=''):?>
                       <li><a target="_blank" class="pinterest" href="<?php echo esc_url(get_theme_mod('pinterest'));?>" ><i class="fab fa-pinterest-square"></i></a></li>
                   <?php endif;?>
                    <?php if(get_theme_mod('google','#') !=''):?>
                        <li><a target="_blank" class="gg" href="<?php echo esc_url(get_theme_mod('google'));?>" ><i class="fab fa-google-plus"></i></a></li>
                    <?php endif;?>
                   <?php if(get_theme_mod('instagram','#')!=''):?>
                       <li><a target="_blank" class="inta" href="<?php echo esc_url(get_theme_mod('instagram'));?>" ><i class="fab fa-instagram"></i></a></li>
                   <?php endif;?>
               </ul>
        <?php endif;   
    }    
}

if(!function_exists('barlounge_get_relatedpost')){
    function barlounge_get_relatedpost() {
        if(get_theme_mod('blog_single_related','yes')): ?>
                <?php                    
                    global $post;
                    $orig_post = $post;
                    $categories = wp_get_post_categories($post->ID);
                    if ($categories) :
                        $categories_ids = array();
                        foreach($categories as $individual_categories) {
                            if (isset($individual_categories->term_id)) {
                              $categories_ids[] = $individual_categories->term_id;

                            }
                        }
                        $args=array(
                            'tag__in' => $categories_ids,
                            'post__not_in' => array($post->ID),
                            'posts_per_page'=>3, // Number of related posts to display.
                            'ignore_sticky_posts '=>0,
                        );
                        $my_query = new wp_query( $args );
                        if ($my_query->have_posts()): ?>
                            <div class="related-posts">
                                <h3><span><?php echo esc_html__('Read Related Posts','barlounge')?></span></h3>
                                <div class="wrapp row">
                                    <?php    
                                        while( $my_query->have_posts() ):
                                            $my_query->the_post();
                                    ?>
                                        <?php  if (!is_sticky()):?>  
										<div class="item-post col-xl col-md-4">
											<div class="wrapp">
											<?php if ( has_post_thumbnail() ) : ?>
												<div class="thumb"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('blog'); ?></a></div>
											<?php endif; ?>
												<div class="wrap-content">
													<div class="wrap">
														<h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
														<p class="desc"><?php echo wp_trim_words( get_the_excerpt(), $num_words = 12, '' ) ?></p>
														<p class="meta"><?php echo _e('Written By &colon;','barlounge'); ?> <span class="author"><?php the_author_posts_link(); ?></span> &sol; <?php the_time('d M'); ?></p>
													</div>
												</div>
											</div>
										</div>
                            <?php 
                                        endif; 
                                    endwhile;
                                endif;
                                $post = $orig_post;
                                wp_reset_query();
                                ?>
                                </div>
                            </div>
        <?php       
                        endif;
       endif;
    }
}

function barlounge_pagination($max_num_pages = null) {
    global $wp_query, $wp_rewrite;

    $max_num_pages = ($max_num_pages) ? $max_num_pages : $wp_query->max_num_pages;

    // Don't print empty markup if there's only one page.
    if ($max_num_pages < 2) {
        return;
    }

    $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
    $pagenum_link = html_entity_decode(get_pagenum_link());
    $query_args = array();
    $url_parts = explode('?', $pagenum_link);

    if (isset($url_parts[1])) {
        wp_parse_str($url_parts[1], $query_args);
    }

    $pagenum_link = remove_query_arg(array_keys($query_args), $pagenum_link);
    $pagenum_link = trailingslashit($pagenum_link) . '%_%';

    $format = $wp_rewrite->using_index_permalinks() && !strpos($pagenum_link, 'index.php') ? 'index.php/' : '';
    $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit($wp_rewrite->pagination_base . '/%#%', 'paged') : '?paged=%#%';

    // Set up paginated links.
    $links = paginate_links(array(
        'base' => $pagenum_link,
        'format' => $format,
        'total' => $max_num_pages,
        'current' => $paged,
        'end_size' => 1,
        'mid_size' => 1,
        'prev_next' => true,
        'prev_text' => '<span class="prev-text">Previous Page</span>',
        'next_text' => '<span class="next-text">Next Page</span>',
        'type' => 'list'
            ));

    if ($links) :
        ?>
        <nav class="pagination">
            <?php echo wp_kses($links, barlounge_allow_html()); ?>
        </nav>
        <?php
    endif;
}
/** 
    * Add Back to Top button.
*/
add_action( 'wp_footer', 'barlounge_overlay' );
function barlounge_overlay() {
    echo '<div class="overlay"></div>';
}

function barlounge_get_attachment( $attachment_id, $size = 'full' ) {
    if (!$attachment_id)
        return false;
    $attachment = get_post( $attachment_id );
    $image = wp_get_attachment_image_src($attachment_id, $size);

    if (!$attachment)
        return false;

    return array(
        'alt' => esc_attr(get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true )),
        'caption' => esc_attr($attachment->post_excerpt),
        'description' => esc_html($attachment->post_content),
        'href' => get_permalink( $attachment->ID ),
        'src' => esc_url($image[0]),
        'title' => esc_attr($attachment->post_title),
        'width' => esc_attr($image[1]),
        'height' => esc_attr($image[2])
    );
}

//get search template
if ( ! function_exists( 'barlounge_get_search_form' ) ) {
    function barlounge_get_search_form() {
        global $barlounge_settings;
        $template = get_search_form(false);  
        $output = '';
        ob_start();
        ?>
        <span class="btn-search search_button"><i class="fas fa-search"></i></span>      
        <div class="top-search content-filter">
            <?php echo wp_kses($template,barlounge_allow_html()); ?>
        </div>      
        <?php
        $output .= ob_get_clean();
        return $output;
    }
}

if(!function_exists('barlounge_get_sidebar_left')){
    function barlounge_get_sidebar_left() {
        $result = '';
        global $wp_query, $barlounge_sidebar_left;

        if (empty($barlounge_sidebar_left)) {
            $result = get_theme_mod('sidebar_left');
            if (is_404()) {
                $result = '';
            } else if (is_category()) {
                $cat = $wp_query->get_queried_object();
                $cat_sidebar = get_metadata('category', $cat->term_id, 'left-sidebar', true);
                if (!empty($cat_sidebar) && $cat_sidebar != 'default') {
                    $result = $cat_sidebar;
                }else if($cat_sidebar =='none') {
                    $result = "none";
                } else {
                    $result = get_theme_mod('blog_sidebar_left');
                }
            }else if (is_tag()){
                $result = get_theme_mod('blog_sidebar_left');
            }else if (is_front_page()){
                $result = get_theme_mod('home_sidebar_left');
            }
            else if (is_search()){
                $result = get_theme_mod('blog_sidebar_left');
            }
            else {
                if (is_singular()) {
                    $single_sidebar = get_post_meta(get_the_id(), 'left-sidebar', true);
                    if (!empty($single_sidebar) && $single_sidebar != 'default') {
                        $result = $single_sidebar;
                    }else if($single_sidebar =='none') {
                        $result = "none";
                    } else {
                        switch (get_post_type()) {
                            case 'post':
                                $result = get_theme_mod('blog_sidebar_left');
                                break;    
                            default:
                                $result = get_theme_mod('sidebar_left');
                        }
                    }
                } else {
                    if (is_home() && !is_front_page()) {
                        $result = get_theme_mod('blog_sidebar_left');
                    }
                }
            }
            $barlounge_sidebar_left = $result;
        }
        return $barlounge_sidebar_left;
    }
}

if(!function_exists('barlounge_get_sidebar_right')){
    function barlounge_get_sidebar_right() {
        $result = '';
        global $wp_query,$barlounge_sidebar_right;

        if (empty($barlounge_sidebar_right)) {
            $result = get_theme_mod('sidebar_right','general-sidebar');
            if (is_404()) {
                $result = 'none';
            }else if (is_category()) {
                $cat = $wp_query->get_queried_object();
                $cat_sidebar = get_metadata('category', $cat->term_id, 'right-sidebar', true);
                if (!empty($cat_sidebar) && $cat_sidebar != 'default') {
                    $result = $cat_sidebar;
                }else if($cat_sidebar =='none') {
                    $result = "none";
                } else {
                    $result = get_theme_mod('blog_sidebar_right','general-sidebar');
                }
            }else if (is_tag()){
                $result = get_theme_mod('blog_sidebar_right');
            }else if (is_front_page()){
                $result = get_theme_mod('home_sidebar_right','general-sidebar');
            }else if (is_search()){
                $result = get_theme_mod('blog_sidebar_right');
            }else {
                if (is_singular()) {
                    $single_sidebar = get_post_meta(get_the_id(), 'right-sidebar', true);
                    if (!empty($single_sidebar) && $single_sidebar != 'default') {
                        $result = $single_sidebar;
                    }else if($single_sidebar =='none') {
                        $result = "none";
                    } else {
                        switch (get_post_type()) {
                            case 'post':
                                $result = get_theme_mod('blog_sidebar_right');
                                break;
                            default:
                                $result = get_theme_mod('sidebar_right','general-sidebar');
                        }
                    }
                } else {
                    if (is_home() && !is_front_page()) {
                        $result = get_theme_mod('blog_sidebar_right');
                    }
                }
            }
            $barlounge_sidebar_right = $result;
        }
        return $barlounge_sidebar_right;
    }
}

/*=====banner section in Homepage=======*/
if(!function_exists('barlounge_banner_home')){
	function barlounge_banner_home(){
		if(get_theme_mod('banner_show','yes')== 'yes'){
			$banner_title = get_theme_mod('banner_title','LOUNGE &  BAR');
			$banner_sub_title = get_theme_mod('banner_sub_title','<span>Coctail - Bar&Lounge</span> where the time flows smoothly through the night, mugs of beer keeps overflowing till morning...');
			$button_banner_left = get_theme_mod('button_banner_left','Check Menu');
			$button_banner_right = get_theme_mod('button_banner_right','View Pricing');
			$link_banner_left = get_theme_mod('link_banner_left','#');
			$link_banner_right = get_theme_mod('link_banner_right','#');
			$background_banner = get_theme_mod('background_banner',get_template_directory_uri() . '/images/banner-home-img.jpg');
			?>
			<div class="section section-banner" <?php if(!empty($background_banner)){ ?> style="background-image:url(<?php echo $background_banner; ?>)" <?php } ?>>
				<div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-8">
						<?php if(!empty($banner_title)){ ?>
							<h2><?php echo $banner_title; ?></h2>
						<?php } ?>
						<?php if(!empty($banner_sub_title)){ ?>
							<p class="sub-title"><?php echo $banner_sub_title; ?></p>
						<?php } ?>
						<?php if(!empty($link_banner_left) || !empty($button_banner_left)){ ?>
							<a class="btn btn-home" href="<?php echo $link_banner_left; ?>"><?php echo $button_banner_left; ?></a>
						<?php } ?>
                        <?php if(!empty($link_banner_right) || !empty($button_banner_right)){ ?>
                            <a class="btn btn-home bg-transparent" href="<?php echo $link_banner_right; ?>"><?php echo $button_banner_right; ?></a>
                        <?php } ?>
                        </div>
					</div>
				</div>
			</diV>
		<?php
		}
	}
}

/*=====about section in Homepage=======*/
if(!function_exists('barlounge_about_home')){
	function barlounge_about_home(){
		if(get_theme_mod('about_show','yes')== 'yes'){
			$title_about = get_theme_mod('title_about','Founded by two friends in 1869');
			$about_content = get_theme_mod('about_content','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper di gnissim cras tincidunt lobortis feugiat vivamus at. Lorem ipsum adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at.</p>');
			$about_image = get_theme_mod('about_image',get_template_directory_uri() . '/images/image-home.jpg');
            $about_icon = get_theme_mod('about_icon',get_template_directory_uri() . '/images/icon-about.png');
			?>
			<div class="section section-about">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-4 col-md-8">
                        <?php if(!empty($about_icon)){ ?>
                            <img class="icon-about" src="<?php echo $about_icon; ?>" alt="<?php echo $title_about; ?>" >
                        <?php } ?>
						<?php if(!empty($title_about)){ ?>
							<h2><?php echo $title_about; ?></h2>
						<?php } ?>
						<?php if(!empty($about_content)){ ?>
							<?php echo apply_filters( 'the_content', $about_content); ?>
						<?php } ?>
						</div>
                        <?php if(!empty($about_image)){ ?>
                            <div class="col-lg-4 col-md-8">
                                <img class="float-lg-right" src="<?php echo $about_image; ?>" alt="<?php echo $title_about; ?>" >
                            </div>
                        <?php } ?>
                    </div>
				</div>
			</diV>
		<?php
		}
	}
}

/*=====Reservation section in Homepage=======*/
if(!function_exists('barlounge_reservation_home')){
    function barlounge_reservation_home(){
        if(get_theme_mod('on_reservation_show','yes')== 'yes'){
            $reservation_image = get_theme_mod('reservation_image',get_template_directory_uri() . '/images/image-home-2.jpg');
            $reservation_title = get_theme_mod('reservation_title','Book Your Table Now for Best Beer and Music');
            $drink_today_title = get_theme_mod('drink_today_title','Some of our special drinks for today');
            $reservation_content = get_theme_mod('reservation_content','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper di gnissim cras sed Faucibus a pellente sque sit amet. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at.');
            $button_reservation = get_theme_mod('button_reservation','Contact us');
            $link_reservation = get_theme_mod('link_reservation','#');
            $customizer_repeater_timework = get_theme_mod("customizer_repeater_timework");
            $customizer_repeater_drink_today = get_theme_mod("customizer_repeater_drink_today");
            ?>
            <div class="section section-reservation">
                <div class="container">
                    <div class="row">
                        <?php if(!empty($reservation_image)){ ?>
                            <div class="col-lg-3 col-md-8">
                                <img src="<?php echo $reservation_image; ?>" alt="<?php echo $reservation_title; ?>" >
                            </div>
                        <?php } ?>
                        <div class="col-lg-5 col-md-8 p-xl-0">
                            <?php if(!empty($reservation_title)){ ?>
                                <h2><?php echo $reservation_title; ?></h2>
                            <?php } ?>
                            <div class="row">
                            <?php if(!empty($reservation_content)){ ?>
                                <div class="col-lg-4 col-md-8 pr-xl-5">
                                <?php echo apply_filters( 'the_content', $reservation_content); ?>
                                </div>
                            <?php } ?>
                                <div class="col-lg-4 col-md-8">
                                    <div class="wrapper">
                                <?php if(!empty($link_reservation) || !empty($button_reservation)){ ?>
                                    <a class="btn btn-home bg-v2" href="<?php echo $link_reservation; ?>"><?php echo $button_reservation; ?></a>
                                <?php } ?>
                                <?php
                                if(!empty($customizer_repeater_timework)){
                                    echo '<table class="time-work mt-3 mt-lg-0">';
                                    $customizer_repeater_timework_decoded = json_decode($customizer_repeater_timework);
                                    foreach( $customizer_repeater_timework_decoded as $item ){
                                        ?>
                                        <tr <?php if( date('l', strtotime("now")) === $item->title) echo "class='active'"?>>
                                            <td class="text-right"><?php echo $item->title; ?></td>
                                            <td class="text-center">:</td>
                                            <td class="text-left"><?php echo $item->subtitle; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    echo '</table>';
                                }
                                ?></div></div>
                            </div>
                            <?php if(!empty($drink_today_title)){ ?>
                                <h2><?php echo $drink_today_title; ?></h2>
                            <?php } ?>
                            <?php
                            if(!empty($customizer_repeater_drink_today)){
                                echo '<ul class="drink-today-list d-flex flex-wrap">';
                                $customizer_repeater_drink_today_decoded = json_decode($customizer_repeater_drink_today);
                                foreach( $customizer_repeater_drink_today_decoded as $item ){
                                    echo '<li>'. $item->title .'</li>';
                                }
                                echo '</ul>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </diV>
            <?php
        }
    }
}

/*=====Services section in Homepage=======*/
if(!function_exists('barlounge_services_home')){
	function barlounge_services_home(){
		if(get_theme_mod('services_show','yes')== 'yes'){
            $title_services = get_theme_mod('title_services','Discover the Pure Blends of  Drinks and equisite food...');
            $category_services = get_theme_mod('$category_services','');
            $number_post_services = get_theme_mod('number_post_services','3');
			?>
			<div class="section section-services">
				<div class="container">
					<div class="wrapper">
                        <div class="top-section row">
                            <?php  if($title_services !=''){ ?>
                                <div class="col-xl-4 col-lg-8">
                                    <h2><?php echo $title_services; ?></h2>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if(!empty(number_post_services)){ ?>
                            <div class="list-services">
                                <div class="wrapper row">
                                    <?php
                                    $the_query = new WP_Query(array(
                                        'post_type' => 'post',
                                        'posts_per_page' => $number_post_services,
                                        'post_status' => 'publish',
                                        'orderby'	=> 'date',
                                        'order'	=> 'DESC',
                                        'cat'=> $category_services,
                                    ));
                                    ?>
                                    <?php if ( $the_query->have_posts() ) : $i = 1;
                                        while ( $the_query->have_posts() ) : $the_query->the_post();
                                            $class = '';
                                            switch ($i%3) {
                                                case "1":
                                                    $class = 'pr-xl-6';
                                                    break;
                                                case "2":
                                                    $class = 'px-xl-6';
                                                    break;
                                                case "0":
                                                    $class = 'pl-xl-6';
                                                    break;
                                                default:
                                                    $class = '';
                                            }
                                            ?>
                                            <div class="item-service col-lg <?php echo $class; ?>">
                                                <?php if ( has_post_thumbnail() ) : ?>
                                                     <div class="thumbnail">
                                                         <?php the_post_thumbnail('featured'); ?>
                                                        <h3 class="thumb-info-title">
                                                            <?php the_title(); ?>
                                                        </h3>
                                                     </div>
                                                <?php endif; ?>
                                                <div class="wrapp-content">
                                                    <p><?php echo wp_trim_words( get_the_excerpt(), $num_words = 25, '' ) ?></p>
                                                    <a href="<?php the_permalink(); ?>" class="read-more">Know More</a>
                                                </div>
                                            </div>
                                            <?php $i++;endwhile;
                                    endif;
                                    wp_reset_postdata(); ?>
                                </div>
                            </div>
                        <?php } ?>
					</div>
				</div>
			</diV>
		<?php
		}
	}
}

/*=====Gallery section in Homepage=======*/
if(!function_exists('barlounge_gallery_home')){
	function barlounge_gallery_home(){
		if(get_theme_mod('gallery_show','yes')== 'yes'){
			$title_gallery = get_theme_mod('title_gallery','Makeup Gallery');
			$sub_title_gallery = get_theme_mod('sub_title_gallery','Check the Works');
			?>
			<div class="section section-gallery">
				<div class="container">
					<div class="wrapper">
						<div class="top-section">
							<div class="left-title">
								<?php if(!empty($sub_title_gallery)){ ?>
									<h3 class="sub-title"><?php echo $sub_title_gallery; ?></h3>
								<?php } ?>
								<?php if(!empty($title_gallery)){ ?>
									<h2><?php echo $title_gallery; ?></h2>
								<?php } ?>
							</div>
							<div class="right-gallery-title">
								<?php 
								$terms = get_terms( array(
									'taxonomy' => 'gallery_cat',
									'orderby'    => 'ID', 
									'order'      => 'ASC',
									'hide_empty' => false
								) ); 
								if ( $terms ) { ?>
								<div class="gallery_header">
									<div class="gallery_filter">
										<div id="filters" class="button-group js-radio-button-group">
											<?php $j=0; foreach ( $terms as $key => $term ) : $j++; ?> 
												<div class="inline-block">
													<button class="btn-filter <?php if($j==1){ echo 'is-checked'; }; ?>" data-filter="<?php echo esc_attr($term->slug); ?>"><?php echo esc_html($term->name); ?></button>
												</div>
											<?php endforeach;?>  
										</div>
									</div> 
								</div>  								
								<?php } ?>								
							</div>
						</div>
						<div class="tabs_sort gallery_sort">
							<?php if ( $terms ) { ?>
								<?php foreach ( $terms as $key => $term ) : ?>
								<div id="<?php echo $term->slug; ?>" class="wrap-row">							
								<?php							
									$the_query = new WP_Query(array(
										'post_type' => 'gallery',
										'posts_per_page' => 8,
										'post_status' => 'publish',
										'orderby'	=> 'date',
										'order'	=> 'DESC',
										'tax_query' => array(
											array (
												'taxonomy' => 'gallery_cat',
												'field' => 'slug',
												'terms' => $term->slug
											)
										),										
									));
									if ( $the_query->have_posts() ) : 
										while ( $the_query->have_posts() ) : $the_query->the_post(); 
										?>
											<?php if ( has_post_thumbnail() ) : ?>
												<div class="item"><a title="<?php the_title();?>" href="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'barlounge_blog', false);echo $image[0];//the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><span class="wrap-image" style="background-image:url(<?php echo $image[0];//echo the_post_thumbnail_url(); ?>);">
                                                <?php the_post_thumbnail('barlounge_blog'); ?></span></a></div>
											<?php endif; ?>
									<?php
										endwhile;
									endif;
									wp_reset_postdata();									
								?>
								</div>
								<?php endforeach; ?>
							<?php } ?>
						</div>
					</div>
				</div>
			</diV>
            <script>
            jQuery(document).ready(function($){
                $('.tabs_sort.gallery_sort #woman').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    gallery:{
                        enabled:true
                    }
                });
                $('.tabs_sort.gallery_sort #men').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    gallery:{
                        enabled:true
                    }
                });
                $('.tabs_sort.gallery_sort #children').magnificPopup({
                    delegate: 'a',
                    type: 'image',
                    gallery:{
                        enabled:true
                    }
                });
            })
            </script>
		<?php
		}
	}
}

/*=====Works section in Homepage=======*/
if(!function_exists('barlounge_works_home')){
	function barlounge_works_home(){
		if(get_theme_mod('on_cta_show','yes')== 'yes'){
            $cta_content = get_theme_mod('cta_content','<h2>Coctail - Bar and Lounge is one of the most happening places in the City.</h2>');
			$bg_img_cta = get_theme_mod('bg_img_cta',get_template_directory_uri() . '/images/banner-home-img-2.jpg');
            $button_cta = get_theme_mod('button_cta','Contact Us Now');
            $link_btn_cta = get_theme_mod('link_btn_cta','#');
			?>

			<div class="section section-cta" <?php if(!empty($bg_img_cta)){ ?> style="background-image:url(<?php echo $bg_img_cta; ?>);" <?php } ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-1 col-md-8">
                        <?php if(!empty($cta_content)){ ?>
                            <?php echo apply_filters( 'the_content', $cta_content); ?>
                        <?php } ?>
                        <?php if(!empty($link_btn_cta) || !empty($button_cta)){ ?>
                            <a class="btn btn-home" href="<?php echo $link_btn_cta; ?>"><?php echo $button_cta; ?></a>
                        <?php } ?>
                        </div>
                    </div>
                </div>
			</diV>
		<?php
		}
	}
}

/*----Got a blog... section in Homepage----*/
if(!function_exists('barlounge_get_home_recent_blog')){
    function barlounge_get_home_recent_blog() {
        global $wp_query,$post;
        if(get_theme_mod('blog_show','yes')== 'yes'):
			$number_post_blog = get_theme_mod('number_post_blog','3');
            $category_blog = get_theme_mod('category_blog','');
			$title_blog = get_theme_mod('title_blog','Recent News and Events from Coctail - Lounge & Bar');
            $button_blog = get_theme_mod('button_blog','Go To Blog');
            $link_button_blog = get_theme_mod('link_button_blog','#');
        ?>
            <div class="section section-blog">
                <div class="container">
					<div class="top-section row">
                        <?php  if($title_blog !=''){ ?>
                            <div class="col-xl-5 col-lg-6">
                                <h2><?php echo $title_blog; ?></h2>
                            </div>
                        <?php } ?>
                        <?php if(!empty($link_button_blog) || !empty($button_blog)){ ?>
                            <div class="ml-auto">
                                <a class="btn btn-home" href="<?php echo $link_button_blog; ?>"><?php echo $button_blog; ?></a>
                            </div>
                        <?php } ?>
					</div>
					
					<?php if(!empty($number_post_blog)){ ?>
					<div class="blog-featured">
						<div class="wrapper row">
                            <?php
                            $the_query = new WP_Query(array(
                                'post_type' => 'post',
                                'posts_per_page' => $number_post_blog,
                                'post_status' => 'publish',
                                'cat'=> $category_blog,
                                'orderby'	=> 'date',
                                'order'	=> 'DESC',
                            ));
                            ?>
                            <?php if ( $the_query->have_posts() ) : $i = 1;
                                while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $class = '';
                                    switch ($i%3) {
                                        case "1":
                                            $class = 'pr-xl-6';
                                            break;
                                        case "2":
                                            $class = 'px-xl-6';
                                            break;
                                        case "0":
                                            $class = 'pl-xl-6';
                                            break;
                                        default:
                                            $class = '';
                                    }
                                ?>
                                    <div class="col-lg col-md-8 <?php echo $class; ?>">
                                        <div class="item-post">
                                            <div class="wrapp">
                                                <?php if ( has_post_thumbnail() ) : ?>
                                                    <div class="thumb"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute('featured'); ?>"><span class="wrap-img"  style="background-image:url(<?php echo the_post_thumbnail_url(); ?>);"><?php the_post_thumbnail('featured'); ?></span></a></div>
                                                <?php endif; ?>
                                                <div class="wrap-content">
                                                    <div class="wrap">
                                                        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                                                        <p class="desc"><?php echo wp_trim_words( get_the_excerpt(), $num_words = 25, '' ) ?></p>
                                                        <a href="<?php the_permalink(); ?>" class="read-more">Continue Reading</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $i++;endwhile;
                            endif;
                            wp_reset_postdata(); ?>
						</div>
					</div>  
					<?php } ?>
				</div>
            </div>
        <?php
           
        endif; 
    }
}

/*----testimonial... section in Homepage----*/
if(!function_exists('barlounge_get_home_testimonials')){
    function barlounge_get_home_testimonials() {
		if(get_theme_mod('testimonials_show','yes')== 'yes'){
			$testimonials_title = get_theme_mod('testimonials_title','What they say about our bar & lounge');
			?>
			<div class="section section-testimonial">
                <div class="container">
					<?php  if($testimonials_title !=''){ ?>
						<h2><?php echo $testimonials_title; ?></h2>
					<?php } ?>
					
					<?php
					$the_query = new WP_Query(array(
						'post_type' => 'testimonial',
						'posts_per_page' => 3,
						'post_status' => 'publish',
						'orderby'	=> 'date',
						'order'	=> 'DESC',
					));
					if ( $the_query->have_posts() ) : ?>
						<div class="testimonial-slider">
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<div class="item-testimonial col-4">
                                    <div class="avatar"><?php the_post_thumbnail('full'); ?></div>
									 <div class="description">
                                        <?php the_content(); ?>
                                         <p class="name"><?php the_title(); ?></p>
                                    </div>
								</div>			
							<?php endwhile; ?>
						</div>			
					<?php	
					endif;
					wp_reset_postdata();				
					?>
				</div>
			</div>
		<?php
		}
	}
}


/*----Got a Menu... section in Homepage----*/
if(!function_exists('barlounge_get_home_menu')){
    function barlounge_get_home_menu() {
		if(get_theme_mod('pricing_show','yes')== 'yes'){
            $drink_menu_title = get_theme_mod('drink_menu_title','Drink Menu');
            $food_menu_title = get_theme_mod('food_menu_title','Food Menu');
            $drink_menu_desc = get_theme_mod('drink_menu_desc','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper di gnissim cras sed Faucibus a pellente sque sit amet. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at.');
            $food_menu_desc = get_theme_mod('food_menu_desc','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper di gnissim cras sed Faucibus a pellente sque sit amet. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at.');
            $customizer_repeater_drink_menu = get_theme_mod("customizer_repeater_drink_menu");
            $customizer_repeater_food_menu = get_theme_mod("customizer_repeater_food_menu");
			?>
			<div class="section section-menu">
                <div class="container">
					<div class="menu-wrap">
                        <div class="row">
                            <div class="col-lg-4 col-md-8 col-left mb-lg-0 mb-5">
                            <?php  if($drink_menu_title !=''): ?>
                                <h2 class="menu-title"><span><?php echo $drink_menu_title; ?></span></h2>
                            <?php endif; ?>
                            <div class="wrap-menu">
                                <div class="inner-menu">
                                <?php if(!empty($drink_menu_desc)): ?>
                                    <?php echo apply_filters( 'the_content', $drink_menu_desc); ?>
                                <?php endif; ?>
                                <?php
                                    if(!empty($customizer_repeater_drink_menu)){
                                        echo '<ul class="menu-list">';
                                        $customizer_repeater_example_decoded = json_decode($customizer_repeater_drink_menu);
                                        foreach( $customizer_repeater_example_decoded as $item ){
                                            echo '<li class="d-flex justify-content-between"><div class="title"><h4>'. $item->title .'</h4><span>'. $item->subtitle .'</span></div><div class="price">'. $item->price .'</div></li>';
                                        }
                                        echo '</ul>';
                                    }
                                ?>
                                </div>
                            </div>
                            </div>
                            <div class="col-lg-4 col-md-8 col-right mb-lg-0 mb-5">
                            <?php  if($food_menu_title !=''): ?>
                                <h2 class="menu-title"><span><?php echo $food_menu_title; ?></span></h2>
                            <?php endif; ?>
                            <div class="wrap-menu">
                                <div class="inner-menu">
                                <?php if(!empty($food_menu_desc)): ?>
                                    <?php echo apply_filters( 'the_content', $food_menu_desc); ?>
                                <?php endif; ?>
                                <?php
                                    if(!empty($customizer_repeater_food_menu)){
                                        echo '<ul class="menu-list">';
                                        $customizer_repeater_example_decoded = json_decode($customizer_repeater_food_menu);
                                        foreach( $customizer_repeater_example_decoded as $item ){
                                            echo '<li class="d-flex justify-content-between"><div class="title"><h4>'. $item->title .'</h4><span>'. $item->subtitle .'</span></div><div class="price">'. $item->price .'</div></li>';
                                        }
                                        echo '</ul>';
                                    }
                                ?>
                                </div>
                            </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		<?php
		}
	}
}
/*----Got a contact... section in Homepage----*/
if(!function_exists('barlounge_get_home_contact')){
    function barlounge_get_home_contact() {
		if(get_theme_mod('contact_show','yes')== 'yes'){
			$contact_shortcode = get_theme_mod('contact_shortcode');
			$contact_title = get_theme_mod('contact_title','Have a Project in Mind?');
			$contact_sub_title = get_theme_mod('contact_sub_title','lets make something awesome');
			$contact_desc = get_theme_mod("contact_desc","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been 1500s, scrambled it to make specimen book.");
			
			$contact_address = get_theme_mod('contact_address','204, Eastwood, America');
			$contact_opening = get_theme_mod('contact_opening','10:00 am - 11:00 pm');
			$contact_call = get_theme_mod('contact_call','800 1234 5678');
			?>
			<div class="block block-contact">
                <div class="container">
                    <div class="row">
						<div class="left-contact col-lg-5 col-md-5 col-sm-12 col-xs-12">
							<?php  if($contact_title !=''){ ?>
								<h2><?php echo $contact_title; ?></h2>
							<?php } ?>
							<?php  if($contact_sub_title !=''){ ?>
								<p class="sub-title"><?php echo $contact_sub_title; ?></p>
							<?php } ?>
							<?php  if($contact_desc !=''){ ?>
								<?php echo apply_filters( 'the_content', $contact_desc); ?>
							<?php } ?>
							<div class="info-address">
								<?php  if($contact_address !=''){ ?>
									<p class="address"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $contact_address; ?></p>
								<?php } ?>	
								<?php  if($contact_opening !=''){ ?>
									<p class="opening"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $contact_opening; ?></p>
								<?php } ?>
								<?php  if($contact_call !=''){ ?>
									<p class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:<?php echo str_replace(" ","", $contact_call); ?>"><?php echo $contact_call; ?></a></p>
								<?php } ?>								
							</div>
						</div>
						<div class="right-conatct col-lg-7 col-md-7 col-sm-12 col-xs-12">
							<?php  if($contact_shortcode !=''){ echo do_shortcode(''.$contact_shortcode.''); } ?>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
	}
}

/*-----Contact sectuon*/
if(!function_exists('barlounge_contact_section')){
    function barlounge_contact_section() {
        if(get_theme_mod('contact_show','yes')== 'yes'):
            $contact_form_title = get_theme_mod('contact_form_title','Got questions? contact us for details');
            $contact_shortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="49" title="Contact form home"]');
            $newsletter_image = get_theme_mod('newsletter_image',get_template_directory_uri() . '/images/image-home-9.jpg');
            $contact_title = get_theme_mod('contact_title','COCTAIL');
            $contact_sub_title = get_theme_mod('contact_sub_title','Bar & Lounge');
            $newsletter_title = get_theme_mod('newsletter_title','Subscribe to Email Newsletter');
            $newsletter_sub_title = get_theme_mod('newsletter_sub_title','* We promise we dont spam you!');
            $newsletter_shortcode = get_theme_mod('newsletter_shortcode','[mc4wp_form id="76"]');
            $contact_info = get_theme_mod('contact_info','123 Street, City, State 45678 <br> Phone 1234 56 7890 <br> info@coctailbarandlounge.com <br> coctailbarandlounge.com');
            $contact_icon = get_theme_mod('contact_icon',get_template_directory_uri() . '/images/icon-contact.png');
        ?>
            <div class="section section-contact" id="contact">
                <div class="container-fluid">
                     <div class="row">
                        <div class="col-xl-4 col-md-8 col-sm-8 p-lg-0">
                            <div class="wrap-contact-form">
                                <?php if ( $contact_form_title !='') : ?>
                                    <h2><?php echo wp_kses_post( $contact_form_title); ?></h2>
                                <?php endif;?>
                                <?php if($contact_shortcode != ''): ?>
                                    <?php echo do_shortcode($contact_shortcode); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-xl-4 col-sm-8 p-0">
                            <div class="wrap-info" <?php if(!empty($newsletter_image)){ ?> style="background-image:url(<?php echo $newsletter_image; ?>);" <?php } ?>>
                                <div class="inner-wrapper">
                                <div class="info-address">
                                    <?php if ( $contact_icon !='') : ?>
                                        <img class="icon" src="<?php echo $contact_icon; ?>" alt="<?php echo $contact_icon; ?>" >
                                    <?php endif;?>
                                    <?php if ( $contact_title !='') : ?>
                                        <h2 class="text-white"><?php echo wp_kses_post( $contact_title); ?></h2>
                                    <?php endif;?>
                                    <?php if ( $contact_sub_title !='') : ?>
                                        <h4 class="text-white"><?php echo wp_kses_post( $contact_sub_title); ?></h4>
                                    <?php endif;?>
                                    <?php  if($contact_info !=''): ?>
                                        <div class="address text-white">
                                            <?php echo apply_filters( 'the_content', $contact_info); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <?php
                                if(function_exists('barlounge_social_link')){
                                    barlounge_social_link();
                                }
                                ?>
                                <?php if ( $newsletter_title !='') : ?>
                                    <h3 class="newsletter-title text-white"><?php echo wp_kses_post( $newsletter_title); ?></h3>
                                <?php endif;?>
                                <?php if($newsletter_shortcode != ''): ?>
                                <div class="newsletter-form">
                                    <?php echo do_shortcode($newsletter_shortcode); ?>
                                </div>
                                <?php endif; ?>
                                <?php if ( $newsletter_sub_title !='') : ?>
                                    <p class="newsletter-desc text-white"><?php echo wp_kses_post( $newsletter_sub_title); ?></p>
                                <?php endif;?>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php   endif;
    }
}

/*-----function header menu*/
if(!function_exists('barlounge_header_menu')){
    function barlounge_header_menu() { ?>
		<div class="header-menu">
			<div class="header-container">
				<div class="open-menu-mobile d-lg-none">
					<div class="display-flex">
						<i class="fa fab fa-bars"></i>
					</div>
				</div>
				<div class="header-center hide-menu">
					<h3 class="menu-title d-lg-none"><?php echo esc_html__('Menu','barlounge');?></h3>
					<div class="close-menu-mobile d-lg-none">
						<i class="fa fa-times"></i>
					</div>  
					<nav id="site-navigation" class="main-navigation">
						<?php
							$before_items_wrap = '';
							$after_item_wrap = '';            
							if (has_nav_menu('primary')) {
								wp_nav_menu(array(
									'theme_location' => 'primary',
									'menu_class' => 'mega-menu',
									'items_wrap' => $before_items_wrap . '<ul id="%1$s" class="%2$s">%3$s</ul>' . $after_item_wrap,
									'walker' => new Walker_Nav_Menu()
									)
								);
							}
						?> 
					</nav> 
				</div> 
			</div> 
		</div>
    <?php
    }
}
?>