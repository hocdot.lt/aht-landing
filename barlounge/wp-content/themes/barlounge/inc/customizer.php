<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'barlounge_customize_register' );
function barlounge_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'barlounge_Customize_Sidebar_Control' ) ) {
	    class barlounge_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',			
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',			
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',
                '' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'barlounge') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','barlounge'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','barlounge' ),
	      'description' => esc_html__( 'Upload Logo','barlounge' ),
	      'section' => 'general_settings',
	   )
	) );
	
	$wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'Montserrat:200,300,400,500,600,700',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Montserrat:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Main Google Font',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'barlounge'),
        'description'    => __('Several settings pertaining my theme', 'barlounge'),
    ) );
	
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','barlounge'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
	// title banner
	$wp_customize->add_setting( 'banner_title',
	   array(
	      'default' => 'LOUNGE &  BAR',
	   )
	);	
	$wp_customize->add_control( 'banner_title',
	   array(
	      'label' => esc_html__( 'Title Banner','barlounge' ),
	      'section' => 'home_banner_settings',
	      'type' => 'text',
	   )
	);	
	// sub title banner
	$wp_customize->add_setting( 'banner_sub_title',
	   array(
	      'default' => esc_html__('<span>Coctail - Bar&Lounge</span> where the time flows smoothly through the night, mugs of beer keeps overflowing till morning...','barlounge'),
	   )
	);
	$wp_customize->add_control( 'banner_sub_title',
	   array(
	      'label' => esc_html__( 'Sub Title Banner','barlounge' ),
	      'section' => 'home_banner_settings',
	      'type' => 'textarea',
	   )
	);

	// button banner
	$wp_customize->add_setting( 'button_banner_left',
	   array(
	      'default' => esc_html__('Check Menu','barlounge'),
	   )
	);	
	$wp_customize->add_control('button_banner_left',
		array(
	      'label' => esc_html__( 'Button banner left','barlounge' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',	
	   )
	);
	$wp_customize->add_setting( 'link_banner_left',
	   array(
	      'default' => esc_html__('#','barlounge'),

	   )
	);	
	$wp_customize->add_control('link_banner_left',
		array(
	      'label' => esc_html__( 'Button left URL','barlounge' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',	
	   )
	);
    $wp_customize->add_setting( 'button_banner_right',
        array(
            'default' => esc_html__('View Pricing','barlounge'),

        )
    );
    $wp_customize->add_control('button_banner_right',
        array(
            'label' => esc_html__( 'Button banner right','barlounge' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_banner_right',
        array(
            'default' => esc_html__('#','barlounge'),

        )
    );
    $wp_customize->add_control('link_banner_right',
        array(
            'label' => esc_html__( 'Button right URL','barlounge' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
	// background banner
	$wp_customize->add_setting( 'background_banner',
		array(
			'default' => get_template_directory_uri() . '/images/banner-home-img.jpg',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_banner',
	   array(
	      'label' => esc_html__( 'Background Banner','barlounge' ),
	      'description' => esc_html__( 'Upload Image','barlounge' ),
	      'section' => 'home_banner_settings',
	   )
	) );
	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
            )
	   )
	);
	// End banner section
	
	// Section about
	
	$wp_customize->add_section( 'about_settings',
	   array(
	      'title' => esc_html__( 'About section','barlounge'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
	
	$wp_customize->add_setting( 'about_image',
		array(
			'default' => get_template_directory_uri() . '/images/image-home.jpg',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_image',
	   array(
	      'label' => esc_html__( 'Image About','barlounge' ),
	      'description' => esc_html__( 'Upload Image','barlounge' ),
	      'section' => 'about_settings',
	   )
	) );
    $wp_customize->add_setting( 'about_icon',
        array(
            'default' => get_template_directory_uri() . '/images/icon-about.png',
        )
    );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_icon',
        array(
            'label' => esc_html__( 'Icon','barlounge' ),
            'description' => esc_html__( 'Upload icon','barlounge' ),
            'section' => 'about_settings',
        )
    ) );
	$wp_customize->add_setting( 'title_about',
	   array(
	      'default' => esc_html__('Founded by two friends in 1869','barlounge'),

	   )
	);	
	$wp_customize->add_control('title_about',
		array(
	      'label' => esc_html__( 'Title About','barlounge' ),
	      'section' => 'about_settings',
	       'type' => 'text',	
	   )
	);
	// content
	$wp_customize->add_setting( 'about_content',
		array(
			'default' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper di gnissim cras tincidunt lobortis feugiat vivamus at. Lorem ipsum adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at.</p>",
		)
	);	
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'about_content',
	   array(
	      'label' => esc_html__( 'About content','barlounge' ),
	      'description' => esc_html__( 'About content','barlounge' ),
	      'section' => 'about_settings',
	   )
	) );
	
	$wp_customize->add_setting( 'about_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'about_show',
	   array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'about_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
            )
	   )
	);
	// End about

    // Section reservation
    $wp_customize->add_section( 'reservation_settings',
        array(
            'title' => esc_html__( 'Reservation Section','barlounge'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    $wp_customize->add_setting( 'reservation_image',
        array(
            'default' => get_template_directory_uri() . '/images/image-home-2.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'reservation_image',
        array(
            'label' => esc_html__( 'Image Reservation','barlounge' ),
            'description' => esc_html__( 'Upload Image','barlounge' ),
            'section' => 'reservation_settings',
        )
    ) );
    $wp_customize->add_setting( 'reservation_title',
        array(
            'default' => esc_html__('Book Your Table Now for Best Beer and Music','barlounge'),
        )
    );
    $wp_customize->add_control('reservation_title',
        array(
            'label' => esc_html__( 'Title Section','barlounge' ),
            'section' => 'reservation_settings',
            'type' => 'text',
        )
    );
    // content
    $wp_customize->add_setting( 'reservation_content',
        array(
            'default' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper di gnissim cras sed Faucibus a pellente sque sit amet. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at.",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'reservation_content',
        array(
            'label' => esc_html__( 'Content section','barlounge' ),
            'description' => esc_html__( 'Content section','barlounge' ),
            'section' => 'reservation_settings',
        )
    ) );
    // button reservation
    $wp_customize->add_setting( 'button_reservation',
        array(
            'default' => esc_html__('Contact Us','barlounge'),

        )
    );
    $wp_customize->add_control('button_reservation',
        array(
            'label' => esc_html__( 'Button Reservation','barlounge' ),
            'section' => 'reservation_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_reservation',
        array(
            'default' => esc_html__('#','barlounge'),

        )
    );
    $wp_customize->add_control('link_reservation',
        array(
            'label' => esc_html__( 'Button URL','barlounge' ),
            'section' => 'reservation_settings',
            'type' => 'text',
        )
    );
    //timework
    $wp_customize->add_setting( 'customizer_repeater_timework', array(
        'sanitize_callback' => 'customizer_repeater_sanitize'
    ));
    $wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'customizer_repeater_timework', array(
        'label'   => esc_html__('Timework','customizer-repeater'),
        'section' => 'reservation_settings',
        'customizer_repeater_title_control' => true,
        'customizer_repeater_subtitle_control' => true,
        'customizer_repeater_price_control' => false,
        'customizer_repeater_repeater_control' => true
    ) ) );

    //drink today
    $wp_customize->add_setting( 'drink_today_title',
        array(
            'default' => esc_html__('Some of our special drinks for today','barlounge'),
        )
    );
    $wp_customize->add_control('drink_today_title',
        array(
            'label' => esc_html__( 'Title Section','barlounge' ),
            'section' => 'reservation_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'customizer_repeater_drink_today', array(
        'sanitize_callback' => 'customizer_repeater_sanitize'
    ));
    $wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'customizer_repeater_drink_today', array(
        'label'   => esc_html__('Drink today','customizer-repeater'),
        'section' => 'reservation_settings',
        'customizer_repeater_title_control' => true,
        'customizer_repeater_subtitle_control' => false,
        'customizer_repeater_price_control' => false,
        'customizer_repeater_repeater_control' => true
    ) ) );

    $wp_customize->add_setting( 'on_reservation_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'on_reservation_show',
        array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'reservation_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','barlounge' ),
                'yes' => esc_html__( 'Yes','barlounge' ),
            )
        )
    );
    // End reservation
	
	// start Services
	$wp_customize->add_section( 'services_settings',
	   array(
	      'title' => esc_html__( 'Discover Section','barlounge'  ),
	      'priority' => 20,
		  'panel'  => 'home_page_setting',
	   )
	);
    $wp_customize->add_setting( 'title_services',
        array(
            'default' => esc_html__('Discover the Pure Blends of  Drinks and equisite food...','barlounge'),
        )
    );
    $wp_customize->add_control('title_services',
        array(
            'label' => esc_html__( 'Title Section','barlounge' ),
            'section' => 'services_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'number_post_services',
        array(
            'default' => esc_html__('3','barlounge'),
        )
    );
    $wp_customize->add_control('number_post_services',
        array(
            'label' => esc_html__( 'Number Post','barlounge' ),
            'section' => 'services_settings',
            'type' => 'text',
        )
    );
    //  Select category
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

    $wp_customize->add_setting( 'category_services',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('category_services',
        array(
            'label' => esc_html__( 'Category Post','barlounge' ),
            'section' => 'services_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );


	$wp_customize->add_setting( 'services_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'services_show',
	   array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'services_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
            )
	   )
	);
	// End Services

    // start Menu
    $wp_customize->add_section( 'menu_settings',
        array(
            'title' => esc_html__( 'Menu Section','barlounge'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    // drink menu
    $wp_customize->add_setting( 'drink_menu_title',
        array(
            'default' => esc_html__('Drink Menu','barlounge'),
        )
    );
    $wp_customize->add_control('drink_menu_title',
        array(
            'label' => esc_html__( 'Title Drink Menu','barlounge' ),
            'section' => 'menu_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'drink_menu_desc',
        array(
            'default' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper di gnissim cras sed Faucibus a pellente sque sit amet. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at.",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'drink_menu_desc',
        array(
            'label' => esc_html__( 'Drink menu description','barlounge' ),
            'description' => esc_html__( 'Drink menu description','barlounge' ),
            'section' => 'menu_settings',
        )
    ) );
    $wp_customize->add_setting( 'customizer_repeater_drink_menu', array(
        'sanitize_callback' => 'customizer_repeater_sanitize'
    ));
    $wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'customizer_repeater_drink_menu', array(
        'label'   => esc_html__('Menu drink','customizer-repeater'),
        'section' => 'menu_settings',
        'customizer_repeater_title_control' => true,
        'customizer_repeater_subtitle_control' => true,
        'customizer_repeater_price_control' => true,
        'customizer_repeater_repeater_control' => true
    ) ) );

    // food menu
    $wp_customize->add_setting( 'food_menu_title',
        array(
            'default' => esc_html__('Food Menu','barlounge'),
        )
    );
    $wp_customize->add_control('food_menu_title',
        array(
            'label' => esc_html__( 'Title Food Menu','barlounge' ),
            'section' => 'menu_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'food_menu_desc',
        array(
            'default' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus a pellentesque sit amet. Ullamcorper di gnissim cras sed Faucibus a pellente sque sit amet. Ullamcorper dignissim cras tincidunt lobortis feugiat vivamus at.",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'food_menu_desc',
        array(
            'label' => esc_html__( 'Food menu description','barlounge' ),
            'description' => esc_html__( 'Food menu description','barlounge' ),
            'section' => 'menu_settings',
        )
    ) );
    $wp_customize->add_setting( 'customizer_repeater_food_menu', array(
        'sanitize_callback' => 'customizer_repeater_sanitize'
    ));
    $wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'customizer_repeater_food_menu', array(
        'label'   => esc_html__('Menu food','customizer-repeater'),
        'section' => 'menu_settings',
        'customizer_repeater_title_control' => true,
        'customizer_repeater_subtitle_control' => true,
        'customizer_repeater_price_control' => true,
        'customizer_repeater_repeater_control' => true
    ) ) );
    $wp_customize->add_setting( 'menu_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'menu_show',
        array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'menu_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','barlounge' ),
                'yes' => esc_html__( 'Yes','barlounge' ),
            )
        )
    );
    // End Menu
	
	// start Blog
	$wp_customize->add_section( 'blog_settings',
	   array(
	      'title' => esc_html__( 'Blog Section','barlounge'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
	$wp_customize->add_setting( 'title_blog',
	   array(
	      'default' => esc_html__('Recent News and Events from Coctail - Lounge & Bar','barlounge'),
	   )
	);	
	$wp_customize->add_control('title_blog',
		array(
	      'label' => esc_html__( 'Title Section','barlounge' ),
	      'section' => 'blog_settings',
	       'type' => 'text',	
	   )
	);
	$wp_customize->add_setting( 'number_post_blog',
	   array(
	      'default' => esc_html__('3','barlounge'),
	   )
	);	
	$wp_customize->add_control('number_post_blog',
		array(
	      'label' => esc_html__( 'Number Post','barlounge' ),
	      'section' => 'blog_settings',
	       'type' => 'text',	
	   )
	);

    $wp_customize->add_setting( 'category_blog',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('category_blog',
        array(
            'label' => esc_html__( 'Category Post','barlounge' ),
            'section' => 'blog_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

	$wp_customize->add_setting( 'blog_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'blog_show',
	   array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'blog_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
            )
	   )
	);
	// End blog

    // start call to action
    $wp_customize->add_section( 'cta_settings',
        array(
            'title' => esc_html__( 'Call to action Section','barlounge'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    $wp_customize->add_setting( 'bg_img_cta',
        array(
            'default' => get_template_directory_uri() . '/images/banner-home-img-2.jpg',
        )
    );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_img_cta',
        array(
            'label' => esc_html__( 'Image Background','barlounge' ),
            'description' => esc_html__( 'Upload image','barlounge' ),
            'section' => 'cta_settings',
        )
    ) );
    // content
    $wp_customize->add_setting( 'cta_content',
        array(
            'default' => "Coctail - Bar and Lounge is one of the most happening places in the City.",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'cta_content',
        array(
            'label' => esc_html__( 'Content section','barlounge' ),
            'description' => esc_html__( 'Content section','barlounge' ),
            'section' => 'cta_settings',
        )
    ) );
    // button banner
    $wp_customize->add_setting( 'button_cta',
        array(
            'default' => esc_html__('Contact Us Now','barlounge'),
        )
    );
    $wp_customize->add_control('button_cta',
        array(
            'label' => esc_html__( 'Button Text','barlounge' ),
            'section' => 'cta_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_btn_cta',
        array(
            'default' => esc_html__('#','barlounge'),

        )
    );
    $wp_customize->add_control('link_btn_cta',
        array(
            'label' => esc_html__( 'Button URL','barlounge' ),
            'section' => 'cta_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'on_cta_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'on_cta_show',
        array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'cta_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','barlounge' ),
                'yes' => esc_html__( 'Yes','barlounge' ),
            )
        )
    );
    // End call to action

	// start Testimonials
	$wp_customize->add_section( 'testimonials_settings',
	   array(
	      'title' => esc_html__( 'Testimonials Section','barlounge'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

	// testimonials
	$wp_customize->add_setting( 'testimonials_title',
	   array(
	      'default' => 'What they say about our bar & lounge',
	   )
	);	
	$wp_customize->add_control( 'testimonials_title',
	   array(
	      'label' => esc_html__( 'Testimonials Title','barlounge' ),
	      'section' => 'testimonials_settings',
	      'type' => 'text',
	   )
	);
	$wp_customize->add_setting( 'testimonials_show',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'testimonials_show',
	   array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'testimonials_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
            )
	   )
	);
	// End Testimonials
	
	// start Contact
	$wp_customize->add_section( 'contact_settings',
	   array(
	      'title' => esc_html__( 'Contact Section','barlounge'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
    $wp_customize->add_setting( 'contact_form_title',
        array(
            'default' => 'Got questions? contact us for details',
        )
    );
    $wp_customize->add_control( 'contact_form_title',
        array(
            'label' => esc_html__( 'Contact Form Title','barlounge' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
	$wp_customize->add_setting( 'contact_shortcode',
	   array(
	      'default' => '[contact-form-7 id="49" title="Contact form home"]',
	   )
	);	
	$wp_customize->add_control( 'contact_shortcode',
	   array(
	      'label' => esc_html__( 'Contact Shortcode','barlounge' ),
	      'section' => 'contact_settings',
	      'type' => 'text',
	   )
	);
    $wp_customize->add_setting( 'contact_icon',
        array(
            'default' => get_template_directory_uri() . '/images/icon-contact.png',
        )
    );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'contact_icon',
        array(
            'label' => esc_html__( 'Icon','barlounge' ),
            'description' => esc_html__( 'Upload icon','barlounge' ),
            'section' => 'contact_settings',
        )
    ) );
    $wp_customize->add_setting( 'contact_title',
        array(
            'default' => 'COCTAIL',
        )
    );
    $wp_customize->add_control( 'contact_title',
        array(
            'label' => esc_html__( 'Contact Title','barlounge' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'contact_sub_title',
        array(
            'default' => 'Bar & Lounge',
        )
    );
    $wp_customize->add_control( 'contact_sub_title',
        array(
            'label' => esc_html__( 'Contact Sub Title','barlounge' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'contact_info',
        array(
            'default' => '123 Street, City, State 45678 <br> Phone 1234 56 7890 <br> info@coctailbarandlounge.com <br> coctailbarandlounge.com',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'contact_info',
        array(
            'label' => esc_html__( 'Contact Info','barlounge' ),
            'section' => 'contact_settings',
        )
    ) );
    // Newsletter
    $wp_customize->add_setting( 'newsletter_image',
        array(
            'default' => get_template_directory_uri() . '/images/image-home-9.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'newsletter_image',
        array(
            'label' => esc_html__( 'Background Image','barlounge' ),
            'description' => esc_html__( 'Upload image','barlounge' ),
            'section' => 'contact_settings',
        )
    ) );
    $wp_customize->add_setting( 'newsletter_title',
        array(
            'default' => 'Subscribe To Newsletter',
        )
    );
    $wp_customize->add_control( 'newsletter_title',
        array(
            'label' => esc_html__( 'Newsletter Title','barlounge' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'newsletter_sub_title',
        array(
            'default' => '* We promise we dont spam you!',
        )
    );
    $wp_customize->add_control( 'newsletter_sub_title',
        array(
            'label' => esc_html__( 'Newsletter Sub Title','barlounge' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'newsletter_shortcode',
        array(
            'default' => '[mc4wp_form id="76"]',
        )
    );
    $wp_customize->add_control( 'newsletter_shortcode',
        array(
            'label' => esc_html__( 'Newsletter shortcode','barlounge' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
	$wp_customize->add_setting( 'contact_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'contact_show',
	   array(
            'label' => esc_html__( 'Show Section','barlounge' ),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
            )
	   )
	);
	// End Contact
		
	// End Home Options
	
	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','barlounge'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '(C) 2019. AllRights Reserved. <strong>CocTail Bar & Lounge</strong>. Theme Developed by <a href="#">Template.net</a>',
			'sanitize_callback' => 'wp_kses_post',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','barlounge' ),
	      'section' => 'footer_settings',
	   )
	) );
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','barlounge'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','barlounge' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','barlounge'),
		'instagram'=> esc_html__('Instagram','barlounge'),
		'twitter'=> esc_html__('Twitter','barlounge'),
		'google'=> esc_html__('Google','barlounge'),
        'linkedin'=> esc_html__('Linkedin','barlounge'),
		'pinterest'=> esc_html__('Pinterest','barlounge')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','barlounge' ),
				 'yes' => esc_html__( 'Yes','barlounge' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','barlounge'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','barlounge'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','barlounge' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','barlounge' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','barlounge' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','barlounge' ),
	         'yes' => esc_html__( 'Yes','barlounge' ),
	      )
	   )
	);
}
/**
 * Theme options in the Customizer js
 * @package BarLounge
 */
 
function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'barlounge_customize_admin_menu_hide', 999 );
function barlounge_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="barlounge-theme-options"><span>'.esc_html__( 'Bar Lounge','barlounge' ).'</span>'.esc_html__( 'Theme Options','barlounge' ).'</div>',
         '<div class="barlounge-theme-options"><span>'.esc_html__( 'Bar Lounge','barlounge' ).'</span>'.esc_html__( 'Theme Options','barlounge' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}