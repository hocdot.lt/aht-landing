
</div> <!-- End main-->
<footer id="colophon" class="footer">
    <div class="wrapp-footer">
        <?php
        $copyright = get_theme_mod('copyright','(C) 2019. AllRights Reserved. <strong>CocTail Bar & Lounge</strong>. Theme Developed by <a href="#">Template.net</a>');
        ?>
        <div class="footer-bottom">
            <div class="container">
                <?php if (has_nav_menu('footer')) : ?>
                    <div class="menu-footer">
                        <?php
                        wp_nav_menu(array('theme_location' => 'footer'));
                        ?>
                    </div>
                <?php endif;?>

                <?php if ( $copyright !='') : ?>
                    <div class="copyright-footer text-center">
                        <?php echo wp_kses_post( $copyright); ?>
                    </div>
                <?php endif;?>
            </div>
        </div>

    </div> <!-- End footer -->
</footer> <!-- End colophon -->
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>