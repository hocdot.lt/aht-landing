<?php
    $barlounge_sidebar_right = barlounge_get_sidebar_right();
    ?>
<?php if ($barlounge_sidebar_right && $barlounge_sidebar_right != "none" && is_active_sidebar($barlounge_sidebar_right)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 right-sidebar active-sidebar"><!-- main sidebar -->
        <?php dynamic_sidebar($barlounge_sidebar_right); ?>
    </div>
<?php endif; ?>


