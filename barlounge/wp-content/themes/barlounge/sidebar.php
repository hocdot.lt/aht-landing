<?php
    $barlounge_sidebar_left = barlounge_get_sidebar_left();
    $barlounge_sidebar_right = barlounge_get_sidebar_right();

    ?>
<?php if ($barlounge_sidebar_left && is_active_sidebar($barlounge_sidebar_left)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left-sidebar "><!-- main sidebar -->
        <?php dynamic_sidebar($barlounge_sidebar_left); ?>
    </div>
<?php endif; ?>
<?php if ($barlounge_sidebar_right && is_active_sidebar($barlounge_sidebar_right)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 right-sidebar"><!-- main sidebar -->
        <?php dynamic_sidebar($barlounge_sidebar_right); ?>
    </div>
<?php endif; ?>





