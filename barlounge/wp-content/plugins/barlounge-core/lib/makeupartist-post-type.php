<?php

class BarLoungePostTypes {

    function __construct() {
        // Register post types
    //add_action('init', array($this, 'addGalleryPostType'));
    add_filter('manage_gallery_posts_columns', array($this, 'addGallery_columns'));  
    add_action('manage_gallery_posts_custom_column', array($this, 'addGallery_columns_content'), 10, 2);  
	add_action( 'cmb2_admin_init', array($this, 'resume_core_add_checkbox_field_metabox')); 
    add_action( 'cmb2_admin_init', array($this, 'resume_core_add_text_field_metabox')); 
    }            
   
    // Register Gallery post type
    function addGalleryPostType() {
        global $aki_settings;
        if(isset($aki_settings['gallery_slug'])){
            $gallery_slug = $aki_settings['gallery_slug'];
        }
        else {$gallery_slug = "gallery"; }
        if(isset($aki_settings['gallery_cat_slug'])){
            $gallery_cat_slug = $aki_settings['gallery_cat_slug'];
        }
        else {$gallery_cat_slug = "gallery_cat"; }                 
        register_post_type(
            'gallery', array(
            'labels' => $this->getLabels(esc_html__('Gallery', 'makeupartist'), esc_html__('Galleries', 'makeupartist')),
            'exclude_from_search' => false,
            'has_archive' => true,
            // 'publicly_queryable'  => false,
			'menu_icon' => 'dashicons-format-gallery',
            'public' => true,
            'rewrite' => array('slug' => $gallery_slug),
            'supports' => array('title', 'editor', 'thumbnail', 'comments', 'page-attributes'),
            'can_export' => true
            )
        );
        register_taxonomy(
            'gallery_cat', 'gallery', array(
            'hierarchical' => true,
            'show_in_nav_menus' => true,
            'labels' => $this->getTaxonomyLabels(esc_html__('Gallery Category', 'makeupartist'), esc_html__('Gallery Categories', 'makeupartist')),
            'query_var' => true,
            'rewrite' => array('slug' => $gallery_cat_slug),
            'show_admin_column' => true,
                )
        );
    }
   
	/**
     * Hook in and add a metabox to demonstrate repeatable checkbox fields
     */
    function resume_core_add_checkbox_field_metabox() {
        $prefix = 'resume_core_';
        /**
         * Repeatable Field Groups
         */
        $cmb_term = new_cmb2_box( array(
            'id'               => $prefix . 'edit',
            'title'            => __( 'Settings', 'makeupartist' ), 
            'object_types'     => array( 'term' ),
            'taxonomies'       => array( 'gallery_cat' ), // CHANGE THIS TO YOUR CUSTOM TAXONOMY
            'new_term_section' => true, // This is important as well
            'classes' => array( 'resume_checkbox')
        ) );

        $cmb_term->add_field( array(
            'name' => esc_html__( 'Active Category Filter', 'makeupartist' ),
            'desc' => esc_html__( 'Active category filter', 'makeupartist' ),
            'id'   => 'resume_core_checkbox',
            'type' => 'checkbox',
        ) );
    } 
    function resume_core_add_text_field_metabox() {
        $prefix = 'resume_core_';
        /**
         * Repeatable Field Groups
         */
        $cmb_term = new_cmb2_box( array(
            'id'               => $prefix . 'edit',
            'title'            => __( 'Settings', 'makeupartist' ), 
            'object_types'     => array( 'term' ),
            'taxonomies'       => array( 'gallery_cat'), // CHANGE THIS TO YOUR CUSTOM TAXONOMY
            'new_term_section' => true, // This is important as well
            'classes' => array( 'resume_checkbox')
        ) );
        $cmb_term->add_field( array(
            'name'    => esc_html__( 'Font Icon', 'makeupartist' ),
            'desc'    => esc_html__( 'Enter class font icon', 'makeupartist' ),
            'id'      => 'icon_text',
            'type'    => 'text'
        ) );
    } 
    function get_the_image( $post_id = false ) {
        
        $post_id    = (int) $post_id;
        $cache_key  = "featured_image_post_id-{$post_id}-_thumbnail";
        $cache      = wp_cache_get( $cache_key, null );
        
        if ( !is_array( $cache ) )
            $cache = array();
    
        if ( !array_key_exists( $cache_key, $cache ) ) {
            if ( empty( $cache) || !is_string( $cache ) ) {
                $output = '';
                    
                if ( has_post_thumbnail( $post_id ) ) {
                    $image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), array( 36, 32 ) );
                    
                    if ( is_array( $image_array ) && is_string( $image_array[0] ) )
                        $output = $image_array[0];
                }
                
                if ( empty( $output ) ) {
                    // $output = plugins_url( 'images/default.png', __FILE__ );
                    // $output = apply_filters( 'featured_image_column_default_image', $output );
                }
                
                $output = esc_url( $output );
                $cache[$cache_key] = $output;
                
                wp_cache_set( $cache_key, $cache, null, 60 * 60 * 24 /* 24 hours */ );
            }
        }
        return isset( $cache[$cache_key] ) ? $cache[$cache_key] : $output;
    } 
    function addGallery_columns($defaults) {  

        if ( !is_array( $defaults ) )
            $defaults = array();       
        $new = array();      
        foreach( $defaults as $key => $title ) {
            if ( $key == 'title' ) 
                $new['featured_image'] = 'Image';
            
            $new[$key] = $title;
        }
        
        return $new;         
    }        
    // SHOW THE FEATURED IMAGE
    function addGallery_columns_content($column_name, $post_id) {
        if ( 'featured_image' != $column_name )
                    return;         
                
                $image_src = self::get_the_image( $post_id );
                            
                if ( empty( $image_src ) ) {
                    echo "&nbsp;"; // This helps prevent issues with empty cells
                    return;
                }
                
                echo '<img alt="' . esc_attr( get_the_title() ) . '" src="' . esc_url( $image_src ) . '" />';
    }     
    // Get content type labels
    function getLabels($singular_name, $name, $title = FALSE) {
        if (!$title)
            $title = $name;

        return array(
            "name" => $title,
            "singular_name" => $singular_name,
            "add_new" => esc_html__("Add New", 'makeupartist'),
            "add_new_item" => sprintf(esc_html__("Add New %s", 'makeupartist'), $singular_name),
            "edit_item" => sprintf(esc_html__("Edit %s", 'makeupartist'), $singular_name),
            "new_item" => sprintf(esc_html__("New %s", 'makeupartist'), $singular_name),
            "view_item" => sprintf(esc_html__("View %s", 'makeupartist'), $singular_name),
            "search_items" => sprintf(esc_html__("Search %s", 'makeupartist'), $name),
            "not_found" => sprintf(esc_html__("No %s found", 'makeupartist'), $name),
            "not_found_in_trash" => sprintf(esc_html__("No %s found in Trash", 'makeupartist'), $name),
            "parent_item_colon" => ""
        );
    }

    // Get content type taxonomy labels
    function getTaxonomyLabels($singular_name, $name) {
        return array(
            "name" => $name,
            "singular_name" => $singular_name,
            "search_items" => sprintf(esc_html__("Search %s", 'makeupartist'), $name),
            "all_items" => sprintf(esc_html__("All %s", 'makeupartist'), $name),
            "parent_item" => sprintf(esc_html__("Parent %s", 'makeupartist'), $singular_name),
            "parent_item_colon" => sprintf(esc_html__("Parent %s:", 'makeupartist'), $singular_name),
            "edit_item" => sprintf(esc_html__("Edit %", 'makeupartist'), $singular_name),
            "update_item" => sprintf(esc_html__("Update %s", 'makeupartist'), $singular_name),
            "add_new_item" => sprintf(esc_html__("Add New %s", 'makeupartist'), $singular_name),
            "new_item_name" => sprintf(esc_html__("New %s Name", 'makeupartist'), $singular_name),
            "menu_name" => $name,
        );
    }

}

new BarLoungePostTypes();

/**
 * Register "Testimonials" post type
 * @return [type] [description]
 */
function register_testimonial_post_type() {

	$labels = array(
	    'name' => __('Testimonials', 'barlounge-core'),
	    'singular_name' => __('Testimonials', 'barlounge-core'),
	    'add_new' => __('Add New', 'barlounge-core'),
	    'add_new_item' => __('Add New Testimonial', 'barlounge-core'),
	    'edit_item' => __('Edit Testimonial', 'barlounge-core'),
	    'new_item' => __('New Testimonial', 'barlounge-core'),
	    'all_items' => __('All Testimonial', 'barlounge-core'),
	    'view_item' => __('View Testimonial', 'barlounge-core'),
	    'search_items' => __('Search Testimonials', 'barlounge-core'),
	    'not_found' =>  __('No Testimonials found', 'barlounge-core'),
	    'not_found_in_trash' => __('No Testimonials found in Trash', 'barlounge-core'),
	    'parent_item_colon' => '',
	    'menu_name' => __('Testimonials', 'barlounge-core')
	  );

  	$args = array(
	    'labels' => $labels,
	    'public' => false,
	    'exclude_from_search' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'show_in_menu' => true, 
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'testimonial' ),
	    'capability_type' => 'post',
	    'has_archive' => false, 
	    'hierarchical' => false,
	    'menu_position' => 50,
	    'menu_icon' => 'dashicons-groups',
	    'supports' => array( 'title','editor', 'thumbnail', 'revisions','page-attributes')
  	); 

	register_post_type( 'testimonial', $args);

}
add_action( 'init', 'register_testimonial_post_type');

/**
 * Customize Rug table overview tables
 */

function add_new_testimonial_table_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['image'] = __('Image', 'barlounge-core');
    $new_columns['title'] = _x('Name', 'column name', 'barlounge-core');
    $new_columns['date'] = _x('Date', 'column name', 'barlounge-core');
 
    return $new_columns;
}
// Add to admin_init function
add_filter('manage_testimonial_posts_columns', 'add_new_testimonial_table_columns');
function manage_testimonial_table_columns($column_name, $id) {
    global $wpdb;
	$featured_img_url = get_the_post_thumbnail_url($id, 'thumbnail');
    switch ($column_name) {
	    case 'image':
			if(!empty($featured_img_url)){
				echo '<img src="'. $featured_img_url .'" width="80" >';
			}else{
				echo 'No image';
			}
	        break;
	    default:
	        break;
    } // end switch
}   
// Add to admin_init function
add_action('manage_testimonial_posts_custom_column', 'manage_testimonial_table_columns', 10,2);

/**
 * Register meta box testimonial
 */
function testimonial_meta_box(){
	add_meta_box( 'meta-box-testimonial', 'Testimonial info', 'testimonial_meta_display', 'testimonial', 'side', 'low' );
}
add_action( 'add_meta_boxes', 'testimonial_meta_box' );
/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
 
function testimonial_meta_display(){
    global $post ;
    $testimonial_name = get_post_meta( $post->ID, 'testimonial_name', true );
    $testimonial_position = get_post_meta( $post->ID, 'testimonial_position', true );
	wp_nonce_field( 'save_testimonial', 'testimonial_nonce' );
	?>
    <div class="testimonial-info-wrapper">
        <div class="single-field-wrap">
            <h4><span class="section-title"><?php _e( 'Position', 'barlounge-core' );?></span></h4>
            <span class="section-inputfield"><input style="width:100%;" type="text" name="testimonial_position" value="<?php if( !empty( $testimonial_position ) ){ echo $testimonial_position ; }?>" /></span>
        </div>
    </div>
<?php
}
/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function testimonial_save_meta_box( $post_id ) {

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['testimonial_nonce'] ) || !wp_verify_nonce( $_POST['testimonial_nonce'], 'save_testimonial' ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    if( isset( $_POST['testimonial_position'] ) ){
        update_post_meta( $post_id, 'testimonial_position', wp_kses( $_POST['testimonial_position'] ) );
	}else{
		delete_post_meta( $post_id, 'testimonial_position' );
	}
}
add_action( 'save_post', 'testimonial_save_meta_box' );

function create_shortcode_testimonial($atts) {
	
		extract( shortcode_atts( array (
		  "id"    => ''
		), $atts ) );
		
        $the_query = new WP_Query(array(
			'post_type' => 'testimonial',
			'posts_per_page' => 1,
			'p' => $id,
			'post_status' => 'publish',
			'orderby'	=> 'date',
			'order'	=> 'DESC',
        ));
 
        ob_start();
        if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php $testimonial_fields = get_post_meta( get_the_ID(), 'testimonial_fields', true); if ( $testimonial_fields ) : ?>
					<div class="list-testimonial">
						<?php foreach ( $testimonial_fields as $field ) { ?>
							<div class="item-testimonial">
								<div class="wrap-item">
									<?php if($field['content'] != ''){ ?>
										<div class="content"><?php echo wpautop( $field['content'] ); ?></div>
									<?php } ?>
									<?php if($field['name'] != ''){ ?>
										<p class="name"><?php echo esc_attr( $field['name'] ); ?></p>
									<?php } ?>
									<?php if($field['address'] != ''){ ?>
										<p class="name"><?php echo esc_attr( $field['address'] ); ?></p>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				<?php endif; ?>				
			<?php
			endwhile;
        endif;
		wp_reset_postdata();
        $list_post = ob_get_contents();
 
        ob_end_clean();
 
        return $list_post;
}
add_shortcode('testimonial', 'create_shortcode_testimonial');

/**
 * Register "Features" post type
 * @return [type] [description]
 */
function register_feature_post_type() {

	$labels = array(
	    'name' => __('Features', 'barlounge-core'),
	    'singular_name' => __('Features', 'barlounge-core'),
	    'add_new' => __('Add New', 'barlounge-core'),
	    'add_new_item' => __('Add New Feature', 'barlounge-core'),
	    'edit_item' => __('Edit Feature', 'barlounge-core'),
	    'new_item' => __('New Feature', 'barlounge-core'),
	    'all_items' => __('All Features', 'barlounge-core'),
	    'view_item' => __('View Features', 'barlounge-core'),
	    'search_items' => __('Search Features', 'barlounge-core'),
	    'not_found' =>  __('No Features found', 'barlounge-core'),
	    'not_found_in_trash' => __('No Features found in Trash', 'barlounge-core'),
	    'parent_item_colon' => '',
	    'menu_name' => __('Features', 'barlounge-core')
	  );

  	$args = array(
	    'labels' => $labels,
	    'public' => false,
	    'exclude_from_search' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'show_in_menu' => true, 
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'feature' ),
	    'capability_type' => 'post',
	    'has_archive' => false, 
	    'hierarchical' => false,
	    'menu_position' => 50,
	    'menu_icon' => 'dashicons-format-gallery',
	    'supports' => array( 'title')
  	); 

	register_post_type( 'feature', $args);

}
//add_action( 'init', 'register_feature_post_type');

/**
 * Customize feature overview tables ("feature" -> "All features")
 * Add / modify columns at feature edit overview
 * @param  [type] $gallery_columns [description]
 * @return [type]                  [description]
 */
function add_new_feature_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['title'] = _x('Feature Name', 'column name', 'barlounge-core');
    $new_columns['shortcode'] = __('Shortcode', 'barlounge-core');
    $new_columns['date'] = _x('Date', 'column name', 'barlounge-core');
 
    return $new_columns;
}
// Add to admin_init function
add_filter('manage_feature_posts_columns', 'add_new_feature_columns');
function manage_feature_columns($column_name, $id) {
    global $wpdb;

    switch ($column_name) {
	    case 'shortcode':
	        echo '<input type="text" style="width: 300px;" readonly="readonly" onclick="this.select()" value="[feature id=&quot;'. $id . '&quot;]"/>';
	            break;
	 
	    default:
	        break;
    } // end switch
}   
// Add to admin_init function
add_action('manage_feature_posts_custom_column', 'manage_feature_columns', 10,2);

add_action('admin_init', 'feature_add_meta_boxes', 1);
function feature_add_meta_boxes() {
	add_meta_box( 'feature-fields', 'Features', 'feature_meta_box_display', 'feature', 'normal', 'high');
}
function feature_meta_box_display() {
	global $post;
	$feature_fields = get_post_meta($post->ID, 'feature_fields', true);
	wp_nonce_field( 'feature_meta_box_nonce', 'feature_meta_box_nonce' );
	?>
	<script type="text/javascript">
	jQuery(document).ready(function( $ ){
		$( '#add-row' ).on('click', function() {			
			var row = $( '.empty-row.screen-reader-text' ).clone(true);
			row.removeClass( 'empty-row screen-reader-text' );
			row.insertBefore( '#repeatable-fieldset-one .list-item:last-child' );
			$(row).find(".select2-container").remove();
			var select2 = $(row).find(".select-icon");
			select2.select2({
				width: "100%",
				formatResult: format,
				formatSelection: format,
				escapeMarkup: function(m) { return m; }
			});				 
			return false;
		});
  	
		$( '.remove-row' ).on('click', function() {
			$(this).parents('.list-item').remove();
			return false;
		});
		function format(icon) {
			var originalOption = icon.element;
			return '<i class="fa ' + $(originalOption).val() + '"></i>  ' + icon.text;
		}

		$('.select-icon').select2({
			width: "100%",
			formatResult: format,
			formatSelection: format,
			escapeMarkup: function(m) { return m; }
		});		
	});
	</script>
  
	<div id="repeatable-fieldset-one" width="100%">

		<?php
		
		if ( $feature_fields ) :
		
		foreach ( $feature_fields as $field ) {
		?>
		<div class="list-item">
			<p><label><?php echo _e('Title','barlounge-core'); ?></label><input type="text" class="widefat" name="titleFeature[]" value="<?php if ($field['titleFeature'] != '') echo esc_attr( $field['titleFeature'] ); ?>" /></p>
			<p><label><?php echo _e('Content','barlounge-core'); ?></label><textarea class="widefat" name="content[]"  rows="4"><?php if($field['content'] != '') echo esc_attr( $field['content'] ); ?></textarea></p>
			<p><label><?php echo _e('URL','barlounge-core'); ?></label><input type="text" class="widefat" name="url[]" value="<?php if ($field['url'] != '') echo esc_attr( $field['url'] ); ?>" /></p>
		
			<p><a class="button remove-row" href="#"><?php echo _e('Remove','barlounge-core'); ?></a></p>
		</div>
		<?php
		}
		else :
		// show a blank one
		?>
		<div class="list-item">
			<p><label><?php echo _e('Title','barlounge-core'); ?></label><input type="text" class="widefat" name="titleFeature[]" /></p>
			<p><label><?php echo _e('Content','barlounge-core'); ?></label><textarea class="widefat" name="content[]"  rows="4"></textarea></p>
			<p><label><?php echo _e('URL','barlounge-core'); ?></label><input type="text" class="widefat" name="url[]" value="" /></p>
		
			<p><a class="button remove-row" href="#"><?php echo _e('Remove','barlounge-core'); ?></a></p>
		</div>
		<?php endif; ?>
		
		<!-- empty hidden one for jQuery -->
		<div class="list-item empty-row screen-reader-text">
			<p><label><?php echo _e('Title','barlounge-core'); ?></label><input type="text" class="widefat" name="titleFeature[]" /></p>
			<p><label><?php echo _e('Content','barlounge-core'); ?></label><textarea class="widefat" name="content[]"  rows="4"></textarea></p>
			<p><label><?php echo _e('URL','barlounge-core'); ?></label><input type="text" class="widefat" name="url[]" value="" /></p>
		
			<p><a class="button remove-row" href="#"><?php echo _e('Remove','barlounge-core'); ?></a></p>
		</div>
	</div>
	<div class="clear"></div>
	
	<p><a id="add-row" class="button" href="#"><?php echo _e('New Feature','barlounge-core'); ?></a></p>
	<?php
}
add_action('save_post', 'feature_meta_box_save');
function feature_meta_box_save($post_id) {
	if ( ! isset( $_POST['feature_meta_box_nonce'] ) ||
	! wp_verify_nonce( $_POST['feature_meta_box_nonce'], 'feature_meta_box_nonce' ) )
		return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	
	if (!current_user_can('edit_post', $post_id))
		return;
	
	$old = get_post_meta($post_id, 'feature_fields', true);
	$new = array();

	$titleFeature = $_POST['titleFeature'];
	$content = $_POST['content'];
	$url = $_POST['url'];
	
	$count = count( $titleFeature );
	
	for ( $i = 0; $i < $count; $i++ ) {
		if ( $titleFeature[$i] != '' ) :
			$new[$i]['titleFeature'] = $titleFeature[$i];
			$new[$i]['content'] = stripslashes( strip_tags( $content[$i] ) );
			$new[$i]['url'] = stripslashes( strip_tags( $url[$i] ) );
		endif;
	}
	if ( !empty( $new ) && $new != $old )
		update_post_meta( $post_id, 'feature_fields', $new );
	elseif ( empty($new) && $old )
		delete_post_meta( $post_id, 'feature_fields', $old );
}

function create_shortcode_feature($atts) {
	
		extract( shortcode_atts( array (
		  "id"    => ''
		), $atts ) );
		
        $the_query = new WP_Query(array(
			'post_type' => 'feature',
			'posts_per_page' => 1,
			'p' => $id,
			'post_status' => 'publish',
			'orderby'	=> 'date',
			'order'	=> 'DESC',
        ));
 
        ob_start();
        if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php 
				$feature_fields = get_post_meta( get_the_ID(), 'feature_fields', true); 
				$layout_feature = get_post_meta( get_the_ID(), 'custom_layout_feature_meta_box', true);
				if ( $feature_fields ) : ?>
					<div class="list-feature">
						<div class="row">
							<?php foreach ( $feature_fields as $field ) { ?>
								<div class="item-feature col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="wrapp">
										<?php if($field['titleFeature'] != '' && $field['url'] != ''){ ?>
											<h3><a href="<?php echo esc_url($field['url']); ?>"><?php echo $field['titleFeature']; ?></a></h3>
										<?php }elseif($field['titleFeature'] != ''){ ?>
											<h3><?php echo $field['titleFeature']; ?></h3>
										<?php } ?>
										<?php if($field['content'] != ''){ ?>
											<div class="content"><?php echo wpautop( $field['content'] ); ?></div>
										<?php } ?>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				<?php endif; ?>				
			<?php
			endwhile;
        endif;
		wp_reset_postdata();
        $list_post = ob_get_contents();
 
        ob_end_clean();
 
        return $list_post;
}
add_shortcode('feature', 'create_shortcode_feature');