<?php
/*
  Plugin Name: BarLounge Core
  Plugin URI:
  Description: Core for BarLounge Theme.
  Version: 1.0.0
  Author: BarLounge
  Author URI:
  Text-domain: barlounge-core
 */

// don't load directly
if (!defined('ABSPATH'))
    die('-1');

require_once dirname(__FILE__) . '/social-widget/functions.php'; 
define('MAKEUPARTI_SHORTCODES_URL', plugin_dir_url(__FILE__));
define('MAKEUPARTI_SHORTCODES_LIB', dirname(__FILE__) . '/lib/');
class BarLoungeShortcodesClass {

	function __construct() {

        // Load text domain
        add_action('plugins_loaded', array($this, 'loadTextDomain'));
        // Init plugins
        add_action('init', array($this, 'initPlugin'));

        $this->addShortcodes();

    }

    // Init plugins
    function initPlugin() {
        $this->addTinyMCEButtons();
    }

    // load plugin text domain
    function loadTextDomain() {
        load_plugin_textdomain('barlounge-core', false, dirname(__FILE__) . '/languages/');
    }

    // Add buttons to tinyMCE
    function addTinyMCEButtons() {
        if (!current_user_can('edit_posts') && !current_user_can('edit_pages'))
            return;

        if (get_user_option('rich_editing') == 'true') {
            add_filter('mce_buttons', array(&$this, 'registerTinyMCEButtons'));
        }
    }

    function registerTinyMCEButtons($buttons) {
        array_push($buttons, "barlounge_shortcodes_button");
        return $buttons;
    }

    // Add shortcodes
    function addShortcodes() {
        require_once(MAKEUPARTI_SHORTCODES_LIB . 'functions.php');
        require_once(MAKEUPARTI_SHORTCODES_LIB . 'makeupartist-post-type.php');
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }

}

// Finally initialize code
new BarLoungeShortcodesClass();
