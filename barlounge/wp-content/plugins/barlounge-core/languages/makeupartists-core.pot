#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: BarLounge Core\n"
"POT-Creation-Date: 2018-10-27 11:52+0700\n"
"PO-Revision-Date: 2018-10-27 11:52+0700\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.9\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: makeupartist-shortcodes.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: lib/makeupartist-post-type.php:27
msgid "Gallery"
msgstr ""

#: lib/makeupartist-post-type.php:27
msgid "Galleries"
msgstr ""

#: lib/makeupartist-post-type.php:42
msgid "Gallery Category"
msgstr ""

#: lib/makeupartist-post-type.php:42
msgid "Gallery Categories"
msgstr ""

#: lib/makeupartist-post-type.php:60 lib/makeupartist-post-type.php:81
msgid "Settings"
msgstr ""

#: lib/makeupartist-post-type.php:68
msgid "Active Category Filter"
msgstr ""

#: lib/makeupartist-post-type.php:69
msgid "Active category filter"
msgstr ""

#: lib/makeupartist-post-type.php:88
msgid "Font Icon"
msgstr ""

#: lib/makeupartist-post-type.php:89
msgid "Enter class font icon"
msgstr ""

#: lib/makeupartist-post-type.php:163 lib/makeupartist-post-type.php:205
#: lib/makeupartist-post-type.php:381
msgid "Add New"
msgstr ""

#: lib/makeupartist-post-type.php:164 lib/makeupartist-post-type.php:186
#, php-format
msgid "Add New %s"
msgstr ""

#: lib/makeupartist-post-type.php:165
#, php-format
msgid "Edit %s"
msgstr ""

#: lib/makeupartist-post-type.php:166
#, php-format
msgid "New %s"
msgstr ""

#: lib/makeupartist-post-type.php:167
#, php-format
msgid "View %s"
msgstr ""

#: lib/makeupartist-post-type.php:168 lib/makeupartist-post-type.php:180
#, php-format
msgid "Search %s"
msgstr ""

#: lib/makeupartist-post-type.php:169
#, php-format
msgid "No %s found"
msgstr ""

#: lib/makeupartist-post-type.php:170
#, php-format
msgid "No %s found in Trash"
msgstr ""

#: lib/makeupartist-post-type.php:181
#, php-format
msgid "All %s"
msgstr ""

#: lib/makeupartist-post-type.php:182
#, php-format
msgid "Parent %s"
msgstr ""

#: lib/makeupartist-post-type.php:183
#, php-format
msgid "Parent %s:"
msgstr ""

#: lib/makeupartist-post-type.php:184
msgid "Edit %"
msgstr ""

#: lib/makeupartist-post-type.php:185
#, php-format
msgid "Update %s"
msgstr ""

#: lib/makeupartist-post-type.php:187
#, php-format
msgid "New %s Name"
msgstr ""

#: lib/makeupartist-post-type.php:203 lib/makeupartist-post-type.php:204
#: lib/makeupartist-post-type.php:215
msgid "Testimonials"
msgstr ""

#: lib/makeupartist-post-type.php:206
msgid "Add New Testimonial"
msgstr ""

#: lib/makeupartist-post-type.php:207
msgid "Edit Testimonial"
msgstr ""

#: lib/makeupartist-post-type.php:208
msgid "New Testimonial"
msgstr ""

#: lib/makeupartist-post-type.php:209
msgid "All Testimonial"
msgstr ""

#: lib/makeupartist-post-type.php:210
msgid "View Testimonial"
msgstr ""

#: lib/makeupartist-post-type.php:211
msgid "Search Testimonials"
msgstr ""

#: lib/makeupartist-post-type.php:212
msgid "No Testimonials found"
msgstr ""

#: lib/makeupartist-post-type.php:213
msgid "No Testimonials found in Trash"
msgstr ""

#: lib/makeupartist-post-type.php:246
msgid "Image"
msgstr ""

#: lib/makeupartist-post-type.php:247
msgctxt "column name"
msgid "Name"
msgstr ""

#: lib/makeupartist-post-type.php:248 lib/makeupartist-post-type.php:426
msgctxt "column name"
msgid "Date"
msgstr ""

#: lib/makeupartist-post-type.php:293
msgid "Position"
msgstr ""

#: lib/makeupartist-post-type.php:379 lib/makeupartist-post-type.php:380
#: lib/makeupartist-post-type.php:391
msgid "Features"
msgstr ""

#: lib/makeupartist-post-type.php:382
msgid "Add New Feature"
msgstr ""

#: lib/makeupartist-post-type.php:383
msgid "Edit Feature"
msgstr ""

#: lib/makeupartist-post-type.php:384 lib/makeupartist-post-type.php:531
msgid "New Feature"
msgstr ""

#: lib/makeupartist-post-type.php:385
msgid "All Features"
msgstr ""

#: lib/makeupartist-post-type.php:386
msgid "View Features"
msgstr ""

#: lib/makeupartist-post-type.php:387
msgid "Search Features"
msgstr ""

#: lib/makeupartist-post-type.php:388
msgid "No Features found"
msgstr ""

#: lib/makeupartist-post-type.php:389
msgid "No Features found in Trash"
msgstr ""

#: lib/makeupartist-post-type.php:424
msgctxt "column name"
msgid "Feature Name"
msgstr ""

#: lib/makeupartist-post-type.php:425
msgid "Shortcode"
msgstr ""

#: lib/makeupartist-post-type.php:500 lib/makeupartist-post-type.php:512
#: lib/makeupartist-post-type.php:522
msgid "Title"
msgstr ""

#: lib/makeupartist-post-type.php:501 lib/makeupartist-post-type.php:513
#: lib/makeupartist-post-type.php:523
msgid "Content"
msgstr ""

#: lib/makeupartist-post-type.php:502 lib/makeupartist-post-type.php:514
#: lib/makeupartist-post-type.php:524
msgid "URL"
msgstr ""

#: lib/makeupartist-post-type.php:504 lib/makeupartist-post-type.php:516
#: lib/makeupartist-post-type.php:526
msgid "Remove"
msgstr ""

#: lib/widgets/makeupartist_about.php:12
msgid "Show About Info"
msgstr ""

#: lib/widgets/makeupartist_about.php:90
msgid "About Me"
msgstr ""

#: lib/widgets/makeupartist_about.php:93
msgid "Read More About Myself"
msgstr ""

#: lib/widgets/makeupartist_about.php:104
msgid "Widget Title"
msgstr ""

#: lib/widgets/makeupartist_about.php:108
msgid "Link url image"
msgstr ""

#: lib/widgets/makeupartist_about.php:112
msgid "Descprtion"
msgstr ""

#: lib/widgets/makeupartist_about.php:116
msgid "Title link"
msgstr ""

#: lib/widgets/makeupartist_about.php:120
msgid "Link url"
msgstr ""

#: lib/widgets/makeupartist_about.php:124
msgid "Facebook"
msgstr ""

#: lib/widgets/makeupartist_about.php:128
msgid "Instagram"
msgstr ""

#: lib/widgets/makeupartist_about.php:132
msgid "Twitter"
msgstr ""

#: lib/widgets/makeupartist_about.php:136
msgid "Pinterest"
msgstr ""

#: lib/widgets/makeupartist_about.php:140
msgid "Google Plus"
msgstr ""

#: lib/widgets/makeupartist_recent_comments.php:28
msgid "Your site&#8217;s most recent comments."
msgstr ""

#: lib/widgets/makeupartist_recent_comments.php:31
#: lib/widgets/makeupartist_recent_comments.php:77
msgid "Recent Comments"
msgstr ""

#. translators: comments widget: 1: comment author, 2: post link
#: lib/widgets/makeupartist_recent_comments.php:115
#, php-format
msgctxt "widgets"
msgid "%1$s %2$s"
msgstr ""

#: lib/widgets/makeupartist_recent_comments.php:158
#: lib/widgets/makeupartist_recent_posts.php:134
#: social-widget/functions.php:166
msgid "Title:"
msgstr ""

#: lib/widgets/makeupartist_recent_comments.php:161
msgid "Number of comments to show:"
msgstr ""

#: lib/widgets/makeupartist_recent_posts.php:11
msgid "Your site&#8217;s most recent Posts."
msgstr ""

#: lib/widgets/makeupartist_recent_posts.php:12
msgid "Recent Posts"
msgstr ""

#: lib/widgets/makeupartist_recent_posts.php:87
msgid "By"
msgstr ""

#: lib/widgets/makeupartist_recent_posts.php:137
msgid "Number of posts to show:"
msgstr ""

#: lib/widgets/makeupartist_recent_posts.php:141
msgid "Display post date?"
msgstr ""

#: social-widget/facebook/barlounge-likebox-facebook.php:9
msgid "BarLounge Facebook Like Box"
msgstr ""

#: social-widget/facebook/barlounge-likebox-facebook.php:10
msgid "This Widget show the Facebook"
msgstr ""

#: social-widget/functions.php:21 social-widget/functions.php:22
msgid "Resume Instagram Feed"
msgstr ""

#: social-widget/functions.php:66
msgid "Something wrong, please check the connection or the api config!"
msgstr ""

#: social-widget/functions.php:143
msgid "Access token is not valid."
msgstr ""

#: social-widget/functions.php:170
msgid "Number of photos to display:"
msgstr ""

#: social-widget/functions.php:174
msgid "Hashtag:"
msgstr ""

#: social-widget/functions.php:231
msgid "Follow Us"
msgstr ""

#: social-widget/functions.php:242
msgid "Instagram Plugin error: Plugin not fully configured"
msgstr ""

#: social-widget/twitter/widget.php:11
msgid "BarLounge Latest Tweet"
msgstr ""

#: social-widget/twitter/widget.php:12
msgid "This Widget show the Latest Tweets"
msgstr ""

#: social-widget/twitter/widget.php:52
msgid "Latest Twitter"
msgstr ""

#: social-widget/twitter/widget.php:53
msgid "Follow us on"
msgstr ""

#: social-widget/twitter/widget.php:54
msgid "#"
msgstr ""

#: social-widget/twitter/widget.php:57
msgid "Title Widget:"
msgstr ""

#: social-widget/twitter/widget.php:61
msgid "Title Twitter:"
msgstr ""

#: social-widget/twitter/widget.php:65
msgid "Link Twitter:"
msgstr ""

#: social-widget/twitter/widget.php:72
msgid "Number of tweets:"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "BarLounge Core"
msgstr ""

#. Description of the plugin/theme
msgid "Core for BarLounge Theme."
msgstr ""

#. Author of the plugin/theme
msgid "BarLounge"
msgstr ""
