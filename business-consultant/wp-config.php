<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'business-consultant' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'M_cE_x<s8FJI04c^Q8KH!+`Msi$(ICrP{>1gE KUEU!qbk*6Ap7}9MD- +)up/]1' );
define( 'SECURE_AUTH_KEY',  'U@[:pzw*$2j>_Fe?$RK,:]xqzh*.$#K.75U;XG94^[`HK0-uL&Ow}gLoz}nK2aO!' );
define( 'LOGGED_IN_KEY',    'R5QwPdG.nf:m3>.1j*^T/UeU%#R;^le`~xLJiKv@>TwoB2DD.N5mWEB)!E]UimDU' );
define( 'NONCE_KEY',        'MsD@KODZ~zs.33&rYtE,<s%n!9Sl-D:Wm>KXkt_`|M:6-=L~WDocCr@2Palt=.xZ' );
define( 'AUTH_SALT',        'H<xTR; -E1-h)~pOP]x@tD(aiT`R5E,^WFZmWn;42:jZJ,Bk{<POk/r0HRlEl)<9' );
define( 'SECURE_AUTH_SALT', '@<EC6FB@/v?}*Yu~RY#hvnKL}#zGYXcC6CR_Obq6IIz|}=c+*@CDV]j+$<zF.k47' );
define( 'LOGGED_IN_SALT',   'm_r6;8UiDZ2C~YKWPtF-XwK?ylS^)_#2F5^M1,vSnith=x50{,/TXi!%HDQd7<~(' );
define( 'NONCE_SALT',       '(AJ/A>mT;9T;qGV(#4Mc*uhJFA]P[8e{}kjO]^d7c9{b)ak{My(HE.|UGhv-dCZ4' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
