<?php 
/**
Template Name: Home
*/
get_header();

get_template_part('part-templates/home-banner');
get_template_part('part-templates/home-section1');
get_template_part('part-templates/home-section2');
get_template_part('part-templates/home-section3');
get_template_part('part-templates/home-section4');
get_template_part('part-templates/home-section5');


get_footer(); ?> 