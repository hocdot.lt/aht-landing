<?php
$clientShow = get_theme_mod('client_show', 'yes');
$subtitle = get_theme_mod('client_sub_title','Client List');
$title = get_theme_mod('client_title','We have a vast list of clients some are below');
$ClientCategory = get_theme_mod('client_category','');
$ClientLimit = get_theme_mod('client_number_post',5);
$args = array(
    'posts_per_page'   => $BlogLimit,
    'offset'           => 0,
    'cat'         => $BlogCategory,

    'post_type'        => 'Gallery',
);
$posts_array = get_posts( $args );
$args2 = array(
    'posts_per_page'   => 2,
    'offset'           => 0,
    'order' => 'ASC',
    'post_status' => 'publish',
    'post_type'        => 'testimonial',
);
$posts_array = get_posts( $args2 );


?>

<?php if($clientShow === 'yes'): ?>
<div id="client">
    <div class="container">
        <div class="row">
            <div class="col-xl-3">
                <div class="content-top-ss4">
                    <div class="title">
                        <div class="title-top-ss4">
                            <h3><?php echo $subtitle?></h3>
                        </div>
                        <div class="title-bottom-ss4">
                            <h2><?php echo $title?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-between-ss4">
            <div class="gallery gallery-responsive portfolio_slider">
                <?php
                // the query
    $the_query = new WP_Query( $args ); ?>
    <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col"><img src="<?php echo the_post_thumbnail_url(); ?>"></div>
        <?php endwhile; ?>
    <?php endif;?>
            </div>
        </div>
        <div class="content-bottom-ss4">
            <div class="row">
    <?php
    // the query
    $the_query = new WP_Query( $args2 ); ?>
    <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-sm-4 col-lg-4 col-xl-4">
                    <div class="content-feeback">
                        <div class="text-box">
                            <?php echo the_excerpt()?>
                        </div>
                        <div class="avatar">
                            <div class="img-avart"><img src="<?php echo the_post_thumbnail_url(); ?>"></div>
                            <div class="name">
                                <h3><?php the_title(); ?></h3>
                                <h5><?php echo get_post_meta(get_the_ID(),'testimonial_position',true);?></h5>
                            </div>
                        </div>
                    </div>
                </div>
        <?php endwhile; ?>
    <?php endif;?>

            </div>
        </div>
    </div>
</div>
<?php endif ?>