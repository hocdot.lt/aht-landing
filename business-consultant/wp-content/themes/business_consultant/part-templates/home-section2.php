<?php
$servicesShow = get_theme_mod('services_show', 'yes');
$subtitle = get_theme_mod('service_sub_title','Our Customer Services');
$title = get_theme_mod('service_title','Deliver consumer like experience to your workforce');
$desc = get_theme_mod('description','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ante in Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ante in nibh mauris elit, sed do eiusmod tempor incididunt et dolore magnamattis molestie a.');
$bgLeft = get_theme_mod('background_left',get_template_directory_uri() . '/images/bg-cham1.png');
$bgRight = get_theme_mod('background_right',get_template_directory_uri() . '/images/bg-cham.png');
$serviceCategory = get_theme_mod('Service_category','');
$serviceLimit = get_theme_mod('service_number_post',6);
$args_servies = array(
    'posts_per_page'   => $serviceLimit,
    'offset'           => 0,
    'cat'         => $serviceCategory,
    'order' => 'ASC',
    'post_type'        => 'post',
);
$posts_array = get_posts( $args_servies );
?>
<?php if($servicesShow === 'yes'): ?>

<div id="services">
    <div class="content-top-ss2" style="background-image: url(<?php echo $bgLeft ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-lg-3 col-xl-3">
                    <div class="title-top">
                        <h3><?php echo $subtitle?></h3>
                    </div>
                    <div class="title-bottom">
                        <h2><?php echo $title?></h2>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-lg-5 col-xl-5">
                    <div class="text-desc">
                        <p><?php echo $desc?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-bottom-ss2" style="background-image: url(<?php echo $bgRight ?>);">
        <div class="container">
            <div class="row">
    <?php
    // the query
    $the_query = new WP_Query( $args_servies ); ?>
    <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-lg-post">
                    <div class="item-post">
                        <div class="title">
                            <h4><?php the_category(); ?> </h4>
                            <h3><?php the_title(); ?></h3>
                        </div>
                        <div class="desc">
                            <?php echo the_excerpt()?>
                        </div>
                        <div class="link"><a href="<?php echo the_permalink()?>"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-next.png"/></a></div>
                    </div>
                </div>
        <?php endwhile; ?>
    <?php endif;?>

            </div>
        </div>
    </div>
</div>
<?php endif?>