<?php
$contactShow = get_theme_mod('contact_show', 'yes');
$subtitle = get_theme_mod('contact_sub_title','Consult Our Business Executice');
$title = get_theme_mod('contact_title','Get a Free Quotation for your next Business Development');
$shortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="5" title="Contact"]');
$map = get_theme_mod('contact_map','<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14900.820008282744!2d105.78794855!3d20.984417999999998!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1svi!2s!4v1552961000328" width="600" height="650" frameborder="0" style="border:0" allowfullscreen=""></iframe>');
?>
<?php if($contactShow === 'yes'):?>
<div id="contact">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-4">
                <div class="row justify-content-end">
                    <div class="col-md-8 col-xl-6">
                        <div class="content-top-ss5">
                            <div class="title">
                                <div class="title-top-ss5">
                                    <h3><?php echo $subtitle?></h3>
                                </div>
                                <div class="title-bottom-ss5">
                                    <h2><?php echo $title?></h2>
                                </div>
                            </div>
                        </div>
                        <div class="content-bottom-left-ss5">
                            <?php echo do_shortcode($shortcode)?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 npd">
                <div class="content-right-ss5">
                    <div class="map">
                        <?php echo $map?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>