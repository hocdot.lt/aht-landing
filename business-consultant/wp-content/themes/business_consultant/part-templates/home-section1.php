<?php
$aboutShow = get_theme_mod('banner_show', 'yes');
$titlebottom = get_theme_mod('desc_about','Looking for Experienced Business Consultant for your Online Business? We will help you!');
$buttom = get_theme_mod('button_about','Get Started');
$link = get_theme_mod('link_about','#');
$AboutCategory = get_theme_mod('About_category','');
$AboutLimit = get_theme_mod('about_number_post',3);
$args = array(
    'posts_per_page'   => $AboutLimit,
    'offset'           => 0,
    'cat'         => $AboutCategory,
    'order' => 'ASC',
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );
$i = 1;
?>
<?php if($aboutShow === 'yes'): ?>

<div id="about_us">
    <div class="container">
        <div class="row">
    <?php
    // the query
    $the_query = new WP_Query( $args ); ?>
    <?php if ( $the_query->have_posts() ) : ?>
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <div class="col-lg-post  item">
                <div class="info">
                    <h3><?php echo get_the_title($_post->ID)?></h3>
                    <p><?php echo get_the_excerpt($_post->ID)?></p>
                </div>
                <div class="btn-bottom"><a href="<?php echo get_post_permalink($_post->ID)?>">Learn More</a></div>
            </div>
        <?php endwhile; ?>
    <?php endif;?>
        </div>
    </div>
    <div class="ct-bottom-ss1">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="content-bottom-ss1">
                        <div class="text">
                            <h3><?php echo $titlebottom?></h3>
                        </div>
                        <div class="btn-right"><a href="<?php echo $link?>"><?php echo $buttom?></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif?>