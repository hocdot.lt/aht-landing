<?php
$bannerShow = get_theme_mod('banner_show', 'yes');
$title = get_theme_mod('banner_title','<h1>We ProvideBest SolutionsFor Your Online Business</h1>');
$desc = get_theme_mod('banner_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$buttom = get_theme_mod('button_banner','Get Started');
$link = get_theme_mod('link_banner','#');
$bg_right = get_theme_mod('background_banner', get_template_directory_uri() . '/images/img-banner.png');

?>
<?php if($bannerShow === 'yes'): ?>
<div id="bannerSession">
    <div class="content-banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                        <div class="row justify-content-end  ct-left">
                            <div class="col-md-8 col-lg-8 col-xl-6">
                                <div class="content-left-banner">
                                <div class="inner">
                                    <?php echo $title?>
                                    <h5><?php echo $desc?></h5><a href="<?php echo $link?>"><?php echo $buttom?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="content-right-banner">
                        <div class="img-right-banner"><img src="<?php echo $bg_right?>"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif?>