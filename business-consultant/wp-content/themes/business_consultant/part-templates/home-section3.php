<?php
$blogShow = get_theme_mod('blog_show', 'yes');
$subtitle = get_theme_mod('blog_sub_title','Read Latest Blog Posts');
$title = get_theme_mod('blog_title','Read Interesting Articles about Business Consulting');
$BlogCategory = get_theme_mod('blog_category','');
$BlogLimit = get_theme_mod('blog_number_post',3);
$args = array(
    'posts_per_page'   => $BlogLimit,
    'offset'           => 0,
    'cat'         => $BlogCategory,
    'order' => 'ASC',
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );
?>
<?php if($blogShow === 'yes'): ?>

<div id="blog">
    <div class="container">
        <div class="row">
            <div class="col-xl-3">
                <div class="content-top-ss3">
                    <div class="title">
                        <div class="title-top-ss3">
                            <h3><?php echo $subtitle?></h3>
                        </div>
                        <div class="title-bottom-ss3">
                            <h2><?php echo $title?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-bottom-ss3">
            <div class="row">
    <?php
    // the query
    $the_query = new WP_Query( $args ); ?>
    <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-lg-post">
                    <div class="img-box"><a href="<?php echo the_permalink()?>"><img src="<?php echo the_post_thumbnail_url()?>"></a></div>
                    <div class="text-box">
                        <div class="title-box">
                            <h3><a class="linktitle" href="<?php echo the_permalink()?>"><?php the_title(); ?></a></h3>
                        </div>
                        <div class="desc-box">
                          <?php echo the_excerpt()?>
                        </div>
                        <div class="link-box"><a href="<?php echo the_permalink()?>">Read More</a></div>
                    </div>
                </div>
        <?php endwhile; ?>
    <?php endif;?>

            </div>
        </div>
    </div>
</div>
<?php endif?>