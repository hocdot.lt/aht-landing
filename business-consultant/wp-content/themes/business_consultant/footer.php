
</div> <!-- End main-->
<footer class="footer">
    <div class=".footer-wrapper">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1">
                        <div class="logo">
                            <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                <?php
                                $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                if ($logo_header && $logo_header!=''):
                                    echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                else:
                                    //bloginfo('name');
                                endif;
                                ?>
                        </div>
                    </div>
                    <div class="col-lg-4">
					<?php
					$infofooter = get_theme_mod('desc_footer','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vitae semper quis lectus nulla at. Ipsum a arcu cursus vitae. penatibus et magnis dis parturient montes nascetur. Lorem ipsum dolor sit amet elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p>Please call 1234 56 7890</p>
                            <p>123 Street, City, State 45678</p>
                            <a href="#">www.companyname.com</a>
                            <a href="#">info@companyname.com</a>')
					?>
                        <div class="desc-footer">
                             <?php echo $infofooter?>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="menu_footer">
                            <div class="title-menu">
                                <h3>Site Navigation</h3>
                            </div>
                            <?php
                            if (has_nav_menu('footer')) {
                                wp_nav_menu(array(
                                        'theme_location' => 'footer',
                                        'menu_class' => 'mega-menu',
                                        'items_wrap' =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    )
                                );
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="menu_footer menu_footer_icon">
                            <div class="title-menu">
                                <h3>Site Navigation</h3>
                            </div>
                            <?php
                            if(function_exists('business_consultant_social_link')){
                                business_consultant_social_link();
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="copyright">
                            <?php
                            $copyright =  get_theme_mod('copyright','<h6 class="text-center m-0">(C) All Rights Reserved. Charity Theme, Designed & Developed by<a href="#">Template.net</a></h6>');;
                            if ( $copyright !='') : ?>
                                <?php echo wp_kses_post( $copyright); ?>
                            <?php endif;?>
                                    <!-- - load thư viện js-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>