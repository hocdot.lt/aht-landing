<?php
    $business_consultant_sidebar_left = business_consultant_get_sidebar_left();
    $business_consultant_sidebar_right = business_consultant_get_sidebar_right();

    ?>
<?php if ($business_consultant_sidebar_left && is_active_sidebar($business_consultant_sidebar_left)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left-sidebar "><!-- main sidebar -->
        <?php dynamic_sidebar($business_consultant_sidebar_left); ?>
    </div>
<?php endif; ?>
<?php if ($business_consultant_sidebar_right && is_active_sidebar($business_consultant_sidebar_right)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 right-sidebar"><!-- main sidebar -->
        <?php dynamic_sidebar($business_consultant_sidebar_right); ?>
    </div>
<?php endif; ?>





