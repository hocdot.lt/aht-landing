<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aht_landing_insurance_agency');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'zyjuQkOK');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Wd2unG!FR0:|Nl+<9<:Zg.4#-w([1i/5<}4p_y[J0-J((OorlQ-}<SIDQEu9}+!/');
define('SECURE_AUTH_KEY',  'oS!ex%XSK-{Cq&N^>[bQ?X%X(I4_46d1b@T9&Jt */yQH<l<P//O|4OQ=5-HoOOB');
define('LOGGED_IN_KEY',    'EeYqZ#C1~)F+WBxAPmi8VG#H+a3*&cn(yp%k{Jtf_d;tYa9Bv/6GeBGM0?LWq9&F');
define('NONCE_KEY',        '?0(-[ZGu7v0.5(.Qd/S$@XN>khnt$`Len:V:n_FlU@EG{{1KS$i$;aN/RE^!r~%F');
define('AUTH_SALT',        'ysw9,<A%/tr6bC}y%I1F$mQW^rjgjNs8=@S26r@`Ht]^&RVI,Lg!+<D~^W!XcgpA');
define('SECURE_AUTH_SALT', '-xPO8$-(.-ldd/VGyW,(r1FVcD)er9Yu,Z=jWT+5hao:pd%Wk*h@Y<t@-R<VHMpC');
define('LOGGED_IN_SALT',   '`cfns3K*T&S|9*~iV4Wu*Ee06B>buz=[=x@rB6-?8P||P!1lkGID`x}=`Jta,,hq');
define('NONCE_SALT',       'oJWTqaaxXb,9~(u=UqaDP}Lr&(/2 5G90)~9l<p>X5COev51lk:jFQ*mA/u:@2^C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
