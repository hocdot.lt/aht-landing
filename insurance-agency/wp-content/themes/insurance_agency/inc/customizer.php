<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'insurance_agency_customize_register' );
function insurance_agency_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'insurance_agency_Customize_Sidebar_Control' ) ) {
	    class insurance_agency_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',			
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',			
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',
                'Rozha One' => 'Rozha One',
                'Arial,Helvetica Neue,Helvetica,sans-serif:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'insurance_agency') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','insurance_agency'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','insurance_agency' ),
	      'description' => esc_html__( 'Upload Logo','insurance_agency' ),
	      'section' => 'general_settings',
	   )
	) );

    $wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Main Google Font',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'insurance_agency'),
        'description'    => __('Several settings pertaining my theme', 'insurance_agency'),
    ) );
	
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','insurance_agency'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
    $wp_customize->add_setting( 'slider_type',
        array(
            'default' => 'default',
        )
    );
    $wp_customize->add_control( 'slider_type',
        array(
            'label' => esc_html__( 'Slider','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array(
                'default' => esc_html__( 'Default','insurance_agency' ),
                'rs' => esc_html__( 'Revolution Slider','insurance_agency' ),
            )
        )
    );
    $wp_customize->add_setting( 'revolution_slider_shortcode',
        array(
            'default' => '[rev_slider alias="home-slider"]',
        )
    );
    $wp_customize->add_control( 'revolution_slider_shortcode',
        array(
            'label' => esc_html__( 'Revolution Slider Shortcode','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // start slider 1
    $wp_customize->add_setting( 'img_slider_1',
        array(
            'default' => get_template_directory_uri() . '/images/banner-top.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_slider_1',
        array(
            'label' => esc_html__( 'Image 1','insurance_agency' ),
            'description' => esc_html__( 'Upload Image','insurance_agency' ),
            'section' => 'home_banner_settings',
        )
    ) );
    $wp_customize->add_setting( 'content_slider_1',
        array(
            'default' => "<p>Wide range of Insurance policies to protect and secure yourself and your families from uncertainties of financial loss</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'content_slider_1',
        array(
            'label' => esc_html__( 'Content Slider 1','insurance_agency' ),
            'description' => esc_html__( 'Content Slider 1','insurance_agency' ),
            'section' => 'home_banner_settings',
        )
    ));

    $wp_customize->add_setting( 'button_slider_1',
        array(
            'default' => esc_html__('Compare insurance','insurance_agency'),
        )
    );
    $wp_customize->add_control('button_slider_1',
        array(
            'label' => esc_html__( 'Button Slider 1','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_slider_1',
        array(
            'default' => esc_html__('#','insurance_agency'),
        )
    );
    $wp_customize->add_control('link_slider_1',
        array(
            'label' => esc_html__( 'Button URL Slider 1','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );

    // End slider 1
    // start slider 2
    $wp_customize->add_setting( 'img_slider_2',
        array(
            'default' => get_template_directory_uri() . '/images/banner-top.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_slider_2',
        array(
            'label' => esc_html__( 'Image 2','insurance_agency' ),
            'description' => esc_html__( 'Upload Image','insurance_agency' ),
            'section' => 'home_banner_settings',
        )
    ) );

    $wp_customize->add_setting( 'content_slider_2',
        array(
            'default' => "<p>Wide range of Insurance policies to protect and secure yourself and your families from uncertainties of financial loss</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'content_slider_2',
        array(
            'label' => esc_html__( 'Content Slider 2','insurance_agency' ),
            'description' => esc_html__( 'Content Slider 2','insurance_agency' ),
            'section' => 'home_banner_settings',
        )
    ));

    $wp_customize->add_setting( 'button_slider_2',
        array(
            'default' => esc_html__('Compare insurance','insurance_agency'),

        )
    );
    $wp_customize->add_control('button_slider_2',
        array(
            'label' => esc_html__( 'Button Slider 2','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_slider_2',
        array(
            'default' => esc_html__('#','insurance_agency'),
        )
    );
    $wp_customize->add_control('link_slider_2',
        array(
            'label' => esc_html__( 'Button URL Slider 2','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // End slider 2
    // start slider 3
    $wp_customize->add_setting( 'img_slider_3',
        array(
            'default' => get_template_directory_uri() . '/images/banner-top.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_slider_3',
        array(
            'label' => esc_html__( 'Image 3','insurance_agency' ),
            'description' => esc_html__( 'Upload Image','insurance_agency' ),
            'section' => 'home_banner_settings',
        )
    ) );

    $wp_customize->add_setting( 'content_slider_3',
        array(
            'default' => "<p>Wide range of Insurance policies to protect and secure yourself and your families from uncertainties of financial loss</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'content_slider_3',
        array(
            'label' => esc_html__( 'Content Slider 3','insurance_agency' ),
            'description' => esc_html__( 'Content Slider 3','insurance_agency' ),
            'section' => 'home_banner_settings',
        )
    ));

    $wp_customize->add_setting( 'button_slider_3',
        array(
            'default' => esc_html__('Compare insurance','insurance_agency'),

        )
    );
    $wp_customize->add_control('button_slider_3',
        array(
            'label' => esc_html__( 'Button Slider 3','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_slider_3',
        array(
            'default' => esc_html__('#','insurance_agency'),
        )
    );
    $wp_customize->add_control('link_slider_3',
        array(
            'label' => esc_html__( 'Button URL Slider 3','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // End slider 3

	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','insurance_agency' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','insurance_agency' ),
	         'yes' => esc_html__( 'Yes','insurance_agency' ),
            )
	   )
	);
	// End banner section

    // start get_free_quote
    $wp_customize->add_section( 'get_free_quote_settings',
        array(
            'title' => esc_html__( 'Services Section','insurance_agency'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    $wp_customize->add_setting( 'title_get_free_quote',
        array(
            'default' => esc_html__('Get a Free Insurance Quote!','insurance_agency'),
        )
    );
    $wp_customize->add_control('title_get_free_quote',
        array(
            'label' => esc_html__( 'Title Section','insurance_agency' ),
            'section' => 'get_free_quote_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_get_free_quote',
        array(
            'default' => esc_html__('We care much about environment and your health, take pride in being a Bio Car Wash.','insurance_agency'),
        )
    );
    $wp_customize->add_control('sub_title_get_free_quote',
        array(
            'label' => esc_html__( 'Sub Title Section','insurance_agency' ),
            'section' => 'get_free_quote_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'get_free_quote_content',
        array(
            'default' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'get_free_quote_content',
        array(
            'label' => esc_html__( 'Content Section','insurance_agency' ),
            'description' => esc_html__( 'Content Section','insurance_agency' ),
            'section' => 'get_free_quote_settings',
        )
    ));
    $wp_customize->add_setting( 'contact_shortcode',
        array(
            'default' => '[contact-form-7 id="49" title="Contact form home"]',
        )
    );
    $wp_customize->add_control( 'contact_shortcode',
        array(
            'label' => esc_html__( 'Contact Shortcode','insurance_agency' ),
            'section' => 'get_free_quote_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'on_get_free_quote_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'on_get_free_quote_show',
        array(
            'label' => esc_html__( 'Show Section','insurance_agency' ),
            'section' => 'get_free_quote_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','insurance_agency' ),
                'yes' => esc_html__( 'Yes','insurance_agency' ),
            )
        )
    );
    // End get_free_quote

	// Section about
	
	$wp_customize->add_section( 'about_settings',
	   array(
	      'title' => esc_html__( 'About section','insurance_agency'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

	
	$wp_customize->add_setting( 'title_about',
	   array(
	      'default' => esc_html__('"We secure your family\'s future"','insurance_agency'),

	   )
	);	
	$wp_customize->add_control('title_about',
		array(
	      'label' => esc_html__( 'Title About','insurance_agency' ),
	      'section' => 'about_settings',
	       'type' => 'text',	
	   )
	);
	
	$wp_customize->add_setting( 'sub_title_about',
	   array(
	      'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore','insurance_agency'),
	   )
	);	
	$wp_customize->add_control('sub_title_about',
		array(
	      'label' => esc_html__( 'Sub Title About','insurance_agency' ),
	      'section' => 'about_settings',
	       'type' => 'textarea',
	   )
	);

    $wp_customize->add_setting( 'about_image',
        array(
            'default' => get_template_directory_uri() . '/images/about-us-bg.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_image',
        array(
            'label' => esc_html__( 'Image About','insurance_agency' ),
            'description' => esc_html__( 'Upload Image','insurance_agency' ),
            'section' => 'about_settings',
        )
    ) );

    $wp_customize->add_setting( 'about_content',
        array(
            'default' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'about_content',
        array(
            'label' => esc_html__( 'Content Section','insurance_agency' ),
            'description' => esc_html__( 'Content Section','insurance_agency' ),
            'section' => 'about_settings',
        )
    ));

    $wp_customize->add_setting( 'button_about',
        array(
            'default' => esc_html__('Read about us','insurance_agency'),

        )
    );
    $wp_customize->add_control('button_about',
        array(
            'label' => esc_html__( 'Button Text','insurance_agency' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_about',
        array(
            'default' => esc_html__('#','insurance_agency'),
        )
    );
    $wp_customize->add_control('link_about',
        array(
            'label' => esc_html__( 'Button URL','insurance_agency' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );
	
	$wp_customize->add_setting( 'about_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'about_show',
	   array(
            'label' => esc_html__( 'Show Section','insurance_agency' ),
            'section' => 'about_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','insurance_agency' ),
	         'yes' => esc_html__( 'Yes','insurance_agency' ),
            )
	   )
	);
	// End about

    // start Blog
    $wp_customize->add_section( 'blog_settings',
        array(
            'title' => esc_html__( 'Blog Section','insurance_agency'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    $wp_customize->add_setting( 'title_blog',
        array(
            'default' => esc_html__('Read Important Articles About Insurance','insurance_agency'),
        )
    );
    $wp_customize->add_control('title_blog',
        array(
            'label' => esc_html__( 'Title Section','insurance_agency' ),
            'section' => 'blog_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'sub_title_blog',
        array(
            'default' => esc_html__('Not sure what insurance fits best ?'),
        )
    );
    $wp_customize->add_control('sub_title_blog',
        array(
            'label' => esc_html__( 'Sub Title Section','insurance_agency' ),
            'section' => 'blog_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'number_post_blog',
        array(
            'default' => esc_html__('6','insurance_agency'),
        )
    );
    $wp_customize->add_control('number_post_blog',
        array(
            'label' => esc_html__( 'Number Post','insurance_agency' ),
            'section' => 'blog_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'blog_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'blog_show',
        array(
            'label' => esc_html__( 'Show Section','insurance_agency' ),
            'section' => 'blog_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','insurance_agency' ),
                'yes' => esc_html__( 'Yes','insurance_agency' ),
            )
        )
    );
    // End work

    // start call to action
    $wp_customize->add_section( 'cta_settings',
        array(
            'title' => esc_html__( 'Call To Action Section','insurance_agency'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    $wp_customize->add_setting( 'img_cta',
        array(
            'default' => get_template_directory_uri() . '/images/get-secured-bg.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_cta',
        array(
            'label' => esc_html__( 'Background image','insurance_agency' ),
            'description' => esc_html__( 'Upload image','insurance_agency' ),
            'section' => 'cta_settings',
        )
    ) );

    $wp_customize->add_setting( 'cta_content',
        array(
            'default' => "<p>get your family secured</p><p>Secure health & medical expenses with top Medical insurance Policies</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'cta_content',
        array(
            'label' => esc_html__( 'Content Section','insurance_agency' ),
            'description' => esc_html__( 'Content Section','insurance_agency' ),
            'section' => 'cta_settings',
        )
    ));

    $wp_customize->add_setting( 'button_cta',
        array(
            'default' => esc_html__('Get started now','insurance_agency'),

        )
    );
    $wp_customize->add_control('button_cta',
        array(
            'label' => esc_html__( 'Button Text','insurance_agency' ),
            'section' => 'cta_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_cta',
        array(
            'default' => esc_html__('#','insurance_agency'),
        )
    );
    $wp_customize->add_control('link_cta',
        array(
            'label' => esc_html__( 'Button URL','insurance_agency' ),
            'section' => 'cta_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'cta_content_2',
        array(
            'default' => '<p class="text-uppercase">or call : 800 1234 5678</p>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'cta_content_2',
        array(
            'label' => esc_html__( 'Content Section 2','insurance_agency' ),
            'description' => esc_html__( 'Content Section 2','insurance_agency' ),
            'section' => 'cta_settings',
        )
    ));

    $wp_customize->add_setting( 'cta_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'cta_show',
        array(
            'label' => esc_html__( 'Show Section','insurance_agency' ),
            'section' => 'cta_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','insurance_agency' ),
                'yes' => esc_html__( 'Yes','insurance_agency' ),
            )
        )
    );
    // end call to action

    // start Testimonials
    $wp_customize->add_section( 'testimonials_settings',
        array(
            'title' => esc_html__( 'Testimonials Section','insurance_agency'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    $wp_customize->add_setting( 'testimonials_title',
        array(
            'default' => 'Read words from our customers',
        )
    );
    $wp_customize->add_control( 'testimonials_title',
        array(
            'label' => esc_html__( 'Testimonials Title','insurance_agency' ),
            'section' => 'testimonials_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'testimonials_sub_title',
        array(
            'default' => 'Not sure what insurance fits best ?',
        )
    );
    $wp_customize->add_control( 'testimonials_sub_title',
        array(
            'label' => esc_html__( 'Sub Title','insurance_agency' ),
            'section' => 'testimonials_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting( 'number_post_testimonials',
        array(
            'default' => esc_html__('6','insurance_agency'),
        )
    );
    $wp_customize->add_control('number_post_testimonials',
        array(
            'label' => esc_html__( 'Number Post','insurance_agency' ),
            'section' => 'testimonials_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'testimonials_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'testimonials_show',
        array(
            'label' => esc_html__( 'Show Section','insurance_agency' ),
            'section' => 'testimonials_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','insurance_agency' ),
                'yes' => esc_html__( 'Yes','insurance_agency' ),
            )
        )
    );
    // End Testimonials
		
	// End Home Options
	
	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','insurance_agency'  ),
	      'priority' => 35, 
	   )
	);
    $wp_customize->add_setting( 'logo_footer',
        array(
            'default' => get_template_directory_uri() . '/images/logo-footer.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_footer',
        array(
            'label' => esc_html__( 'Logo footer','insurance_agency' ),
            'description' => esc_html__( 'Upload Logo','insurance_agency' ),
            'section' => 'footer_settings',
        )
    ) );
    $wp_customize->add_setting( 'footer_info',
        array(
            'default' => '<p class="text-capitalize font-weight-bold">insurance services limited</p><p class="text-capitalize">Yoga teacher training center</p><p class="text-capitalize">123 street, city, state 45678</p>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'footer_info',
        array(
            'label' => esc_html__( 'Copyright text','insurance_agency' ),
            'section' => 'footer_settings',
        )
    ) );
	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '<p class="text-capitalize">(C) 2019 all rights reserved, FitnessCenterBigData</p><p>Designed & Developed by <a href="#">Template.net</a></p>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','insurance_agency' ),
	      'section' => 'footer_settings',
	   )
	) );	
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','insurance_agency'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','insurance_agency' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','insurance_agency' ),
	         'yes' => esc_html__( 'Yes','insurance_agency' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','insurance_agency'),
		'instagram'=> esc_html__('Instagram','insurance_agency'),
		'twitter'=> esc_html__('Twitter','insurance_agency'),
		'google'=> esc_html__('Google','insurance_agency'),
        'pinterest'=> esc_html__('Pinterest','insurance_agency'),
		'linkedin'=> esc_html__('Linkedin','insurance_agency')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','insurance_agency' ),
				 'yes' => esc_html__( 'Yes','insurance_agency' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','insurance_agency'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','insurance_agency'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','insurance_agency' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','insurance_agency' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','insurance_agency' ),
	         'yes' => esc_html__( 'Yes','insurance_agency' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','insurance_agency' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','insurance_agency' ),
	         'yes' => esc_html__( 'Yes','insurance_agency' ),
	      )
	   )
	);
}
/**
 * Theme options in the Customizer js
 * @package insurance_agency
 */
 
function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'insurance_agency_customize_admin_menu_hide', 999 );
function insurance_agency_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="insurance_agency-theme-options"><span>'.esc_html__( 'insurance_agency','insurance_agency' ).'</span>'.esc_html__( 'Theme Options','insurance_agency' ).'</div>',
         '<div class="insurance_agency-theme-options"><span>'.esc_html__( 'insurance_agency','insurance_agency' ).'</span>'.esc_html__( 'Theme Options','insurance_agency' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){
                $input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );
			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}