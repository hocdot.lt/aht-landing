
</div> <!-- End main-->
<?php
    $logo_footer = get_theme_mod('logo_footer',get_template_directory_uri() . '/images/logo-footer.png');
    $footer_info = get_theme_mod('footer_info','<p class="text-capitalize font-weight-bold">insurance services limited</p><p class="text-capitalize">Yoga teacher training center</p><p class="text-capitalize">123 street, city, state 45678</p>');
    $copyright = get_theme_mod('copyright','<p class="text-capitalize">(C) 2019 all rights reserved, FitnessCenterBigData</p><p>Designed & Developed by <a href="#">Template.net</a></p>');
?>
<footer class="mt-0" id="footer">
    <div class="container">
        <div class="row py-sm-5 pt-4">
            <div class="col-8 col-md-4 col-xl-2">
                <div class="logo-footer mb-15">
                    <a href="#" rel="home">
                        <?php
                        if ($logo_foooter && $logo_foooter!=''):
                            echo '<img src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_footer)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                        else:
                            echo '<img src="' . get_template_directory_uri() . '/images/logo-footer.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                        endif;
                        ?>
                    </a>
                </div>
            </div>
            <div class="col-8 col-md-4 col-xl-2">
                <div class="wrap-address mb-15">
                    <?php if ( $footer_info !='') : ?>
                        <?php echo wp_kses_post( $footer_info); ?>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-8 col-md-4 col-xl-3">
                <div class="organization-details mb-15">
                    <?php if (has_nav_menu('footer')) : ?>
                        <?php wp_nav_menu(array('theme_location' => 'footer','menu_class' => 'menu-footer',)); ?>
                    <?php endif; ?>
                    <?php if ( $copyright !='') : ?>
                        <?php echo wp_kses_post( $copyright); ?>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-8 col-md-4 col-xl-1">
                <div class="social-list-footer mb-15">
                    <?php
                    if(function_exists('insurance_agency_social_link')){
                        insurance_agency_social_link();
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>