	<div class="top-blog">
		<?php if(get_the_title() != ''):?>
			<h1><?php echo get_the_title(); ?></h1>  
		<?php endif;?> 
		<div class="meta">
			<span class="date"><?php echo get_the_date('F j Y'); ?></span>
			<span class="social-share-post">
			<?php
				if(function_exists('insurance_agency_social_share')){
					echo '<lable class="lable">'. wp_kses('Share :','insurance_agency') .'</lable>';
					insurance_agency_social_share();
				}
			?>	
			</span>
			<span class="comment"><?php echo '&lpar;'.get_comments_number().'&rpar;'; _e(' Comments','insurance_agency');?></span>
		</div>				
	</div>
	<div class="content-blog">	
		<?php the_content();?>		
	</div>
	