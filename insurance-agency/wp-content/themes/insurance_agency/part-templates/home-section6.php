<?php
/**
 * Render testimonials in home page
 */

//Get config
$testimonials_show =  get_theme_mod('testimonials_show','yes');
$number_post_testimonials = get_theme_mod('number_post_testimonials','6');
$testimonials_title = get_theme_mod('testimonials_title','read important articles about insurance');
$testimonials_sub_title = get_theme_mod('testimonials_sub_title','Not sure what insurance fits best ?');
?>
<?php if($testimonials_show === 'yes'): ?>
    <section id="important-quote">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <div class="wrap-article-slide-title">
                        <?php  if($testimonials_sub_title !=''){ ?>
                            <p><?php echo $testimonials_sub_title; ?></p>
                        <?php } ?>
                        <?php  if($testimonials_title !=''){ ?>
                            <p class="text-capitalize"><?php echo $testimonials_title; ?></p>
                        <?php } ?>
                    </div>
                </div>
                <?php
                $the_query = new WP_Query(array(
                    'post_type' => 'testimonial',
                    'posts_per_page' => $number_post_testimonials,
                    'post_status' => 'publish',
                    'orderby'	=> 'date',
                    'order'	=> 'DESC',
                ));
                if ( $the_query->have_posts() ) : ?>
                <div class="owl-carousel owl-theme wrap-slide" data-plugin-options="{'responsive': { '479': {'items': 1}, '768': {'items': 2},'1200': {'items': 3}}, 'center' : true,'loop': true, 'autoHeight': true, 'margin': 10}">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="content text-center"><span>“</span>
                        <p><?php echo wp_trim_words( get_the_excerpt(), $num_words = 35, '' ) ?></p>
                        <p class="font-weight-bold text-capitalize"><?php the_title(); ?></p>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php
                endif;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </section>
<?php endif; ?>