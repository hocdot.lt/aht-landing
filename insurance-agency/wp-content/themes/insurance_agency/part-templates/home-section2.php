<?php
/**
 * Render get_free_quote in home page
 */

//Get config
$on_get_free_quote_show =  get_theme_mod('on_get_free_quote_show','yes');
$title_get_free_quote = get_theme_mod('title_get_free_quote','Get a Free Insurance Quote!');
$sub_title_get_free_quote = get_theme_mod('sub_title_get_free_quote','Welcome to Insurance Services');
$get_free_quote_content = get_theme_mod('get_free_quote_content','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>');
$contact_shortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="49" title="Contact form home"]');
?>
<?php if($on_get_free_quote_show === 'yes'): ?>
    <section id="get-free-quote">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="wrap-get-free-quote">
                        <div class="gft-title">
                            <?php if(!empty($sub_title_get_free_quote)): ?>
                                <p><?php echo $sub_title_get_free_quote; ?></p>
                            <?php endif; ?>
                            <?php if(!empty($title_get_free_quote)): ?>
                                <p><?php echo $title_get_free_quote ?></p>
                            <?php endif; ?>
                        </div>
                        <div class="gft-content">
                            <?php
                            if(!empty($get_free_quote_content)):
                                echo apply_filters( 'the_content', $get_free_quote_content);
                            endif;
                            ?>
                            <?php
                                if (has_nav_menu('insurance')) :
                                    wp_nav_menu(array('theme_location' => 'insurance','menu_class' => 'feature-content',));
                                endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="wrap-get-free-quote-form">
                        <div class="gft-form">
                            <?php if($contact_shortcode != ''): ?>
                                <?php echo do_shortcode($contact_shortcode); ?>
                            <?php endif; ?>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>