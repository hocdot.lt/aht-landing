<?php
/**
 * Render cta in home page
 */

//Get config
$cta_show =  get_theme_mod('cta_show','yes');
$img_cta = get_theme_mod('img_cta',get_template_directory_uri() . '/images/get-secured-bg.jpg');
$cta_content = get_theme_mod('cta_content','<p>get your family secured</p><p>Secure health & medical expenses with top Medical insurance Policies</p>');
$cta_content_2 = get_theme_mod('cta_content_2','<p class="text-uppercase">or call : 800 1234 5678</p>');
$button_cta = get_theme_mod('button_cta','Get started now');
$link_cta = get_theme_mod('link_cta','#');
?>
<?php if($cta_show === 'yes'): ?>
    <section id="get-phone-number" <?php if(!empty($img_cta)): ?> style="background-image:url(<?php echo $img_cta; ?>);" <?php endif; ?>>
        <div class="container-fluid">
            <div class="row">
                <div class="col-8">
                    <div class="wrap-banner-content text-center">
                        <?php if(!empty($cta_content)): ?>
                            <?php echo apply_filters( 'the_content', $cta_content); ?>
                        <?php endif; ?>
                        <div class="separator"></div>
                        <?php if(!empty($button_cta) || !empty($link_cta)){ ?>
                            <div class="banner-btn">
                                <a class="text-uppercase" href="<?php echo $link_cta; ?>"><?php echo $button_cta; ?></a>
                            </div>
                        <?php } ?>
                        <?php if(!empty($cta_content_2)): ?>
                            <?php echo apply_filters( 'the_content', $cta_content_2); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>