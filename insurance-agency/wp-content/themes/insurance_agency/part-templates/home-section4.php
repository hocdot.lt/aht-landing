<?php
/**
 * Render blog in home page
 */

//Get config
$blog_show =  get_theme_mod('blog_show','yes');
$number_post_blog = get_theme_mod('number_post_blog','6');
$title_blog = get_theme_mod('title_blog','Read Important Articles About Insurance');
$blog_sub_title = get_theme_mod('sub_title_blog','Not sure what insurance fits best ?');
?>
<?php if($blog_show === 'yes'): ?>
    <section id="important-article">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <div class="wrap-article-slide-title">
                        <?php if($blog_sub_title !=''){ ?>
                            <p><?php echo $blog_sub_title; ?></p>
                        <?php } ?>
                        <?php if($title_blog !=''){ ?>
                            <p class="font-weight-bold text-capitalize"><?php echo $title_blog; ?></p>
                        <?php } ?>
                    </div>
                </div>
                <?php
                $the_query = new WP_Query(array(
                    'post_type' => 'post',
                    'posts_per_page' => $number_post_blog,
                    'post_status' => 'publish',
                    'orderby'	=> 'date',
                    'order'	=> 'DESC',
                ));
                ?>
                <?php if ( $the_query->have_posts() ) : ?>
                <div class="owl-carousel owl-theme wrap-slide" data-plugin-options="{'responsive': { '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 1}, '1199': {'items': 3}}, 'loop': true, 'autoHeight': true, 'margin': 30}">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="wrap-slide-article">
                        <div class="row">
                            <div class="col-md-3 d-inline-block align-top">
                                <div class="slide-image">
                                <?php if ( has_post_thumbnail() ) : ?>
                                     <?php the_post_thumbnail('blog'); ?>
                                <?php else: ?>
                                    <img src="<?php echo get_template_directory_uri()?>//images/no-image-blog.png" alt="<?php the_title_attribute(); ?>"/>
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-5 d-inline-block align-top">
                                <div class="wrap-slide-content">
                                    <div class="article-content"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                    <div class="article-date"><i class="far fa-clock d-inline"></i>
                                        <p class="d-inline"><?php echo get_the_date('M j Y'); ?></p>
                                    </div>
                                    <div class="social-list">
                                        <?php
                                        if(function_exists('insurance_agency_social_share')){
                                            insurance_agency_social_share();
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif;wp_reset_postdata(); ?>
            </div>
        </div>
    </section>
<?php endif; ?>