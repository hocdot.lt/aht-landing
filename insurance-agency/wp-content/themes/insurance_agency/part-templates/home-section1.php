<?php
/**
 * Render banner in home page
 */

//Get config
$banner_show =  get_theme_mod('banner_show','yes');
$slider_type =  get_theme_mod('slider_type','default');
$revolution_slider_shortcode = get_theme_mod('revolution_slider_shortcode','[rev_slider alias="home-slider"]');

$content_slider_1 = get_theme_mod('content_slider_1','<p>Wide range of Insurance policies to protect and secure yourself and your families from uncertainties of financial loss</p>');
$button_slider_1 = get_theme_mod('button_slider_1','Compare insurance');
$link_slider_1 = get_theme_mod('link_slider_1','#');
$img_slider_1 = get_theme_mod('img_slider_1',get_template_directory_uri() . '/images/banner-top.jpg');

$content_slider_2 = get_theme_mod('content_slider_2','<p>Wide range of Insurance policies to protect and secure yourself and your families from uncertainties of financial loss</p>');
$button_slider_2 = get_theme_mod('button_slider_2','Compare insurance');
$link_slider_2 = get_theme_mod('link_slider_2','#');
$img_slider_2 = get_theme_mod('img_slider_2',get_template_directory_uri() . '/images/banner-top.jpg');

$content_slider_3 = get_theme_mod('content_slider_3','<p>Wide range of Insurance policies to protect and secure yourself and your families from uncertainties of financial loss</p>');
$button_slider_3 = get_theme_mod('button_slider_3','Compare insurance');
$link_slider_3 = get_theme_mod('link_slider_3','#');
$img_slider_3 = get_theme_mod('img_slider_3',get_template_directory_uri() . '/images/banner-top.jpg');

?>
<?php if($banner_show === 'yes'): ?>
    <section id="banner">
        <div class="container-fluid">
            <div class="row">
                <?php if($slider_type === 'default'): ?>
                <div class="owl-carousel owl-theme full-width" data-plugin-options="{'items': 1, 'loop': true, 'nav': false}">
                    <?php if(!empty($slider_title_1) || !empty($sub_title_slider_1) || !empty($link_slider_1) || !empty($img_slider_1)): ?>
                        <div class="wrap-banner-top" <?php if(!empty($img_slider_1)): ?> style="background-image:url(<?php echo $img_slider_1; ?>);" <?php endif; ?>>
                            <div class="wrap-banner-content">
                                <?php
                                if(!empty($content_slider_1)):
                                    echo apply_filters( 'the_content', $content_slider_1);
                                endif;
                                ?>
                                <?php if(!empty($link_slider_1) || !empty($button_slider_1)){ ?>
                                    <a class="banner-btn text-uppercase" href="<?php echo $link_slider_1; ?>"><?php echo $button_slider_1; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(!empty($slider_title_2) || !empty($sub_title_slider_2) || !empty($link_slider_2) || !empty($img_slider_2)): ?>
                        <div class="wrap-banner-top" <?php if(!empty($img_slider_2)): ?> style="background-image:url(<?php echo $img_slider_2; ?>);" <?php endif; ?>>
                            <div class="wrap-banner-content">
                                <?php
                                if(!empty($content_slider_2)):
                                    echo apply_filters( 'the_content', $content_slider_2);
                                endif;
                                ?>
                                <?php if(!empty($link_slider_2) || !empty($button_slider_2)){ ?>
                                    <a class="banner-btn text-uppercase" href="<?php echo $link_slider_2; ?>"><?php echo $button_slider_2; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(!empty($slider_title_3) || !empty($sub_title_slider_3) || !empty($link_slider_3) || !empty($img_slider_3)): ?>
                        <div class="wrap-banner-top" <?php if(!empty($img_slider_3)): ?> style="background-image:url(<?php echo $img_slider_3; ?>);" <?php endif; ?>>
                            <div class="wrap-banner-content">
                                <?php
                                if(!empty($content_slider_3)):
                                    echo apply_filters( 'the_content', $content_slider_3);
                                endif;
                                ?>
                                <?php if(!empty($link_slider_3) || !empty($button_slider_3)){ ?>
                                    <a class="banner-btn text-uppercase" href="<?php echo $link_slider_3; ?>"><?php echo $button_slider_3; ?></a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
                <?php if($slider_type === 'rs'): ?>
                    <?php if($revolution_slider_shortcode != ''): ?>
                        <?php echo do_shortcode($revolution_slider_shortcode); ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>