<?php
/**
 * Render about-us in home page
 */

//Get config
$aboutShow =  get_theme_mod('about_show','yes');
$title_about = get_theme_mod('title_about','"We secure your family\'s future"');
$sub_title_about = get_theme_mod('sub_title_about','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore');
$about_content = get_theme_mod('about_content','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>');
$about_image = get_theme_mod('about_image',get_template_directory_uri() . '/images/about-us-bg.png');
$button_about = get_theme_mod('button_about','Read about us');
$link_about = get_theme_mod('link_about','#');
?>
<?php if($aboutShow === 'yes'): ?>
    <section id="about-us">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 no-padding">
                    <div class="wrap-left-block" <?php if(!empty($about_image)): ?> style="background-image:url(<?php echo $about_image; ?>);" <?php endif; ?>>
                        <div class="row row-height no-gutters">
                            <div class="col-xl-5 col-lg-8 d-inline-block">
                                <?php if(!empty($sub_title_about)){ ?>
                                    <div class="left-content">
                                        <p><?php echo $sub_title_about; ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-xl-3 col-lg-8 d-inline-block border-block">
                                <div class="right-content text-uppercase text-center">
                                    <?php if(!empty($title_about)){ ?>
                                        <p><?php echo $title_about; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="wrap-right-block">
                        <?php if(!empty($about_content)): ?>
                        <div class="right-block-content">
                            <?php echo apply_filters( 'the_content', $about_content); ?>
                        </div>
                        <?php endif; ?>
                        <?php if(!empty($button_about) || !empty($link_about)){ ?>
                            <div class="right-block-submit-btn"><a class="banner-btn text-uppercase" href="<?php echo $link_about; ?>"><?php echo $button_about; ?></a></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
