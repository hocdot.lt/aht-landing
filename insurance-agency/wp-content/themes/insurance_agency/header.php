<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
$insurance_agency_sidebar_left = insurance_agency_get_sidebar_left();
$insurance_agency_sidebar_right = insurance_agency_get_sidebar_right();
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site ">
        <header class="header-effect-shrink" id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
            <div class="header-body header-body-bottom-border-fixed">
                <div class="header-container container">
                    <div class="header-row">
                        <div class="header-column">
                            <div class="header-row">
                                <div class="header-logo">
                                    <?php if (is_front_page()) : ?>
                                        <h1 class="logo">
                                            <?php else: ?>
                                            <h2 class="logo">
                                                <?php endif; ?>
                                                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                                    <?php
                                                    $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                                    if ($logo_header && $logo_header!=''):
                                                        echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                                    else:
                                                        echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                                    endif;
                                                    ?>
                                                </a>
                                                <?php if (is_front_page()) : ?>
                                        </h1>
                                    <?php else: ?>
                                        </h2>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="header-column justify-content-end">
                            <div class="header-row">
                                <?php
                                if(function_exists('insurance_agency_header_menu')){
                                    insurance_agency_header_menu();
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="main" class="wrapper insurance-page">
				
            

                    
        
       
