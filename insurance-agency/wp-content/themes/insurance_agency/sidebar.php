<?php
    $insurance_agency_sidebar_left = insurance_agency_get_sidebar_left();
    $insurance_agency_sidebar_right = insurance_agency_get_sidebar_right();

    ?>
<?php if ($insurance_agency_sidebar_left && is_active_sidebar($insurance_agency_sidebar_left)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left-sidebar "><!-- main sidebar -->
        <?php dynamic_sidebar($insurance_agency_sidebar_left); ?>
    </div>
<?php endif; ?>
<?php if ($insurance_agency_sidebar_right && is_active_sidebar($insurance_agency_sidebar_right)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 right-sidebar"><!-- main sidebar -->
        <?php dynamic_sidebar($insurance_agency_sidebar_right); ?>
    </div>
<?php endif; ?>





