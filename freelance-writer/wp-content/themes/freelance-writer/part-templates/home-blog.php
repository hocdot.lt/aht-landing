<?php

$blogs_show = get_theme_mod('blogs_show','yes');
$blogs_title = get_theme_mod('blogs_title','Checkout some of the latest writings and articles for different clients a niches');
$blogs_sub_title = get_theme_mod('blogs_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur');
$blogsCategory = get_theme_mod('blogs_category','');
$blogsLimit = get_theme_mod('blogs_number_post',2);

$args = array(
	'posts_per_page'   => $blogsLimit,
	'offset'           => 0,
	'cat'         => $blogsCategory,
	'post_type'        => 'post',
);
$posts_array = get_posts( $args );
$i = 1;
?>
<?php if($blogs_show=='yes'): ?>
  <section id="latest-article">
    <div class="container">
      <div class="row">
        <div class="col-8 col-xl-5">
          <div class="wrap-la-title">
            <p class="title"><?php echo esc_html($blogs_title); ?></p>
            <p class="sub-title"><?php echo esc_html($blogs_sub_title); ?></p>
          </div>
        </div>
        <?php $i=0;?>
        <?php foreach ($posts_array as $_post): ?>
           <?php  if($i%2==0){ ?>
                <div class="col-8 mb-90">
                  <div class="row">
                    <div class="col-sm-8 col-lg-3">
                      <div class="wrap-la-img-top"><?php echo get_the_post_thumbnail($_post->ID); ?></div>
                    </div>
                    <div class="col-sm-8 col-lg-5">
                      <div class="wrap-la">
                        <div class="wrap-la-content">
                          <p><?php echo get_the_title($_post->ID); ?></p>
                          <p><?php echo get_the_excerpt($_post->ID); ?></p>
                        </div>
                        <div class="continue-reading"><a class="text-capitalize" href="<?php echo get_post_permalink($_post->ID); ?>"><?php echo __('continue reading','freelance_writer'); ?></a></div>
                      </div>
                    </div>
                  </div>
                </div>
           <?php } else{ ?> 
                <div class="col-8 mb-50">
                  <div class="row">
                    <div class="col-sm-8 col-lg-5">
                      <div class="wrap-la float-right">
                        <div class="wrap-la-content">
                          <p><?php echo get_the_title($_post->ID); ?></p>
                          <p><?php echo get_the_excerpt($_post->ID); ?></p>
                        </div>
                        <div class="continue-reading"><a class="text-capitalize" href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo __('continue reading','freelance_writer'); ?></a></div>
                      </div>
                    </div>
                    <div class="col-sm-8 col-lg-3">
                      <div class="wrap-la-img-bot"><?php echo get_the_post_thumbnail($_post->ID); ?></div>
                    </div>
                  </div>
                </div>
            <?php } $i++;?>
        <?php endforeach; ?>
        <div class="col-8">
          <div class="wrap-separator"></div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>