
<?php

/**
 * Render about-us in home page
 */

//Get config
$about_show =  get_theme_mod('about_show','yes');
$about_title = get_theme_mod('about_title','"Hello and welcome to my website,');
$about_desb = get_theme_mod('about_desb','Need high quality original content that is SEO optimized and vertified for originally? Our premium article writing service is here to do just that."');
//Column 1
$abColumn1Image = get_theme_mod('about_col_1_image',get_template_directory_uri() . '/images/notepad.png');
$abColumn1Title = get_theme_mod('about_col_1_title','Websites, emails and copywriting');
//Column 2
$abColumn2Image = get_theme_mod('about_col_2_image',get_template_directory_uri() . '/images/history.png');
$abColumn2Title = get_theme_mod('about_col_2_title','Articles, blogs, content marketing');

//Column 3
$abColumn3Image = get_theme_mod('about_col_3_image',get_template_directory_uri() . '/images/pencil.png');
$abColumn3Title = get_theme_mod('about_col_3_title','Independent research and reporting');
//Column 4
$abColumn4Image = get_theme_mod('about_col_4_image',get_template_directory_uri() . '/images/customer-review.png');
$abColumn4Title = get_theme_mod('about_col_4_title','100% Satisfaction Guaranteed');
//Column 5
$abColumn5Image = get_theme_mod('about_col_5_image',get_template_directory_uri() . '/images/scoring.png');
$abColumn5Title = get_theme_mod('about_col_5_title','Ghostwritten - Rights Transferred');
//Column 6
$abColumn6Image = get_theme_mod('about_col_6_image',get_template_directory_uri() . '/images/survey.png');
$abColumn6Title = get_theme_mod('about_col_6_title','Fresh, 100% Original & Unique Content');
?>
<?php if($about_show === 'yes'): ?>
  <section id="about-us">
    <div class="container">
      <div class="row">
        <div class="col-8 col-xl-5">
          <div class="wrap-welcome-title">
            <p class="title font-italic"><?php echo esc_html($about_title); ?></p>
            <p class="sub-title"><?php echo esc_html($about_desb); ?></p>
          </div>
        </div>
        <div class="col-8 pl-10">
              <div class="row wrap-row-upper">
                <div class="col-8 col-md-4 col-lg">
                  <div class="wrap-what-we-do">
                    <div class="wrap-img"><img src="<?php echo $abColumn1Image; ?>" alt="<?php echo $abColumn1Title; ?>"></div>
                    <div class="wrap-wwd">
                      <p class="font-weight-semibold"><?php echo $abColumn1Title; ?>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-8 col-md-4 col-lg">
                  <div class="wrap-what-we-do">
                    <div class="wrap-img"><img src="<?php echo $abColumn2Image; ?>" alt="<?php echo $abColumn2Title; ?>"></div>
                    <div class="wrap-wwd">
                      <p class="font-weight-semibold"><?php echo $abColumn2Title; ?>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-8 col-md-4 col-lg">
                  <div class="wrap-what-we-do">
                    <div class="wrap-img"><img src="<?php echo $abColumn3Image; ?>" alt="<?php echo $abColumn3Title; ?>"></div>
                    <div class="wrap-wwd">
                      <p class="font-weight-semibold"><?php echo $abColumn3Title; ?>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row wrap-row-below">
                <div class="col-8 col-md-4 col-lg">
                  <div class="wrap-what-we-do">
                    <div class="wrap-img"><img src="<?php echo $abColumn4Image; ?>" alt="<?php echo $abColumn4Title; ?>"></div>
                    <div class="wrap-wwd">
                      <p class="font-weight-semibold"><?php echo $abColumn4Title; ?>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-8 col-md-4 col-lg">
                  <div class="wrap-what-we-do">
                    <div class="wrap-img"><img src="<?php echo $abColumn5Image; ?>" alt="<?php echo $abColumn5ImageTitle; ?>"></div>
                    <div class="wrap-wwd">
                      <p class="font-weight-semibold"><?php echo $abColumn5Title; ?>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-8 col-md-4 col-lg">
                  <div class="wrap-what-we-do">
                    <div class="wrap-img"><img src="<?php echo $abColumn6Image; ?>" alt="<?php echo $abColumn6Title; ?>"></div>
                    <div class="wrap-wwd">
                      <p class="font-weight-semibold"><?php echo $abColumn6Title; ?>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-8">
            <div class="wrap-separator"></div>
          </div>
        </div>
      </div>
  </section>
<?php endif; ?>