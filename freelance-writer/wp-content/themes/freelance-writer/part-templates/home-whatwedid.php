<?php
$whatwedidShow = get_theme_mod('whatwedid_show','yes');
$whatwedidTitle = get_theme_mod('whatwedid_title','See what our clients have to say about our services ...');
$whatwedidSubTitle = get_theme_mod('whatwedid_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur');
$whatwedid_right_banner=get_theme_mod('whatwedid_right_banner',get_template_directory_uri() . '/images/image-mid-4.png');
$whatwedidCategory = get_theme_mod('whatwedid_category','');
$whatwedidLimit = get_theme_mod('whatwedid_number_post',4);

$args = array(
    'posts_per_page'   => $whatwedidLimit,
    'offset'           => 0,
    'post_type'        => 'testimonials',
);
$posts_array = get_posts( $args );
?>
<?php if($whatwedidShow=='yes'): ?>
  <section id="feedback">
    <div class="container-fluid custom-container">
      <div class="row">
        <div class="col-8 col-xl-5 p-two-side">
          <div class="wrap-feedback-content">
            <p class="title"><?php echo esc_html($whatwedidTitle); ?></p>
            <p class="sub-title"><?php echo esc_html($whatwedidSubTitle); ?></p>
          </div>
          <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'loop': true, 'nav': false}">
            <?php $i=0; ?>
            <?php foreach ($posts_array as $_post): ?>
                <?php if($i%2==0){ ?>
                    <div class="wrap-feedback-slide">
                <?php } ?>
                    
                    <?php if($i%2===1){?>
                        <div class="wrap-client-feedback">
                            <div class="wrap-feedback d-inline-block">
                              <p class="font-italic"><?php echo $_post->post_content;?></p>
                              <p class="orange text-capitalize mb-2"><?php echo get_the_title($_post->ID); ?></p>
                              <p class="font-weight-semibold text-capitalize text-dark"><?php echo get_the_excerpt($_post->ID) ?></p>
                            </div>
                            <div class="wrap-img-right d-inline-block"><?php echo get_the_post_thumbnail($_post->ID); ?></div>
                      </div>    
                    <?php } else{ ?>
                        <div class="wrap-client-feedback">
                            <div class="wrap-img-left d-inline-block"><?php echo get_the_post_thumbnail($_post->ID); ?></div>
                            <div class="wrap-feedback d-inline-block">
                              <p class="font-italic"><?php echo $_post->post_content; ?></p>
                              <p class="orange text-capitalize mb-2"><?php echo get_the_title($_post->ID);?></p>
                              <p class="font-weight-semibold text-capitalize text-dark"><?php echo get_the_excerpt($_post->ID) ?></p>
                            </div>
                          </div>
                    <?php } ?>

                <?php  if($i%2===1){ ?>
                    </div>
                <?php } ?> 
              <?php $i++; endforeach;?> 
               <?php  if($i%2===1){ ?>
                    </div>
                <?php } ?>  
          </div>
        </div>
        <div class="col-3 remove-col">
          <div class="wrap-feedback-banner"><img src="<?php echo  esc_url($whatwedid_right_banner); ?>"></div>
        </div>
        <div class="col-8">
          <div class="wrap-separator"></div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>