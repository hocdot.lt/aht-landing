<?php
/**
 * Render contact sesion
 */
//Get Config
$contactShow = get_theme_mod('contact_show','yes');
$contact_title = get_theme_mod('contact_title','Have a project in mind? Contact us, we will assist you further ...');
$contact_sub_title = get_theme_mod('contact_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliq');

$contact_name = get_theme_mod('contact_name','Freelance Writer');
$contact_address= get_theme_mod('contact_address','Door No. 301. Lurels Apartments, East Vervinia, USA');
$contact_email = get_theme_mod('contact_email','info@freelancewriter.com');
$contact_website = get_theme_mod('contact_website','freelancewriter.com');
$contact_phone = get_theme_mod('contact_phone','800 1230 456');
$contactShortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="68" title="Contact form"]');
//Get social
$share_facebook =get_theme_mod('share_facebook','yes');
$facebook =get_theme_mod('facebook','#');
$share_instagram =get_theme_mod('share_instagram','yes');
$instagram =get_theme_mod('instagram','#');
$share_twitter =get_theme_mod('share_twitter','yes');
$twitter =get_theme_mod('twitter','#');
$share_google =get_theme_mod('share_google','yes');
$google =get_theme_mod('google','#');
$share_linkedin =get_theme_mod('share_linkedin','yes');
$linkedin =get_theme_mod('linkedin','#');
$share_pinterest =get_theme_mod('share_pinterest','yes');
$pinterest =get_theme_mod('pinterest','#');
?>
<?php if($contactShow=='yes'): ?>
      <section id="contact-us">
        <div class="container">
          <div class="row">
            <div class="col-8">
              <div class="wrap-contact-us-title text-center">
                <div class="ct-title">
                  <p class="title"><?php echo esc_html($contact_title); ?></p>
                </div>
                <div class="ct-sub-title">
                  <p class="sub-title"><?php echo esc_html($contact_sub_title); ?></p>
                </div>
              </div>
            </div>
            <div class="col-md-8 col-xl-4">
              <div class="company-info text-center">
                <div class="company-name">
                  <p class="font-weight-bold text-capitalize"><?php echo esc_html($contact_name); ?></p>
                </div>
                <div class="company-address">
                  <p><?php echo esc_html($contact_address); ?></p>
                </div>
                <div class="company-mail">
                  <p><?php echo esc_html($contact_email); ?></p>
                </div>
                <div class="company-website"><a class="orange" href="<?php echo esc_url($contact_website); ?>"><?php echo esc_html($contact_website); ?></a></div>
                <div class="company-dial">
                  <p class="font-weight-semibold d-inline"><?php echo __('Call:','freelance_writer'); ?></p>
                  <p class="font-weight-semibold d-inline"><?php echo esc_html($contact_phone); ?></p>
                </div>
                <div class="social-media">
                  <ul>
                  		<?php if($share_facebook=='yes'){ ?>
							<li><a target=_blank href="<?php echo esc_url($facebook); ?>"><i class="fab fa-facebook"></i></a></li>
                  		<?php } ?>
                        <?php if($share_twitter=='yes'){ ?>
							<li><a target=_blank href="<?php echo esc_url($twitter); ?>"><i class="fab fa-twitter-square"></i></a></li>
                  		<?php } ?>
                  		<?php if($share_linkedin=='yes'){ ?>
							 <li><a target=_blank href="<?php echo esc_url($linkedin); ?>"><i class="fab fa-linkedin"></i></a></li>
                  		<?php } ?>
                  		<?php if($share_pinterest=='yes'){ ?>
							<li><a target=_blank href="<?php echo esc_url($pinterest); ?>"><i class="fab fa-pinterest"></i></a></li>
                  		<?php } ?>
                  		<?php if($share_google=='yes'){ ?>
							<li><a target=_blank href="<?php echo esc_url($google); ?>"><i class="fab fa-google-plus"></i></a></li>
                  		<?php } ?>
                  		<?php if($share_instagram=='yes'){ ?>
							<li><a target=_blank href="<?php echo esc_url($instagram); ?>"><i class="fab fa-instagram"></i></a></li>
                  		<?php } ?>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-md-8 col-xl-3">
            	<?php echo do_shortcode($contactShortcode) ?>
            </div>
          </div>
        </div>
      </section>
<?php endif; ?>