<?php
/**
 * Render banner in home page
 */

//Get config
$bannerShow = get_theme_mod('banner_show','yes');
$bannerTitle =  get_theme_mod('banner_title','<span class="orange d-inline-block">connect</span> with your audience through powerful<span class="orange d-inline-block">content</span>');
$bannerBg = get_theme_mod('background_banner',get_template_directory_uri() . '/images/image-top.png');
?>  
<?php if($bannerShow === 'yes'): ?>
      <section id="banner">
        <div class="container-fluid h-100 custom-container">
          <div class="row h-100">
            <div class="col-xl-3 pr-0 backgrounnd-wrap">
              <div class="row">
                <div class="col-8 visible-h">
                  <div class="top-blank-block"></div>
                </div>
                <div class="col-8 visible-h p-0">
                  <div class="bottom-blank-block"></div>
                </div>
              </div>
            </div>
            <div class="col-8 col-xl-5 pr-0">
              <div class="wrap-right-banner-block h-100 text-uppercase font-weight-bold" style="background-image: url('<?php echo esc_url($bannerBg);?>')">
                <div class="wrap-text">
                    <?php echo $bannerTitle; ?>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
<?php endif; ?>