<?php
/**
 * Render what do we do session
 */

//Get config
$whatdoweShow = get_theme_mod('whatDoWeDo_show','yes');
$whatdowe_title = get_theme_mod('whatDoWeDo_title','Let the visitors coming back for more engaging and interaction to your website');
$whatDoWeDo_desb = get_theme_mod('whatDoWeDo_desb','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur');
$whatDoWeDo_image_banner_left = get_theme_mod('whatDoWeDo_image_banner_left',get_template_directory_uri() . '/images/image-mid.png');
//Column 1
$whatdowe_col1_title = get_theme_mod('whatDoWeDo_col1_title','Blog Articles');
$whatdowe_col1_subtitle = get_theme_mod('whatDoWeDo_col1_subtitle','From $2/100 words');
$whatDoWeDo_col1_content = get_theme_mod('whatDoWeDo_col1_content','<ul><li>Articles by professional writers</li><li>SEO-friendly content</li><li>100% unique. Copyscape checked</li></ul>');
$whatDoWeDo_col1_link = get_theme_mod('whatDoWeDo_col1_link','#');
$whatDoWeDo_col1_link_title = get_theme_mod('whatDoWeDo_col1_link_title','order');
//Column 2
$whatdowe_col2_title = get_theme_mod('whatDoWeDo_col2_title','Copyrighting');
$whatdowe_col2_subtitle = get_theme_mod('whatDoWeDo_col2_subtitle','From $2/100 words');
$whatDoWeDo_col2_content = get_theme_mod('whatDoWeDo_col2_content','<ul><li>Articles by professional writers</li><li>SEO-friendly content</li><li>100% unique. Copyscape checked</li></ul>');
$whatDoWeDo_col2_link = get_theme_mod('whatDoWeDo_col2_link','#');
$whatDoWeDo_col2_link_title = get_theme_mod('whatDoWeDo_col2_link_title','order');
//Column 3
$whatdowe_col3_title = get_theme_mod('whatDoWeDo_col3_title','Social Media');
$whatdowe_col3_subtitle = get_theme_mod('whatDoWeDo_col3_subtitle','From $2/100 words');
$whatDoWeDo_col3_content = get_theme_mod('whatDoWeDo_col3_content','<ul><li>Articles by professional writers</li><li>SEO-friendly content</li><li>100% unique. Copyscape checked</li></ul>');
$whatDoWeDo_col3_link = get_theme_mod('whatDoWeDo_col3_link','#');
$whatDoWeDo_col3_link_title = get_theme_mod('whatDoWeDo_col3_link_title','order');
?>
<?php if($whatdoweShow === 'yes'): ?>
  <section id="order-now">
    <div class="container-fluid custom-container">
      <div class="row">
        <div class="col-8 col-xl-5">
          <div class="wrap-order-now-title">
            <div class="orn-title">
              <p class="title"><?php echo esc_html($whatdowe_title); ?></p>
              <p class="sub-tille"><?php echo esc_html($whatDoWeDo_desb); ?></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row wrap-orn-below">
        <div class="col-8 col-xl-3 p-0">
          <div class="wrap-order-bg"><img src="<?php echo $whatDoWeDo_image_banner_left; ?>" alt="Pricing"></div>
        </div>
        <div class="col-8 col-xl-5 pad-0">
          <div class="wrap-order-now">
                <div class="wrap-articles d-inline-block align-middle">
                  <div class="article-title">
                    <p class="text-capitalize"><?php echo esc_html($whatdowe_col1_title); ?></p>
                    <p><?php echo esc_html($whatdowe_col1_subtitle); ?></p>
                  </div>
                  <div class="article-content">
                    <?php echo $whatDoWeDo_col1_content; ?>
                  </div>
                  <div class="order-btn"><a class="text-uppercase" href="<?php echo $whatDoWeDo_col1_link; ?>"><?php echo ($whatDoWeDo_col1_link_title); ?></a></div>
                </div>
                <div class="wrap-articles d-inline-block align-middle">
                  <div class="article-title">
                    <p class="text-capitalize"><?php echo esc_html($whatdowe_col2_title); ?></p>
                    <p><?php echo esc_html($whatdowe_col2_subtitle); ?></p>
                  </div>
                  <div class="article-content">
                    <?php echo $whatDoWeDo_col2_content; ?>
                  </div>
                  <div class="order-btn"><a class="text-uppercase" href="<?php echo $whatDoWeDo_col2_link; ?>"><?php echo ($whatDoWeDo_col2_link_title); ?></a></div>
                </div>
                <div class="wrap-articles d-inline-block align-middle">
                  <div class="article-title">
                    <p class="text-capitalize"><?php echo esc_html($whatdowe_col3_title); ?></p>
                    <p><?php echo esc_html($whatdowe_col3_subtitle); ?></p>
                  </div>
                  <div class="article-content">
                    <?php echo $whatDoWeDo_col3_content; ?>
                  </div>
                  <div class="order-btn"><a class="text-uppercase" href="<?php echo $whatDoWeDo_col3_link; ?>"><?php echo ($whatDoWeDo_col3_link_title); ?></a></div>
                </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>