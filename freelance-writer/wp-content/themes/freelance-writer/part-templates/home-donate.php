<?php
/**
 * Render Donate session
 */

//Get config
$donateShow = get_theme_mod('donate_show','yes');
$donateBackground = get_theme_mod('donate_bg',get_template_directory_uri() . '/images/bg-session.jpg');
$donateLeftTitle = get_theme_mod('donate_left_title','Giving the Hands to the Poor help us to get help them');
$donateLeftContent = get_theme_mod('donate_left_content','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis.</p>');
$donateRightTitle = get_theme_mod('donate_left_title','For Further Information walk in or contact us');
$donateRightContent = get_theme_mod('donate_right_content','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis.</p>');
$donateButtonTitle = get_theme_mod('donate_button','Donate Now');
$donateButtonLink = get_theme_mod('donate_link','#');
?>
<?php if($donateShow === 'yes'): ?>
    <div id="donateSession" style="background-image:url(<?php echo $donateBackground ?>)">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="pr-lg-4">
                        <h3><?php echo $donateLeftTitle ?></h3>
                        <div class="">
                            <?php echo $donateLeftContent ?>
                        </div>
                        <a class="donate" href="<?php echo $donateButtonLink ?>"><?php echo $donateButtonTitle ?></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pl-lg-13 pr-lg-5">
                        <h3><?php echo $donateRightTitle ?></h3>
                        <div>
                            <?php echo $donateRightContent ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
