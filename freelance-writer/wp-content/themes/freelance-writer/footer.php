
</div> <!-- End main-->
 <footer id="footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-8 col-xl-3 p-0">
            <div class="logo-footer mb-15"><img src="<?php echo get_theme_mod('footer_banner',get_template_directory_uri() . '/images/image-bot.png');?>" alt="Footer"></div>
          </div>
          <div class="col-8 col-xl-5">
            <div class="organization-details mb-15">
              <div class="wrap-menu-info">
                <?php
                    wp_nav_menu(array(
                        'theme_location'    => 'footer',
                        'menu_class'        => 'menu-footer',
                ))
                ?>
                <?php  
                $copyright =  get_theme_mod('copyright','<p class="text-capitalize">(C) 2019 all rights reserved. freelance writer. designed &amp; developed by<a href="#">Template.net</a></p>');
                if ( $copyright !='') : ?>
                        <?php echo wp_kses_post( $copyright); ?>
                <?php endif;?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
</div> <!-- End page-->
<a class="btn-top" href="#"><i class="fas fa-chevron-up"></i></a>
<?php wp_footer(); ?>
</body>
</html>