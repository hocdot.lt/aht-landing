	<div class="top-blog">
		<?php if(get_the_title() != ''):?>
			<h1><?php echo get_the_title(); ?></h1>  
		<?php endif;?> 
		<div class="meta">
			<span class="date"><?php echo get_the_date('F j Y'); ?></span>
			<span class="social-share-post">
			<?php
				if(function_exists('freelance_writer_social_share')){
					echo '<lable class="lable">'. wp_kses('Share :','freelance_writer') .'</lable>';
					freelance_writer_social_share();
				}
			?>	
			</span>
			<span class="comment"><?php echo '&lpar;'.get_comments_number().'&rpar;'; _e(' Comments','freelance_writer');?></span>
		</div>				
	</div>
	<div class="content-blog">	
		<?php the_content();?>		
	</div>
	