(function($){
    $(document).ready(function () {
        //reponsive open menu button click
        $('.open-menu-mobile').click(function (e) {
            e.preventDefault();
            $(this).closest('.main-menu').find('#site-navigation').addClass('open');
        })

        //reponsive close menu button click
        $('.close-menu-mobile').click(function (e) {
            e.preventDefault();
            $(this).closest('#site-navigation').removeClass('open');
        })
        //reponsive close menu button click
        $('.close').click(function (e) {
            e.preventDefault();
            $(this).closest('#site-navigation').removeClass('open');

        })
        //Scroll function
        $(window).scroll(function(){
            //Static header
            if($(window).scrollTop() >= 1){
                $('header.header').addClass('static-header');
            }else{
                $('header.header').removeClass('static-header');
            }
        });
    });
})(jQuery);