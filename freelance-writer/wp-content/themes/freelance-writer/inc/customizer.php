<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'freelance_writer_customize_register' );
function freelance_writer_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'freelance_writer_Customize_Sidebar_Control' ) ) {
	    class freelance_writer_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Baloo Tamma' => 'Baloo Tamma',
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',			
				'' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'freelance_writer') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','freelance_writer'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','freelance_writer' ),
	      'description' => esc_html__( 'Upload Logo','freelance_writer' ),
	      'section' => 'general_settings',
	   )
	) );
	
	$wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Main Google Font',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'freelance_writer'),
        'description'    => __('Several settings pertaining my theme', 'freelance_writer'),
    ) );
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','freelance_writer'  ),
	      'priority' => 20,
		  'panel'  => 'home_page_setting',
	   )
	);
	// title banner
    $wp_customize->add_setting( 'banner_title',
        array(
            'default' => '<span class="orange d-inline-block">connect</span> with your audience through powerful<span class="orange d-inline-block">content</span>',
        )
    );

        $wp_customize->add_control( 'banner_title',
        array(
            'label' => esc_html__( 'The left content','freelance_writer' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );
	// background banner
	$wp_customize->add_setting( 'background_banner',
		array(
			'default' => get_template_directory_uri() . '/images/image-top.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_banner',
	   array(
	      'label' => esc_html__( 'Background Banner','freelance_writer' ),
	      'description' => esc_html__( 'Upload Image','freelance_writer' ),
	      'section' => 'home_banner_settings',
	   )
	) );
	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','freelance_writer' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array(
	         'no' => esc_html__( 'No','freelance_writer' ),
	         'yes' => esc_html__( 'Yes','freelance_writer' ),
            )
	   )
	);
	// End banner section



	
	// Section about
	    // Title
    $wp_customize->add_setting( 'about_title',
        array(
            'default' => esc_html__('"Hello and welcome to my website,','freelance_writer'),

        )
    );
    $wp_customize->add_control('about_title',
        array(
            'label' => esc_html__( 'Title','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'about_desb',
        array(
            'default' => esc_html__('Need high quality original content that is SEO optimized and vertified for originally? Our premium article writing service is here to do just that."','freelance_writer'),

        )
    );
    $wp_customize->add_control('about_desb',
        array(
            'label' => esc_html__( 'Description','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );
	$wp_customize->add_section( 'about_settings',
	   array(
	      'title' => esc_html__( 'About section','freelance_writer'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
	//Column 1: Image
    $wp_customize->add_setting( 'about_col_1_image',
        array(
            'default' => get_template_directory_uri() . '/images/notepad.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_1_image',
        array(
            'label' => esc_html__( 'Column 1: Image','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 1: Title
    $wp_customize->add_setting( 'about_col_1_title',
        array(
            'default' => esc_html__('Websites, emails and copywriting','freelance_writer'),

        )
    );
    $wp_customize->add_control('about_col_1_title',
        array(
            'label' => esc_html__( 'Column 1: Title','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );


    //Column 2: Image
    $wp_customize->add_setting( 'about_col_2_image',
        array(
            'default' => get_template_directory_uri() . '/images/history.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_2_image',
        array(
            'label' => esc_html__( 'Column 2: Image','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 2: Title
    $wp_customize->add_setting( 'about_col_2_title',
        array(
            'default' => esc_html__('Articles, blogs, content marketing','freelance_writer'),

        )
    );
    $wp_customize->add_control('about_col_2_title',
        array(
            'label' => esc_html__( 'Column 2: Title','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );
    //Column 3: Image
    $wp_customize->add_setting( 'about_col_3_image',
        array(
            'default' => get_template_directory_uri() . '/images/pencil.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_3_image',
        array(
            'label' => esc_html__( 'Column 3: Image','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 3: Title
    $wp_customize->add_setting( 'about_col_3_title',
        array(
            'default' => esc_html__('Independent research and reporting','freelance_writer'),

        )
    );
    $wp_customize->add_control('about_col_3_title',
        array(
            'label' => esc_html__( 'Column 3: Title','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );
    //Column 4: Image
    $wp_customize->add_setting( 'about_col_4_image',
        array(
            'default' => get_template_directory_uri() . '/images/customer-review.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_4_image',
        array(
            'label' => esc_html__( 'Column 4: Image','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 4: Title
    $wp_customize->add_setting( 'about_col_4_title',
        array(
            'default' => esc_html__('100% Satisfaction Guaranteed','freelance_writer'),

        )
    );
    $wp_customize->add_control('about_col_4_title',
        array(
            'label' => esc_html__( 'Column 4: Title','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );
    //Column 5: Image
    $wp_customize->add_setting( 'about_col_5_image',
        array(
            'default' => get_template_directory_uri() . '/images/scoring.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_5_image',
        array(
            'label' => esc_html__( 'Column 5: Image','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 5: Title
    $wp_customize->add_setting( 'about_col_5_title',
        array(
            'default' => esc_html__('Ghostwritten - Rights Transferred','freelance_writer'),

        )
    );
    $wp_customize->add_control('about_col_5_title',
        array(
            'label' => esc_html__( 'Column 5: Title','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );
    //Column 6: Image
    $wp_customize->add_setting( 'about_col_6_image',
        array(
            'default' => get_template_directory_uri() . '/images/survey.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_6_image',
        array(
            'label' => esc_html__( 'Column 6: Image','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 6: Title
    $wp_customize->add_setting( 'about_col_6_title',
        array(
            'default' => esc_html__('Fresh, 100% Original & Unique Content','freelance_writer'),

        )
    );
    $wp_customize->add_control('about_col_6_title',
        array(
            'label' => esc_html__( 'Column 6: Title','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );


    //Show/Hide about
    $wp_customize->add_setting( 'about_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'about_show',
        array(
            'label' => esc_html__( 'Show Section','freelance_writer' ),
            'section' => 'about_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','freelance_writer' ),
                'yes' => esc_html__( 'Yes','freelance_writer' ),
            )
        )
    );

    // End about*/

    //What do we do
    $wp_customize->add_section( 'whatdowedo_settings',
        array(
            'title' => esc_html__( '"What do we do" section','freelance_writer'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    //Title
    $wp_customize->add_setting( 'whatDoWeDo_title',
        array(
            'default' => 'What do we do? and what inspirs us to do this?',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_title',
        array(
            'label' => esc_html__( 'Session Title','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text'
        )
    );
    // sub Title
    $wp_customize->add_setting( 'whatDoWeDo_desb',
        array(
            'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_desb',
        array(
            'label' => esc_html__( 'Session Sub Title','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'textarea'
        )
    );
    //Banner left
    $wp_customize->add_setting( 'whatDoWeDo_image_banner_left',
        array(
            'default' => get_template_directory_uri() . '/images/image-mid.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'whatDoWeDo_image_banner_left',
        array(
            'label' => esc_html__( 'Banner left','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'whatdowedo_settings',
        )
    ) );


    //Column 1: Title
    $wp_customize->add_setting( 'whatDoWeDo_col1_title',
        array(
            'default' => esc_html__('Blog Articles','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col1_title',
        array(
            'label' => esc_html__( 'Column 1: Title','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );

    // Column 1: Sub title
    $wp_customize->add_setting( 'whatDoWeDo_col1_subtitle',
        array(
            'default' => 'From $2/100 words',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_col1_subtitle',
        array(
            'label' => esc_html__( 'Column 2: Sub Title','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'textarea',
        )
    );
    // Column 1: content
    $wp_customize->add_setting( 'whatDoWeDo_col1_content',
        array(
            'default' => '<ul>
                          <li>Articles by professional writers</li>
                          <li>SEO-friendly conten</li>
                          <li>100% unique. Copyscape checked</li>
                        </ul>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'whatDoWeDo_col1_content',
        array(
            'label' => esc_html__( 'Col 1 content','freelance_writer' ),
            'section' => 'whatdowedo_settings',
        )
    ));
        //Column 1: Link
    $wp_customize->add_setting( 'whatDoWeDo_col1_link',
        array(
            'default' => esc_html__('#','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col1_link',
        array(
            'label' => esc_html__( 'Column 1: Link button','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );
        //Column 1: Link title
    $wp_customize->add_setting( 'whatDoWeDo_col1_link_title',
        array(
            'default' => esc_html__('order','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col1_link_title',
        array(
            'label' => esc_html__( 'Column 1: Title button','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );
   //Column 2: Title
    $wp_customize->add_setting( 'whatDoWeDo_col2_title',
        array(
            'default' => esc_html__('Blog Articles','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col2_title',
        array(
            'label' => esc_html__( 'Column 2: Title','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );

    // Column 2: Sub title
    $wp_customize->add_setting( 'whatDoWeDo_col2_subtitle',
        array(
            'default' => 'From $2/200 words',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_col2_subtitle',
        array(
            'label' => esc_html__( 'Column 2: Sub Title','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'textarea',
        )
    );
    // Column 2: content
    $wp_customize->add_setting( 'whatDoWeDo_col2_content',
        array(
            'default' => '<ul>
                          <li>Articles by professional writers</li>
                          <li>SEO-friendly conten</li>
                          <li>200% unique. Copyscape checked</li>
                        </ul>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'whatDoWeDo_col2_content',
        array(
            'label' => esc_html__( 'Col 2 content','freelance_writer' ),
            'section' => 'whatdowedo_settings',
        )
    ));
        //Column 2: Link
    $wp_customize->add_setting( 'whatDoWeDo_col2_link',
        array(
            'default' => esc_html__('#','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col2_link',
        array(
            'label' => esc_html__( 'Column 2: Link button','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );
        //Column 2: Link title
    $wp_customize->add_setting( 'whatDoWeDo_col2_link_title',
        array(
            'default' => esc_html__('order','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col2_link_title',
        array(
            'label' => esc_html__( 'Column 2: Title button','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );
   //Column 3: Title
    $wp_customize->add_setting( 'whatDoWeDo_col3_title',
        array(
            'default' => esc_html__('Blog Articles','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col3_title',
        array(
            'label' => esc_html__( 'Column 3: Title','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );

    // Column 3: Sub title
    $wp_customize->add_setting( 'whatDoWeDo_col3_subtitle',
        array(
            'default' => 'From $3/300 words',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_col3_subtitle',
        array(
            'label' => esc_html__( 'Column 3: Sub Title','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'textarea',
        )
    );
    // Column 3: content
    $wp_customize->add_setting( 'whatDoWeDo_col3_content',
        array(
            'default' => '<ul>
                          <li>Articles by professional writers</li>
                          <li>SEO-friendly conten</li>
                          <li>300% unique. Copyscape checked</li>
                        </ul>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'whatDoWeDo_col3_content',
        array(
            'label' => esc_html__( 'Col 3 content','freelance_writer' ),
            'section' => 'whatdowedo_settings',
        )
    ));
        //Column 3: Link
    $wp_customize->add_setting( 'whatDoWeDo_col3_link',
        array(
            'default' => esc_html__('#','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col3_link',
        array(
            'label' => esc_html__( 'Column 3: Link button','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );
        //Column 3: Link title
    $wp_customize->add_setting( 'whatDoWeDo_col3_link_title',
        array(
            'default' => esc_html__('order','freelance_writer'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col3_link_title',
        array(
            'label' => esc_html__( 'Column 3: Title button','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );
    //Show/Hide session
    $wp_customize->add_setting( 'whatDoWeDo_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_show',
        array(
            'label' => esc_html__( 'Show Section','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','freelance_writer' ),
                'yes' => esc_html__( 'Yes','freelance_writer' ),
            )
        )
    );



    //Show/Hide session
    $wp_customize->add_setting( 'whatDoWeDo_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_show',
        array(
            'label' => esc_html__( 'Show Section','freelance_writer' ),
            'section' => 'whatdowedo_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','freelance_writer' ),
                'yes' => esc_html__( 'Yes','freelance_writer' ),
            )
        )
    );

    //End What do we do

    //Blog
    $wp_customize->add_section( 'blogs_settings',
        array(
            'title' => esc_html__( 'Blog Section','freelance_writer'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    //Title
    $wp_customize->add_setting( 'blogs_title',
        array(
            'default' => esc_html__('Checkout some of the latest writings and articles for different clients a niches','freelance_writer'),
        )
    );
    $wp_customize->add_control('blogs_title',
        array(
            'label' => esc_html__( 'Title Section','freelance_writer' ),
            'section' => 'blogs_settings',
            'type' => 'text',
        )
    );
    //Sub Title
    $wp_customize->add_setting( 'blogs_sub_title',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur','freelance_writer'),
        )
    );
    $wp_customize->add_control('blogs_sub_title',
        array(
            'label' => esc_html__( 'Title Section','freelance_writer' ),
            'section' => 'blogs_settings',
            'type' => 'text',
        )
    );
        //  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }
    //Category
    $wp_customize->add_setting( 'blogs_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('blogs_category',
        array(
            'label' => esc_html__( 'Category Post','freelance_writer' ),
            'section' => 'blogs_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

    //Number post
    $wp_customize->add_setting( 'blogs_number_post',
        array(
            'default' => esc_html__('2','freelance_writer'),
        )
    );
    $wp_customize->add_control('blogs_number_post',
        array(
            'label' => esc_html__( 'Number Post','freelance_writer' ),
            'section' => 'blogs_settings',
            'type' => 'text',
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'blogs_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'blogs_show',
        array(
            'label' => esc_html__( 'Show Section','freelance_writer' ),
            'section' => 'blogs_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','freelance_writer' ),
                'yes' => esc_html__( 'Yes','freelance_writer' ),
            )
        )
    );



    //End Blog
    //What we did
    $wp_customize->add_section( 'whatwedid_settings',
        array(
            'title' => esc_html__( 'Feedback session','freelance_writer'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    //Tile
    $wp_customize->add_setting( 'whatwedid_title',
        array(
            'default' => esc_html__('See what our clients have to say about our services ...','freelance_writer'),
        )
    );
    $wp_customize->add_control('whatwedid_title',
        array(
            'label' => esc_html__( 'Title Section','freelance_writer' ),
            'section' => 'whatwedid_settings',
            'type' => 'text',
        )
    );
    //Tile
    $wp_customize->add_setting( 'whatwedid_sub_title',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur','freelance_writer'),
        )
    );
    $wp_customize->add_control('whatwedid_sub_title',
        array(
            'label' => esc_html__( 'Sub Title Section','freelance_writer' ),
            'section' => 'whatwedid_settings',
            'type' => 'text',
        )
    );
	//Right banner
    $wp_customize->add_setting( 'whatwedid_right_banner',
        array(
            'default' => get_template_directory_uri() . '/images/image-mid-4.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'whatwedid_right_banner',
        array(
            'label' => esc_html__( 'Right banner','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'whatwedid_settings',
        )
    ));
    //Number post
    $wp_customize->add_setting( 'whatwedid_number_post',
        array(
            'default' => esc_html__('4','freelance_writer'),
        )
    );
    $wp_customize->add_control('whatwedid_number_post',
        array(
            'label' => esc_html__( 'Number Post','freelance_writer' ),
            'section' => 'whatwedid_settings',
            'type' => 'text',
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'whatwedid_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'whatwedid_show',
        array(
            'label' => esc_html__( 'Show Section','freelance_writer' ),
            'section' => 'whatwedid_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','freelance_writer' ),
                'yes' => esc_html__( 'Yes','freelance_writer' ),
            )
        )
    );

    //End what we did




    //Contact session
    $wp_customize->add_section( 'contact_settings',
        array(
            'title' => esc_html__( 'Contact section','freelance_writer'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
//Contact title
    $wp_customize->add_setting( 'contact_title',
        array(
            'default' => 'Have a project in mind? Contact us, we will assist you further ...',
        )
    );
    $wp_customize->add_control( 'contact_title',
        array(
            'label' => esc_html__( 'Contact title','freelance_writer' ),
            'section' => 'contact_settings',
            'type' => 'text'
        )
    );
	//Contact sub title
	    $wp_customize->add_setting( 'contact_sub_title',
	        array(
	            'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliq',
	        )
	    );
	    $wp_customize->add_control( 'contact_sub_title',
	        array(
	            'label' => esc_html__( 'Contact Sub title','freelance_writer' ),
	            'section' => 'contact_settings',
	            'type' => 'text'
	        )
	    );
	//Contact name
	    $wp_customize->add_setting( 'contact_name',
	        array(
	            'default' => 'Freelance Writer',
	        )
	    );
	    $wp_customize->add_control( 'contact_name',
	        array(
	            'label' => esc_html__( 'Contact Name','freelance_writer' ),
	            'section' => 'contact_settings',
	            'type' => 'text'
	        )
	    );
	//Contact address
	    $wp_customize->add_setting( 'contact_address',
	        array(
	            'default' => 'Door No. 301. Lurels Apartments, East Vervinia, USA',
	        )
	    );
	    $wp_customize->add_control( 'contact_address',
	        array(
	            'label' => esc_html__( 'Contact address','freelance_writer' ),
	            'section' => 'contact_settings',
	            'type' => 'text'
	        )
	    );
	//Contact email
	    $wp_customize->add_setting( 'contact_email',
	        array(
	            'default' => 'info@freelancewriter.com',
	        )
	    );
	    $wp_customize->add_control( 'contact_email',
	        array(
	            'label' => esc_html__( 'Contact email','freelance_writer' ),
	            'section' => 'contact_settings',
	            'type' => 'text'
	        )
	    );
	//Contact website
	    $wp_customize->add_setting( 'contact_website',
	        array(
	            'default' => 'freelancewriter.com',
	        )
	    );
	    $wp_customize->add_control( 'contact_website',
	        array(
	            'label' => esc_html__( 'Contact website','freelance_writer' ),
	            'section' => 'contact_settings',
	            'type' => 'text'
	        )
	    );
	//Contact phone
	    $wp_customize->add_setting( 'contact_phone',
	        array(
	            'default' => '800 1230 456',
	        )
	    );
	    $wp_customize->add_control( 'contact_phone',
	        array(
	            'label' => esc_html__( 'Contact phone','freelance_writer' ),
	            'section' => 'contact_settings',
	            'type' => 'text'
	        )
	    );
    //Contact shortcode
    $wp_customize->add_setting( 'contact_shortcode',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control( 'contact_shortcode',
        array(
            'label' => esc_html__( 'Contact shortcode','freelance_writer' ),
            'section' => 'contact_settings',
            'type' => 'text'
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'contact_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'contact_show',
        array(
            'label' => esc_html__( 'Show Section','freelance_writer' ),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','freelance_writer' ),
                'yes' => esc_html__( 'Yes','freelance_writer' ),
            )
        )
    );

    //End Contact

	// End Home Options
	
	// Footer 
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','freelance_writer'  ),
	      'priority' => 35, 
	   )
	);

   //banner left
    $wp_customize->add_setting( 'footer_banner',
        array(
            'default' => get_template_directory_uri() . '/images/image-bot.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_banner',
        array(
            'label' => esc_html__( 'Footer banner left','freelance_writer' ),
            'description' => esc_html__( 'Upload Image','freelance_writer' ),
            'section' => 'footer_settings',
        )
    ));

	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '<p class="text-capitalize">(C) 2019 all rights reserved. freelance writer. designed &amp; developed by<a href="#">Template.net
                   </a></p>',
			'sanitize_callback' => 'wp_kses_post',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','freelance_writer' ),
	      'section' => 'footer_settings',
	   )
	) );	
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','freelance_writer'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','freelance_writer' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','freelance_writer' ),
	         'yes' => esc_html__( 'Yes','freelance_writer' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','freelance_writer'),
		'instagram'=> esc_html__('Instagram','freelance_writer'),
		'twitter'=> esc_html__('Twitter','freelance_writer'),
		'google'=> esc_html__('Google','freelance_writer'),
		'linkedin'=> esc_html__('Linkedin','freelance_writer'),
		'pinterest'=> esc_html__('Pinterest','freelance_writer')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','freelance_writer' ),
				 'yes' => esc_html__( 'Yes','freelance_writer' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','freelance_writer'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','freelance_writer'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','freelance_writer' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','freelance_writer' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','freelance_writer' ),
	         'yes' => esc_html__( 'Yes','freelance_writer' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','freelance_writer' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','freelance_writer' ),
	         'yes' => esc_html__( 'Yes','freelance_writer' ),
	      )
	   )
	);
}




















/**
 * Theme options in the Customizer js
 * @package freelance_writer
 */

function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'freelance_writer_customize_admin_menu_hide', 999 );
function freelance_writer_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="freelance_writer-theme-options"><span>'.esc_html__( 'freelance_writer','freelance_writer' ).'</span>'.esc_html__( 'Theme Options','freelance_writer' ).'</div>',
         '<div class="freelance_writer-theme-options"><span>'.esc_html__( 'freelance_writer','freelance_writer' ).'</span>'.esc_html__( 'Theme Options','freelance_writer' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}