<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
     
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
    $classAdminBar = is_admin_bar_showing() ? 'adminbar':'';
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
        <header class="header-effect-shrink" id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
          <div class="header-body header-body-bottom-border-fixed">
            <div class="header-container container">
              <div class="header-row">
                <div class="header-column">
                  <div class="header-row">
                    <div class="header-logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <?php
                                $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                if ($logo_header && $logo_header!=''):
                                    echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                else:
                                    echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                endif;
                            ?>
                        </a>

                    </div>
                  </div>
                </div>
                <div class="header-column justify-content-end">
                  <div class="header-row">
                    <div class="open-menu-mobile d-xl-none">
                      <div class="d-flex">
                        <div class="fa fab fa-bars"></div>
                      </div>
                    </div>
                    <div class="wrap-menu hide-menu">
                      <div class="close-menu-mobile"><i></i></div>
							<nav class="">
								<?php
									wp_nav_menu(array(
										'theme_location'    => 'primary',
										'menu_class'        => 'nav',
										'container'         => 'ul',
										'menu_id'           => 'menu-main-menu',
								))
								?>
                          </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div id="main" class="wrapper">
				
            

                    
        
       
