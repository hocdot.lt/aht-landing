<?php
$theme = wp_get_theme();
define('CHARITY_VERSION', $theme->get('Version'));
define('CHARITY_LIB', get_template_directory() . '/inc');
define('CHARITY_ADMIN', CHARITY_LIB . '/admin');
define('CHARITY_PLUGINS', CHARITY_LIB . '/plugins');
define('CHARITY_FUNCTIONS', CHARITY_LIB . '/functions');
define('CHARITY_CSS', get_template_directory_uri() . '/css');
define('CHARITY_JS', get_template_directory_uri() . '/js');

require_once(CHARITY_ADMIN . '/functions.php');
require_once(CHARITY_FUNCTIONS . '/functions.php');
require_once(CHARITY_PLUGINS . '/functions.php');
require_once(CHARITY_LIB. '/customizer.php');
// Set up the content width value based on the theme's design and stylesheet.
if (!isset($content_width)) {
    $content_width = 1140;
}
if (!function_exists('freelance_writer_setup')) {
    /**
     *
     * freelance_writer default setup
     * Registers support for various WordPress features.
     *
     * @since freelance_writer 1.0
     */
    function freelance_writer_setup() {
        /*
         * Make freelance_writer available for translation.
         *
         * Translations can be added to the /languages/ directory.
         */
        load_theme_textdomain('freelance_writer', get_template_directory() . '/languages');

        // Add theme style editor support
        add_editor_style( array( 'style.css' ) );

        add_theme_support( 'title-tag' );

        // Add RSS feed links to <head> for posts and comments.
        add_theme_support('automatic-feed-links');

        // register menu location
        register_nav_menus( array(
            'primary' => esc_html__('Primary Menu', 'freelance_writer'),
            'footer' => esc_html__('Footer Menu', 'freelance_writer'),
        ));
        
        add_theme_support( 'custom-header' );

        // This theme allows users to set a custom background.
        add_theme_support( 'custom-background' );
        
        add_theme_support( 'post-formats', array(
                'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
        ) );
        // Enable support for Post Thumbnails, and declare two sizes.
        add_theme_support( 'post-thumbnails' );
        add_image_size('galley-img-2', 846,451, false);
        add_image_size('galley-img-3', 450,358, false);
        add_image_size('galley-img-4', 180,179, true);
    }
}
add_action('after_setup_theme', 'freelance_writer_setup');

/**
 *
 * Enqueue style file for backend
 *
 * @since freelance_writer 1.0
 */
add_action('admin_enqueue_scripts', 'CHARITY_ADMIN_scripts_css');
function CHARITY_ADMIN_scripts_css() {
    wp_enqueue_style('CHARITY_ADMIN_css', get_template_directory_uri() . '/inc/admin/css/admin.css', false);
    wp_enqueue_style('freelance_writer_select_css', get_template_directory_uri() . '/inc/admin/css/select2.min.css', false);
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css?ver=' . CHARITY_VERSION);
	wp_enqueue_script('select-js', get_template_directory_uri() . '/inc/admin/js/select2.min.js', array('jquery'), CHARITY_VERSION, true);
}

/**
 *
 * Setup default google font for Mavikmover
 *
 * @since Mavikmover 1.0
 */
function freelance_writer_fonts_url() {
    $font_url = '';
    $fonts     = array();
    $subsets   = 'latin,latin-ext';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    
    $body_font = esc_html(get_theme_mod('google_font','Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i'));
	$google_font_h = get_theme_mod( 'google_font_h','Oswald:200,300,400,500,600,700' );
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'freelance_writer' ) ) {
		if(!empty($body_font) || !empty($google_font_h)){
		    if($body_font){
                $fonts[] = $body_font;
            }
            if($google_font_h){
                $fonts[] = $google_font_h;
            }
            $fonts[] = 'Montserrat'.':100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i';
		}else{
            $fonts[] = 'Montserrat'.':100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i';
        }
    }
    /*
     * Translators: To add an additional character subset specific to your language,
     * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
     */
    $subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'modern' );

    if ( 'cyrillic' == $subset ) {
        $subsets .= ',cyrillic,cyrillic-ext';
    } elseif ( 'greek' == $subset ) {
        $subsets .= ',greek,greek-ext';
    } elseif ( 'devanagari' == $subset ) {
        $subsets .= ',devanagari';
    } elseif ( 'vietnamese' == $subset ) {
        $subsets .= ',vietnamese';
    }

    if ( $fonts ) {
        $font_url = add_query_arg( array(
            'family' => urlencode(implode( '|', $fonts )),
            'subset' => urlencode( $subsets ),
        ), '//fonts.googleapis.com/css' );
    }

    return $font_url;
}

/**
 * Output CSS in document head.
 */
function freelance_writer_google_fonts_css() {
	$font_size 	= get_theme_mod( 'font_size', '18' ); 
	$font_size 	= $font_size . 'px';
    // $a=get_theme_mod( 'google_font');
    // var_dump($a);
    // die();
	if(!empty( get_theme_mod( 'google_font','Roboto, sans-serif' ) )){
		$body_fonts = get_theme_mod( 'google_font','Montserrat, sans-serif');
		$body_font = explode(":", $body_fonts);
	}else{
        $body_fonts = 'Roboto, Helvetica, sans-serif';
        $body_font = explode(":", $body_fonts);
    }
	if(!empty( get_theme_mod( 'google_font_h','Montserrat, sans-serif' ) )){
		$google_font_hs = get_theme_mod( 'google_font_h','Montserrat, sans-serif' );
		$google_font_h = explode(":", $google_font_hs);
	}
?>
	<style type='text/css' media='all'>
		body{
			font-family: <?php esc_html_e( $body_font[0] ); ?>;
			font-size: <?php esc_html_e( $font_size ); ?>;
		}
		.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{
			font-family: <?php esc_html_e( $google_font_h[0] ); ?>;
		}
	</style>
<?php 
}
add_action( 'wp_head', 'freelance_writer_google_fonts_css' );

/**
 *
 * Enqueue script & styles
 * Registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since freelance_writer 1.0
 */

function freelance_writer_scripts_js() {

    global $freelance_writer_settings, $wp_query, $wp_styles;
    $freelance_writers_valid_form = '';
    // comment reply
    if ( is_singular() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
    $freelance_writers_suffix  = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

    /** Enqueue Google Fonts */
    wp_enqueue_style( 'freelance-fonts', freelance_writer_fonts_url(), array(), null );

    wp_enqueue_style('freelance_writer-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css?ver=' . CHARITY_VERSION);
    wp_enqueue_style('freelance_writer-carousel', get_template_directory_uri() . '/css/owl.carousel.min.css?ver=' . CHARITY_VERSION);
    wp_enqueue_style('freelance_writer-carousel-theme', get_template_directory_uri() . '/css/owl.theme.default.min.css?ver=' . CHARITY_VERSION);
    wp_enqueue_style('freelance_writer-theme-css', get_template_directory_uri() . '/css/theme.css?ver=' . CHARITY_VERSION);
    wp_enqueue_style('freelance_writer-main-css', get_template_directory_uri() . '/css/main.min.css?ver=' . CHARITY_VERSION);
    wp_enqueue_style('freelance_writer-awsome', get_template_directory_uri() . '/css/Awesome.min.css?ver=' . CHARITY_VERSION);
     wp_enqueue_style('freelance_writer-theme-default', get_template_directory_uri() . '/css/theme_default.min.css?ver=' . CHARITY_VERSION);



    wp_deregister_style( 'freelance_writer-style' );
    wp_register_style( 'freelance_writer-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'freelance_writer-style' );      

  
     wp_enqueue_script('freelance_writer-theme', get_template_directory_uri() . '/js/theme.js', array('jquery'), CHARITY_VERSION, true);
       wp_enqueue_script('freelance_writer-main-dist', get_template_directory_uri() . '/js/main-dist.js', array('jquery'), CHARITY_VERSION, true);
    wp_enqueue_script('freelance_writer-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), CHARITY_VERSION, true);
    wp_enqueue_script('freelance_writer-theme-init', get_template_directory_uri() . '/js/theme.init.js', array('jquery'), CHARITY_VERSION, true);
    wp_enqueue_script('freelance_writer-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), CHARITY_VERSION, true);
}
add_action('wp_enqueue_scripts', 'freelance_writer_scripts_js');

// fix validator.w3.org attribute css and js

add_filter('style_loader_tag', 'codeless_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'codeless_remove_type_attr', 10, 2);
function codeless_remove_type_attr($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}
function pietergoosen_comments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
        if ( 'div' == $args['style'] ) {
            $tag = 'div';
            $add_below = 'comment';
        } else {
            $tag = 'li';
            $add_below = 'div-comment';
        }
    ?>
    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
        <p><?php _e( 'Pingback:', 'pietergoosen' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'pietergoosen' ), '<span class="edit-link">', '</span>' ); ?></p>
    <?php
            break;
        default :
        global $post;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
                <footer class="comment-meta">
                    <div class="comment-author vcard">
                        <?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
                    </div><!-- .comment-author -->

                    <?php if ( '0' == $comment->comment_approved ) : ?>
                    <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'pietergoosen' ); ?></p>
                    <?php endif; ?>
                </footer><!-- .comment-meta -->

                <div class="comment-content">
					<?php printf( __( '%s' ), sprintf( '<h4 class="name">%s</h4>', get_comment_author_link() ) ); ?>
                    <?php comment_text(); ?>
					<ul class="social-author social-share">
						<?php 
							$comment = get_comment( );
							$comment_author_id = $comment -> user_id; 
						?>
						<?php if(get_user_meta( $comment_author_id, 'twitter', true )){ ?>
							<li><a class="tw" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'twitter', true ); ?>"><i class="fab fa-twitter-square"></i></a></li>
						<?php } ?>
						<?php if(get_user_meta( $comment_author_id, 'facebook', true )){ ?>
							<li><a class="fb" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'facebook', true ); ?>"><i class="fab fa-facebook-square"></i></a></li>
						<?php } ?>
						<?php if(get_user_meta( $comment_author_id, 'instagram', true )){ ?>
							<li><a class="inta" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'instagram', true ); ?>"><i class="fab fa-instagram"></i></a></li>
						<?php } ?>
						<?php if(get_user_meta( $comment_author_id, 'google', true )){ ?>
							<li><a class="gg" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'google', true ); ?>"><i class="fab fa-google-plus"></i></a></li>
						<?php } ?>
						<?php if(get_user_meta( $comment_author_id, 'linkedin', true )){ ?>
							<li><a class="linkedin" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'linkedin', true ); ?>"><i class="fab fa-linkedin"></i></a></li>
						<?php } ?>
					</ul>
                </div><!-- .comment-content -->

                <div class="reply">
                    <?php comment_reply_link( array_merge( $args, array( 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                </div><!-- .reply -->
            </article><!-- .comment-body -->
    <?php
        break;
    endswitch; 
}

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="facebook"><?php _e("Facebook"); ?></label></th>
        <td>
            <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="twitter"><?php _e("Twitter"); ?></label></th>
        <td>
            <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="instagram"><?php _e("Instagram"); ?></label></th>
        <td>
            <input type="text" name="instagram" id="instagram" value="<?php echo esc_attr( get_the_author_meta( 'instagram', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="google"><?php _e("Google"); ?></label></th>
        <td>
            <input type="text" name="google" id="google" value="<?php echo esc_attr( get_the_author_meta( 'google', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="linkedin"><?php _e("Linkedin"); ?></label></th>
        <td>
            <input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    </table>
<?php }
add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
    update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
    update_user_meta( $user_id, 'instagram', $_POST['instagram'] );
    update_user_meta( $user_id, 'google', $_POST['google'] );
    update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
}

function one_page_scroll(){
    ?>
    <script>
    jQuery(document).ready(function($){
        $('.header-container .mega-menu > li.menu-item.current-menu-item > a,.menu-footer li a').click(function(e){
            e.preventDefault();
            if($('body').hasClass('home')){
                var href=$(this).attr('href');
                href=href.split('#');
                if(href.length>1){
                    var top=0;                    
                    if(!$('header.site-header').hasClass('sticky')){
                        $('header.site-header').addClass('sticky');
                    }
                    if(href[1]==''){
                        top=0;
                    }else if(href[1]=='contact'){
                        top=$('.contact-section-wrapper').offset().top-$('header.site-header').outerHeight();
                    }else{top=$('.section-'+href[1]).offset().top-$('header.site-header').outerHeight();}
                    console.log(href[1]);
                    $('body,html').animate({scrollTop:top},'300',function(){
                        window.location.hash = '#'+href[1];
                    });
                }else{
                    location.href=$(this).attr('href');
                }
            }else{
                location.href=$(this).attr('href');
            }
        });
        
        $(window).scroll(function(){
            if($(window).scrollTop()>70){
                if(!$('header.site-header').hasClass('sticky')){
                    $('header.site-header').addClass('sticky');
                }
                var h=$('header.site-header').outerHeight();
                $('header.site-header').next().css('margin-top',h);
            }else{
                $('header.site-header').removeClass('sticky');
                $('header.site-header').next().css('margin-top','');
                
            }
        });
    
    })
    </script>
    <style>
    body.admin-bar header.site-header.sticky{padding-top:32px;}
    header.site-header.sticky{position:fixed;width:100%;top:0;left:0;background:white;-webkit-box-shadow: 0 0 6px 0 rgba(0,0,0,.3);
        box-shadow: 0 0 6px 0 rgba(0,0,0,.3);z-index:999;}
    .site-header h1.header-logo{-webkit-transition:all .3s ease-in-out;-moz-transition:all .3s ease-in-out;-ms-transition:all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;transition: all .3s ease-in-out;}
    header.site-header.sticky h1.header-logo{padding:20px 0 15px;}
    header.site-header h1.header-logo img{-webkit-transition:all .3s ease-in-out;-moz-transition:all .3s ease-in-out;-ms-transition:all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;transition: all .3s ease-in-out;}
    header.site-header.sticky h1.header-logo img{width:100px;}
    header.site-header.sticky .main-navigation{padding:20px 0;}
    header.site-header.sticky .header-container .mega-menu li a{font-size:20px;}
    @media only screen and ( max-width: 991px ){
        header.site-header.sticky .header-center{z-index:100;}
        header.site-header.sticky .open-menu-mobile{top:27px}
    }
    @media only screen and ( max-width: 782px ){
        body.admin-bar header.site-header.sticky{padding-top:46px;}
    }
    @media only screen and ( max-width: 600px ){
        body.admin-bar header.site-header.sticky{padding-top:0;}
    }
    </style>
    <?php
}
//add_action('wp_head','one_page_scroll');
/**
* Register "Testimonials" post type
* @return [type] [description]
*/
function create_post_type() {
  register_post_type( 'testimonials',
    array(
      'labels' => array(
        'name' => __( 'Testimonials' ),
        'singular_name' => __( 'Testimonial' )
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
}
add_action( 'init', 'create_post_type' );
/**
 * Enables the Excerpt meta box in post type edit screen.
 */
function wpcodex_add_excerpt_support_for_post() {
    add_post_type_support( 'testimonials', 'excerpt' );
    add_post_type_support( 'testimonials', 'thumbnail' );
}
add_action( 'init', 'wpcodex_add_excerpt_support_for_post' );
?>
