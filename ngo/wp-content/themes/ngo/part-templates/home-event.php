<?php
$eventeShow = get_theme_mod('event_show','yes');
$bgEvent = get_theme_mod('bg_event',get_template_directory_uri() . '/images/bg-event.jpg');
$TitleEvent = get_theme_mod('title_event','<h2>Our Upcoming Events</h2>');
$SubtitleEvent = get_theme_mod('sub_title_event','We Believe in Working to Build Better Place to Live');
$eventcate = get_theme_mod('event_category','');
$eventlimit = get_theme_mod('event_number_post',4);
$args = array(
    'posts_per_page'   => $eventlimit,
    'offset'           => 0,
    'cat'         => $eventcate,
    'order' => 'ASC',
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );
?>
<?php if($eventeShow):?>
    <div id="event" style="background-image:url(<?php echo $bgEvent?>)">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="session-title">
                        <?php echo $TitleEvent?>
                        <p class="text-white"><?php echo $SubtitleEvent?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-8">
                    <div class="event-slider">
    <?php foreach ($posts_array as $_post): setup_postdata( $_post );?>
                        <div class="event-item">
                            <div class="thumbnail"><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_post_thumbnail($_post->ID); ?></a>
                            </div>
                            <div class="title">
                                <h3 class="my-3"><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                                </h3>
                            </div>
                        </div>
    <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif?>