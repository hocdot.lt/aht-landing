<?php
$effectsShow = get_theme_mod('effects_show','yes');
$effectsTitle = get_theme_mod('title_effects','<h2>Photo Gallery From Past</h2>');
$effectsSubtitle = get_theme_mod('effects_sub_title','View Our Photo Gallery to Believe');
$effectsTextbt = get_theme_mod('description_bottom','Our aim of Youth Encouragement Services is to improve the welfare of orphaned and vulnerable youth');
$effectsTextLink = get_theme_mod('text_bottom','Donate Now');
$effectsLink = get_theme_mod('link_bottom','#');
$effectsCate = get_theme_mod('effects_category','');
$effectsLimit = get_theme_mod('effects_number_post',8);
$args = array(
    'posts_per_page'   => $effectsLimit,
    'offset'           => 0,
    'cat'         => $effectsCate,
    'order' => 'ASC',
    'post_type'        => 'Gallery',
);
$posts_array = get_posts( $args );
?>
<?php if($effectsShow):?>
    <div id="effects">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="session-title">
                        <?php echo $effectsTitle?>
                        <p><?php echo $effectsSubtitle?></p>
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
    <?php
    // the query
    $the_query = new WP_Query( $args ); ?>
    <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-xl-2 col-md-4">
                    <div class="effects-item hv-scale"><a class="image-link" href="<?php echo the_post_thumbnail_url()?>"><img src="<?php echo the_post_thumbnail_url()?>"/></a></div>
                </div>
        <?php endwhile; ?>
    <?php endif;?>
            </div>
        </div>
    </div>
    <div id="cta">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <h4><?php echo $effectsTextbt?></h4>
                </div>
                <div class="col-lg-2"><a class="btn-home v2 btn" href="<?php echo $effectsLink?>"><?php echo $effectsTextLink?></a>
                </div>
            </div>
        </div>
    </div>
<?php endif?>