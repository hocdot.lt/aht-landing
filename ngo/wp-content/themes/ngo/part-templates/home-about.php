<?php
$AboutShow = get_theme_mod('about_show','yes');
$aboutTitle = get_theme_mod('title_about','<h2>Our Mission &amp; Vision</h2>');
$aboutSubTitle = get_theme_mod('about_sub_title','Our mission is to Eradicate Poverty & Child Abuses');
//donate

$abouttitle1 = get_theme_mod('about_title_bt','Education for all children');
$aboutdesc1 = get_theme_mod('Sub_title_bt','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Non diam phasellus vestibulum lorem sed risus. Nunc scelerisque viverra mauris in aliquam. Adipiscing at in tellus integer feugiat scelerisque.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>');
$abouttextLink1 = get_theme_mod('button_donate_1','Donate Now');
$aboutlink1 = get_theme_mod('link_donate_1','#');
$aboutImages1 = get_theme_mod('image_donate_1',get_template_directory_uri() . '/images/about1.jpg');



$abouttitle2 = get_theme_mod('about_title_bt','Food & Clean Water for Everyone');
$aboutdesc2 = get_theme_mod('Sub_title_bt','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Non diam phasellus vestibulum lorem sed risus. Nunc scelerisque viverra mauris in aliquam. Adipiscing at in tellus integer feugiat scelerisque.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>');
$abouttextLink2 = get_theme_mod('button_donate_1','Donate Now');
$aboutlink2 = get_theme_mod('link_donate_1','#');
$aboutImages2 = get_theme_mod('image_donate_1',get_template_directory_uri() . '/images/about2.jpg');




?>
<?php if($AboutShow):?>
<div id="aboutChaity">
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center">
                <div class="session-title">
                   <?php echo $aboutTitle?>
                    <p><?php echo $aboutSubTitle?></p>
                </div>
            </div>
        </div>
        <div class="row about-item align-items-center flex-md-row-reverse">
            <div class="col-lg-4 media-img p-0"><img src="<?php echo $aboutImages1?>" alt="title"/>
            </div>
            <div class="col-lg-4 media-body mx">
                <h3><?php echo $abouttitle1?></h3>
                <?php echo $aboutdesc1?>
                <a class="btn-home btn my-3" href="<?php echo $aboutlink1?>"><?php echo $abouttextLink1?></a>
            </div>
        </div>
        <div class="row about-item align-items-center">
            <div class="col-lg-4 media-img p-0"><img src="<?php echo $aboutImages2?>" alt="title"/>
            </div>
            <div class="col-lg-4 media-body mx">
                <h3><?php echo $abouttitle2?></h3>
                <?php echo $aboutdesc2?>
               <a class="btn-home btn my-3" href="<?php echo $aboutlink2?>"><?php echo $abouttextLink2?></a>
            </div>
        </div>
    </div>
</div>
<?php endif?>