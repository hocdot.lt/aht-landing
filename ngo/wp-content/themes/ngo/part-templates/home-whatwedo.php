<?php
$Whatwedoshow = get_theme_mod('Whatwedo_show','yes');


$WhatwedoTitle = get_theme_mod('title_whatwedo','<h2>See What Happens When you Help</h2>');
$WhatwedoSubtitle = get_theme_mod('subtitle_whatwedo','What We Do When You Donate Us');
$Whatwedolinkvideo = get_theme_mod('link_video_whatwedo','http://vimeo.com/123123');
$WhatwedoBgvideo = get_theme_mod('poter_video', get_template_directory_uri() . '/images/bg-video.jpg');

$WhatwedoCate = get_theme_mod('whatwedo_category','');

$args = array(
    'posts_per_page'   => 4,
    'offset'           => 0,
    'cat'         => $WhatwedoCate,
    'order' => 'ASC',
    'post_type'        => 'post',
);

$posts_array = get_posts( $args );
$diver = ceil(count($posts_array)/2);
$Whatwedo_1 = array_slice($posts_array,0,$diver);
$Whatwedo_2 = array_slice($posts_array,$diver);
$i = 1;
?>
<?php if($Whatwedoshow):?>
    <div id="whatWeDo">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="session-title">
                        <?php echo $WhatwedoTitle?>
                        <p><?php echo $WhatwedoSubtitle?></p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-xl-4">
    <?php foreach ($Whatwedo_1 as $_post): setup_postdata( $_post );?>
                    <div class="wid">
                        <div class="title">
                            <h3><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                            </h3>
                        </div>
                        <div class="desc">
                            <p><?php echo get_the_excerpt($_post->ID)?></p>
                        </div>
                    </div>
    <?php endforeach; wp_reset_postdata();?>
                </div>
                <div class="col-xl-4"><a class="video-play" href="<?php echo $Whatwedolinkvideo?>">
                        <div class="video-boxes"><img src="<?php echo $WhatwedoBgvideo?>"/>
                        </div></a></div>
            </div>
            <div class="row my-5">

                    <?php foreach ($Whatwedo_1 as $_post): setup_postdata( $_post );?>
                        <div class="col-xl-4">
                                <div class="wid">
                                    <div class="title">
                                        <h3><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                                        </h3>
                                    </div>
                                    <div class="desc">
                                        <p><?php echo get_the_excerpt($_post->ID)?></p>
                                    </div>
                                </div>
                        </div>
                    <?php endforeach; wp_reset_postdata();?>

            </div>
        </div>
    </div>
<?php endif?>