<?php

$BannerShow = get_theme_mod('banner_show','yes');
$slidercategory = get_theme_mod('slider_category','');
$sildernumberpost = get_theme_mod('silder_number_post',3);
$args = array(
   'posts_per_page'   => $sildernumberpost,
   'offset'           => 0,
   'cat'         => $slidercategory,
   'order' => 'ASC',
   'post_type'        => 'testimonial',
);
$posts_array = get_posts( $args );
   ?>
<?php if($BannerShow):?>
    <div id="section-banner">
        <div class="slide-top">
            <?php foreach ($posts_array as $_post): setup_postdata( $_post );?>
                <div class="rate-content text-center">
                    <div class="customer-bg" style="background-image: url('<?php echo get_the_post_thumbnail_url($_post->ID) ?>')">
                        <div class="customer-title">
                            <h2 class="text-capitalize"><?php echo get_the_title($_post->ID) ?></h2>
                            <h1><?php echo get_the_excerpt($_post->ID) ?></h1>
                            <a href="<?php echo get_the_permalink($_post->ID)?>">Donate Now</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
<?php endif?>