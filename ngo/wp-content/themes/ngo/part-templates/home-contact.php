

<?php
$contactShow = get_theme_mod('contact_show','yes');
$contactTitle = get_theme_mod('contact_title','Contact Us for Donations');
$contactSubTitle = get_theme_mod('contact_sub_title','Let’s make a better living place');
$contactForm2 = get_theme_mod('contact_shortcode_2','[contact-form-7 id="5" title="Contact"]');
$contactMail = get_theme_mod('Mail_contact','info@accountancyfirm.com');
$contactInfo = get_theme_mod('info_contact',' Accountancy FIrm LLC
                                    85 Broad Street, 28th Floor
                                    New York, NY 10004
                                    800 - 123 - 4567');
$contactMap = get_theme_mod('contact_map','<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14900.82102572999!2d105.78793714999999!3d20.9844078!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1553070062443" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>');
?>
<?php if($contactShow):?>
    <div id="contact">
        <div class="container">
            <div class="row">
                <div class="col-xl-4">
                    <div class="session-title">
                        <h2><?php echo $contactTitle?></h2>
                        <p><?php echo $contactSubTitle?></p>
                    </div>
                    <?php echo do_shortcode($contactForm2)?>
                </div>
                <div class="col-xl-3 offset-xl-1 px-0">
                    <div class="contact-wrap">
                        <div class="overlay">
                            <ul class="list-group">
                                <li class="list-group-item d-flex"><i class="fas fa-envelope"></i><?php echo $contactMail?></li>
                                <li class="list-group-item d-flex"><i class="fas fa-map-marker-alt"></i>
                                   <?php echo $contactInfo?>
                                </li>
                            </ul>
                            <?php
                            if(function_exists('ngo_social_link')){
                                ngo_social_link();
                            }
                            ?>
                        </div><?php echo $contactMap?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif?>