<?php 
/**
Template Name: Home
*/
get_header();

get_template_part('part-templates/home-banner');
get_template_part('part-templates/home-about');
get_template_part('part-templates/home-whatwedo');
get_template_part('part-templates/home-effects');
get_template_part('part-templates/home-event');
get_template_part('part-templates/home-contact');

get_footer(); ?> 