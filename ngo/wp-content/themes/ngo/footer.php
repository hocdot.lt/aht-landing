
</div> <!-- End main-->
<footer class="footer">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-1">
                <div class="footer-logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                        <?php
                        $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                        if ($logo_header && $logo_header!=''):
                            echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                        else:
                            //bloginfo('name');
                        endif;
                        ?>
                    </a>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="footer-content text-lg-right">
                    <div class="menu-footer">
                        <?php
                        if (has_nav_menu('primary')) {
                            wp_nav_menu(array(
                                    'theme_location' => 'primary',
                                    'menu_class' => 'mega-menu',
                                    'items_wrap' =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                )
                            );
                        }
                        ?>
                    </div>
                    <div class="copyright">
                        <?php
                        $copyright =  get_theme_mod('copyright','<p class="text-center m-0">(C) All Rights Reserved. Charity Theme, Designed & Developed by<a href="#">Template.net</a></p>');;
                        if ( $copyright !='') : ?>
                            <?php echo wp_kses_post( $copyright); ?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>