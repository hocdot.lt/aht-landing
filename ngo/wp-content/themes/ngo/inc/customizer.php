<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'ngo_customize_register' );
function ngo_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'ngo_Customize_Sidebar_Control' ) ) {
	    class ngo_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Baloo Tamma' => 'Baloo Tamma',
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',
                'Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Poppins',
                'Arial:100,200,300,400' => 'Arial',
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',			
				'' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'ngo') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','ngo'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','ngo' ),
	      'description' => esc_html__( 'Upload Logo','ngo' ),
	      'section' => 'general_settings',
	   )
	) );
	
	$wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Arial:100,200,300,400',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Font Main',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'ngo'),
        'description'    => __('Several settings pertaining my theme', 'ngo'),
    ) );
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','ngo'  ),
	      'priority' => 20,
		  'panel'  => 'home_page_setting',
	   )
	);
// shortcode banner

    $wp_customize->add_setting( 'banner_shortcode',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control( 'banner_shortcode',
        array(
            'label' => esc_html__( 'Slider shortcode','ngo' ),
            'section' => 'home_banner_settings',
            'type' => 'text'
        )
    );

	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','ngo' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array(
	         'no' => esc_html__( 'No','ngo' ),
	         'yes' => esc_html__( 'Yes','ngo' ),
            )
	   )
	);
	// End banner section
    //About Section
    $wp_customize->add_section( 'about_settings',
        array(
            'title' => esc_html__( 'About section','ngo'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    $wp_customize->add_setting( 'title_about',
        array(
            'default' => '<h2>Our Mission &amp; Vision</h2>',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'title_about',
        array(
            'label' => esc_html__( 'Title','ngo' ),
            'section' => 'about_settings',
        )
    ) );
    //subTitle
    $wp_customize->add_setting( 'about_sub_title',
        array(
            'default' => 'Our mission is to Eradicate Poverty & Child Abuses',
        )
    );
    $wp_customize->add_control( 'about_sub_title',
        array(
            'label' => esc_html__( 'Title Sub','ngo' ),
            'section' => 'about_settings',
            'type' => 'text'
        )
    );




    // bottom
    //Title
    $wp_customize->add_setting( 'about_title_bt',
        array(
            'default' => 'Education for all children',
        )
    );
    $wp_customize->add_control( 'about_title_bt',
        array(
            'label' => esc_html__( 'Title Donate 1','ngo' ),
            'section' => 'about_settings',
            'type' => 'text'
        )
    );
    //subTitle
    $wp_customize->add_setting( 'Sub_title_bt',
        array(
            'default' => ' <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Non diam phasellus vestibulum lorem sed risus. Nunc scelerisque viverra mauris in aliquam. Adipiscing at in tellus integer feugiat scelerisque.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'Sub_title_bt',
        array(
            'label' => esc_html__( 'Description Donate 1','ngo' ),
            'section' => 'about_settings',
        )
    ) );
   //button
    $wp_customize->add_setting( 'button_donate_1',
        array(
            'default' => 'Donate Now',
        )
    );
    $wp_customize->add_control( 'button_donate_1',
        array(
            'label' => esc_html__( 'Text Donate 1','ngo' ),
            'section' => 'about_settings',
            'type' => 'text'
        )
    );
    //link button
    $wp_customize->add_setting( 'link_donate_1',
        array(
            'default' => '#',
        )
    );
    $wp_customize->add_control( 'link_donate_1',
        array(
            'label' => esc_html__( 'Link Donate 1','ngo' ),
            'section' => 'about_settings',
            'type' => 'text'
        )
    );
    //images
    $wp_customize->add_setting( 'image_donate_1',
        array(
            'default' => get_template_directory_uri() . '/images/about1.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_donate_1',
        array(
            'label' => esc_html__( 'Images Donnate 1','ngo' ),
            'description' => esc_html__( 'Upload Image','ngo' ),
            'section' => 'about_settings',
        )
    ) );
    // bottom
    //Title
    $wp_customize->add_setting( 'about_title2_bt',
        array(
            'default' => 'Food & Clean Water for Everyone',
        )
    );
    $wp_customize->add_control( 'about_title2_bt',
        array(
            'label' => esc_html__( 'Title Donate 2','ngo' ),
            'section' => 'about_settings',
            'type' => 'text'
        )
    );
    //subTitle
    $wp_customize->add_setting( 'Sub2_title_bt',
        array(
            'default' => ' <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Non diam phasellus vestibulum lorem sed risus. Nunc scelerisque viverra mauris in aliquam. Adipiscing at in tellus integer feugiat scelerisque.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'Sub2_title_bt',
        array(
            'label' => esc_html__( 'Description Donate 2','ngo' ),
            'section' => 'about_settings',
        )
    ) );
   //button
    $wp_customize->add_setting( 'button_donate_2',
        array(
            'default' => 'Donate Now',
        )
    );
    $wp_customize->add_control( 'button_donate_2',
        array(
            'label' => esc_html__( 'Text Donate 2','ngo' ),
            'section' => 'about_settings',
            'type' => 'text'
        )
    );
    //link button
    $wp_customize->add_setting( 'link_donate_2',
        array(
            'default' => '#',
        )
    );
    $wp_customize->add_control( 'link_donate_2',
        array(
            'label' => esc_html__( 'Link Donate 2','ngo' ),
            'section' => 'about_settings',
            'type' => 'text'
        )
    );
    //images
    $wp_customize->add_setting( 'image_donate_2',
        array(
            'default' => get_template_directory_uri() . '/images/about2.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_donate_2',
        array(
            'label' => esc_html__( 'Images Donnate 2','ngo' ),
            'description' => esc_html__( 'Upload Image','ngo' ),
            'section' => 'about_settings',
        )
    ) );
    //Show/Hide session
    $wp_customize->add_setting( 'about_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'about_show',
        array(
            'label' => esc_html__( 'Show Section','ngo' ),
            'section' => 'about_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','ngo' ),
                'yes' => esc_html__( 'Yes','ngo' ),
            )
        )
    );

    //End serice
    //What we do
    $wp_customize->add_section( 'whatwedo_settings',
        array(
            'title' => esc_html__( 'What we do section','ngo'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    //Tile
    $wp_customize->add_setting( 'title_whatwedo',
        array(
            'default' => '<h2>See What Happens When you Help</h2>',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'title_whatwedo',
        array(
            'label' => esc_html__( 'Title','ngo' ),
            'section' => 'whatwedo_settings',
        )
    ) );
    //sub
    $wp_customize->add_setting( 'subtitle_whatwedo',
        array(
            'default' => 'What We Do When You Donate Us',
        )
    );
    $wp_customize->add_control( 'subtitle_whatwedo',
        array(
            'label' => esc_html__( 'Sub title','ngo' ),
            'section' => 'whatwedo_settings',
            'type' => 'text'
        )
    );
    //link video
    $wp_customize->add_setting( 'link_video_whatwedo',
        array(
            'default' => '#',
        )
    );
    $wp_customize->add_control( 'link_video_whatwedo',
        array(
            'label' => esc_html__( 'link video','ngo' ),
            'section' => 'whatwedo_settings',
            'type' => 'text'
        )
    );
    //Bg speakers_settings
    $wp_customize->add_setting( 'poter_video',
        array(
            'default' => get_template_directory_uri() . '/images/bg-video.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'poter_video',
        array(
            'label' => esc_html__( 'Poster Video','ngo' ),
            'description' => esc_html__( 'Upload Poster','ngo' ),
            'section' => 'whatwedo_settings',
        )
    ) );
    //  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }



    $wp_customize->add_setting( 'whatwedo_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('whatwedo_category',
        array(
            'label' => esc_html__( 'Category Post','ngo' ),
            'section' => 'whatwedo_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'whatwedo_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'whatwedo_show',
        array(
            'label' => esc_html__( 'Show Section','ngo' ),
            'section' => 'whatwedo_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','ngo' ),
                'yes' => esc_html__( 'Yes','ngo' ),
            )
        )
    );
    //end SPEAKING
    // //effects
    $wp_customize->add_section( 'effects_settings',
        array(
            'title' => esc_html__( 'effects section','ngo'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    //Tile
    $wp_customize->add_setting( 'title_effects',
        array(
            'default' => '<h2>Photo Gallery From Past</h2>',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'title_effects',
        array(
            'label' => esc_html__( 'Title','ngo' ),
            'section' => 'effects_settings',
        )
    ) );
    //SubTile
    $wp_customize->add_setting( 'effects_sub_title',
        array(
            'default' => esc_html__('View Our Photo Gallery to Believe','ngo'),
        )
    );
    $wp_customize->add_control('effects_sub_title',
        array(
            'label' => esc_html__( 'Sub Title','ngo' ),
            'section' => 'effects_settings',
            'type' => 'textarea',
        )
    );   //SubTile
    $wp_customize->add_setting( 'effects_sub_title',
        array(
            'default' => esc_html__('View Our Photo Gallery to Believe','ngo'),
        )
    );
    $wp_customize->add_control('effects_sub_title',
        array(
            'label' => esc_html__( 'Sub Title','ngo' ),
            'section' => 'effects_settings',
            'type' => 'textarea',
        )
    );
    //description botoom
    $wp_customize->add_setting( 'description_bottom',
        array(
            'default' => esc_html__('Our aim of Youth Encouragement Services is to improve the welfare of orphaned and vulnerable youth','ngo'),
        )
    );
    $wp_customize->add_control('description_bottom',
        array(
            'label' => esc_html__( 'Description Bottom','ngo' ),
            'section' => 'effects_settings',
            'type' => 'text',
        )
    );
    //texxt link donate
    $wp_customize->add_setting( 'text_bottom',
        array(
            'default' => esc_html__('Donate Now','ngo'),
        )
    );
    $wp_customize->add_control('text_bottom',
        array(
            'label' => esc_html__( 'Text link','ngo' ),
            'section' => 'effects_settings',
            'type' => 'text',
        )
    );
    //link donate
    $wp_customize->add_setting( 'link_bottom',
        array(
            'default' => esc_html__('#','ngo'),
        )
    );
    $wp_customize->add_control('link_bottom',
        array(
            'label' => esc_html__( 'Link Donate','ngo' ),
            'section' => 'effects_settings',
            'type' => 'text',
        )
    );

    //  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

    $wp_customize->add_setting( 'effects_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('effects_category',
        array(
            'label' => esc_html__( 'Category Post','ngo' ),
            'section' => 'effects_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

    //Number post
    $wp_customize->add_setting( 'effects_number_post',
        array(
            'default' => esc_html__('','ngo'),
        )
    );
    $wp_customize->add_control('effects_number_post',
        array(
            'label' => esc_html__( 'Number Post','ngo' ),
            'section' => 'effects_settings',
            'type' => 'text',
        )
    );

    //list box
    //Show/Hide session
    $wp_customize->add_setting( 'effects_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'effects_show',
        array(
            'label' => esc_html__( 'Show Section','ngo' ),
            'section' => 'effects_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','ngo' ),
                'yes' => esc_html__( 'Yes','ngo' ),
            )
        )
    );
    //end SCHEDULE
    //event
    $wp_customize->add_section( 'event_settings',
        array(
            'title' => esc_html__( 'event section','ngo'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    //img blog
    $wp_customize->add_setting( 'bg_event',
        array(
            'default' => get_template_directory_uri() . '/images/bg-event.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_event',
        array(
            'label' => esc_html__( 'Background eEent','ngo' ),
            'description' => esc_html__( 'Upload Background','ngo' ),
            'section' => 'event_settings',
        )
    ) );
    //Tile
    $wp_customize->add_setting( 'title_event',
        array(
            'default' => '<h2>Our Upcoming Events</h2>',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'title_event',
        array(
            'label' => esc_html__( 'Title','ngo' ),
            'section' => 'event_settings',
        )
    ) );
    //Tile
    $wp_customize->add_setting( 'sub_title_event',
        array(
            'default' => esc_html__('We Believe in Working to Build Better Place to Live','ngo'),
        )
    );
    $wp_customize->add_control('sub_title_event',
        array(
            'label' => esc_html__( 'Sub Title','ngo' ),
            'section' => 'event_settings',
            'type' => 'text',
        )
    );

    //  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

    $wp_customize->add_setting( 'event_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('event_category',
        array(
            'label' => esc_html__( 'Category Post','ngo' ),
            'section' => 'event_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

    //Number post
    $wp_customize->add_setting( 'event_number_post',
        array(
            'default' => esc_html__('4','ngo'),
        )
    );
    $wp_customize->add_control('event_number_post',
        array(
            'label' => esc_html__( 'Number Post','ngo' ),
            'section' => 'event_settings',
            'type' => 'text',
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'event_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'event_show',
        array(
            'label' => esc_html__( 'Show Section','ngo' ),
            'section' => 'event_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','ngo' ),
                'yes' => esc_html__( 'Yes','ngo' ),
            )
        )
    );

    //End blog


    //contact Session
    $wp_customize->add_section( 'contact_settings',
        array(
            'title' => esc_html__( 'Contact section','ngo'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    //img whychose

//Conta
    //Tile
    $wp_customize->add_setting( 'contact_title',
        array(
            'default' => esc_html__('Contact Us for Donations','ngo'),
        )
    );
    $wp_customize->add_control('contact_title',
        array(
            'label' => esc_html__( 'Title Contact','ngo' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    //SubTile
    $wp_customize->add_setting( 'contact_sub_title',
        array(
            'default' => esc_html__('Let’s make a better living place','ngo'),
        )
    );
    $wp_customize->add_control('contact_sub_title',
        array(
            'label' => esc_html__( 'Sub Title Contact','ngo' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    //Mail
    $wp_customize->add_setting( 'Mail_contact',
        array(
            'default' => esc_html__('info@accountancyfirm.com','ngo'),
        )
    );
    $wp_customize->add_control('Mail_contact',
        array(
            'label' => esc_html__( 'Mail Contact','ngo' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
   //info
    $wp_customize->add_setting( 'info_contact',
        array(
            'default' => 'Accountancy FIrm LLC
                                    85 Broad Street, 28th Floor
                                    New York, NY 10004
                                    800 - 123 - 4567',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'info_contact',
        array(
            'label' => esc_html__( 'Info Contact','ngo' ),
            'section' => 'contact_settings',
        )
    ) );


    //
    //Contact shortcode2
    $wp_customize->add_setting( 'contact_shortcode_2',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control( 'contact_shortcode_2',
        array(
            'label' => esc_html__( 'Contact shortcode Form','ngo' ),
            'section' => 'contact_settings',
            'type' => 'text'
        )
    );
    //Map
    $wp_customize->add_setting( 'contact_map',
        array(
            'default' => esc_html__('','ngo'),
        )
    );
    $wp_customize->add_control('contact_map',
        array(
            'label' => esc_html__( 'Contact Map','ngo' ),
            'section' => 'contact_settings',
            'type' => 'textarea',
        )
    );
    //Show/Hide session
    $wp_customize->add_setting( 'contact_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'contact_show',
        array(
            'label' => esc_html__( 'Show Section','ngo' ),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','ngo' ),
                'yes' => esc_html__( 'Yes','ngo' ),
            )
        )
    );

    //End Donate


	// End Home Options
	
	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','ngo'  ),
	      'priority' => 35, 
	   )
	);

	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '<p>(C) 2018. AllRights Reserved. Divine Makeup Artist. Designed by <a href="#">Template.net</a></p>',
			'sanitize_callback' => 'wp_kses_post',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','ngo' ),
	      'section' => 'footer_settings',
	   )
	) );

	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','ngo'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','ngo' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','ngo' ),
	         'yes' => esc_html__( 'Yes','ngo' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','ngo'),
		'instagram'=> esc_html__('Instagram','ngo'),
		'twitter'=> esc_html__('Twitter','ngo'),
		'google'=> esc_html__('Google','ngo'),
		'linkedin'=> esc_html__('Linkedin','ngo')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','ngo' ),
				 'yes' => esc_html__( 'Yes','ngo' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','ngo'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','ngo'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','ngo' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','ngo' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','ngo' ),
	         'yes' => esc_html__( 'Yes','ngo' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','ngo' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','ngo' ),
	         'yes' => esc_html__( 'Yes','ngo' ),
	      )
	   )
	);
}




















/**
 * Theme options in the Customizer js
 * @package ngo
 */

function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'ngo_customize_admin_menu_hide', 999 );
function ngo_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="ngo-theme-options"><span>'.esc_html__( 'ngo','ngo' ).'</span>'.esc_html__( 'Theme Options','ngo' ).'</div>',
         '<div class="ngo-theme-options"><span>'.esc_html__( 'ngo','ngo' ).'</span>'.esc_html__( 'Theme Options','ngo' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}