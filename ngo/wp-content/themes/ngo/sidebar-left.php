<?php
    $ngo_sidebar_left = ngo_get_sidebar_left();
?>
<?php if ($ngo_sidebar_left && $ngo_sidebar_left != "none" && is_active_sidebar($ngo_sidebar_left)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left-sidebar active-sidebar"><!-- main sidebar -->
        <?php dynamic_sidebar($ngo_sidebar_left); ?>
    </div>
<?php endif; ?>


