(function ($) {
    $(".open-menu-mobile").click(function(e) {
        e.preventDefault();
        $('.overlay').toggleClass('overlay-menu');
        $('header').toggleClass("open");
        $(this).find('.openMenu').toggleClass("open");
    });
    $('.overlay').click(function(e) {
        $(".open-menu-mobile").click()
    });
    $('.hide-menu').click(function(e) {
        $(".open-menu-mobile").click()
    });
    $('.menu-container .mega-menu > li.menu-item.current-menu-item > a,.menu-footer li a').click(function(e){
        e.preventDefault();
        if($('body').hasClass('home')){
            var href=$(this).attr('href');
            href=href.split('#');
            if(href.length>1){
                var top=0;
                if(href[1]==''){
                    top=0;
                }else{
                    top = $('#section-'+href[1]).offset().top;
                }
                $('body,html').animate({scrollTop:top},'300',function(){
                    window.location.hash = '#'+href[1];
                });
            }else{
                location.href=$(this).attr('href');
            }
        }else{
            location.href=$(this).attr('href');
        }
    });
    menuScroll();
    $(window).scroll(function(){menuScroll();});

    function menuScroll(){
        if($('body').hasClass('home')){
            var sections = {};
            $('#main div[id*="section"]').each(function( index , el) {
                var key = $(el).attr('id');
                key = key.split('-')[1]
                if(key){
                    sections[key] = el.offsetTop;
                }
            });
            //console.log(sections);
            for (var i in sections) {
                if (sections[i] < $(window).scrollTop() + 10) {
                    $('.mega-menu li').removeClass('active');
                    if($('.mega-menu li a[href*=#' + i + ']').length){
                        $('.mega-menu li a[href*=#' + i + ']').closest('li').addClass('active');
                    }else{
                        $('.mega-menu li a[href$=#]').closest('li').addClass('active');
                    }
                }
            }
        }
    }
})(jQuery);