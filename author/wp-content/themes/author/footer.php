
</div> <!-- End main-->
<?php
    $logo_footer = get_theme_mod('logo_footer',get_template_directory_uri() . '/images/logo-footer.png');
    $copyright = get_theme_mod('copyright','<p>(C) All Rights Reserved Amilla - Writer & Editor. Designed and Developed by <a href="#">Template.net</a></p>');
?>
<footer class="footer">
    <div class="container ml-xl-0">
        <div class="row">
            <div class="col-xl-6 col-8 offset-xl-2">
                <div class="d-flex flex-column flex-lg-row">
                    <div class="footer-logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <?php
                            if ($logo_foooter && $logo_foooter!=''):
                                echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_footer)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                            else:
                                echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo-footer.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                            endif;
                            ?>
                        </a>
                    </div>
                    <div class="footer-content">
                        <div class="menu-footer">
                            <?php if (has_nav_menu('footer')) : ?>
                                <div class="menu-footer">
                                    <?php wp_nav_menu(array('theme_location' => 'footer','menu_class' => 'mega-menu',)); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="copyright">
                            <?php echo apply_filters( 'the_content', $copyright); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>