<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
$author_sidebar_left = author_get_sidebar_left();
$author_sidebar_right = author_get_sidebar_right();
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site ">
        <header class="header">
            <div class="header-wrapper">
                <div class="container header-container">
                    <div class="header-column flex-column justify-content-center h-100">
                        <div class="header-row justify-content-lg-center justify-content-start py-5">
                            <?php if (is_front_page()) : ?>
                                <h1 class="logo text-md-center">
                                    <?php else: ?>
                                    <h2 class="logo text-md-center">
                                        <?php endif; ?>
                                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                            <?php
                                            $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                            if ($logo_header && $logo_header!=''):
                                                echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                            else:
                                                echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                            endif;
                                            ?>
                                        </a>
                                        <?php if (is_front_page()) : ?>
                                </h1>
                            <?php else: ?>
                                </h2>
                            <?php endif; ?>
                            <?php
                            $logo_subtitle = get_theme_mod('logo_subtitle','<h3>Amellia<span>Writer & Editor</span></h3>');
                            ?>
                            <?php if ( $logo_subtitle !='') :?>
                                <div class="logo-subtitle text-center">
                                    <?php echo apply_filters( 'the_content', $logo_subtitle); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="header-row justify-content-end">
                            <?php
                            if(function_exists('author_header_menu')){
                                author_header_menu();
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="main" class="wrapper">
				
            

                    
        
       
