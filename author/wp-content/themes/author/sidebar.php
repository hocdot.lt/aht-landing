<?php
    $author_sidebar_left = author_get_sidebar_left();
    $author_sidebar_right = author_get_sidebar_right();

    ?>
<?php if ($author_sidebar_left && is_active_sidebar($author_sidebar_left)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left-sidebar "><!-- main sidebar -->
        <?php dynamic_sidebar($author_sidebar_left); ?>
    </div>
<?php endif; ?>
<?php if ($author_sidebar_right && is_active_sidebar($author_sidebar_right)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 right-sidebar"><!-- main sidebar -->
        <?php dynamic_sidebar($author_sidebar_right); ?>
    </div>
<?php endif; ?>





