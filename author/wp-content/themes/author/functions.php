<?php
$theme = wp_get_theme();
define('author_VERSION', $theme->get('Version'));
define('author_LIB', get_template_directory() . '/inc');
define('author_ADMIN', author_LIB . '/admin');
define('author_PLUGINS', author_LIB . '/plugins');
define('author_FUNCTIONS', author_LIB . '/functions');
define('author_CSS', get_template_directory_uri() . '/css');
define('author_JS', get_template_directory_uri() . '/js');

require_once(author_ADMIN . '/functions.php');
require_once(author_FUNCTIONS . '/functions.php');
require_once(author_PLUGINS . '/functions.php');
require_once(author_LIB. '/customizer.php');
// Set up the content width value based on the theme's design and stylesheet.
if (!isset($content_width)) {
    $content_width = 1140;
}
if (!function_exists('author_setup')) {
    /**
     *
     * author default setup
     * Registers support for various WordPress features.2
     *
     * @since author 1.0
     */
    function author_setup() {
        /*
         * Make author available for translation.
         *
         * Translations can be added to the /languages/ directory.
         */
        load_theme_textdomain('author', get_template_directory() . '/languages');

        // Add theme style editor support
        add_editor_style( array( 'style.css' ) );


        add_theme_support( 'title-tag' );

        // Add RSS feed links to <head> for posts and comments.
        add_theme_support('automatic-feed-links');

        // register menu location
        register_nav_menus( array(
            'primary' => esc_html__('Primary Menu', 'author'),
            'footer' => esc_html__('Footer Menu', 'author'),
        ));
        
        add_theme_support( 'custom-header' );

        // This theme allows users to set a custom background.
        add_theme_support( 'custom-background' );
        
        add_theme_support( 'post-formats', array(
                'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
        ) );
        // Enable support for Post Thumbnails, and declare two sizes.
        add_theme_support( 'post-thumbnails' );
        add_image_size('writing', 370, 370, true);
        add_image_size('book', 276, 413, true);
    }
}
add_action('after_setup_theme', 'author_setup');

/**
 *
 * Enqueue style file for backend
 *
 * @since author 1.0
 */
add_action('admin_enqueue_scripts', 'author_admin_scripts_css');
function author_admin_scripts_css() {
    wp_enqueue_style('author_admin_css', get_template_directory_uri() . '/inc/admin/css/admin.css', false);
    wp_enqueue_style('author_select_css', get_template_directory_uri() . '/inc/admin/css/select2.min.css', false);
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/fontawesome-all.min.css?ver=' . author_VERSION);
	
	wp_enqueue_script('select-js', get_template_directory_uri() . '/inc/admin/js/select2.min.js', array('jquery'), author_VERSION, true);
}

/**
 *
 * Setup default google font for Mavikmover
 *
 * @since Mavikmover 1.0
 */
function author_fonts_url() {
    $font_url = '';
    $fonts     = array();
    $subsets   = 'latin,latin-ext';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    
    $body_font = esc_html(get_theme_mod('google_font','Old Standard TT:400,400i,700'));
	$google_font_h = get_theme_mod( 'google_font_h','' );
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'author' ) ) {
		if(!empty($body_font) || !empty($google_font_h)){
            if($body_font){
                $fonts[] = $body_font;
            }
            if($google_font_h){
                $fonts[] = $google_font_h;
            }
            $fonts[] = '';
		}else{
			$fonts[] = '';
		}
    }
    /*
     * Translators: To add an additional character subset specific to your language,
     * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
     */
    $subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'modern' );

    if ( 'cyrillic' == $subset ) {
        $subsets .= ',cyrillic,cyrillic-ext';
    } elseif ( 'greek' == $subset ) {
        $subsets .= ',greek,greek-ext';
    } elseif ( 'devanagari' == $subset ) {
        $subsets .= ',devanagari';
    } elseif ( 'vietnamese' == $subset ) {
        $subsets .= ',vietnamese';
    }

    if ( $fonts ) {
        $font_url = add_query_arg( array(
            'family' => urlencode( implode( '|', $fonts ) ),
            'subset' => urlencode( $subsets ),
        ), '//fonts.googleapis.com/css' );
    }

    return $font_url;
}

/**
 * Output CSS in document head.
 */
function author_google_fonts_css() {
	$font_size 	= get_theme_mod( 'font_size', '18' ); 
	$font_size 	= $font_size . 'px';
	if(!empty( get_theme_mod( 'google_font','Old Standard TT:400,400i,700') )){
		$body_fonts = get_theme_mod( 'google_font','Old Standard TT:400,400i,700' );
		$body_font = explode(":", $body_fonts);
	}
	if(!empty( get_theme_mod( 'google_font_h','Old Standard TT:400,400i,700') )){
		$google_font_hs = get_theme_mod( 'google_font_h','Old Standard TT:400,400i,700' );
		$google_font_h = explode(":", $google_font_hs);
	}else{
        $google_font_h[] = 'Old Standard TT';
    }
?>
	<style type='text/css' media='all'>
		body{
			font-family: "<?php esc_html_e( $body_font[0] ); ?>"; 
			font-size: <?php esc_html_e( $font_size ); ?>;
		}
		.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{
			font-family: "<?php esc_html_e( $google_font_h[0] ); ?>";
			font-weight: bold;
		}
	</style>
<?php 
}
add_action( 'wp_head', 'author_google_fonts_css' );

/**
 *
 * Enqueue script & styles
 * Registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since author 1.0
 */

function author_scripts_js() {

    global $author_settings, $wp_query, $wp_styles;
    $author_valid_form = '';
    // comment reply
    if ( is_singular() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    $author_suffix  = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

    /** Enqueue Google Fonts */
    wp_enqueue_style( 'author-fonts', author_fonts_url(), array(), null );

   wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/fontawesome-all.min.css?ver=' . author_VERSION);

    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/plugin/bootstrap.min.css?ver=' . author_VERSION);

    //wp_enqueue_style('author-animate', get_template_directory_uri() . '/css/animate.min.css?ver=' . author_VERSION);
    //wp_enqueue_style('author-magnific', get_template_directory_uri() . '/css/magnific-popup.css?ver=' . author_VERSION);
    wp_enqueue_style('slick', get_template_directory_uri() . '/css/slick.css?ver=' . author_VERSION);

    wp_enqueue_style('author-theme', get_template_directory_uri() . '/css/theme'.$author_suffix.'.css?ver=' . author_VERSION);
	
    wp_enqueue_style('author-style-css', get_template_directory_uri() . '/css/style.css?ver=' . author_VERSION);

    wp_deregister_style( 'author-style' );
    wp_register_style( 'author-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'author-style' );

    // Enqueue script 
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), author_VERSION, true);

    wp_enqueue_script( 'jquery-ui-autocomplete' );

    if( post_type_supports( get_post_type(), 'comments' ) ) {
        if( comments_open() ) {
            $author_valid_form = 'yes';
            wp_enqueue_script('validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array('jquery'), author_VERSION);
        }
    }

    wp_enqueue_script('author-script', get_template_directory_uri() . '/js/un-minify/author_theme.min.js', array('jquery'), author_VERSION, true);

    wp_localize_script('author-script', 'author_params', array(
        'ajax_url' => esc_js(admin_url( 'admin-ajax.php' )),
        'ajax_loader_url' => esc_js(str_replace(array('http:', 'https'), array('', ''), author_CSS . '/images/ajax-loader.gif')),
        'author_valid_form' => esc_js($author_valid_form),
        'author_search_no_result' => esc_js(__('Search no result','author')),
        'author_like_text' => esc_js(__('Like','author')),
        'author_unlike_text' => esc_js(__('Unlike','author')),
        'request_error' => esc_js(__('The requested content cannot be loaded.<br/>Please try again later.', 'author')),
    ));
}
add_action('wp_enqueue_scripts', 'author_scripts_js');

// fix validator.w3.org attribute css and js

add_filter('style_loader_tag', 'codeless_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'codeless_remove_type_attr', 10, 2);
function codeless_remove_type_attr($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}
function pietergoosen_comments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
        if ( 'div' == $args['style'] ) {
            $tag = 'div';
            $add_below = 'comment';
        } else {
            $tag = 'li';
            $add_below = 'div-comment';
        }
    ?>
    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
        <p><?php _e( 'Pingback:', 'pietergoosen' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'pietergoosen' ), '<span class="edit-link">', '</span>' ); ?></p>
    <?php
            break;
        default :
        global $post;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
                <footer class="comment-meta">
                    <div class="comment-author vcard">
                        <?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
                    </div><!-- .comment-author -->

                    <?php if ( '0' == $comment->comment_approved ) : ?>
                    <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'pietergoosen' ); ?></p>
                    <?php endif; ?>
                </footer><!-- .comment-meta -->

                <div class="comment-content">
					<?php printf( __( '%s' ), sprintf( '<h4 class="name">%s</h4>', get_comment_author_link() ) ); ?>
                    <?php comment_text(); ?>
					<ul class="social-author social-share">
						<?php 
							$comment = get_comment( );
							$comment_author_id = $comment -> user_id; 
						?>
						<?php if(get_user_meta( $comment_author_id, 'twitter', true )){ ?>
							<li><a class="tw" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'twitter', true ); ?>"><i class="fab fa-twitter-square"></i></a></li>
						<?php } ?>
						<?php if(get_user_meta( $comment_author_id, 'facebook', true )){ ?>
							<li><a class="fb" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'facebook', true ); ?>"><i class="fab fa-facebook-square"></i></a></li>
						<?php } ?>
						<?php if(get_user_meta( $comment_author_id, 'instagram', true )){ ?>
							<li><a class="inta" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'instagram', true ); ?>"><i class="fab fa-instagram"></i></a></li>
						<?php } ?>
						<?php if(get_user_meta( $comment_author_id, 'google', true )){ ?>
							<li><a class="gg" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'google', true ); ?>"><i class="fab fa-google-plus"></i></a></li>
						<?php } ?>
						<?php if(get_user_meta( $comment_author_id, 'linkedin', true )){ ?>
							<li><a class="linkedin" target="_blank" href="<?php echo get_user_meta( $comment_author_id, 'linkedin', true ); ?>"><i class="fab fa-linkedin"></i></a></li>
						<?php } ?>
					</ul>
                </div><!-- .comment-content -->

                <div class="reply">
                    <?php comment_reply_link( array_merge( $args, array( 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                </div><!-- .reply -->
            </article><!-- .comment-body -->
    <?php
        break;
    endswitch; 
}

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="facebook"><?php _e("Facebook"); ?></label></th>
        <td>
            <input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="twitter"><?php _e("Twitter"); ?></label></th>
        <td>
            <input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="instagram"><?php _e("Instagram"); ?></label></th>
        <td>
            <input type="text" name="instagram" id="instagram" value="<?php echo esc_attr( get_the_author_meta( 'instagram', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="google"><?php _e("Google"); ?></label></th>
        <td>
            <input type="text" name="google" id="google" value="<?php echo esc_attr( get_the_author_meta( 'google', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    <tr>
    <th><label for="linkedin"><?php _e("Linkedin"); ?></label></th>
        <td>
            <input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter link."); ?></span>
        </td>
    </tr>
    </table>
<?php }
add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
    update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
    update_user_meta( $user_id, 'instagram', $_POST['instagram'] );
    update_user_meta( $user_id, 'google', $_POST['google'] );
    update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
}
