<div class="col-xl-5 offset-xl-2">
    <div class="top-blog">
		<?php if(get_the_title() != ''):?>
			<h1><?php echo get_the_title(); ?></h1>  
		<?php endif;?> 
		<div class="meta">
			<span class="date"><?php echo get_the_date('F j Y'); ?></span>
			<span class="social-share-post">
			<?php
				if(function_exists('author_social_share')){
					echo '<lable class="lable">'. wp_kses('Share :','author') .'</lable>';
					author_social_share();
				}
			?>	
			</span>
			<span class="comment"><?php echo '&lpar;'.get_comments_number().'&rpar;'; _e(' Comments','author');?></span>
		</div>				
	</div>
	<div class="content-blog">
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="img-featured full-image"><?php the_post_thumbnail('full'); ?></div>
		<?php endif; ?>		
		<?php the_content();?>		
	</div>
</div>
