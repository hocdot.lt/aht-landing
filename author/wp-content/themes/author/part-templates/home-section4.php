<?php
/**
 * Render books in home page
 */

//Get config
$on_book_show =  get_theme_mod('on_book_show','yes');
$title_book = get_theme_mod('title_book','Chek Some Books I Wrote');
$sub_title_book = get_theme_mod('sub_title_book','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$number_post_book = get_theme_mod('number_post_book','3');
$category_book = get_theme_mod('category_book','');
?>
<?php if($on_book_show === 'yes'): ?>
    <div id="section-myBooks">
        <div class="container ml-xl-0">
            <div class="row">
                <div class="col-xl-5 offset-xl-2">
                    <div class="session-title">
                        <?php  if($title_book !=''){ ?>
                            <h2><?php echo $title_book; ?></h2>
                        <?php } ?>
                        <?php  if($sub_title_book !=''){ ?>
                            <p class="mb-md-5"><?php echo $sub_title_book; ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 offset-xl-2">
                    <?php
                    $the_query = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => $number_post_book,
                        'post_status' => 'publish',
                        'cat' => $category_book,
                        'orderby'	=> 'date',
                        'order'	=> 'DESC',
                    ));
                    if ( $the_query->have_posts() ) : $i = 1;?>
                    <div class="d-flex flex-column flex-lg-row">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="book text-center mb-5 ">
                            <div class="icon hv-scale">
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <?php the_post_thumbnail('book'); ?>
                                <?php else: ?>
                                    <img src="<?php echo get_template_directory_uri()?>/images/book.png"/>
                                <?php endif; ?>
                            </div>
                            <div class="title">
                                <h3><?php the_title() ?></h3>
                            </div>
                            <div class="desc">
                                <p><?php echo wp_trim_words( get_the_excerpt(), $num_words = 12, '' ) ?></p>
                                <a class="btn-link" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">&gt;&gt;</a>
                            </div>
                        </div>
                    <?php if($i%3 == 0 && $i < $the_query->post_count): ?>
                        </div>
                        <div class="d-flex flex-column flex-lg-row">
                    <?php endif; ?>
                        <?php $i++;endwhile; ?>
                    </div>
                    <?php
                    endif;
                    wp_reset_postdata();?>
                </div>
            </div>
        </div>
    </div>
<?php endif;