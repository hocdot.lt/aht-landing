<?php
/**
 * Render subscribe in home page
 */

//Get config
$subscribe_show =  get_theme_mod('subscribe_show','yes');
$subscribe_title = get_theme_mod('subscribe_title','Subscribe to My Newsletter');
$subscribe_sub_title = get_theme_mod('subscribe_sub_title','Newsletter to get in touch');
$bg_subscribe = get_theme_mod('bg_subscribe',get_template_directory_uri() . '/images/bg-section5.jpg');
$newsletter_shortcode = get_theme_mod('newsletter_shortcode','[mc4wp_form id="76"]');
?>
<?php if($subscribe_show === 'yes'): ?>
    <div id="section-subscribe">
        <div class="container ml-xl-0">
            <div class="row">
                <div class="col-xl-6 offset-xl-2">
                    <div class="wrapper">
                        <div class="bg-cover" <?php if(!empty($bg_subscribe)){ ?> style="background-image:url(<?php echo $bg_subscribe; ?>);" <?php } ?>></div>
                        <div class="session-title">
                            <?php  if($subscribe_title !=''){ ?>
                                <h2 class="v-2"><?php echo wp_kses_post($subscribe_title); ?></h2>
                            <?php } ?>
                            <?php  if($subscribe_sub_title !=''){ ?>
                                <p class="v-2"><?php echo wp_kses_post($subscribe_sub_title); ?></p>
                            <?php } ?>
                        </div>
                        <?php echo do_shortcode( $newsletter_shortcode ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>