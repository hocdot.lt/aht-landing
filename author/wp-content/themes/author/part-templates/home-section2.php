<?php
/**
 * Render whatido in home page
 */

//Get config
$whatido_show =  get_theme_mod('on_whatido_show','yes');
$whatido_title = get_theme_mod('whatido_title','How I Help Entrepreneurs');
$whatido_sub_title = get_theme_mod('whatido_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$customizer_repeater_whatido = get_theme_mod("customizer_repeater_whatido");
?>
<?php if($whatido_show === 'yes'): ?>
    <div id="section-whatIDo">
        <div class="container ml-xl-0">
            <div class="row">
                <div class="col-xl-5 offset-xl-2">
                    <div class="session-title">
                        <?php if(!empty($whatido_title)){ ?>
                            <h2><?php echo $whatido_title; ?></h2>
                        <?php } ?>
                        <?php if(!empty($whatido_sub_title)){ ?>
                            <p class="session-desc"><?php echo $whatido_sub_title; ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6 offset-xl-2">
                    <?php if(!empty($customizer_repeater_whatido)): ?>
                    <div class="d-flex flex-column flex-lg-row">
                        <?php
                            $customizer_repeater_example_decoded = json_decode($customizer_repeater_whatido);
                            $count = 1;
                            foreach( $customizer_repeater_example_decoded as $item ):
                        ?>
                            <div class="wid mb-5 text-center">
                                <?php if(!empty($item->image_url)){ ?>
                                    <div class="icon"><img src="<?php echo $item->image_url;?>" alt="<?php echo $item->title;?>"></div>
                                <?php } ?>
                                <div class="title">
                                    <h3><?php echo $item->title;?></h3>
                                </div>
                                <div class="desc">
                                    <p><?php echo $item->text;?></p>
                                    <a class="btn-link" href="<?php echo $item->link;?>">&gt;&gt;</a>
                                </div>
                            </div>
                            <?php if($count % 3 == 0 && $count < count($customizer_repeater_example_decoded)): ?>
                                </div>
                                <div class="d-flex flex-column flex-lg-row">
                            <?php endif; ?>
                        <?php $count++;endforeach; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>