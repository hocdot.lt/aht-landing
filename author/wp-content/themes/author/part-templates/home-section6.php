<?php
/**
 * Render contact in home page
 */

//Get config
$contact_show =  get_theme_mod('contact_show','yes');
$contact_title = get_theme_mod('contact_title','Got something in mind?');
$contact_sub_title = get_theme_mod('contact_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$contact_info = get_theme_mod('contact_info','<h3 class="mb-lg-4">Amilla<span>Writer & Editor</span></h3><p>145 Amands Street,</p><p>Beverly Hill Sights,</p><p>NYC 2345</p><p>Phone:(800) 0123 4567 890</p><p>E-Mail: amilla@email.com</p>');
$contact_shortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="49" title="Contact form home"]');
?>
<?php if($contact_show === 'yes'): ?>
<div id="section-contact">
    <div class="container ml-xl-0">
        <div class="row">
            <div class="col-xl-4 offset-xl-2">
                <div class="session-title">
                    <?php  if($contact_title !=''){ ?>
                        <h2><?php echo wp_kses_post($contact_title); ?></h2>
                    <?php } ?>
                    <?php  if($contact_sub_title !=''){ ?>
                        <p class="mb-lg-5"><?php echo wp_kses_post($contact_sub_title); ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-xl-4 offset-xl-2">
                <?php if($contact_shortcode != ''): ?>
                    <?php echo do_shortcode($contact_shortcode); ?>
                <?php endif; ?>
            </div>
            <div class="col-lg-3 col-xl-2">
                <div class="contact-info text-center text-lg-left">
                    <?php echo apply_filters( 'the_content', $contact_info); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>