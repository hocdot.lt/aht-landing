<?php
/**
 * Render myseft-us in home page
 */

//Get config
$myseftShow =  get_theme_mod('myseft_show','yes');
$title_myseft = get_theme_mod('title_myseft','“If there\'s a book<br> that you want to read,<br> but it hasn\'t been written yet,<br> then you must write”');
$myseft_image = get_theme_mod('myseft_image',get_template_directory_uri() . '/images/about.jpg');
$myseft_content = get_theme_mod('myseft_content','<p>Hello! Welcome to my website, I am Amellia, Writer & Editor, presently working for a internet company, basedin Florida, I love to write books on Nature, People & Internet...</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p><a class="read-more btn" href="#">Read more about me</a>');
$myseft_info = get_theme_mod('myseft_info','<p>info@amelliawriter.com</p><p>800 236589547</p>');
?>
<?php if($myseftShow === 'yes'): ?>
    <div id="section-mySeft">
        <div class="container ml-xl-0">
            <div class="row justify-content-between">
                <div class="col-sm-4 col-xl-3 offset-xl-2">
                    <?php if(!empty($myseft_info)){ ?>
                    <div class="about-info mb-1 text-sm-left text-center">
                        <?php echo apply_filters( 'the_content', $myseft_info); ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-sm-4 col-lg-3">
                    <?php
                    if(function_exists('author_social_link')){
                        author_social_link();
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-5 offset-xl-2">
                    <?php if(!empty($title_myseft)){ ?>
                    <div class="session-title mt-5">
                        <h2><?php echo $title_myseft; ?></h2>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="row flex-column-reverse flex-lg-row">
                <div class="col-lg-4 col-xl-3 offset-xl-2 pr-lg-0">
                    <?php if(!empty($myseft_content)){ ?>
                        <div class="about-content">
                            <?php echo apply_filters( 'the_content', $myseft_content); ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-4 col-xl-3 pr-xl-2">
                    <?php if(!empty($myseft_image)){ ?>
                    <div class="media justify-content-lg-end justify-content-center">
                        <img class="my-2" src="<?php echo $myseft_image; ?>" alt="<?php echo $title_myseft; ?>"/>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>