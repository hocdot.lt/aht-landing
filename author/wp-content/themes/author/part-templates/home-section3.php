<?php
/**
 * Render writings in home page
 */

//Get config
$on_writing_show =  get_theme_mod('on_writing_show','yes');
$title_writing = get_theme_mod('title_writing','My Writings & Thoughts');
$sub_title_writing = get_theme_mod('sub_title_writing','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$number_post_writing = get_theme_mod('number_post_writing','2');
$category_writing = get_theme_mod('category_writing','');
?>
<?php if($on_writing_show === 'yes'): ?>
    <div id="section-myWritings">
        <div class="container ml-xl-0">
            <div class="row">
                <div class="col-xl-5 offset-xl-2">
                    <div class="session-title">
                        <?php  if($title_writing !=''){ ?>
                            <h2><?php echo $title_writing; ?></h2>
                        <?php } ?>
                        <?php  if($sub_title_writing !=''){ ?>
                            <p><?php echo $sub_title_writing; ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php
            $the_query = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => $number_post_writing,
                'post_status' => 'publish',
                'cat' => $category_writing,
                'orderby'	=> 'date',
                'order'	=> 'DESC',
            ));
            if ( $the_query->have_posts() ) :  $i = 1;?>
            <div class="row">
                <div class="col-xl-6 offset-xl-2">
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="media justify-content-between flex-column flex-md-row  <?php if($i%2 == 0) echo 'flex-md-row-reverse'; ?>">
                        <div class="media-img mr-md-3">
                            <?php if ( has_post_thumbnail() ) : ?>
                                <?php the_post_thumbnail('writing', ['class' => 'mb-3 media']); ?>
                            <?php else: ?>
                                <img class="mb-3" src="<?php echo get_template_directory_uri()?>//images/writing.png"/>
                            <?php endif; ?>
                        </div>
                        <div class="media-body">
                            <h3><?php the_title() ?></h3>
                            <p><?php echo wp_trim_words( get_the_excerpt(), $num_words = 40, '' ) ?></p>
                            <a class="read-more btn" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Continue Reading....</a>
                        </div>
                    </div>
                <?php $i++;endwhile; ?>
                </div>
            </div>
            <?php
            endif;
            wp_reset_postdata();?>
        </div>
    </div>
<?php endif;