<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'author_customize_register' );
function author_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'author_Customize_Sidebar_Control' ) ) {
	    class author_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                echo $dropdown;
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',			
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',			
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',
                'Rozha One' => 'Rozha One',
                '' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'author') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','author'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','author' ),
	      'description' => esc_html__( 'Upload Logo','author' ),
	      'section' => 'general_settings',
	   )
	) );
    $wp_customize->add_setting( 'logo_subtitle',
        array(
            'default' => '<h3>Amellia<span>Writer & Editor</span></h3>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'logo_subtitle',
        array(
            'label' => esc_html__( 'Logo subtitle','author' ),
            'section' => 'general_settings',
        )
    ) );

    $wp_customize->add_setting( 'google_font_h',
		array(
			'default' => '',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Old Standard TT:400,400i,700',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Main Google Font',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'author'),
        'description'    => __('Several settings pertaining my theme', 'author'),
    ) );
	
    // Section Myseft
	
	$wp_customize->add_section( 'myseft_settings',
	   array(
	      'title' => esc_html__( 'Myseft section','author'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

    // content
    $wp_customize->add_setting( 'myseft_info',
        array(
            'default' => "<p>info@amelliawriter.com</p><p>800 236589547</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'myseft_info',
        array(
            'label' => esc_html__( 'Myseft info','author' ),
            'description' => esc_html__( 'Myseft info','author' ),
            'section' => 'myseft_settings',
        )
    ) );

	$wp_customize->add_setting( 'title_myseft',
	   array(
	      'default' => esc_html__('“If there\'s a book<br> that you want to read,<br> but it hasn\'t been written yet,<br> then you must write”','author'),

	   )
	);	
	$wp_customize->add_control('title_myseft',
		array(
	      'label' => esc_html__( 'Title Myseft','author' ),
	      'section' => 'myseft_settings',
	       'type' => 'text',	
	   )
	);
	

    $wp_customize->add_setting( 'myseft_image',
        array(
            'default' => get_template_directory_uri() . '/images/about.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'myseft_image',
        array(
            'label' => esc_html__( 'Image Myseft','author' ),
            'description' => esc_html__( 'Upload Image','author' ),
            'section' => 'myseft_settings',
        )
    ) );

	// content
	$wp_customize->add_setting( 'myseft_content',
		array(
			'default' => "<p>Hello! Welcome to my website, I am Amellia, Writer & Editor, presently working for a internet company, basedin Florida, I love to write books on Nature, People & Internet...</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p><a class=\"read-more btn\" href=\"#\">Read more about me</a>",
		)
	);	
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'myseft_content',
	   array(
	      'label' => esc_html__( 'Myseft content','author' ),
	      'description' => esc_html__( 'Myseft content','author' ),
	      'section' => 'myseft_settings',
	   )
	) );

	$wp_customize->add_setting( 'myseft_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'myseft_show',
	   array(
            'label' => esc_html__( 'Show Section','author' ),
            'section' => 'myseft_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','author' ),
	         'yes' => esc_html__( 'Yes','author' ),
            )
	   )
	);
	// End myseft


	// start whatIDo
	$wp_customize->add_section( 'whatIDo_settings',
	   array(
	      'title' => esc_html__( 'What I Do Section','author'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

    $wp_customize->add_setting( 'title_whatido',
        array(
            'default' => esc_html__('How I Help Entrepreneurs','author'),
        )
    );
    $wp_customize->add_control('title_whatido',
        array(
            'label' => esc_html__( 'Title Section','author' ),
            'section' => 'whatIDo_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_whatido',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','author'),
        )
    );
    $wp_customize->add_control('sub_title_whatido',
        array(
            'label' => esc_html__( 'Sub title Section','author' ),
            'section' => 'whatIDo_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'customizer_repeater_whatido', array(
        'sanitize_callback' => 'customizer_repeater_sanitize'
    ));
    $wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'customizer_repeater_whatido', array(
        'label'   => esc_html__('What I Do','customizer-repeater'),
        'section' => 'whatIDo_settings',
        'customizer_repeater_title_control' => true,
        'customizer_repeater_text_control' => true,
        'customizer_repeater_image_control' => true,
        'customizer_repeater_link_control' => true,
    ) ) );

	$wp_customize->add_setting( 'on_whatido_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'on_whatido_show',
	   array(
            'label' => esc_html__( 'Show Section','author' ),
            'section' => 'whatIDo_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','author' ),
	         'yes' => esc_html__( 'Yes','author' ),
            )
	   )
	);
	// End whatIDo

    // start My Writings
    $wp_customize->add_section( 'writing_settings',
        array(
            'title' => esc_html__( 'My Writings Section','author'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    $wp_customize->add_setting( 'title_writing',
        array(
            'default' => esc_html__('My Writings & Thoughts','author'),
        )
    );
    $wp_customize->add_control('title_writing',
        array(
            'label' => esc_html__( 'Title Section','author' ),
            'section' => 'writing_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_writing',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','author'),
        )
    );
    $wp_customize->add_control('sub_title_writing',
        array(
            'label' => esc_html__( 'Sub title Section','author' ),
            'section' => 'writing_settings',
            'type' => 'textarea',
        )
    );
    //  Select category
    $wp_customize->add_setting( 'category_writing', array(
        'default'           => 0,
        'sanitize_callback' => 'absint',
    ) );

    $wp_customize->add_control( new My_Dropdown_Category_Control( $wp_customize, 'category_writing',
        array(
            'section'       => 'writing_settings',
            'label'         => esc_html__( 'Category Post', 'author' ),
        )
    ) );
    $wp_customize->add_setting( 'number_post_writing',
        array(
            'default' => esc_html__('2','author'),
        )
    );
    $wp_customize->add_control('number_post_writing',
        array(
            'label' => esc_html__( 'Number Post','author' ),
            'section' => 'writing_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'writing_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'writing_show',
        array(
            'label' => esc_html__( 'Show Section','author' ),
            'section' => 'writing_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','author' ),
                'yes' => esc_html__( 'Yes','author' ),
            )
        )
    );
    // End My Writings

    // start My Books
    $wp_customize->add_section( 'book_settings',
        array(
            'title' => esc_html__( 'My Books Section','author'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    $wp_customize->add_setting( 'title_book',
        array(
            'default' => esc_html__('Chek Some Books I Wrote','author'),
        )
    );
    $wp_customize->add_control('title_book',
        array(
            'label' => esc_html__( 'Title Section','author' ),
            'section' => 'book_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_book',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','author'),
        )
    );
    $wp_customize->add_control('sub_title_book',
        array(
            'label' => esc_html__( 'Sub title Section','author' ),
            'section' => 'book_settings',
            'type' => 'textarea',
        )
    );
    //  Select category
    $wp_customize->add_setting( 'category_book', array(
        'default'           => 0,
        'sanitize_callback' => 'absint',
    ) );

    $wp_customize->add_control( new My_Dropdown_Category_Control( $wp_customize, 'category_book',
        array(
            'section'       => 'book_settings',
            'label'         => esc_html__( 'Category Post', 'author' ),
        )
    ) );
    $wp_customize->add_setting( 'number_post_book',
        array(
            'default' => esc_html__('3','author'),
        )
    );
    $wp_customize->add_control('number_post_book',
        array(
            'label' => esc_html__( 'Number Post','author' ),
            'section' => 'book_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'book_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'book_show',
        array(
            'label' => esc_html__( 'Show Section','author' ),
            'section' => 'book_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','author' ),
                'yes' => esc_html__( 'Yes','author' ),
            )
        )
    );
    // End My Books

    // start Subscribe
    $wp_customize->add_section( 'subscribe_settings',
        array(
            'title' => esc_html__( 'Subscribe Section','author'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    $wp_customize->add_setting( 'subscribe_title',
        array(
            'default' => 'Subscribe to My Newsletter',
        )
    );
    $wp_customize->add_control( 'subscribe_title',
        array(
            'label' => esc_html__( 'Title Section','author' ),
            'section' => 'subscribe_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'subscribe_sub_title',
        array(
            'default' => 'Newsletter to get in touch',
        )
    );
    $wp_customize->add_control( 'subscribe_sub_title',
        array(
            'label' => esc_html__( 'Sub Title','author' ),
            'section' => 'subscribe_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting( 'newsletter_shortcode',
        array(
            'default' => '[mc4wp_form id="76"]',
        )
    );
    $wp_customize->add_control( 'newsletter_shortcode',
        array(
            'label' => esc_html__( 'Newsletter shortcode','author' ),
            'section' => 'subscribe_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'bg_subscribe',
        array(
            'default' => get_template_directory_uri() . '/images/bg-section5.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_subscribe',
        array(
            'label' => esc_html__( 'Background','author' ),
            'description' => esc_html__( 'Upload Image','author' ),
            'section' => 'subscribe_settings',
        )
    ) );
    $wp_customize->add_setting( 'subscribe_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'subscribe_show',
        array(
            'label' => esc_html__( 'Show Section','author' ),
            'section' => 'subscribe_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','author' ),
                'yes' => esc_html__( 'Yes','author' ),
            )
        )
    );
    // End Subscribe

	// start Contact
	$wp_customize->add_section( 'contact_settings',
	   array(
	      'title' => esc_html__( 'Contact Section','author'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
    $wp_customize->add_setting( 'contact_title',
        array(
            'default' => 'Got something in mind?',
        )
    );
    $wp_customize->add_control( 'contact_title',
        array(
            'label' => esc_html__( 'Title Section','author' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'contact_sub_title',
        array(
            'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        )
    );
    $wp_customize->add_control( 'contact_sub_title',
        array(
            'label' => esc_html__( 'Sub Title','author' ),
            'section' => 'contact_settings',
            'type' => 'textarea',
        )
    );
	$wp_customize->add_setting( 'contact_shortcode',
	   array(
	      'default' => '[contact-form-7 id="49" title="Contact form home"]',
	   )
	);	
	$wp_customize->add_control( 'contact_shortcode',
	   array(
	      'label' => esc_html__( 'Contact Shortcode','author' ),
	      'section' => 'contact_settings',
	      'type' => 'text',
	   )
	);
    $wp_customize->add_setting( 'contact_info',
        array(
            'default' => "<h3 class=\"mb-lg-4\">Amilla<span>Writer & Editor</span></h3><p>145 Amands Street,</p><p>Beverly Hill Sights,</p><p>NYC 2345</p><p>Phone:(800) 0123 4567 890</p><p>E-Mail: amilla@email.com</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'contact_info',
        array(
            'label' => esc_html__( 'Contact info','author' ),
            'description' => esc_html__( 'Myseft content','author' ),
            'section' => 'contact_settings',
        )
    ) );
	$wp_customize->add_setting( 'contact_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'contact_show',
	   array(
            'label' => esc_html__( 'Show Section','author' ),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','author' ),
	         'yes' => esc_html__( 'Yes','author' ),
            )
	   )
	);
	// End Contact
		
	// End Home Options
	
	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','author'  ),
	      'priority' => 35, 
	   )
	);
    $wp_customize->add_setting( 'logo_footer',
        array(
            'default' => get_template_directory_uri() . '/images/logo-footer.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_footer',
        array(
            'label' => esc_html__( 'Logo footer','author' ),
            'description' => esc_html__( 'Upload Logo','author' ),
            'section' => 'footer_settings',
        )
    ) );
	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '<p>(C) All Rights Reserved Amilla - Writer & Editor. Designed and Developed by <a href="#">Template.net</a></p>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','author' ),
	      'section' => 'footer_settings',
	   )
	) );	
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','author'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','author' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','author' ),
	         'yes' => esc_html__( 'Yes','author' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','author'),
		'instagram'=> esc_html__('Instagram','author'),
		'twitter'=> esc_html__('Twitter','author'),
		'google'=> esc_html__('Google','author'),
		'linkedin'=> esc_html__('Linkedin','author'),
		'tumblr'=> esc_html__('Tumblr','author'),
		'pinterest'=> esc_html__('Pinterest','author')
	);
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','author' ),
				 'yes' => esc_html__( 'Yes','author' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','author'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','author'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','author' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','author' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','author' ),
	         'yes' => esc_html__( 'Yes','author' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','author' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','author' ),
	         'yes' => esc_html__( 'Yes','author' ),
	      )
	   )
	);
}
/**
 * Theme options in the Customizer js
 * @package author
 */
 
function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'author_customize_admin_menu_hide', 999 );
function author_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="author-theme-options"><span>'.esc_html__( 'author','author' ).'</span>'.esc_html__( 'Theme Options','author' ).'</div>',
         '<div class="author-theme-options"><span>'.esc_html__( 'author','author' ).'</span>'.esc_html__( 'Theme Options','author' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}