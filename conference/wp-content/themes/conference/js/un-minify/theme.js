(function($){
    function one_page_scroll(){
        $('.main-menu .mega-menu > li.menu-item > a,.menu-footer li a').click(function(e){
            e.preventDefault();
            console.log(1);
            if($('body').hasClass('home')){
                var href=$(this).attr('href');
                href=href.split('#');
                if(href.length>1){
                    var top=0;
                    if(!$('header.header').hasClass('sticky')){
                        $('header.header').addClass('sticky');
                    }

                    if(href[1]==''){
                        top=0;
                    }else{top=$('#'+href[1]).offset().top - $('header.header').outerHeight();}

                    $('body,html').animate({scrollTop:top},'300',function(){
                        window.location.hash = '#'+href[1];
                    });
                }else{
                    location.href=$(this).attr('href');
                }
            }else{
                location.href=$(this).attr('href');
            }
        });

        $(window).scroll(function(){
            if($(window).scrollTop()>50){
                if(!$('header.header').hasClass('sticky')){
                    $('header.header').addClass('sticky');
                }
                var h=$('header.header').outerHeight();
                $('header.header').next().css('margin-top',h);
            }else{
                $('header.header').removeClass('sticky');
                $('header.header').next().css('margin-top','');

            }
        });
    }

    function mobile_menu() {
        //reponsive open menu button click
        $('.open-menu-mobile').click(function (e) {
            e.preventDefault();
            $(this).closest('.main-menu').find('#site-navigation').addClass('open');
        })

        //reponsive close menu button click
        $('.close-menu-mobile').click(function (e) {
            e.preventDefault();
            $(this).closest('#site-navigation').removeClass('open');
        })
        //reponsive close menu button click
        $('.hide-mobile').click(function (e) {
            e.preventDefault();
            $(this).closest('#site-navigation').removeClass('open');
        })
    }
    function skills(){
        $(".bar").each(function () {
            $(this).find(".bar-inner").animate({
                width: $(this).attr("data-width")
            }, 2000)
        });
    }
    function portfolio_slider(){
        $('.center').slick({
            arrows: false,
            dots: true,
            centerMode: true,
            centerPadding: '200px',
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 1023,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 1
                    }
                }
            ]
        });
    }

    function init(){
        one_page_scroll();
        mobile_menu();
        portfolio_slider();
        skills();
    }

    $(document).ready(function () {
        init();
    });
})(jQuery);