<?php 
/**
Template Name: Home
*/
get_header();

get_template_part('part-templates/home-banner');
get_template_part('part-templates/home-the-conference');
get_template_part('part-templates/home-speakers');
get_template_part('part-templates/home-schedule');
get_template_part('part-templates/home-blog');
get_template_part('part-templates/home-contact');

get_footer(); ?> 