<?php
$BannerShow = get_theme_mod('banner_show','yes');
$BannerTime = get_theme_mod('banner_sub_title','12/05/2020');
$BannerTitle = get_theme_mod('banner_title','GLOBAL BUSINESS CONFERENCE');
$BannerBackground = get_theme_mod('background_banner',get_template_directory_uri() . '/images/bg-banner.jpg');
$BannerPoster = get_theme_mod('poster_video',get_template_directory_uri() . '/images/img2.jpg');
$BannerLinkvideo = get_theme_mod('link_video','#');
$BannerImg1 = get_theme_mod('img1_right_banner',get_template_directory_uri() . '/images/img1.jpg');
$BannerImg2 = get_theme_mod('img2_right_banner',get_template_directory_uri() . '/images/img3.jpg');
?>
<?php if($BannerShow):?>
    <div id="bannerSession" style="background-image: url(<?php echo $BannerBackground?>);">
        <div class="content-banner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-4 col-md-4 col-lg-4">
                        <div class="row justify-content-end">
                            <div class="col-lg-8 col-xl-6">
                                <div class="content-left-banner">
                                    <h4><?php echo $BannerTime?></h4>
                                    <h3><?php echo $BannerTitle?></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 col-md-4 col-lg-4">
                        <div class="content-rigth-banner">
                            <div class="video-right-banner">
                                <a href="<?php echo $BannerLinkvideo?>"><img src="<?php echo $BannerPoster?>">
                                </a>
                            </div>
                            <div class="img-right-banner"><img src="<?php echo $BannerImg1?>"><img src="<?php echo $BannerImg2?>"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif?>