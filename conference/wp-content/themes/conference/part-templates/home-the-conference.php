<?php
$conferencShow = get_theme_mod('conferenc_show','yes');
$conferencTitle = get_theme_mod('conference_title','WHY YOU SHOULD JOIN THE CONFERENCE');
$conferencSuubtitle = get_theme_mod('conference_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim ut sem viverra aliquet eget sit amet tellus cras.At erat pellentesque adipiscing commodo. sed enim ut sem viverra. Amet commodo nulla facilisi nullam vehicula ipsum a arcu cursus..');
$conferencTitlebottom = get_theme_mod('conference_title_bt','INTERESTED? BUY YOUR TICKETS NOW');
$conferencSubTitlebottom = get_theme_mod('conference_sub_title_bt','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$conferencLinkButton = get_theme_mod('conference_button_link','#');
$conferencTextButton = get_theme_mod('conference_button','Book Tickets Now');
$conferencCate = get_theme_mod('conferenc_category',5);
$conferencLimit = get_theme_mod('conferenc_number_post',3);
$args = array(
    'posts_per_page'   => $conferencLimit,
    'offset'           => 0,
    'cat'         => $conferencCate,
    'order' => 'ASC',
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );
?>
<?php if($conferencShow):?>
  <div id="the_conference">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="title">
                        <h3><?php echo $conferencTitle?></h3>
                        <p>
                            <?php echo $conferencSuubtitle?>
                        </p>
                    </div>
                </div>
    <?php foreach ($posts_array as $_post): setup_postdata( $_post );?>
                <div class="col-lg-4">
                    <div class="post_the_conference"><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                        <div class="content_post"><?php echo get_the_post_thumbnail($_post->ID); ?>
                            <p><?php echo get_the_excerpt($_post->ID) ?></p>
                        </div>
                    </div>
                </div>
    <?php endforeach; ?>
            </div>
        </div>
        <div class="content_bt_conference">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <h3><?php echo $conferencTitlebottom?></h3>
                        <p><?php echo $conferencSubTitlebottom?></p>
                    </div>
                    <div class="col-lg-3"><a href="<?php echo $conferencLinkButton?>"><?php echo $conferencTextButton?></a></div>
                </div>
            </div>
        </div>
    </div>
<?php endif?>