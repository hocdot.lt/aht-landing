<?php
$scheduleShow = get_theme_mod('schedule_show','yes');
$scheduleTitle = get_theme_mod('schedule_title','CONFERENCE SCHEDULE, HOUR BY HOUR INRORMATION');
$scheduleSubtitle = get_theme_mod('schedule_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua onsectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
//box list
//day1
$Day1 = get_theme_mod('day1','<h3>day 01</h3>');
$Titleday1 = get_theme_mod('title_day1','In-depth Lecture on Digital Media and Future Media');
$Authorday1 = get_theme_mod('author_day1','<h6>(1-2 Hours by Samantha Jason)</h6>');
//day2
$Day2 = get_theme_mod('day2','<h3>day 02</h3>');
$Titleday2 = get_theme_mod('title_day2','In-depth Lecture on Digital Media and Future Media');
$Authorday2 = get_theme_mod('author_day2','<h6>(1-2 Hours by Samantha Jason)</h6>');
//day3
$Day3 = get_theme_mod('day3','<h3>day 03</h3>');
$Titleday3 = get_theme_mod('title_day3','In-depth Lecture on Digital Media and Future Media');
$Authorday3 = get_theme_mod('author_day3','<h6>(1-2 Hours by Samantha Jason)</h6>');
//day4
$Day4 = get_theme_mod('day1','<h3>day 04</h3>');
$Titleday4 = get_theme_mod('title_day4','In-depth Lecture on Digital Media and Future Media');
$Authorday4 = get_theme_mod('author_day4','<h6>(1-2 Hours by Samantha Jason)</h6>');
?>
<?php if($scheduleShow):?>
<div id="schedule">
    <div class="container">
        <div class="title">
            <h3><?php echo $scheduleTitle?></h3>
            <p><?php echo $scheduleSubtitle?></p>
        </div>
        <div class="content-schedule">
            <div class="row">
                <div class="col-lg-4">
                    <div class="box-content-schedule" style="background-image: url(<?php echo get_template_directory_uri()?>/images/bg-box.png);">
                        <div class="calendar">
                            <?php echo $Day1?>
                        </div>
                        <div class="text">
                            <h3><?php echo $Titleday1?></h3>
                           <?php echo $Authorday1?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box-content-schedule" style="background-image: url(<?php echo get_template_directory_uri()?>/images/bg-box.png);">
                        <div class="calendar">
                            <?php echo $Day2?>
                        </div>
                        <div class="text">
                            <h3><?php echo $Titleday2?></h3>
                            <?php echo $Authorday2?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box-content-schedule" style="background-image: url(<?php echo get_template_directory_uri()?>/images/bg-box.png);">
                        <div class="calendar">
                            <?php echo $Day3?>
                        </div>
                        <div class="text">
                            <h3><?php echo $Titleday3?></h3>
                            <?php echo $Authorday3?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="box-content-schedule" style="background-image: url(<?php echo get_template_directory_uri()?>/images/bg-box.png);">
                        <div class="calendar">
                            <?php echo $Day4?>
                        </div>
                        <div class="text">
                            <h3><?php echo $Titleday4?></h3>
                            <?php echo $Authorday4?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif?>