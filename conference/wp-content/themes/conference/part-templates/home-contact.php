

<?php
$contactShow = get_theme_mod('contact_show','yes');
$contactForm1 = get_theme_mod('contact_shortcode','[mc4wp_form id="6"]');
$contactTitle = get_theme_mod('contact_title','CONFERENCE LOCATION & MAP');
$contactdesc = get_theme_mod('desc_contact',' <p>info@openconference.com</p>
                                <p>Accountancy FIrm LLC, 85 Broad Street, 28th Floor</p>
                                <p>New York, NY 10004, 800 - 123 - 4567</p>');
$contactForm2 = get_theme_mod('contact_shortcode_2','[contact-form-7 id="190" title="contact2"]');
$contactMap = get_theme_mod('contact_map','<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14900.820008282744!2d105.78794855!3d20.984417999999998!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1553672414819" width="" height="" frameborder="0" style="border:0" allowfullscreen=""></iframe>');
?>
<?php if($contactShow):?>
<div id="contact">
    <div class="content-top-contact">
        <div class="container">
            <?php echo do_shortcode($contactForm1)?>
        </div>
    </div>
    <div class="content-bottom-contact">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="row justify-content-end">
                        <div class="col-lg-6">
                            <div class="title">
                                <h3><?php echo $contactTitle?></h3>
                            </div>
                            <div class="info-contact">
                                <?php echo $contactdesc?>
                            </div>
                            <?php echo do_shortcode($contactForm2)?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 nopd">
                    <div class="map">
                       <?php echo $contactMap?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif?>