<?php
$blogShow = get_theme_mod('blog_show','yes');
$bgBlog = get_theme_mod('bg_blog',get_template_directory_uri() . '/images/background2.png');
$blogtTitle = get_theme_mod('blog_title','NEWS & UPDATES OUR RECENT BLOG POSTS');
$blogCate = get_theme_mod('blog_category','');
$blogLimit = get_theme_mod('blog_number_post','');
$args = array(
    'posts_per_page'   => $blogLimit,
    'offset'           => 0,
    'cat'         => $blogCate,
    'order' => 'ASC',
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );
?>
<div id="blog">
    <div class="content-blog" style="background-image: url(<?php echo $bgBlog?>)">
        <div class="container">
            <div class="title">
                <h3><?php echo $blogtTitle?></h3>
            </div>
        </div>
        <div class="slider-blog">
            <div class="center">
                <?php foreach ($posts_array as $_post): setup_postdata( $_post );?>
                    <div><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_post_thumbnail($_post->ID); ?></a>
                    <div class="text-item">
                        <h5><?php echo get_the_date('j/M/Y'); ?></h5><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>