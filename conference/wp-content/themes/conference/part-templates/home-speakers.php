<?php
$speakersShow = get_theme_mod('speaker_show','yes');
$speakersTitle = get_theme_mod('speakers_title','WHO’S SPEAKING? MORE ABOU THE SPEAKERS');
$speakersBackground = get_theme_mod('bg_speakers',get_template_directory_uri() . '/images/background1.png');
$speakerCate = get_theme_mod('speaker_category','');
$speakerLimit = get_theme_mod('speaker_number_post',4);
$args = array(
    'posts_per_page'   => $speakerLimit,
    'offset'           => 0,
    'cat'         => $speakerCate,
    'order' => 'ASC',
    'post_type'        => 'testimonial',
);
$posts_array = get_posts( $args );
?>
<?php if($speakersShow):?>
<div id="speakers">
    <div class="content-speakers" style="background-image: url(<?php echo $speakersBackground?>);">
        <div class="container">
            <div class="title">
                <h3><?php echo $speakersTitle?></h3>
            </div>
            <div class="main-content-speakers">
                <div class="row">
                    <?php foreach ($posts_array as $_post): setup_postdata( $_post );?>
                    <div class="col-lg-4">
                        <div class="nd-content-speakers">
                            <div class="row">

                                <div class="col-3 col-lg-4"><?php echo get_the_post_thumbnail($_post->ID); ?></div>
                                <div class="col-5 col-lg-4">
                                    <div class="text-content">
                                        <div class="nd-content">
                                            <h3><?php echo get_the_title($_post->ID);?></h3>
                                            <h4><?php echo get_post_meta($_post->ID,'testimonial_position',true);?></h4>
                                            <p><?php echo get_the_excerpt($_post->ID) ?></p>
                                        </div>
                                        <div class="mxh">
                                            <ul>
                                                <li><a href="<?php echo get_post_meta($_post->ID,'testimonial_facebook',true);?>"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="<?php echo get_post_meta($_post->ID,'testimonial_twitter',true);?>"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="<?php echo get_post_meta($_post->ID,'testimonial_youtube',true);?>"><i class="fab fa-youtube"></i></a></li>
                                                <li><a href="<?php echo get_post_meta($_post->ID,'testimonial_pinterest',true);?>"><i class="fab fa-pinterest-p"></i></a></li>
                                                <li><a href="<?php echo get_post_meta($_post->ID,'testimonial_google',true);?>"><i class="fab fa-google-plus-g"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php endif?>