<?php

add_action('widgets_init','charity_about_init_widget');

function charity_about_init_widget(){
	register_widget('charity_about_widget');
}

class charity_about_widget extends WP_Widget{

	function __construct(){
		parent::__construct( 'charity_about_widget','charity About',array('description' => esc_html__('Show About Info','charity-core')));
	}


	/*-------------------------------------------------------
	 *				Front-end display of widget
	 *-------------------------------------------------------*/

	function widget($args, $instance){

		extract($args);

		$title 			= isset($instance['title'])?apply_filters('widget_title', $instance['title'] ):'';
		$link_img = isset($instance['link_img'])? $instance['link_img']:'';
		$desc = isset($instance['desc'])? $instance['desc']:'';
		$title_link = isset($instance['title_link'])? $instance['title_link']:'';
		$link = isset($instance['link'])? $instance['link']:'';
		$instagram = isset($instance['instagram'])? $instance['instagram']:'';
		$facebook 	= isset($instance['facebook'])?$instance['facebook']:'';
		$twitter 	= isset($instance['twitter'])? $instance['twitter']:'';
		$pinterest = isset($instance['pinterest'])? $instance['pinterest']:'';
		$google = isset($instance['google']) ? $instance['google']: '';
		$output = '';
		echo $before_widget;
		if ( $title )
			echo $before_title . $title . $after_title;

?>
	<div class="about-us">
		<div class="about-top">
			<?php if($link_img != ''): ?>
				<div class="img-about">
					<img src="<?php echo esc_url($link_img);?>" alt=""/>
				</div>
			<?php endif; ?>	
			<?php if($desc != ''): ?>
				<p><?php echo esc_attr($desc);?></p>
			<?php endif; ?>
				<a href="<?php echo esc_url($link);?>"><?php echo esc_attr($title_link);?></a>
			
		</div>
		<ul class="social-header">
			<?php if($facebook != ''): ?>
				<li><a target="_blank" class="fb" href="<?php echo esc_url($facebook);?>"><i class="fab fa-facebook-square" aria-hidden="true"></i></a> </li>
			<?php endif; ?>
			<?php if($instagram != ''): ?>
				<li><a target="_blank" class="inta" href="<?php echo esc_url($instagram);?>"><i class="fab fa-instagram" aria-hidden="true"></i></a> </li>
			<?php endif; ?>
			<?php if($twitter != ''): ?>
				<li><a target="_blank" class="tw" href="<?php echo esc_url($twitter);?>"><i class="fab fa-twitter" aria-hidden="true"></i></a> </li>
			<?php endif; ?>
			<?php if($pinterest != ''): ?>
				<li><a target="_blank" class="pin" href="<?php echo esc_url($pinterest);?>"><i class="fab fa-pinterest" aria-hidden="true"></i></a> </li>
			<?php endif; ?>
			<?php if($google != ''): ?>
				<li><a target="_blank" class="gg" href="<?php echo esc_url($google);?>"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a> </li>
			<?php endif; ?>
		</ul>
	</div>
<?php
		echo $after_widget;
	}
	function update( $new_instance, $old_instance ){
		$instance = $old_instance;
		$instance['title'] 			= strip_tags( $new_instance['title'] );
		$instance['link_img'] 			= strip_tags( $new_instance['link_img'] );
		$instance['link'] 			= strip_tags( $new_instance['link'] );
		$instance['title_link'] 			= strip_tags( $new_instance['title_link'] );
		$instance['desc'] 			= strip_tags( $new_instance['desc'] );
		$instance['facebook'] 	= strip_tags( $new_instance['facebook'] );
		$instance['twitter'] = strip_tags( $new_instance['twitter'] );
		$instance['instagram'] 	= strip_tags( $new_instance['instagram'] );
		$instance['pinterest'] 	= strip_tags( $new_instance['pinterest'] );
		$instance['google'] 	= strip_tags( $new_instance['google'] );
		return $instance;
	}
	function form($instance){
		$defaults = array( 
			'title' 		=> esc_html__('About Me','charity-core'),
			'link_img' 		=> '',
			'link' 		=> '#',
			'title_link' 		=> esc_html__('Read More About Myself','charity-core'),
			'desc' 		=> '',
			'facebook' 		=> '#',
			'twitter' 	=> '#',
			'instagram' 	=> '#',
			'pinterest' => '#',
			'google' => '#',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e('Widget Title', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'link_img' )); ?>"><?php esc_html_e('Link url image', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'link_img' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'link_img' )); ?>" value="<?php echo esc_attr($instance['link_img']); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'desc' )); ?>"><?php esc_html_e('Descprtion', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'desc' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'desc' )); ?>" value="<?php echo esc_attr($instance['desc']); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title_link' )); ?>"><?php esc_html_e('Title link', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'title_link' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title_link' )); ?>" value="<?php echo esc_attr($instance['title_link']); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'link' )); ?>"><?php esc_html_e('Link url', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'link' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'link' )); ?>" value="<?php echo esc_attr($instance['link']); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'facebook' )); ?>"><?php esc_html_e('Facebook', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'facebook' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'facebook' )); ?>" value="<?php echo esc_attr($instance['facebook']); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'instagram' )); ?>"><?php esc_html_e('Instagram', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'instagram' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'instagram' )); ?>" value="<?php echo esc_attr($instance['instagram']); ?>" style="width:100%;" />
		</p>
		<p >
			<label for="<?php echo esc_attr($this->get_field_id( 'twitter' )); ?>"><?php esc_html_e('Twitter', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'twitter' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'twitter' )); ?>" value="<?php echo esc_attr($instance['twitter']); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'pinterest' )); ?>"><?php esc_html_e('Pinterest', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'pinterest' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'pinterest' )); ?>" value="<?php echo esc_attr($instance['pinterest']); ?>" style="width:100%;" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'google' )); ?>"><?php esc_html_e('Google Plus', 'charity-core'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'google' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'google' )); ?>" value="<?php echo esc_attr($instance['google']); ?>" style="width:100%;" />
		</p>
	<?php
	}
}