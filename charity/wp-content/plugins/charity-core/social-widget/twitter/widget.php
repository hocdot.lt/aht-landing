<?php

class charityLatestTweetWidget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
                'charity_latest_tweet', // Base ID
                __('charity Latest Tweet', 'charity-core'), // Name
                array('description' => __('This Widget show the Latest Tweets', 'charity-core'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance) {
		$tweet_tw = isset($instance['tweet_tw'])? $instance['tweet_tw']:'';
		$tweet_link = isset($instance['tweet_link'])? $instance['tweet_link']:'';
        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        $number_tweets = 5;
        if (!empty($instance['tweet_limit']) && $instance['tweet_limit'] >= 0) {
            $number_tweets = $instance['tweet_limit'];
        }
        $tweets = new charity_social();
        $tweets->get_tweets($number_tweets);
        //content here
		?>
			<a href="<?php echo esc_url($tweet_link);?>" class="title-tw" target="_blank"><span><?php echo esc_attr($tweet_tw);?></span><i class="fab fa-twitter"></i></a>
		<?php
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('Latest Twitter', 'charity-core');
        $tweet_tw = !empty($instance['tweet_tw']) ? $instance['tweet_tw'] : __('Follow us on', 'charity-core');
        $tweet_link = !empty($instance['tweet_link']) ? $instance['tweet_link'] : __('#', 'charity-core');
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title Widget:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
		<p>
            <label for="<?php echo $this->get_field_id('tweet_tw'); ?>"><?php _e('Title Twitter:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('tweet_tw'); ?>" name="<?php echo $this->get_field_name('tweet_tw'); ?>" type="text" value="<?php echo esc_attr($tweet_tw); ?>">
        </p>
		<p>
            <label for="<?php echo $this->get_field_id('tweet_link'); ?>"><?php _e('Link Twitter:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('tweet_link'); ?>" name="<?php echo $this->get_field_name('tweet_link'); ?>" type="text" value="<?php echo esc_attr($tweet_link); ?>">
        </p>
        <?php
			$tweet_limit = !empty($instance['tweet_limit']) ? $instance['tweet_limit'] : 5;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('tweet_limit'); ?>"><?php _e('Number of tweets:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('tweet_limit'); ?>" name="<?php echo $this->get_field_name('tweet_limit'); ?>" type="text" value="<?php echo esc_attr($tweet_limit); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        $instance['tweet_tw'] = (!empty($new_instance['tweet_tw']) ) ? strip_tags($new_instance['tweet_tw']) : '';
        $instance['tweet_link'] = (!empty($new_instance['tweet_link']) ) ? strip_tags($new_instance['tweet_link']) : '';
        $instance['tweet_limit'] = (!empty($new_instance['tweet_limit']) ) ? strip_tags($new_instance['tweet_limit']) : '';

        return $instance;
    }

}

add_action('widgets_init', create_function('', 'return register_widget("charityLatestTweetWidget");'));
