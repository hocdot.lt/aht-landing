<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
    $classAdminBar = is_admin_bar_showing() ? 'adminbar':'';
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
        <header class="header <?php echo $classAdminBar ?>">
            <div class="header-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-6">
                            <?php if (is_front_page()) : ?>
                            <h1 class="logo">
                            <?php else: ?>
                            <h2 class="logo">
                            <?php endif; ?>
                                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                    <?php
                                        $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                        if ($logo_header && $logo_header!=''):
                                            echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                        else:
                                            //bloginfo('name');
                                        endif;
                                    ?>
                                </a>
                            <?php if (is_front_page()) : ?>
                            </h1>
                            <?php else: ?>
                            </h2>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-6 d-flex flex-column justify-content-center col-2">
                            <div class="main-menu">
                                <div class="open-menu-mobile d-xl-none">
                                    <div class="d-flex">
                                        <div class="fa fab fa-bars"></div>
                                    </div>
                                </div>
                                <div class="menu-container">
                                    <?php
                                    if(function_exists('render_header_menu')){
                                        render_header_menu();
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="main" class="wrapper">
				
            

                    
        
       
