(function($){
    $(document).ready(function () {
        //reponsive open menu button click
        $('.open-menu-mobile').click(function (e) {
            e.preventDefault();
            $(this).closest('.main-menu').find('#site-navigation').addClass('open');
        })

        //reponsive close menu button click
        $('.close-menu-mobile').click(function (e) {
            e.preventDefault();
            $(this).closest('#site-navigation').removeClass('open');
        })


        //Scroll function
        $(window).scroll(function(){
            //Static header
            if($(window).scrollTop() >= 1){
                $('header.header').addClass('sticky');
            }else{
                $('header.header').removeClass('sticky');
            }
        });
    });
    //scroll menu
        $('.mega-menu > li.menu-item > a,.menu-footer li a').click(function(e){
            e.preventDefault();
            console.log(1);
            if($('body').hasClass('home')){
                var href=$(this).attr('href');
                href=href.split('#');
                if(href.length>1){
                    var top=0;
                    if(!$('header.header').hasClass('sticky')){
                        $('header.header').addClass('sticky');
                    }

                    if(href[1]==''){
                        top=0;
                    }else{top=$('#'+href[1]).offset().top - $('header.header').outerHeight();}

                    $('body,html').animate({scrollTop:top},'300',function(){
                        window.location.hash = '#'+href[1];
                    });
                }else{
                    location.href=$(this).attr('href');
                }
            }else{
                location.href=$(this).attr('href');
            }
        });


})(jQuery);
