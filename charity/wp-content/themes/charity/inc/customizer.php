<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'charity_customize_register' );
function charity_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'charity_Customize_Sidebar_Control' ) ) {
	    class charity_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Baloo Tamma' => 'Baloo Tamma',
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',			
				'' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'charity') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','charity'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','charity' ),
	      'description' => esc_html__( 'Upload Logo','charity' ),
	      'section' => 'general_settings',
	   )
	) );
	
	$wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'Oswald:200,300,400,500,600,700',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Main Google Font',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'charity'),
        'description'    => __('Several settings pertaining my theme', 'charity'),
    ) );
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','charity'  ),
	      'priority' => 20,
		  'panel'  => 'home_page_setting',
	   )
	);
	// title banner
	$wp_customize->add_setting( 'banner_title',
	   array(
	      'default' => 'Forget what you can get and see what you can give',
	   )
	);
	$wp_customize->add_control( 'banner_title',
	   array(
	      'label' => esc_html__( 'Title Banner','charity' ),
	      'section' => 'home_banner_settings',
	      'type' => 'text',
	   )
	);
	// sub title banner
	$wp_customize->add_setting( 'banner_sub_title',
	   array(
	      'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas auctor maximus eros sed vehicula. Aliquam ut iaculis odio','charity'),
	   )
	);
	$wp_customize->add_control( 'banner_sub_title',
	   array(
	      'label' => esc_html__( 'Sub Title Banner','charity' ),
	      'section' => 'home_banner_settings',
	      'type' => 'textarea',
	   )
	);
	// button banner
	$wp_customize->add_setting( 'button_banner',
	   array(
	      'default' => esc_html__('Donate','charity'),

	   )
	);
	$wp_customize->add_control('button_banner',
		array(
	      'label' => esc_html__( 'Button banner','charity' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',
	   )
	);
	$wp_customize->add_setting( 'link_banner',
	   array(
	      'default' => esc_html__('#','charity'),

	   )
	);
	$wp_customize->add_control('link_banner',
		array(
	      'label' => esc_html__( 'Button URL','charity' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',
	   )
	);
	// background banner
	$wp_customize->add_setting( 'background_banner',
		array(
			'default' => get_template_directory_uri() . '/images/banner-home-img.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_banner',
	   array(
	      'label' => esc_html__( 'Background Banner','charity' ),
	      'description' => esc_html__( 'Upload Image','charity' ),
	      'section' => 'home_banner_settings',
	   )
	) );
	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','charity' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array(
	         'no' => esc_html__( 'No','charity' ),
	         'yes' => esc_html__( 'Yes','charity' ),
            )
	   )
	);
	// End banner section



	
	// Section about
	
	$wp_customize->add_section( 'about_settings',
	   array(
	      'title' => esc_html__( 'About section','charity'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
	//Column 1: Image
    $wp_customize->add_setting( 'about_col_1_image',
        array(
            'default' => get_template_directory_uri() . '/images/about-1.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_1_image',
        array(
            'label' => esc_html__( 'Column 1: Image','charity' ),
            'description' => esc_html__( 'Upload Image','charity' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 1: Title
    $wp_customize->add_setting( 'about_col_1_title',
        array(
            'default' => esc_html__('Our Mission','charity'),

        )
    );
    $wp_customize->add_control('about_col_1_title',
        array(
            'label' => esc_html__( 'Column 1: Title','charity' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );

    // Column 1: Sub title
    $wp_customize->add_setting( 'about_col_1_subtitle',
        array(
            'default' => 'We are a group united by our passion to make a difference. We vow to help create change by making a better living condition for the poor.',
        )
    );
    $wp_customize->add_control( 'about_col_1_subtitle',
        array(
            'label' => esc_html__( 'Column 1: Sub Title','charity' ),
            'section' => 'about_settings',
            'type' => 'textarea',
        )
    );

    //Column 2: Image
    $wp_customize->add_setting( 'about_col_2_image',
        array(
            'default' => get_template_directory_uri() . '/images/about-2.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_2_image',
        array(
            'label' => esc_html__( 'Column 2: Image','charity' ),
            'description' => esc_html__( 'Upload Image','charity' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 2: Title
    $wp_customize->add_setting( 'about_col_2_title',
        array(
            'default' => esc_html__('Our GOals','charity'),

        )
    );
    $wp_customize->add_control('about_col_2_title',
        array(
            'label' => esc_html__( 'Column 2: Title','charity' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );

    // Column 2: Sub title
    $wp_customize->add_setting( 'about_col_2_subtitle',
        array(
            'default' => 'We are a group united by our passion to make a difference. We vow to help create change by making a better living condition for the poor.',
        )
    );
    $wp_customize->add_control( 'about_col_2_subtitle',
        array(
            'label' => esc_html__( 'Column 2: Sub Title','charity' ),
            'section' => 'about_settings',
            'type' => 'textarea',
        )
    );

    //Column 3: Image
    $wp_customize->add_setting( 'about_col_3_image',
        array(
            'default' => get_template_directory_uri() . '/images/about-3.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_col_3_image',
        array(
            'label' => esc_html__( 'Column 3: Image','charity' ),
            'description' => esc_html__( 'Upload Image','charity' ),
            'section' => 'about_settings',
        )
    ) );

    //Column 3: Title
    $wp_customize->add_setting( 'about_col_3_title',
        array(
            'default' => esc_html__('volunteer','charity'),

        )
    );
    $wp_customize->add_control('about_col_3_title',
        array(
            'label' => esc_html__( 'Column 3: Title','charity' ),
            'section' => 'about_settings',
            'type' => 'text',
        )
    );

    // Column 3: Sub title
    $wp_customize->add_setting( 'about_col_3_subtitle',
        array(
            'default' => 'We are a group united by our passion to make a difference. We vow to help create change by making a better living condition for the poor.',
        )
    );
    $wp_customize->add_control( 'about_col_3_subtitle',
        array(
            'label' => esc_html__( 'Column 3: Sub Title','charity' ),
            'section' => 'about_settings',
            'type' => 'textarea',
        )
    );

    //Show/Hide about
    $wp_customize->add_setting( 'about_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'about_show',
        array(
            'label' => esc_html__( 'Show Section','charity' ),
            'section' => 'about_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','charity' ),
                'yes' => esc_html__( 'Yes','charity' ),
            )
        )
    );

    // End about*/

    //What do we do
    $wp_customize->add_section( 'whatdowedo_settings',
        array(
            'title' => esc_html__( '"What do we do" section','charity'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    //Title
    $wp_customize->add_setting( 'whatDoWeDo_title',
        array(
            'default' => 'What do we do? and what inspirs us to do this?',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_title',
        array(
            'label' => esc_html__( 'Session Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text'
        )
    );

    //Column 1: Icon
    $wp_customize->add_setting( 'whatDoWeDo_col1_image',
        array(
            'default' => get_template_directory_uri() . '/images/icon-1.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'whatDoWeDo_col1_image',
        array(
            'label' => esc_html__( 'Column 1: Icon','charity' ),
            'description' => esc_html__( 'Upload Image','charity' ),
            'section' => 'whatdowedo_settings',
        )
    ) );

    //Column 1: Title
    $wp_customize->add_setting( 'whatDoWeDo_col1_title',
        array(
            'default' => esc_html__('Charity','charity'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col1_title',
        array(
            'label' => esc_html__( 'Column 1: Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );

    // Column 1: Sub title
    $wp_customize->add_setting( 'whatDoWeDo_col1_subtitle',
        array(
            'default' => 'Charity reflects on your own inner values',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_col1_subtitle',
        array(
            'label' => esc_html__( 'Column 2: Sub Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'textarea',
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'whatDoWeDo_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_show',
        array(
            'label' => esc_html__( 'Show Section','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','charity' ),
                'yes' => esc_html__( 'Yes','charity' ),
            )
        )
    );

    //Column 2: Icon
    $wp_customize->add_setting( 'whatDoWeDo_col2_image',
        array(
            'default' => get_template_directory_uri() . '/images/icon-2.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'whatDoWeDo_col2_image',
        array(
            'label' => esc_html__( 'Column 2: Icon','charity' ),
            'description' => esc_html__( 'Upload Image','charity' ),
            'section' => 'whatdowedo_settings',
        )
    ) );

    //Column 2: Title
    $wp_customize->add_setting( 'whatDoWeDo_col2_title',
        array(
            'default' => esc_html__('Helping','charity'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col2_title',
        array(
            'label' => esc_html__( 'Column 2: Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );

    // Column 2: Sub title
    $wp_customize->add_setting( 'whatDoWeDo_col2_subtitle',
        array(
            'default' => 'A great way to commemorate love',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_col2_subtitle',
        array(
            'label' => esc_html__( 'Column 2: Sub Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'textarea',
        )
    );

    //Column 3: Icon
    $wp_customize->add_setting( 'whatDoWeDo_col3_image',
        array(
            'default' => get_template_directory_uri() . '/images/icon-3.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'whatDoWeDo_col3_image',
        array(
            'label' => esc_html__( 'Column 3: Icon','charity' ),
            'description' => esc_html__( 'Upload Image','charity' ),
            'section' => 'whatdowedo_settings',
        )
    ) );

    //Column 3: Title
    $wp_customize->add_setting( 'whatDoWeDo_col3_title',
        array(
            'default' => esc_html__('Caring','charity'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col3_title',
        array(
            'label' => esc_html__( 'Column 3: Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );

    // Column 3: Sub title
    $wp_customize->add_setting( 'whatDoWeDo_col3_subtitle',
        array(
            'default' => 'An opportunity to give something back',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_col3_subtitle',
        array(
            'label' => esc_html__( 'Column 3: Sub Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'textarea',
        )
    );

    //Column 4: Icon
    $wp_customize->add_setting( 'whatDoWeDo_col4_image',
        array(
            'default' => get_template_directory_uri() . '/images/icon-4.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'whatDoWeDo_col4_image',
        array(
            'label' => esc_html__( 'Column 4: Icon','charity' ),
            'description' => esc_html__( 'Upload Image','charity' ),
            'section' => 'whatdowedo_settings',
        )
    ) );

    //Column 4: Title
    $wp_customize->add_setting( 'whatDoWeDo_col4_title',
        array(
            'default' => esc_html__('Donation','charity'),

        )
    );
    $wp_customize->add_control('whatDoWeDo_col4_title',
        array(
            'label' => esc_html__( 'Column 4: Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'text',
        )
    );

    // Column 4: Sub title
    $wp_customize->add_setting( 'whatDoWeDo_col4_subtitle',
        array(
            'default' => 'Helps earn respect, and turn tears into smiles',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_col4_subtitle',
        array(
            'label' => esc_html__( 'Column 4: Sub Title','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'textarea',
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'whatDoWeDo_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'whatDoWeDo_show',
        array(
            'label' => esc_html__( 'Show Section','charity' ),
            'section' => 'whatdowedo_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','charity' ),
                'yes' => esc_html__( 'Yes','charity' ),
            )
        )
    );

    //End What do we do

    //What we did
    $wp_customize->add_section( 'whatwedid_settings',
        array(
            'title' => esc_html__( '"What we did" Section','charity'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    //Tile
    $wp_customize->add_setting( 'whatwedid_title',
        array(
            'default' => esc_html__('What we did last year? Help us to help others...','charity'),
        )
    );
    $wp_customize->add_control('whatwedid_title',
        array(
            'label' => esc_html__( 'Title Section','charity' ),
            'section' => 'whatwedid_settings',
            'type' => 'text',
        )
    );

    //  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

    $wp_customize->add_setting( 'whatwedid_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('whatwedid_category',
        array(
            'label' => esc_html__( 'Category Post','charity' ),
            'section' => 'whatwedid_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

    //Number post
    $wp_customize->add_setting( 'whatwedid_number_post',
        array(
            'default' => esc_html__('4','charity'),
        )
    );
    $wp_customize->add_control('whatwedid_number_post',
        array(
            'label' => esc_html__( 'Number Post','charity' ),
            'section' => 'whatwedid_settings',
            'type' => 'text',
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'whatwedid_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'whatwedid_show',
        array(
            'label' => esc_html__( 'Show Section','charity' ),
            'section' => 'whatwedid_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','charity' ),
                'yes' => esc_html__( 'Yes','charity' ),
            )
        )
    );

    //End what we did

    //Blog
    $wp_customize->add_section( 'blogs_settings',
        array(
            'title' => esc_html__( 'Blog Section','charity'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    //Title
    $wp_customize->add_setting( 'blogs_title',
        array(
            'default' => esc_html__('Latest News & Events Articles about charities...','charity'),
        )
    );
    $wp_customize->add_control('blogs_title',
        array(
            'label' => esc_html__( 'Title Section','charity' ),
            'section' => 'blogs_settings',
            'type' => 'text',
        )
    );

    //Category
    $wp_customize->add_setting( 'blogs_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('blogs_category',
        array(
            'label' => esc_html__( 'Category Post','charity' ),
            'section' => 'blogs_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

    //Number post
    $wp_customize->add_setting( 'blogs_number_post',
        array(
            'default' => esc_html__('2','charity'),
        )
    );
    $wp_customize->add_control('blogs_number_post',
        array(
            'label' => esc_html__( 'Number Post','charity' ),
            'section' => 'blogs_settings',
            'type' => 'text',
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'blogs_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'blogs_show',
        array(
            'label' => esc_html__( 'Show Section','charity' ),
            'section' => 'blogs_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','charity' ),
                'yes' => esc_html__( 'Yes','charity' ),
            )
        )
    );



    //End Blog



    //Donate Session
    $wp_customize->add_section( 'donate_settings',
        array(
            'title' => esc_html__( 'Donate section','charity'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    //Donate Background
    $wp_customize->add_setting( 'donate_bg',
        array(
            'default' => get_template_directory_uri() . '/images/bg-session.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'donate_bg',
        array(
            'label' => esc_html__( 'Background','charity' ),
            'description' => esc_html__( 'Upload Image','charity' ),
            'section' => 'donate_settings',
        )
    ));

    //The left title
    $wp_customize->add_setting( 'donate_left_title',
        array(
            'default' => 'Giving the Hands to the Poor help us to get help them',
        )
    );
    $wp_customize->add_control( 'donate_left_title',
        array(
            'label' => esc_html__( 'The left title','charity' ),
            'section' => 'donate_settings',
            'type' => 'text'
        )
    );

    //The left content
    $wp_customize->add_setting( 'donate_left_content',
        array(
            'default' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ornare arcu dui vivamus arcu. Scelerisque eleifend donec pretium vulputate sapien nec sagittis.</p>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'donate_left_content',
        array(
            'label' => esc_html__( 'The left content','charity' ),
            'description' => esc_html__( 'The left content','charity' ),
            'section' => 'donate_settings',
        )
    ));

    //The right title
    $wp_customize->add_setting( 'donate_right_title',
        array(
            'default' => 'For Further Information walk in or contact us',
        )
    );
    $wp_customize->add_control( 'donate_right_title',
        array(
            'label' => esc_html__( 'The right title','charity' ),
            'section' => 'donate_settings',
            'type' => 'text'
        )
    );

    //The right content
    $wp_customize->add_setting( 'donate_right_content',
        array(
            'default' => '<p>145 Amands Street, Beverly Hill Sights, NYC 2345</p>
                        <p>Phone:<a href="tel:(800) 0123 4567 890">(800) 0123 4567 890</a></p>
                        <p>Email:<a href="mailto:barbershop@email.com">barbershop@email.com</a></p>
                        <p><a href="www.charitytheme.com">www.charitytheme.com</a></p>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'donate_right_content',
        array(
            'label' => esc_html__( 'The right content','charity' ),
            'description' => esc_html__( 'The right content','charity' ),
            'section' => 'donate_settings',
        )
    ));

    //Button
    $wp_customize->add_setting( 'donate_button',
        array(
            'default' => esc_html__('Donate','charity'),

        )
    );
    $wp_customize->add_control('donate_button',
        array(
            'label' => esc_html__( 'Button title','charity' ),
            'section' => 'donate_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'donate_link',
        array(
            'default' => esc_html__('#','charity'),

        )
    );
    $wp_customize->add_control('donate_link',
        array(
            'label' => esc_html__( 'Button URL','charity' ),
            'section' => 'donate_settings',
            'type' => 'text',
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'donate_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'donate_show',
        array(
            'label' => esc_html__( 'Show Section','charity' ),
            'section' => 'donate_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','charity' ),
                'yes' => esc_html__( 'Yes','charity' ),
            )
        )
    );

    //End Donate

    //Contact session
    $wp_customize->add_section( 'contact_settings',
        array(
            'title' => esc_html__( 'Contact section','charity'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    //Contact shortcode
    $wp_customize->add_setting( 'contact_shortcode',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control( 'contact_shortcode',
        array(
            'label' => esc_html__( 'Contact shortcode','charity' ),
            'section' => 'contact_settings',
            'type' => 'text'
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'contact_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'contact_show',
        array(
            'label' => esc_html__( 'Show Section','charity' ),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','charity' ),
                'yes' => esc_html__( 'Yes','charity' ),
            )
        )
    );

    //End Contact

	// End Home Options
	
	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','charity'  ),
	      'priority' => 35, 
	   )
	);

	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '(C) 2018. AllRights Reserved. Divine Makeup Artist. Designed by <a href="#">Template.net</a>',
			'sanitize_callback' => 'wp_kses_post',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','charity' ),
	      'section' => 'footer_settings',
	   )
	) );	
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','charity'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','charity' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','charity' ),
	         'yes' => esc_html__( 'Yes','charity' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','charity'),
		'instagram'=> esc_html__('Instagram','charity'),
		'twitter'=> esc_html__('Twitter','charity'),
		'google'=> esc_html__('Google','charity'),
		'linkedin'=> esc_html__('Linkedin','charity')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','charity' ),
				 'yes' => esc_html__( 'Yes','charity' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','charity'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','charity'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','charity' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','charity' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','charity' ),
	         'yes' => esc_html__( 'Yes','charity' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','charity' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','charity' ),
	         'yes' => esc_html__( 'Yes','charity' ),
	      )
	   )
	);
}




















/**
 * Theme options in the Customizer js
 * @package charity
 */

function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'charity_customize_admin_menu_hide', 999 );
function charity_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="charity-theme-options"><span>'.esc_html__( 'charity','charity' ).'</span>'.esc_html__( 'Theme Options','charity' ).'</div>',
         '<div class="charity-theme-options"><span>'.esc_html__( 'charity','charity' ).'</span>'.esc_html__( 'Theme Options','charity' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}