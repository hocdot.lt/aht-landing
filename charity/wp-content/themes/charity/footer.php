
</div> <!-- End main-->
<footer class="footer">
    <div class=".footer-wrapper">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <h2 class="logo">
                            <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                <?php
                                $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                if ($logo_header && $logo_header!=''):
                                    echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                else:
                                    //bloginfo('name');
                                endif;
                                ?>
                            </a>
                        </h2>
                    </div>
                    <div class="col-lg-6 d-flex flex-column justify-content-center">
                        <div class="main-menu">
                            <div class="menu-container">
                                <?php
                                if(function_exists('render_header_menu')){
                                    render_header_menu();
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="copyright">
                            <?php
                                $copyright =  get_theme_mod('copyright','<p class="text-center m-0">(C) All Rights Reserved. Charity Theme, Designed & Developed by<a href="#">Template.net</a></p>');;
                                if ( $copyright !='') : ?>
                                    <?php echo wp_kses_post( $copyright); ?>
                            <?php endif;?>
                            <!--<p class="text-center m-0">(C) All Rights Reserved. Charity Theme, Designed & Developed by<a href="#">Template.net</a></p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>