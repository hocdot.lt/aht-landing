<?php
/**
 * Render contact sesion
 */
//Get Config
$contactShow = get_theme_mod('contact_show','yes');
$contactShortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="5" title="Contact"]');
?>
<?php if($contactShow): ?>
    <div id="contactSession">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-1">
                    <?php echo do_shortcode($contactShortcode) ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>