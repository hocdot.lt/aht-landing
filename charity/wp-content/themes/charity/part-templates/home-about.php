
<?php
/**
 * Render about-us in home page
 */

//Get config
$aboutShow =  get_theme_mod('about_show','yes');
//Column 1
$abColumn1Image = get_theme_mod('about_col_1_image',get_template_directory_uri() . '/images/about-1.png');
$abColumn1Title = get_theme_mod('about_col_1_title','Our Mission');
$abColumn1SubTitle = get_theme_mod('about_col_1_subtitle','We are a group united by our passion to make a difference. We vow to help create change by making a better living condition for the poor.');
//Column 2
$abColumn2Image = get_theme_mod('about_col_2_image',get_template_directory_uri() . '/images/about-2.png');
$abColumn2Title = get_theme_mod('about_col_2_title','Our GOals');
$abColumn2SubTitle = get_theme_mod('about_col_2_subtitle','We are a group united by our passion to make a difference. We vow to help create change by making a better living condition for the poor.');
//Column 3
$abColumn3Image = get_theme_mod('about_col_3_image',get_template_directory_uri() . '/images/about-3.png');
$abColumn3Title = get_theme_mod('about_col_3_title','volunteer');
$abColumn3SubTitle = get_theme_mod('about_col_3_subtitle','We are a group united by our passion to make a difference. We vow to help create change by making a better living condition for the poor.');

?>
<?php if($aboutShow === 'yes'): ?>
    <div id="aboutUsSession">
        <div class="container">
            <div class="row">
                <div class="col item">
                    <div class="media"><img src="<?php echo $abColumn1Image ?>"></div>
                    <div class="info">
                        <h3><?php echo $abColumn1Title ?></h3>
                        <p><?php echo $abColumn1SubTitle ?></p>
                    </div>
                </div>
                <div class="col item">
                    <div class="media"><img src="<?php echo $abColumn2Image ?>"></div>
                    <div class="info">
                        <p><?php echo $abColumn2SubTitle ?></p>
                        <h3><?php echo $abColumn2Title ?></h3>
                    </div>
                </div>
                <div class="col item">
                    <div class="media"><img src="<?php echo $abColumn3Image ?>"></div>
                    <div class="info">
                        <h3><?php echo $abColumn3Title ?></h3>
                        <p><?php echo $abColumn3SubTitle ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>