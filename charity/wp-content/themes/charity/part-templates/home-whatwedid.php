<?php
$whatwedidShow = get_theme_mod('whatwedid_show','yes');
$whatwedidTitle = get_theme_mod('whatwedid_title','What we did last year? Help us to help others...');
$whatwedidCategory = get_theme_mod('whatwedid_category','');
$whatwedidLimit = get_theme_mod('whatwedid_number_post',4);

$args = array(
    'posts_per_page'   => $whatwedidLimit,
    'offset'           => 0,
    'cat'         => $whatwedidCategory,
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );
$i = 1;
?>
<?php if($whatwedidShow): ?>
<div id="whatwedidSession">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="session-title">
                    <h2><?php echo $whatwedidTitle ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($posts_array as $_post): ?>
            <div class="col-lg-4 item">
                <div class="media"> <?php echo get_the_post_thumbnail($_post->ID); ?></div>
                <div class="info">
                    <h3><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a></h3>
                    <p><?php echo get_the_excerpt($_post->ID) ?></p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <hr>
    </div>
</div>
<?php endif; ?>