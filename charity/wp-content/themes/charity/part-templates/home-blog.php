<?php

$blogsShow = get_theme_mod('blogs_show','yes');
$blogsTitle = get_theme_mod('blogs_title','Latest News & Events Articles about charities...');
$blogsCategory = get_theme_mod('blogs_category','');
$blogsLimit = get_theme_mod('blogs_number_post',4);

$args = array(
	'posts_per_page'   => $blogsLimit,
	'offset'           => 0,
	'cat'         => $blogsCategory,
	'post_type'        => 'post',
);
$posts_array = get_posts( $args );
$i = 1;
?>
<?php if($blogsShow): ?>
<div id="blogSession">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="session-title">
                    <h2><?php echo $blogsTitle ?></h2>
                </div>
            </div>
        </div>
        <?php foreach ($posts_array as $_post): ?>
        <?php if($i == 1): ?>
            <?php $i=2; ?>
            <div class="row item">
            <div class="col-xl-5">
                <div class="pr-xl-4">
                    <div class="media">
                        <?php echo get_the_post_thumbnail($_post->ID); ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 info">
                <div class="pl-xl-3">
                    <h3><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a></h3>
                    <p><?php echo get_the_excerpt($_post->ID) ?></p>
                    <a class="readmore" href="<?php echo get_post_permalink($_post->ID) ?>">Continue Reading</a>
                </div>
            </div>
        </div>
        <?php else: ?>
                <?php $i=1; ?>
            <div class="row item">
            <div class="col-xl-3 info">
                <h3><a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a></h3>
                <p><?php echo get_the_excerpt($_post->ID) ?></p>
                <a class="readmore" href="<?php echo get_post_permalink($_post->ID) ?>">Continue Reading</a>
            </div>
            <div class="col-xl-5">
                <div class="pl-xl-3">
                    <div class="media"> <?php echo get_the_post_thumbnail($_post->ID); ?></div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php endforeach; ?>

    </div>
</div>
<?php endif; ?>