<?php
/**
 * Render banner in home page
 */

//Get config
$bannerShow = get_theme_mod('banner_show','yes');
$bannerTitle =  get_theme_mod('banner_title','');
$bannerSubTitle =  get_theme_mod('banner_sub_title','');
$bannerButtonText =  get_theme_mod('button_banner','Donate');
$bannerButtonLink =  get_theme_mod('link_banner','#');
$bannerBg = get_theme_mod('background_banner',get_template_directory_uri() . '/images/banner.png');
?>
<?php if($bannerShow === 'yes'): ?>
    <div id="bannerSession">
        <div class="media"><img src="<?php echo $bannerBg ?>"></div>
        <div class="cap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="inner">
                            <?php if($bannerTitle): ?>
                                <h3><?php echo $bannerTitle ?></h3>
                            <?php endif; ?>
                            <?php if($bannerSubTitle): ?>
                                <p><?php echo $bannerSubTitle ?></p>
                            <?php endif; ?>
                            <?php if($bannerButtonText): ?>
                                <h4><a href="<?php echo $bannerButtonLink ?>"><?php echo $bannerButtonText ?></a></h4>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>