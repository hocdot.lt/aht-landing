<?php
/**
 * Render what do we do session
 */

//Get config
$whatdoweShow = get_theme_mod('whatDoWeDo_show','yes');
$whatdowe_title = get_theme_mod('whatDoWeDo_title','What do we do? and what inspirs us to do this?');
//Column 1
$whatdowe_col1_icon = get_theme_mod('whatDoWeDo_col1_image',get_template_directory_uri() . '/images/icon-1.png');
$whatdowe_col1_title = get_theme_mod('whatDoWeDo_col1_title','Charity');
$whatdowe_col1_subtitle = get_theme_mod('whatDoWeDo_col1_subtitle','Charity reflects on your own inner values');
//Column 2
$whatdowe_col2_icon = get_theme_mod('whatDoWeDo_col2_image',get_template_directory_uri() . '/images/icon-2.png');
$whatdowe_col2_title = get_theme_mod('whatDoWeDo_col2_title','Helping');
$whatdowe_col2_subtitle = get_theme_mod('whatDoWeDo_col2_subtitle','A great way to commemorate love');
//Column 3
$whatdowe_col3_icon = get_theme_mod('whatDoWeDo_col3_image',get_template_directory_uri() . '/images/icon-3.png');
$whatdowe_col3_title = get_theme_mod('whatDoWeDo_col3_title','Caring');
$whatdowe_col3_subtitle = get_theme_mod('whatDoWeDo_col3_subtitle','An opportunity to give something back');
//Column 4
$whatdowe_col4_icon = get_theme_mod('whatDoWeDo_col4_image',get_template_directory_uri() . '/images/icon-4.png');
$whatdowe_col4_title = get_theme_mod('whatDoWeDo_col4_title','Donation');
$whatdowe_col4_subtitle = get_theme_mod('whatDoWeDo_col4_subtitle','Helps earn respect, and turn tears into smiles');
?>
<?php if($whatdoweShow === 'yes'): ?>
    <div id="whatdoweSession">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="session-title">
                        <h2><?php echo $whatdowe_title ?></h2>
                    </div>
                </div>
            </div>
            <div class="row items">
                <div class="col-lg-2 col-sm-4 item">
                    <div class="inner">
                        <div class="icon"><img src="<?php echo $whatdowe_col1_icon ?>"></div>
                        <div class="text">
                            <p><?php echo $whatdowe_col1_subtitle ?></p>
                        </div>
                        <div class="title">
                            <h3><?php echo $whatdowe_col1_title ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-4 item">
                    <div class="inner">
                        <div class="icon"><img src="<?php echo $whatdowe_col2_icon ?>"></div>
                        <div class="text">
                            <p><?php echo $whatdowe_col2_subtitle ?></p>
                        </div>
                        <div class="title">
                            <h3><?php echo $whatdowe_col2_title ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-4 item">
                    <div class="inner">
                        <div class="icon"><img src="<?php echo $whatdowe_col3_icon ?>"></div>
                        <div class="text">
                            <p><?php echo $whatdowe_col3_subtitle ?></p>
                        </div>
                        <div class="title">
                            <h3><?php echo $whatdowe_col3_title ?></h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-4 item">
                    <div class="inner">
                        <div class="icon"><img src="<?php echo $whatdowe_col4_icon ?>"></div>
                        <div class="text">
                            <p><?php echo $whatdowe_col4_subtitle ?></p>
                        </div>
                        <div class="title">
                            <h3><?php echo $whatdowe_col4_title ?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>