<?php 
/**
Template Name: Home
*/
get_header();

get_template_part('part-templates/home-banner');
get_template_part('part-templates/home-about');
get_template_part('part-templates/home-whatdowe');
get_template_part('part-templates/home-whatwedid');
get_template_part('part-templates/home-blog');
get_template_part('part-templates/home-donate');
get_template_part('part-templates/home-contact');

get_footer(); ?> 