-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2019 at 10:09 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beer_pub`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_akeeba_common`
--

CREATE TABLE `wp_akeeba_common` (
  `key` varchar(190) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `wp_ak_params`
--

CREATE TABLE `wp_ak_params` (
  `tag` varchar(255) NOT NULL,
  `data` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_ak_params`
--

INSERT INTO `wp_ak_params` (`tag`, `data`) VALUES
('update_version', '3.1.0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_ak_profiles`
--

CREATE TABLE `wp_ak_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) NOT NULL,
  `configuration` longtext,
  `filters` longtext,
  `quickicon` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_ak_profiles`
--

INSERT INTO `wp_ak_profiles` (`id`, `description`, `configuration`, `filters`, `quickicon`) VALUES
(1, 'Default Backup Profile', '###AES128###hC/6OsHC37aKfIC5NpDYOfzpCrrNCenm7fWzITDk2nJ3g7Jh/qYJxupgUEBfkOT2pxufOq12FGdKnUZ99S5EdqhDVuL/b4j+y0aCPoNRBzmCWpJ8UWlqFrwMH4ePiElAP2FY3oHe/JwHCgjeRwRqKVg/Mqp2NmuiZmQkCu63cxmO8deySqmgMTxkAkaL2p+ecTNGg+3W/VAJZek0uWDEUBaO1HnyGUl7RmOrV19t2G5mJ/cgDFerKIuge/QtpLlWl0BRCL26kTQ56OH4VKfw4Ma63BXN5uVM8v/X0oLSJ70W6cJCg0WWl9eUU+/Wv2arZm4+b/87B+CSVlh0KfuWT++PZlceifDYfI8TarmH4mhJ+uj77o+2oXU1/jPHymgsLQ9zN4w8jb0eXnVo+OLHoCkgUpBSrhhJ8YhjZ847WdBFdtyBZXeCkLOhz2J1Y0eTXHx5syJB3V96riTbx2S+cdcqlKe2Z7myYoxT8E4GCYu5xvz4mQr0zMWVyrXkDSNEs1wk5YU848edtxxfkFoJZWuQHpGrLfGX27UMiAZPtEU0sig9zeYp9W6xURpVI7wqEdrxNeguiitzyIDWX8bsHks9W2PRiQ8ntmMEhItb9NSUiP5pcTaiInGYDCcNufFF0TgWHUp+Dumwv7yoYtEnLReZSgXSC0sOUs019FcEQJwFQ/w7QmMhudqKOfxypKe1OpvziYF/eh2t4qY7LYj3GeKOnO877XgHUAJKcELWsZuqr1praqagpd/lFYTCu5n0A5D7TQcIdZTxTzR9GO+JxWtlvJVnjXF7QW+vHbaa+YRxftT2qDdF1eZVrL5zJQ1OuNvD+erVqFc2FSkxjnxuEJDcsHWxU12+QcH5zKVwzovdlaUUWi++XVggGv3wOcDAFrLZKHo4N9LZB+BUfDv7hp5t64m6DKYadVE/iYSat+7aeuM+fnaflyg5O7/wjoR8IE9HO9ldhOl0gKw36CCG9/4DgIv1OY55VwqFn1MAsugEX7dTAgYvlZtG/KKa49QJE0OHWf6XeiV7QZfo2FyAsUu0ThLbb1ZwygcIfLxXWLaXdS+1p0O13fcuiPi7ky6eTQU3ymNUTL7SmEZY4knyFzI34q1P1o/FwBkIPNR7L04C/ZJbJYHGH6SkjhEByAKuo8hFRONrPHn0fzRp3R6zXl46sAuUpU7E8PR3O6kd5l8dbZH/x7sS8XLqwiciKBFaMGP9s+TGG/F45Z/fdzTzE5zTZvAWlmZ7BN6fUTf70i6Qd8+v2e8oOhabhHKLue69cMJ5tryXP13jGa6lIjnhFmRSilMsShYCMkFYb98pU6Wx/2gDox9n1VCrHXe5jwVpYvVJ8GeiSQbX+x/iX6mwU4fZl/HCUGxE9gqQ03BZsg5OjsowFIKMobzEjhfRFiMGZsMj/pAQCMEQ2V0uSIBORYQz4XsRbmpCA/fj1s0I+m7sm6VhZrGkZYTBTkpMbynLFeWIfOs75Uxia8Jw7m+58FeHIiFFWtqknetO+P5gSsuQpbnAylhSH1NbbufDxlKnIjT12C6wePrc1f3gvgtBEZgdiG6HCUwzbeiMFywnF489cA8zCBhmgsPT1B8kTwBwMw9Hd9Z43N7Dl79N37nfwhOcZLHRdq9p/AaZlYqPmAfLflmiusZzqwkHkVhQx7ETan7LT4+hulQYv8xegsDTkk2punTi0oJVfoogNHJpqX1tN2++Af1qjL9lk6LIxdwbnahihL0+6ceObEtBAQR1z7R8NW79QrLvyiEP9gSBrufoD1scx2oPrnZfTw6Tco2RoLcNkoxBLdrqTY3zhXFGzHKE2pQydeC9Z2lBvOmSd3nW++dAfm0cJtq5NjMtSwJQhFYyvxldw3o1tsPQuqfQgKdotDo6ZVQN2JPHiVflayxUonEJHiBArX8h/qy6TD+aCg7hm2sGBsF5ZPOOdsJif2NiZsKblSkbSw139IqM/q2XCyCU3IWcfhxOJP+rj0VJ33/OcKsAv4H83X5d7kzgJRMfdPidPL56qhHEgUKsiI+DxvK/iMyJCOu6hpzppmpz7dcdn/F4H5UC5LzkfM51dDOPI4Oht4hNx22GraatFUGS83apxISDAx88dVgZ5nAGy7Na7iz88Im6OXmT5VbGTPIl/p7CabNMXuCrnwZzOWAdrreXDg2W5AA9nceGCbiQKRgdAHhKvs9+ILO7ebzOuaXPc4WRizt5rvoovVLRHSubWYXqm7LtJar+uh8YY4bnGqTEbAmxQWI61suON0U33BLskds+UFK3Jb/QDc1T7RUnOUSiV+HSuLfhMcbhJDVWNorC2NjdylJ/x0Nlmc9mUWfEoiWz5fqo4rZN5uUTfAkAHU+W9mnIv2PWaOC7sh+seJO34tVxRUXx7mwZalDHue5jYQUtcgH5FuGALXc7apI1iSAKQf3FwTexoe/z2sjg6eDSG0+U58RCSF27BvnZAoJXWQZaUVJc9OH2Ngs7gN/XTSjATHBytqqdy9l+O+IQOQw+niko1TJT7qgbRPoiFCtige4duY4q7mlLQki3r5X+0FSW7se37Dys7n+/wxxRUvMA6kkdYAR01N8IpreVCHDYZeraxRIKZ4t532kgC9r9kqhW9duD0eCIk0zjXDvWYDkefdNKdY+9TeRvLIquo5wxV6jJJbEPoZ2ZFWO9M3NKUFNUM2b/50TABtkyI4lvbD6vvnkr/kRD3ofMHkozt5+Nu9mDVh3z2XZjtaDHXJmaiqSWHDsWrHVowrZm1kdtrfHtrEpQSVY2gdqdPXnJm9ewhQDgjxYsxgcAAA==', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_ak_stats`
--

CREATE TABLE `wp_ak_stats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) NOT NULL,
  `comment` longtext,
  `backupstart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `backupend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('run','fail','complete') NOT NULL DEFAULT 'run',
  `origin` varchar(30) NOT NULL DEFAULT 'backend',
  `type` varchar(30) NOT NULL DEFAULT 'full',
  `profile_id` bigint(20) NOT NULL DEFAULT '1',
  `archivename` longtext,
  `absolute_path` longtext,
  `multipart` int(11) NOT NULL DEFAULT '0',
  `tag` varchar(255) DEFAULT NULL,
  `backupid` varchar(255) DEFAULT NULL,
  `filesexist` tinyint(3) NOT NULL DEFAULT '1',
  `remote_filename` varchar(1000) DEFAULT NULL,
  `total_size` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_ak_stats`
--

INSERT INTO `wp_ak_stats` (`id`, `description`, `comment`, `backupstart`, `backupend`, `status`, `origin`, `type`, `profile_id`, `archivename`, `absolute_path`, `multipart`, `tag`, `backupid`, `filesexist`, `remote_filename`, `total_size`) VALUES
(1, 'admin', '', '2019-02-13 21:10:02', '2019-02-13 21:11:51', 'complete', 'backend', 'full', 1, 'site-localhost-20190214-111002utc.jpa', 'D:/xampp5/htdocs/aht/landing-pages/charity/wp-content/plugins/akeebabackupwp/app/backups/site-localhost-20190214-111002utc.jpa', 1, 'backend', 'id1', 1, NULL, 29814183);

-- --------------------------------------------------------

--
-- Table structure for table `wp_ak_storage`
--

CREATE TABLE `wp_ak_storage` (
  `tag` varchar(255) NOT NULL,
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_ak_storage`
--

INSERT INTO `wp_ak_storage` (`tag`, `lastupdate`, `data`) VALUES
('liveupdate', '2019-02-14 23:37:37', '{\"stuck\":0,\"software\":\"Akeeba Backup for WordPress\",\"version\":\"3.4.0\",\"link\":\"http:\\/\\/cdn.akeebabackup.com\\/downloads\\/backupwp\\/3.4.0\\/akeebabackupwp-3.4.0-core.zip\",\"date\":\"2019-02-12\",\"releasenotes\":\"<h3>What\'s new<\\/h3><p>    <strong>Removed support for PrestaShop<\\/strong>. The release of PrestaShop 1.7.5, which is essentially a completely different application, means that <a href=\\\"https:\\/\\/github.com\\/akeeba\\/angie\\/issues\\/63\\\">we can no longer make transferred sites work properly<\\/a>. As a result we had to discontinue support for PrestaShop altogether.<\\/p><p>    <strong>Better WordPress .htaccess support<\\/strong>. Our restoration script handles many more cases for transferring a WordPress site between hosts and folders. Please note that at this point it\'s not possible to handle .htaccess changes correctly when transferring from the domain root to a subdirectory of the same or a different (sub)domain. Our troubleshooter includes instructions for manually handling these cases \\u2013 essentially removing .htaccess content that\'s not core WordPress and regenerating it manually through the plugins of the restored site.<\\/p><h3>PHP versions supported<\\/h3><p>    We only officially support using our software with PHP 5.6, 7.1, 7.2 or 7.3. We strongly advise you to run the latest available version of PHP on a branch currently maintained by the PHP project for security and performance reasons. Older versions of PHP have known major security issues which are being actively exploited to hack sites and they have stopped receiving security updates, leaving you exposed to these issues. Moreover, they are slower, therefore consuming more server resources to perform the same tasks.<\\/p><p>    Kindly note that our policy is to officially support only the PHP versions which are not yet End Of Life per the official PHP project with a voluntarily extension of support for 6 to 9 months after they become End of Life. After that time we stop providing any support for these obsolete versions of PHP without any further notice.<\\/p><h3>Changelog<\\/h3><h4>Bug fixes<\\/h4><ul>\\t<li>[HIGH] Google Storage JSON API could not download files when the path or filename contained spaces<\\/li>\\t<li>[HIGH] Google Storage would create large files with %2F in the filename instead of using subdirectories (the Google API documentation was, unfortunately, instructing us to do something wrong)<\\/li>\\t<li>[LOW] Fixed styling in ALICE page<\\/li>\\t<li>[LOW] Google Storage would not work on hosts which disable parse_ini_string()<\\/li>\\t<li>[LOW] Some character combinations in configuration values (e.g. $) could get changed or removed upon saving<\\/li>\\t<li>[MEDIUM] Integrated restoration with JPS archives wasn\'t allowed<\\/li>\\t<li>[MEDIUM] Restoring with the FTP or Hybrid file write mode didn\'t work properly<\\/li><\\/ul><h4>New features<\\/h4><ul>\\t<li>WordPress restoration: Handling of more .htaccess use cases<\\/li><\\/ul><h4>Removed features<\\/h4><ul>\\t<li>Removed support for PrestaShop<\\/li><\\/ul>\",\"infourl\":\"https:\\/\\/www.akeebabackup.com\\/download\\/backupwp\\/3-4-0.html\",\"md5\":\"50bcc02e315d47d2ab74a4b63266aa70\",\"sha1\":\"b7df0c460b8d4d4d978ca291b49899b929fc161c\",\"sha256\":\"360d5f14ac2f6f523af83b69c49dafb12f214e508dcabf95c2534cbe40c8d3d3\",\"sha384\":\"a3c56277faaf032c9b9d14bb683ce95c6a05b9dd17af1ab7a8f5b84e61a3beeb358652e5c5b7587583f14dd02da9010b\",\"sha512\":\"e61dcebff16885ec94396309b38ae0de88303fc478e19c1ae9ef5c807485a4cd64930c79faac694d084448ffb8f0695aca09a52aa5d7462e7432b4c6f5374067\",\"platforms\":\"classicpress\\/1.0,php\\/5.4,php\\/5.5,php\\/5.6,php\\/7.0,php\\/7.1,php\\/7.2,php\\/7.3,wordpress\\/3.8+\",\"loadedUpdate\":1,\"stability\":\"stable\"}'),
('liveupdate_lastcheck', '2019-02-14 23:37:36', '1550212656');

-- --------------------------------------------------------

--
-- Table structure for table `wp_ak_users`
--

CREATE TABLE `wp_ak_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `parameters` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL,
  `comment_date_gmt` datetime NOT NULL,
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL,
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/wordpress/', 'yes'),
(2, 'home', 'http://localhost/wordpress/', 'yes'),
(3, 'blogname', 'Beer Pub', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'example@example.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '', 'yes'),
(29, 'rewrite_rules', '', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:2:{i:0;s:36:\"contact-form-7/wp-contact-form-7.php\";i:1;s:37:\"mailchimp-for-wp/mailchimp-for-wp.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'beer_pub', 'yes'),
(41, 'stylesheet', 'beer_pub', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '180', 'yes'),
(59, 'thumbnail_size_h', '179', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '846', 'yes'),
(62, 'medium_size_h', '451', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', '', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:33:\"akeebabackupwp/akeebabackupwp.php\";a:2:{i:0;s:14:\"AkeebaBackupWP\";i:1;s:9:\"uninstall\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:7:{i:1553592976;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1553598018;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1553618176;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1553660340;a:1:{s:29:\"mc4wp_refresh_mailchimp_lists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1553661489;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1553661490;a:1:{s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentynineteen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1553504187;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}s:18:\"nav_menu_locations\";a:2:{s:6:\"menu-1\";i:2;s:6:\"footer\";i:3;}}', 'yes'),
(137, 'current_theme', 'beer_pub', 'yes'),
(138, 'theme_mods_makeupartist', 'a:7:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:4:\"logo\";s:78:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/logo.png\";s:16:\"background_image\";s:76:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/bg.jpg\";s:15:\"background_size\";s:5:\"cover\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1550033884;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(139, 'theme_switched', '', 'yes'),
(141, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1550032751;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(142, 'recently_activated', 'a:2:{s:31:\"query-monitor/query-monitor.php\";i:1550212661;s:33:\"akeebabackupwp/akeebabackupwp.php\";i:1550212658;}', 'yes'),
(143, 'widget_makeupartists_latest_tweet', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(144, 'widget_makeupartists_facebook_likebox', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(145, 'widget_makeupartists_instagram_feed', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(146, 'widget_makeupartists_about_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(147, 'widget_mc4wp_form_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(148, 'mc4wp_version', '4.3.3', 'yes'),
(151, 'mc4wp_flash_messages', 'a:0:{}', 'no'),
(152, 'mc4wp', 'a:4:{s:7:\"api_key\";s:37:\"e3c623f71aedd9077b9b00f96ac0ffb3-us13\";s:20:\"allow_usage_tracking\";i:0;s:15:\"debug_log_level\";s:7:\"warning\";s:18:\"first_activated_on\";i:1550032787;}', 'yes'),
(153, 'mc4wp_default_form_id', '6', 'yes'),
(154, 'mc4wp_form_stylesheets', 'a:0:{}', 'yes'),
(157, 'mc4wp_mailchimp_list_ids', 'a:5:{i:0;s:10:\"3eaf265522\";i:1;s:10:\"40a0127437\";i:2;s:10:\"5aa392c949\";i:3;s:10:\"bdc2fcba4f\";i:4;s:10:\"f6840a0d51\";}', 'no'),
(158, 'mc4wp_mailchimp_list_5aa392c949', 'O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"5aa392c949\";s:6:\"web_id\";i:227941;s:4:\"name\";s:9:\"Test List\";s:16:\"subscriber_count\";i:1;s:12:\"merge_fields\";a:2:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:9:\"Le Truong\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}', 'no'),
(159, 'mc4wp_mailchimp_list_bdc2fcba4f', 'O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"bdc2fcba4f\";s:6:\"web_id\";i:354489;s:4:\"name\";s:10:\"newsletter\";s:16:\"subscriber_count\";i:1;s:12:\"merge_fields\";a:3:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:10:\"First Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"FNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:2;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:10:\"Full State\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}', 'no'),
(160, 'mc4wp_mailchimp_list_3eaf265522', 'O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"3eaf265522\";s:6:\"web_id\";i:354533;s:4:\"name\";s:9:\"User - EN\";s:16:\"subscriber_count\";i:8;s:12:\"merge_fields\";a:3:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:10:\"First Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"FNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:2;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:7:\"User EN\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}', 'no'),
(161, 'mc4wp_mailchimp_list_f6840a0d51', 'O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"f6840a0d51\";s:6:\"web_id\";i:354541;s:4:\"name\";s:9:\"User - DE\";s:16:\"subscriber_count\";i:1;s:12:\"merge_fields\";a:3:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:10:\"First Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"FNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:2;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:7:\"User De\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}', 'no'),
(162, 'mc4wp_mailchimp_list_40a0127437', 'O:20:\"MC4WP_MailChimp_List\":7:{s:2:\"id\";s:10:\"40a0127437\";s:6:\"web_id\";i:354537;s:4:\"name\";s:9:\"User - FR\";s:16:\"subscriber_count\";i:0;s:12:\"merge_fields\";a:3:{i:0;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:13:\"Email Address\";s:10:\"field_type\";s:5:\"email\";s:3:\"tag\";s:5:\"EMAIL\";s:8:\"required\";b:1;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:1;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:10:\"First Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"FNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}i:2;O:27:\"MC4WP_MailChimp_Merge_Field\":7:{s:4:\"name\";s:9:\"Last Name\";s:10:\"field_type\";s:4:\"text\";s:3:\"tag\";s:5:\"LNAME\";s:8:\"required\";b:0;s:7:\"choices\";a:0:{}s:6:\"public\";b:1;s:13:\"default_value\";s:0:\"\";}}s:19:\"interest_categories\";a:0:{}s:17:\"campaign_defaults\";O:8:\"stdClass\":2:{s:9:\"from_name\";s:7:\"User Fr\";s:10:\"from_email\";s:18:\"truonglv@wizweb.io\";}}', 'no'),
(169, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(175, 'theme_mods_charity', 'a:20:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"background_image\";s:76:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/bg.jpg\";s:4:\"logo\";s:78:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/logo.png\";s:13:\"google_font_h\";s:11:\"Baloo Tamma\";s:11:\"google_font\";s:0:\"\";s:20:\"newsletter_shortcode\";s:0:\"\";s:9:\"copyright\";s:127:\"<p class=\"text-center m-0\">(C) All Rights Reserved. Charity Theme, Designed &amp; Developed by <a href=\"#\">Template.net</a></p>\";s:16:\"banner_sub_title\";s:137:\"We are a group united by our passion to make a difference. We vow to help create change by making a better living condition for the poor.\";s:17:\"background_banner\";s:80:\"http://localhost/aht/landing-pages/charity/wp-content/uploads/2019/02/banner.png\";s:12:\"banner_title\";s:49:\"Forget what you can get and see what you can give\";s:17:\"about_col_1_title\";s:11:\"Our Mission\";s:20:\"donate_right_content\";s:265:\"<p>145 Amands Street, Beverly Hill Sights, NYC 2345</p>\n<p>Phone:<a href=\"tel:(800) 0123 4567 890\">(800) 0123 4567 890</a></p>\n<p>Email:<a href=\"mailto:barbershop@email.com\">barbershop@email.com</a></p>\n<p><a href=\"www.charitytheme.com\">www.charitytheme.com</a></p>\";s:13:\"donate_button\";s:10:\"Donate Now\";s:17:\"contact_shortcode\";s:39:\"[contact-form-7 id=\"5\" title=\"Contact\"]\";s:18:\"whatwedid_category\";s:1:\"3\";s:10:\"blogs_show\";s:3:\"yes\";s:14:\"blogs_category\";s:1:\"4\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1552988238;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(259, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1553574994;s:7:\"checked\";a:3:{s:19:\"akismet/akismet.php\";s:5:\"4.1.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.1\";s:9:\"hello.php\";s:5:\"1.7.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(265, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1553574993;s:7:\"checked\";a:4:{s:8:\"beer_pub\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"1.3\";s:15:\"twentyseventeen\";s:3:\"2.1\";s:13:\"twentysixteen\";s:3:\"1.9\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(266, 'db_upgraded', '', 'yes'),
(267, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.1.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.1.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1553574989;s:15:\"version_checked\";s:5:\"5.1.1\";s:12:\"translations\";a:0:{}}', 'no'),
(269, 'can_compress_scripts', '1', 'no'),
(271, 'theme_mods_business-consultant', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1552988312;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(275, 'theme_mods_business_consultant', 'a:18:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:7:\"primary\";i:2;s:6:\"footer\";i:3;}s:18:\"custom_css_post_id\";i:-1;s:13:\"google_font_h\";s:88:\"Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i\";s:11:\"google_font\";s:21:\"Arial:100,200,300,400\";s:13:\"google_font_p\";s:21:\"Arial:100,200,300,400\";s:9:\"copyright\";s:100:\"<h6>(C) 2018. AllRights Reserved. Divine Makeup Artist. Designed b <a href=\"#\">Template.net</a></h6>\";s:17:\"about_number_post\";s:1:\"3\";s:10:\"desc_about\";s:87:\"Looking for Experienced Business Consultant for your Online Business? We will help you!\";s:14:\"About_category\";s:1:\"4\";s:16:\"Service_category\";s:1:\"5\";s:13:\"blog_category\";s:1:\"6\";s:17:\"contact_shortcode\";s:42:\"[contact-form-7 id=\"190\" title=\"contact2\"]\";s:12:\"banner_title\";s:60:\"Delivering Sustainable Growth & Profitability For Businesses\";s:19:\"service_number_post\";s:1:\"4\";s:16:\"banner_shortcode\";s:39:\"[contact-form-7 id=\"5\" title=\"Contact\"]\";s:28:\"customizer_repeater_whychose\";s:699:\"[{\"text\":\"undefined\",\"text2\":\"undefined\",\"title\":\"01)   FINANCIAL ANALYSIS\",\"subtitle\":\"90\",\"social_repeater\":\"\",\"id\":\"social-repeater-5c94ca7c6392b\",\"shortcode\":\"undefined\"},{\"text\":\"undefined\",\"text2\":\"undefined\",\"title\":\"02)   INVESTMENTS\",\"subtitle\":\"80\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c9879da291ef\",\"shortcode\":\"undefined\"},{\"text\":\"undefined\",\"text2\":\"undefined\",\"title\":\"03)   FINANCIAL PLANNER\",\"subtitle\":\"95\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c987aba32d89\",\"shortcode\":\"undefined\"},{\"text\":\"undefined\",\"text2\":\"undefined\",\"title\":\"04)   BUSINESS SUPPORT\",\"subtitle\":\"85\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c987ad209b48\",\"shortcode\":\"undefined\"}]\";s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1553504183;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(326, '_site_transient_timeout_browser_75b3341da9e7208fc03d0909f69991aa', '1553684746', 'no'),
(327, '_site_transient_browser_75b3341da9e7208fc03d0909f69991aa', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"72.0.3626.121\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(328, '_site_transient_timeout_php_check_2106b130eb124b3cdba89dd0ad6dc509', '1553684747', 'no'),
(329, '_site_transient_php_check_2106b130eb124b3cdba89dd0ad6dc509', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:1;}', 'no'),
(359, 'WPLANG', '', 'yes'),
(360, 'new_admin_email', 'example@example.com', 'yes'),
(385, 'theme_mods_beer_pub', 'a:12:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:6:\"footer\";i:3;s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"The_pub_category\";s:1:\"6\";s:20:\"our_brewery_category\";s:1:\"5\";s:25:\"customizer_repeater_rates\";s:1812:\"[{\"text\":\"$15&#x2F;-\",\"link\":\"#\",\"text2\":\"undefined\",\"title\":\"Red Cow Craft Beer\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\",\"social_repeater\":\"\",\"id\":\"social-repeater-5c998f3ae2b3e\",\"shortcode\":\"undefined\"},{\"text\":\"$15&#x2F;-\",\"link\":\"#\",\"text2\":\"undefined\",\"title\":\"Narragansett Lager\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c998f6a90975\",\"shortcode\":\"undefined\"},{\"text\":\"$25&#x2F;-\",\"link\":\"#\",\"text2\":\"undefined\",\"title\":\"Schofferhofer Grapefruit\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c998f93a38a9\",\"shortcode\":\"undefined\"},{\"text\":\"$15&#x2F;-\",\"link\":\"#\",\"text2\":\"undefined\",\"title\":\"Guinness Draught\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c998fb75d20b\",\"shortcode\":\"undefined\"},{\"text\":\"$15&#x2F;-\",\"link\":\"#\",\"text2\":\"undefined\",\"title\":\"The Perfect Beer\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c998fd004669\",\"shortcode\":\"undefined\"},{\"text\":\"$15&#x2F;-\",\"link\":\"#\",\"text2\":\"undefined\",\"title\":\"Awesome Beer\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c998fe614e44\",\"shortcode\":\"undefined\"},{\"text\":\"$15&#x2F;-\",\"link\":\"#\",\"text2\":\"undefined\",\"title\":\"That Kind Of Beer\",\"subtitle\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\",\"social_repeater\":\"\",\"id\":\"customizer-repeater-5c998ff70d377\",\"shortcode\":\"undefined\"}]\";s:28:\"customizer_repeater_whychose\";s:149:\"[{\"text\":\"undefined\",\"text2\":\"undefined\",\"title\":\"\",\"subtitle\":\"\",\"social_repeater\":\"\",\"id\":\"social-repeater-5c998f3aec9b6\",\"shortcode\":\"undefined\"}]\";s:13:\"blog_category\";s:1:\"7\";s:13:\"google_font_h\";s:19:\"Lily Script One:400\";s:18:\"google_font_header\";s:19:\"Life Savers:400,700\";s:13:\"google_font_p\";s:30:\"Libre Baskerville:400,400i,700\";s:11:\"map_contact\";s:277:\"<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14900.820008282744!2d105.78794855!3d20.984417999999998!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1svi!2s!4v1553584360760\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\";}', 'yes'),
(409, 'category_children', 'a:0:{}', 'yes'),
(417, '_site_transient_timeout_theme_roots', '1553576792', 'no'),
(418, '_site_transient_theme_roots', 'a:4:{s:8:\"beer_pub\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'homepage.php'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_form', '<div class=\"header-form\">\n                <h3>Get A Free Quote</h3>\n                <h5>-    Contact us today for a free no obligation consultation</h5>\n</div>\n<div class=\"form-group row\">\n	<div class=\"col-lg-2 col-xl-2\">\n		[text* your-name  class:form-control form-control-lg placeholder\"Enter Your Name\"]\n	</div>\n	<div class=\"col-lg-2 col-xl-2\">      \n		[email* your-email class:form-control form-control-lg placeholder \"Your Email\"]\n	</div>\n	<div class=\"col-lg-2 col-xl-2\">      \n		[text* your-name  class:form-control form-control-lg placeholder\"Mobile Number\"]\n	</div>\n	<div class=\"col-lg-2 col-xl-2\">\n		[submit class:btn \"Get A Quote\"]\n	</div>\n\n</div>'),
(4, 5, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:24:\"Charity \"[your-subject]\"\";s:6:\"sender\";s:29:\"Charity <example@example.com>\";s:9:\"recipient\";s:19:\"example@example.com\";s:4:\"body\";s:188:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Charity (http://localhost/aht/landing-pages/charity)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(5, 5, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:24:\"Charity \"[your-subject]\"\";s:6:\"sender\";s:29:\"Charity <example@example.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:130:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Charity (http://localhost/aht/landing-pages/charity)\";s:18:\"additional_headers\";s:29:\"Reply-To: example@example.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(6, 5, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(7, 5, '_additional_settings', ''),
(8, 5, '_locale', 'en_US'),
(9, 6, '_mc4wp_settings', 'a:8:{s:15:\"required_fields\";s:5:\"EMAIL\";s:12:\"double_optin\";s:1:\"1\";s:15:\"update_existing\";s:1:\"0\";s:17:\"replace_interests\";s:1:\"1\";s:18:\"hide_after_success\";s:1:\"0\";s:8:\"redirect\";s:0:\"\";s:3:\"css\";s:1:\"0\";s:5:\"lists\";a:0:{}}'),
(10, 6, 'text_subscribed', 'Thank you, your sign-up request was successful! Please check your email inbox to confirm.'),
(11, 6, 'text_invalid_email', 'Please provide a valid email address.'),
(12, 6, 'text_required_field_missing', 'Please fill in the required fields.'),
(13, 6, 'text_already_subscribed', 'Given email address is already subscribed, thank you!'),
(14, 6, 'text_error', 'Oops. Something went wrong. Please try again later.'),
(15, 6, 'text_unsubscribed', 'You were successfully unsubscribed.'),
(16, 6, 'text_not_subscribed', 'Given email address is not subscribed.'),
(17, 6, 'text_no_lists_selected', 'Please select at least one list.'),
(18, 6, 'text_updated', 'Thank you, your records have been updated!'),
(19, 2, '_edit_lock', '1550045565:1'),
(20, 8, '_menu_item_type', 'post_type'),
(21, 8, '_menu_item_menu_item_parent', '0'),
(22, 8, '_menu_item_object_id', '2'),
(23, 8, '_menu_item_object', 'page'),
(24, 8, '_menu_item_target', ''),
(25, 8, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(26, 8, '_menu_item_xfn', ''),
(27, 8, '_menu_item_url', ''),
(29, 9, '_menu_item_type', 'custom'),
(30, 9, '_menu_item_menu_item_parent', '0'),
(31, 9, '_menu_item_object_id', '9'),
(32, 9, '_menu_item_object', 'custom'),
(33, 9, '_menu_item_target', ''),
(34, 9, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(35, 9, '_menu_item_xfn', ''),
(36, 9, '_menu_item_url', '#about-us'),
(38, 10, '_menu_item_type', 'custom'),
(39, 10, '_menu_item_menu_item_parent', '0'),
(40, 10, '_menu_item_object_id', '10'),
(41, 10, '_menu_item_object', 'custom'),
(42, 10, '_menu_item_target', ''),
(43, 10, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(44, 10, '_menu_item_xfn', ''),
(45, 10, '_menu_item_url', '#the-pub'),
(56, 12, '_menu_item_type', 'custom'),
(57, 12, '_menu_item_menu_item_parent', '0'),
(58, 12, '_menu_item_object_id', '12'),
(59, 12, '_menu_item_object', 'custom'),
(60, 12, '_menu_item_target', ''),
(61, 12, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(62, 12, '_menu_item_xfn', ''),
(63, 12, '_menu_item_url', '#our-brewery'),
(65, 13, '_menu_item_type', 'custom'),
(66, 13, '_menu_item_menu_item_parent', '0'),
(67, 13, '_menu_item_object_id', '13'),
(68, 13, '_menu_item_object', 'custom'),
(69, 13, '_menu_item_target', ''),
(70, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(71, 13, '_menu_item_xfn', ''),
(72, 13, '_menu_item_url', '#rates'),
(220, 69, '_edit_lock', '1552988306:1'),
(221, 70, '_wp_trash_meta_status', 'publish'),
(222, 70, '_wp_trash_meta_time', '1553048865'),
(224, 71, '_customize_changeset_uuid', '2bdfb04b-4abc-498f-81c3-e3cba9dc9735'),
(225, 72, '_edit_lock', '1553052131:1'),
(226, 74, '_menu_item_type', 'post_type'),
(227, 74, '_menu_item_menu_item_parent', '0'),
(228, 74, '_menu_item_object_id', '71'),
(229, 74, '_menu_item_object', 'page'),
(230, 74, '_menu_item_target', ''),
(231, 74, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(232, 74, '_menu_item_xfn', ''),
(233, 74, '_menu_item_url', ''),
(242, 76, '_menu_item_type', 'custom'),
(243, 76, '_menu_item_menu_item_parent', '0'),
(244, 76, '_menu_item_object_id', '76'),
(245, 76, '_menu_item_object', 'custom'),
(246, 76, '_menu_item_target', ''),
(247, 76, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(248, 76, '_menu_item_xfn', ''),
(249, 76, '_menu_item_url', '#'),
(250, 77, '_menu_item_type', 'custom'),
(251, 77, '_menu_item_menu_item_parent', '0'),
(252, 77, '_menu_item_object_id', '77'),
(253, 77, '_menu_item_object', 'custom'),
(254, 77, '_menu_item_target', ''),
(255, 77, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(256, 77, '_menu_item_xfn', ''),
(257, 77, '_menu_item_url', '#blog'),
(258, 78, '_menu_item_type', 'custom'),
(259, 78, '_menu_item_menu_item_parent', '0'),
(260, 78, '_menu_item_object_id', '78'),
(261, 78, '_menu_item_object', 'custom'),
(262, 78, '_menu_item_target', ''),
(263, 78, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(264, 78, '_menu_item_xfn', ''),
(265, 78, '_menu_item_url', '#contact'),
(266, 79, '_menu_item_type', 'custom'),
(267, 79, '_menu_item_menu_item_parent', '0'),
(268, 79, '_menu_item_object_id', '79'),
(269, 79, '_menu_item_object', 'custom'),
(270, 79, '_menu_item_target', ''),
(271, 79, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(272, 79, '_menu_item_xfn', ''),
(273, 79, '_menu_item_url', '#services'),
(274, 72, '_wp_trash_meta_status', 'publish'),
(275, 72, '_wp_trash_meta_time', '1553052140'),
(276, 80, '_menu_item_type', 'custom'),
(277, 80, '_menu_item_menu_item_parent', '0'),
(278, 80, '_menu_item_object_id', '80'),
(279, 80, '_menu_item_object', 'custom'),
(280, 80, '_menu_item_target', ''),
(281, 80, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(282, 80, '_menu_item_xfn', ''),
(283, 80, '_menu_item_url', '#about_us'),
(285, 81, '_wp_trash_meta_status', 'publish'),
(286, 81, '_wp_trash_meta_time', '1553052574'),
(297, 88, '_edit_lock', '1553054542:1'),
(298, 88, '_customize_restore_dismissed', '1'),
(299, 89, '_wp_trash_meta_status', 'publish'),
(300, 89, '_wp_trash_meta_time', '1553055157'),
(301, 90, '_wp_trash_meta_status', 'publish'),
(302, 90, '_wp_trash_meta_time', '1553055370'),
(303, 91, '_wp_trash_meta_status', 'publish'),
(304, 91, '_wp_trash_meta_time', '1553055401'),
(305, 92, '_edit_lock', '1553055362:1'),
(306, 93, '_edit_lock', '1553056059:1'),
(321, 100, '_edit_lock', '1553056700:1'),
(328, 104, '_wp_trash_meta_status', 'publish'),
(329, 104, '_wp_trash_meta_time', '1553056803'),
(376, 127, '_edit_lock', '1553067237:1'),
(379, 129, '_wp_trash_meta_status', 'publish'),
(380, 129, '_wp_trash_meta_time', '1553067912'),
(381, 130, '_wp_trash_meta_status', 'publish'),
(382, 130, '_wp_trash_meta_time', '1553068270'),
(400, 142, '_wp_trash_meta_status', 'publish'),
(401, 142, '_wp_trash_meta_time', '1553069348'),
(403, 143, '_customize_restore_dismissed', '1'),
(404, 144, '_edit_lock', '1553071010:1'),
(405, 144, '_customize_restore_dismissed', '1'),
(406, 145, '_wp_trash_meta_status', 'publish'),
(407, 145, '_wp_trash_meta_time', '1553071991'),
(408, 146, '_wp_trash_meta_status', 'publish'),
(409, 146, '_wp_trash_meta_time', '1553072847'),
(410, 147, '_edit_lock', '1553073804:1'),
(411, 147, '_customize_restore_dismissed', '1'),
(412, 148, '_wp_trash_meta_status', 'publish'),
(413, 148, '_wp_trash_meta_time', '1553073855'),
(417, 151, '_edit_lock', '1553074432:1'),
(428, 149, '_customize_restore_dismissed', '1'),
(429, 157, '_wp_trash_meta_status', 'publish'),
(430, 157, '_wp_trash_meta_time', '1553075763'),
(431, 158, '_edit_lock', '1553076306:1'),
(432, 158, '_customize_restore_dismissed', '1'),
(433, 160, '_wp_trash_meta_status', 'publish'),
(434, 160, '_wp_trash_meta_time', '1553238993'),
(495, 161, '_edit_lock', '1553240044:1'),
(502, 166, '_edit_lock', '1553240302:1'),
(509, 171, '_wp_trash_meta_status', 'publish'),
(510, 171, '_wp_trash_meta_time', '1553241209'),
(511, 172, '_wp_trash_meta_status', 'publish'),
(512, 172, '_wp_trash_meta_time', '1553241236'),
(516, 174, '_edit_last', '1'),
(518, 174, '_edit_lock', '1553245427:1'),
(528, 182, '_edit_lock', '1553245644:1'),
(544, 173, '_customize_restore_dismissed', '1'),
(545, 189, '_wp_trash_meta_status', 'publish'),
(546, 189, '_wp_trash_meta_time', '1553246276'),
(547, 190, '_form', '<div class=\"header-form\">\n                <h3>Contact us for further information</h3>\n</div>\n<div class=\"form-group row\">\n	<div class=\"col-xl-4\">\n		[text* your-name  class:form-control form-control-lg placeholder\"Enter Your Name\"]\n	</div>\n	<div class=\"col-lg-4\">      \n		[email* your-email class:form-control form-control-lg placeholder \"Enter Your Email Address\"]\n	</div>\n</div>\n\n<div class=\"form-group\">\n	<div class=\"\">      \n		[textarea* your-message x3 class:form-control form-control-lg placeholder\"Type Your Message\"]\n	</div>\n</div>\n<div class=\"form-group\">\n\n		[submit class:btn \"Contact Us for free quotation\"]\n	</div>'),
(548, 190, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:33:\"Accountancy Firm \"[your-subject]\"\";s:6:\"sender\";s:38:\"Accountancy Firm <example@example.com>\";s:9:\"recipient\";s:19:\"example@example.com\";s:4:\"body\";s:181:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Accountancy Firm (http://localhost/wordpress)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(549, 190, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:33:\"Accountancy Firm \"[your-subject]\"\";s:6:\"sender\";s:38:\"Accountancy Firm <example@example.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:123:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Accountancy Firm (http://localhost/wordpress)\";s:18:\"additional_headers\";s:29:\"Reply-To: example@example.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(550, 190, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(551, 190, '_additional_settings', ''),
(552, 190, '_locale', 'en_US'),
(553, 191, '_wp_trash_meta_status', 'publish'),
(554, 191, '_wp_trash_meta_time', '1553248405'),
(555, 192, '_wp_trash_meta_status', 'publish'),
(556, 192, '_wp_trash_meta_time', '1553252243'),
(557, 193, '_wp_trash_meta_status', 'publish'),
(558, 193, '_wp_trash_meta_time', '1553253210'),
(559, 194, '_wp_trash_meta_status', 'publish'),
(560, 194, '_wp_trash_meta_time', '1553253593'),
(563, 195, '_wp_trash_meta_status', 'publish'),
(564, 195, '_wp_trash_meta_time', '1553255069'),
(565, 196, '_edit_lock', '1553496426:1'),
(566, 196, '_customize_restore_dismissed', '1'),
(567, 197, '_edit_lock', '1553496804:1'),
(568, 197, '_wp_trash_meta_status', 'publish'),
(569, 197, '_wp_trash_meta_time', '1553496806'),
(570, 198, '_wp_trash_meta_status', 'publish'),
(571, 198, '_wp_trash_meta_time', '1553497157'),
(573, 199, '_customize_changeset_uuid', '689d5bf3-dc76-4656-9797-dc02a5902ccc'),
(575, 200, '_customize_changeset_uuid', '689d5bf3-dc76-4656-9797-dc02a5902ccc'),
(576, 204, '_menu_item_type', 'custom'),
(577, 204, '_menu_item_menu_item_parent', '0'),
(578, 204, '_menu_item_object_id', '204'),
(579, 204, '_menu_item_object', 'custom'),
(580, 204, '_menu_item_target', ''),
(581, 204, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(582, 204, '_menu_item_xfn', ''),
(583, 204, '_menu_item_url', '#blog'),
(584, 205, '_menu_item_type', 'custom'),
(585, 205, '_menu_item_menu_item_parent', '0'),
(586, 205, '_menu_item_object_id', '205'),
(587, 205, '_menu_item_object', 'custom'),
(588, 205, '_menu_item_target', ''),
(589, 205, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(590, 205, '_menu_item_xfn', ''),
(591, 205, '_menu_item_url', '#contact'),
(592, 201, '_wp_trash_meta_status', 'publish'),
(593, 201, '_wp_trash_meta_time', '1553508366'),
(618, 206, '_edit_lock', '1553510250:1'),
(619, 207, '_wp_attached_file', '2019/03/the-pub-img-3.jpg'),
(620, 207, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:366;s:6:\"height\";i:359;s:4:\"file\";s:25:\"2019/03/the-pub-img-3.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"the-pub-img-3-180x179.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"galley-img-3\";a:4:{s:4:\"file\";s:25:\"the-pub-img-3-365x358.jpg\";s:5:\"width\";i:365;s:6:\"height\";i:358;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"galley-img-4\";a:4:{s:4:\"file\";s:25:\"the-pub-img-3-180x179.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(623, 206, '_thumbnail_id', '207'),
(624, 209, '_edit_lock', '1553510293:1'),
(625, 210, '_wp_attached_file', '2019/03/the-pub-img-2.jpg'),
(626, 210, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:367;s:6:\"height\";i:359;s:4:\"file\";s:25:\"2019/03/the-pub-img-2.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"the-pub-img-2-180x179.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"galley-img-3\";a:4:{s:4:\"file\";s:25:\"the-pub-img-2-366x358.jpg\";s:5:\"width\";i:366;s:6:\"height\";i:358;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"galley-img-4\";a:4:{s:4:\"file\";s:25:\"the-pub-img-2-180x179.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(629, 209, '_thumbnail_id', '210'),
(630, 212, '_edit_lock', '1553513216:1'),
(631, 213, '_wp_attached_file', '2019/03/the-pub-img-1.jpg'),
(632, 213, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:366;s:6:\"height\";i:359;s:4:\"file\";s:25:\"2019/03/the-pub-img-1.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"the-pub-img-1-180x179.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"galley-img-3\";a:4:{s:4:\"file\";s:25:\"the-pub-img-1-365x358.jpg\";s:5:\"width\";i:365;s:6:\"height\";i:358;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"galley-img-4\";a:4:{s:4:\"file\";s:25:\"the-pub-img-1-180x179.jpg\";s:5:\"width\";i:180;s:6:\"height\";i:179;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(635, 212, '_thumbnail_id', '213'),
(636, 215, '_edit_lock', '1553511749:1'),
(637, 216, '_wp_attached_file', '2019/03/our-brewery-img-1.png'),
(638, 216, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:143;s:4:\"file\";s:29:\"2019/03/our-brewery-img-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(641, 215, '_thumbnail_id', '216'),
(642, 218, '_wp_trash_meta_status', 'publish'),
(643, 218, '_wp_trash_meta_time', '1553511839'),
(644, 219, '_edit_lock', '1553511754:1'),
(645, 220, '_edit_lock', '1553513260:1'),
(646, 221, '_wp_attached_file', '2019/03/our-brewery-img-2.png'),
(647, 221, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:175;s:6:\"height\";i:147;s:4:\"file\";s:29:\"2019/03/our-brewery-img-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(650, 220, '_thumbnail_id', '221'),
(651, 223, '_edit_lock', '1553513306:1'),
(652, 224, '_wp_attached_file', '2019/03/our-brewery-img-3.png'),
(653, 224, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:147;s:6:\"height\";i:169;s:4:\"file\";s:29:\"2019/03/our-brewery-img-3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(656, 223, '_thumbnail_id', '224'),
(657, 226, '_edit_lock', '1553513339:1'),
(658, 227, '_wp_attached_file', '2019/03/our-brewery-img-4.png'),
(659, 227, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:157;s:6:\"height\";i:174;s:4:\"file\";s:29:\"2019/03/our-brewery-img-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(662, 226, '_thumbnail_id', '227'),
(663, 229, '_edit_last', '1'),
(664, 229, '_edit_lock', '1553565397:1'),
(665, 230, '_wp_attached_file', '2019/03/avatar.png'),
(666, 230, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:100;s:6:\"height\";i:100;s:4:\"file\";s:18:\"2019/03/avatar.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(667, 229, '_thumbnail_id', '230'),
(668, 229, 'testimonial_position', ''),
(669, 232, '_edit_last', '1'),
(670, 232, '_thumbnail_id', '230'),
(671, 232, 'testimonial_position', ''),
(672, 232, '_edit_lock', '1553565415:1'),
(673, 235, '_edit_last', '1'),
(674, 235, '_thumbnail_id', '230'),
(675, 235, 'testimonial_position', ''),
(676, 235, '_edit_lock', '1553566035:1'),
(677, 239, '_wp_trash_meta_status', 'publish'),
(678, 239, '_wp_trash_meta_time', '1553566081'),
(679, 240, '_edit_lock', '1553567754:1'),
(680, 240, '_wp_trash_meta_status', 'publish'),
(681, 240, '_wp_trash_meta_time', '1553567761'),
(682, 241, '_wp_trash_meta_status', 'publish'),
(683, 241, '_wp_trash_meta_time', '1553568340'),
(684, 242, '_wp_trash_meta_status', 'publish'),
(685, 242, '_wp_trash_meta_time', '1553568611'),
(686, 243, '_wp_trash_meta_status', 'publish'),
(687, 243, '_wp_trash_meta_time', '1553568647'),
(688, 244, '_wp_trash_meta_status', 'publish'),
(689, 244, '_wp_trash_meta_time', '1553568763'),
(690, 245, '_edit_lock', '1553569367:1'),
(691, 246, '_edit_lock', '1553569403:1'),
(694, 248, '_edit_lock', '1553569452:1'),
(697, 250, '_edit_lock', '1553569466:1'),
(700, 252, '_wp_trash_meta_status', 'publish'),
(701, 252, '_wp_trash_meta_time', '1553570092'),
(703, 253, '_customize_restore_dismissed', '1'),
(705, 254, '_customize_restore_dismissed', '1'),
(706, 255, '_edit_lock', '1553582359:1'),
(707, 255, '_customize_restore_dismissed', '1'),
(708, 256, '_edit_lock', '1553582424:1'),
(709, 256, '_customize_restore_dismissed', '1'),
(710, 257, '_edit_lock', '1553582569:1'),
(711, 257, '_customize_restore_dismissed', '1'),
(712, 258, '_wp_trash_meta_status', 'publish'),
(713, 258, '_wp_trash_meta_time', '1553583174'),
(715, 259, '_customize_restore_dismissed', '1'),
(716, 260, '_edit_lock', '1553583375:1'),
(717, 260, '_customize_restore_dismissed', '1'),
(718, 261, '_wp_trash_meta_status', 'publish'),
(719, 261, '_wp_trash_meta_time', '1553583570'),
(720, 262, '_edit_lock', '1553583666:1'),
(721, 262, '_wp_trash_meta_status', 'publish'),
(722, 262, '_wp_trash_meta_time', '1553583684'),
(723, 263, '_wp_trash_meta_status', 'publish'),
(724, 263, '_wp_trash_meta_time', '1553586443'),
(725, 264, '_edit_lock', '1553588032:1'),
(726, 264, '_wp_trash_meta_status', 'publish'),
(727, 264, '_wp_trash_meta_time', '1553588053'),
(728, 265, '_wp_trash_meta_status', 'publish'),
(729, 265, '_wp_trash_meta_time', '1553588512'),
(730, 266, '_edit_lock', '1553590926:1'),
(731, 266, '_wp_trash_meta_status', 'publish'),
(732, 266, '_wp_trash_meta_time', '1553590950'),
(733, 267, '_edit_lock', '1553591013:1'),
(734, 267, '_customize_restore_dismissed', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL,
  `post_date_gmt` datetime NOT NULL,
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL,
  `post_modified_gmt` datetime NOT NULL,
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(2, 1, '2019-02-13 04:36:14', '2019-02-13 04:36:14', '', 'Home', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-02-13 07:53:05', '2019-02-13 07:53:05', '', 0, 'http://localhost/aht/landing-pages/charity/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-02-13 04:36:14', '2019-02-13 04:36:14', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/aht/landing-pages/charity.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-02-13 04:36:14', '2019-02-13 04:36:14', '', 0, 'http://localhost/aht/landing-pages/charity/?page_id=3', 0, 'page', '', 0),
(5, 1, '2019-02-13 04:39:11', '2019-02-13 04:39:11', '<div class=\"header-form\">\r\n                <h3>Get A Free Quote</h3>\r\n                <h5>-    Contact us today for a free no obligation consultation</h5>\r\n</div>\r\n<div class=\"form-group row\">\r\n	<div class=\"col-lg-2 col-xl-2\">\r\n		[text* your-name  class:form-control form-control-lg placeholder\"Enter Your Name\"]\r\n	</div>\r\n	<div class=\"col-lg-2 col-xl-2\">      \r\n		[email* your-email class:form-control form-control-lg placeholder \"Your Email\"]\r\n	</div>\r\n	<div class=\"col-lg-2 col-xl-2\">      \r\n		[text* your-name  class:form-control form-control-lg placeholder\"Mobile Number\"]\r\n	</div>\r\n	<div class=\"col-lg-2 col-xl-2\">\r\n		[submit class:btn \"Get A Quote\"]\r\n	</div>\r\n\r\n</div>\n1\nCharity \"[your-subject]\"\nCharity <example@example.com>\nexample@example.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Charity (http://localhost/aht/landing-pages/charity)\nReply-To: [your-email]\n\n\n\n\nCharity \"[your-subject]\"\nCharity <example@example.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Charity (http://localhost/aht/landing-pages/charity)\nReply-To: example@example.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-03-22 07:15:05', '2019-03-22 07:15:05', '', 0, 'http://localhost/aht/landing-pages/charity/?post_type=wpcf7_contact_form&#038;p=5', 0, 'wpcf7_contact_form', '', 0),
(6, 1, '2019-02-13 04:39:59', '2019-02-13 04:39:59', '<div class=\"form-box\">\r\n  <div class=\"field-box\">\r\n    <label class=\"screen-reader-text\">>Email address: </label>\r\n    <input type=\"email\" name=\"EMAIL\" placeholder=\"Subscribe to Newsletter\" required />\r\n  </div>\r\n  <div class=\"action-box\">\r\n    <button type=\"submit\">Subscribe</button>\r\n  </div>\r\n</div>', '', '', 'publish', 'closed', 'closed', '', '6', '', '', '2019-02-13 04:39:59', '2019-02-13 04:39:59', '', 0, 'http://localhost/aht/landing-pages/charity/mc4wp-form/6/', 0, 'mc4wp-form', '', 0),
(7, 1, '2019-02-13 04:43:03', '2019-02-13 04:43:03', '', 'Home', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-02-13 04:43:03', '2019-02-13 04:43:03', '', 2, 'http://localhost/aht/landing-pages/charity/2019/02/13/2-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2019-02-13 04:44:55', '2019-02-13 04:44:55', ' ', '', '', 'publish', 'closed', 'closed', '', '8', '', '', '2019-03-25 10:07:41', '2019-03-25 10:07:41', '', 0, 'http://localhost/aht/landing-pages/charity/?p=8', 1, 'nav_menu_item', '', 0),
(9, 1, '2019-02-13 04:44:56', '2019-02-13 04:44:56', '', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2019-03-25 10:07:41', '2019-03-25 10:07:41', '', 0, 'http://localhost/aht/landing-pages/charity/?p=9', 2, 'nav_menu_item', '', 0),
(10, 1, '2019-02-13 04:44:56', '2019-02-13 04:44:56', '', 'the pub', '', 'publish', 'closed', 'closed', '', 'donate', '', '', '2019-03-25 10:07:41', '2019-03-25 10:07:41', '', 0, 'http://localhost/aht/landing-pages/charity/?p=10', 3, 'nav_menu_item', '', 0),
(12, 1, '2019-02-13 04:44:56', '2019-02-13 04:44:56', '', 'our brewery', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2019-03-25 10:07:42', '2019-03-25 10:07:42', '', 0, 'http://localhost/aht/landing-pages/charity/?p=12', 4, 'nav_menu_item', '', 0),
(13, 1, '2019-02-13 04:44:56', '2019-02-13 04:44:56', '', 'rates', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2019-03-25 10:07:42', '2019-03-25 10:07:42', '', 0, 'http://localhost/aht/landing-pages/charity/?p=13', 5, 'nav_menu_item', '', 0),
(69, 1, '2019-03-19 09:40:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-19 09:40:02', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=69', 0, 'post', '', 0),
(70, 1, '2019-03-20 02:27:44', '2019-03-20 02:27:44', '{\n    \"business_consultant::google_font_h\": {\n        \"value\": \"Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 02:27:44\"\n    },\n    \"business_consultant::google_font\": {\n        \"value\": \"Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 02:27:44\"\n    },\n    \"business_consultant::google_font_p\": {\n        \"value\": \"Arial:100,200,300,400\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 02:27:44\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b5a258f5-0823-4d9f-9255-1ce326c96e51', '', '', '2019-03-20 02:27:44', '2019-03-20 02:27:44', '', 0, 'http://landing.wizworks.io/business-consultant/b5a258f5-0823-4d9f-9255-1ce326c96e51/', 0, 'customize_changeset', '', 0),
(71, 1, '2019-03-20 03:22:13', '2019-03-20 03:22:13', '', 'Back Home', '', 'publish', 'closed', 'closed', '', 'back-home', '', '', '2019-03-20 03:22:13', '2019-03-20 03:22:13', '', 0, 'http://landing.wizworks.io/business-consultant/?page_id=71', 0, 'page', '', 0),
(72, 1, '2019-03-20 03:22:12', '2019-03-20 03:22:12', '{\n    \"nav_menus_created_posts\": {\n        \"value\": [\n            71\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 03:21:11\"\n    },\n    \"nav_menu_item[-4104963916564361000]\": {\n        \"value\": {\n            \"object_id\": 71,\n            \"object\": \"page\",\n            \"menu_item_parent\": 0,\n            \"position\": 1,\n            \"type\": \"post_type\",\n            \"title\": \"Back Home\",\n            \"url\": \"http://landing.wizworks.io/business-consultant/?page_id=71\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Back Home\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Page\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 03:21:11\"\n    },\n    \"nav_menu_item[-171736407321032700]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 2,\n            \"type\": \"custom\",\n            \"title\": \"About Us\",\n            \"url\": \"#about_us\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"About Us\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 03:21:11\"\n    },\n    \"nav_menu_item[-8351433867140606000]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 4,\n            \"type\": \"custom\",\n            \"title\": \"Our Clients\",\n            \"url\": \"#client\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Our Clients\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 03:22:11\"\n    },\n    \"nav_menu_item[-4616873919506977000]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 5,\n            \"type\": \"custom\",\n            \"title\": \"Read Our Blog\",\n            \"url\": \"#blog\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Read Our Blog\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 03:22:11\"\n    },\n    \"nav_menu_item[-5794855943661638000]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 6,\n            \"type\": \"custom\",\n            \"title\": \"Contact Us\",\n            \"url\": \"#contact\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Contact Us\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 03:22:11\"\n    },\n    \"nav_menu_item[-2078521508215742500]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 3,\n            \"type\": \"custom\",\n            \"title\": \"Our Services\",\n            \"url\": \"#services\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"Our Services\",\n            \"nav_menu_term_id\": 3,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 03:22:12\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '2bdfb04b-4abc-498f-81c3-e3cba9dc9735', '', '', '2019-03-20 03:22:12', '2019-03-20 03:22:12', '', 0, 'http://landing.wizworks.io/business-consultant/?p=72', 0, 'customize_changeset', '', 0),
(73, 1, '2019-03-20 03:22:13', '2019-03-20 03:22:13', '', 'Back Home', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2019-03-20 03:22:13', '2019-03-20 03:22:13', '', 71, 'http://landing.wizworks.io/business-consultant/71-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2019-03-20 03:22:13', '2019-03-20 03:22:13', '', 'Home', '', 'publish', 'closed', 'closed', '', '74', '', '', '2019-03-25 10:03:08', '2019-03-25 10:03:08', '', 0, 'http://landing.wizworks.io/business-consultant/74/', 1, 'nav_menu_item', '', 0),
(76, 1, '2019-03-20 03:22:15', '2019-03-20 03:22:15', '', 'rates', '', 'publish', 'closed', 'closed', '', 'our-clients', '', '', '2019-03-25 10:03:08', '2019-03-25 10:03:08', '', 0, 'http://landing.wizworks.io/business-consultant/our-clients/', 5, 'nav_menu_item', '', 0),
(77, 1, '2019-03-20 03:22:16', '2019-03-20 03:22:16', '', 'our brewery', '', 'publish', 'closed', 'closed', '', 'read-our-blog', '', '', '2019-03-25 10:03:08', '2019-03-25 10:03:08', '', 0, 'http://landing.wizworks.io/business-consultant/read-our-blog/', 4, 'nav_menu_item', '', 0),
(78, 1, '2019-03-20 03:22:17', '2019-03-20 03:22:17', '', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2019-03-25 10:03:09', '2019-03-25 10:03:09', '', 0, 'http://landing.wizworks.io/business-consultant/contact-us/', 6, 'nav_menu_item', '', 0),
(79, 1, '2019-03-20 03:22:19', '2019-03-20 03:22:19', '', 'The Pub', '', 'publish', 'closed', 'closed', '', 'our-services', '', '', '2019-03-25 10:03:08', '2019-03-25 10:03:08', '', 0, 'http://landing.wizworks.io/business-consultant/our-services/', 3, 'nav_menu_item', '', 0),
(80, 1, '2019-03-20 03:25:26', '2019-03-20 03:25:26', '', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us-3', '', '', '2019-03-25 10:03:08', '2019-03-25 10:03:08', '', 0, 'http://landing.wizworks.io/business-consultant/?p=80', 2, 'nav_menu_item', '', 0),
(81, 1, '2019-03-20 03:29:33', '2019-03-20 03:29:33', '{\n    \"business_consultant::copyright\": {\n        \"value\": \"<p>(C) 2018. AllRights Reserved. Divine Makeup Artist. Designed b <a href=\\\"#\\\">Template.net</a></p>\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 03:29:33\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '1f749baf-e768-44ed-a56e-36a769ca6754', '', '', '2019-03-20 03:29:33', '2019-03-20 03:29:33', '', 0, 'http://landing.wizworks.io/business-consultant/1f749baf-e768-44ed-a56e-36a769ca6754/', 0, 'customize_changeset', '', 0),
(88, 1, '2019-03-20 04:02:22', '0000-00-00 00:00:00', '{\n    \"business_consultant::banner_title\": {\n        \"value\": \"<h1>We ProvideBest SolutionsFor Your Online Business</h1>\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 04:02:22\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'f89a3a09-6542-4eb8-a3a8-fd80ff694456', '', '', '2019-03-20 04:02:22', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=88', 0, 'customize_changeset', '', 0),
(89, 1, '2019-03-20 04:12:37', '2019-03-20 04:12:37', '{\n    \"business_consultant::about_number_post\": {\n        \"value\": \"3\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 04:12:37\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '2e53f5f2-c14d-4039-b1fa-56d452da727f', '', '', '2019-03-20 04:12:37', '2019-03-20 04:12:37', '', 0, 'http://landing.wizworks.io/business-consultant/2e53f5f2-c14d-4039-b1fa-56d452da727f/', 0, 'customize_changeset', '', 0),
(90, 1, '2019-03-20 04:16:09', '2019-03-20 04:16:09', '{\n    \"business_consultant::desc_about\": {\n        \"value\": \"Lookin for Experienced Business Consultant for your Online Business? We will help you!\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 04:16:09\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4467d42c-a7bd-4ae6-bc7f-81dbd76c1837', '', '', '2019-03-20 04:16:09', '2019-03-20 04:16:09', '', 0, 'http://landing.wizworks.io/business-consultant/4467d42c-a7bd-4ae6-bc7f-81dbd76c1837/', 0, 'customize_changeset', '', 0),
(91, 1, '2019-03-20 04:16:40', '2019-03-20 04:16:40', '{\n    \"business_consultant::desc_about\": {\n        \"value\": \"Looking for Experienced Business Consultant for your Online Business? We will help you!\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 04:16:40\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'e5ec06dc-42f8-4eeb-9db1-9dbeeb119b7f', '', '', '2019-03-20 04:16:40', '2019-03-20 04:16:40', '', 0, 'http://landing.wizworks.io/business-consultant/e5ec06dc-42f8-4eeb-9db1-9dbeeb119b7f/', 0, 'customize_changeset', '', 0),
(92, 1, '2019-03-20 04:18:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-20 04:18:02', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=92', 0, 'post', '', 0),
(93, 1, '2019-03-20 04:22:59', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-20 04:22:59', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=93', 0, 'post', '', 0),
(100, 1, '2019-03-20 04:38:19', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-20 04:38:19', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=100', 0, 'post', '', 0),
(104, 1, '2019-03-20 04:40:03', '2019-03-20 04:40:03', '{\n    \"business_consultant::About_category\": {\n        \"value\": \"4\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 04:40:03\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '96f1be68-35eb-4c78-8407-9f075a789809', '', '', '2019-03-20 04:40:03', '2019-03-20 04:40:03', '', 0, 'http://landing.wizworks.io/business-consultant/96f1be68-35eb-4c78-8407-9f075a789809/', 0, 'customize_changeset', '', 0),
(123, 1, '2019-03-20 07:25:15', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-03-20 07:25:15', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?post_type=testimonial&p=123', 0, 'testimonial', '', 0),
(124, 1, '2019-03-20 07:25:55', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-03-20 07:25:55', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?post_type=testimonial&p=124', 0, 'testimonial', '', 0),
(127, 1, '2019-03-20 07:36:15', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-20 07:36:15', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=127', 0, 'post', '', 0),
(129, 1, '2019-03-20 07:45:12', '2019-03-20 07:45:12', '{\n    \"business_consultant::Service_category\": {\n        \"value\": \"5\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 07:45:12\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'd0670634-5fa0-457a-8110-6de0dcbcc3f3', '', '', '2019-03-20 07:45:12', '2019-03-20 07:45:12', '', 0, 'http://landing.wizworks.io/business-consultant/d0670634-5fa0-457a-8110-6de0dcbcc3f3/', 0, 'customize_changeset', '', 0),
(130, 1, '2019-03-20 07:51:10', '2019-03-20 07:51:10', '{\n    \"business_consultant::blog_category\": {\n        \"value\": \"6\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 07:51:10\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '980f72ae-07d1-48e3-9e15-5394abf25ba0', '', '', '2019-03-20 07:51:10', '2019-03-20 07:51:10', '', 0, 'http://landing.wizworks.io/business-consultant/980f72ae-07d1-48e3-9e15-5394abf25ba0/', 0, 'customize_changeset', '', 0),
(142, 1, '2019-03-20 08:09:07', '2019-03-20 08:09:07', '{\n    \"business_consultant::Service_category\": {\n        \"value\": \"5\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 08:09:07\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '249d8eb9-f52d-4cd5-bd4d-832d37ccae49', '', '', '2019-03-20 08:09:07', '2019-03-20 08:09:07', '', 0, 'http://landing.wizworks.io/business-consultant/249d8eb9-f52d-4cd5-bd4d-832d37ccae49/', 0, 'customize_changeset', '', 0),
(143, 1, '2019-03-20 08:16:14', '0000-00-00 00:00:00', '{\n    \"business_consultant::client_category\": {\n        \"value\": \"5\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 08:13:14\"\n    },\n    \"business_consultant::service_number_post\": {\n        \"value\": \"2\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 08:16:14\"\n    },\n    \"business_consultant::client_number_post\": {\n        \"value\": \"6\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 08:15:14\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '3da2a93a-c2b0-4913-8401-4e4cc364dc1e', '', '', '2019-03-20 08:16:14', '2019-03-20 08:16:14', '', 0, 'http://landing.wizworks.io/business-consultant/?p=143', 0, 'customize_changeset', '', 0),
(144, 1, '2019-03-20 08:36:02', '0000-00-00 00:00:00', '{\n    \"business_consultant::google_font_h\": {\n        \"value\": \"Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 08:36:02\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'dae134b9-d0bb-463f-b452-83999bf4f9a9', '', '', '2019-03-20 08:36:02', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=144', 0, 'customize_changeset', '', 0),
(145, 1, '2019-03-20 08:53:10', '2019-03-20 08:53:10', '{\n    \"business_consultant::copyright\": {\n        \"value\": \"<h6>(C) 2018. AllRights Reserved. Divine Makeup Artist. Designed b <a href=\\\"#\\\">Template.net</a></h6>\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 08:53:10\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9743a314-c7a8-48e8-9127-9337fc7086e5', '', '', '2019-03-20 08:53:10', '2019-03-20 08:53:10', '', 0, 'http://landing.wizworks.io/business-consultant/9743a314-c7a8-48e8-9127-9337fc7086e5/', 0, 'customize_changeset', '', 0),
(146, 1, '2019-03-20 09:07:27', '2019-03-20 09:07:27', '{\n    \"business_consultant::google_font\": {\n        \"value\": \"Arial:100,200,300,400\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 09:07:27\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '0ffabcd5-bfeb-4d7b-8d6d-f3b5ad493b0d', '', '', '2019-03-20 09:07:27', '2019-03-20 09:07:27', '', 0, 'http://landing.wizworks.io/business-consultant/0ffabcd5-bfeb-4d7b-8d6d-f3b5ad493b0d/', 0, 'customize_changeset', '', 0),
(147, 1, '2019-03-20 09:22:58', '0000-00-00 00:00:00', '{\n    \"business_consultant::contact_shortcode\": {\n        \"value\": \"[contact-form-7 id=\\\"5\\\" title=\\\"Contact\\\"]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 09:22:58\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'fd6d968a-61d0-48ca-b245-775706679f1f', '', '', '2019-03-20 09:22:58', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=147', 0, 'customize_changeset', '', 0),
(148, 1, '2019-03-20 09:24:15', '2019-03-20 09:24:15', '{\n    \"business_consultant::contact_shortcode\": {\n        \"value\": \"[contact-form-7 id=\\\"5\\\" title=\\\"Contact\\\"]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 09:24:15\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '21d0cfd9-4af2-4f82-afee-7b4d67a88249', '', '', '2019-03-20 09:24:15', '2019-03-20 09:24:15', '', 0, 'http://landing.wizworks.io/business-consultant/21d0cfd9-4af2-4f82-afee-7b4d67a88249/', 0, 'customize_changeset', '', 0),
(149, 1, '2019-03-20 09:30:00', '0000-00-00 00:00:00', '{\n    \"business_consultant::contact_map\": {\n        \"value\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14900.820008282744!2d105.78794855!3d20.984417999999998!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1svi!2s!4v1552961000328\\\" width=\\\"600\\\" height=\\\"650\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen=\\\"\\\"></iframe>\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 09:30:00\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '709cb67b-fbeb-4916-b04f-2b16cafa9d47', '', '', '2019-03-20 09:30:00', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=149', 0, 'customize_changeset', '', 0),
(151, 1, '2019-03-20 09:33:52', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-20 09:33:52', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=151', 0, 'post', '', 0),
(157, 1, '2019-03-20 09:56:03', '2019-03-20 09:56:03', '{\n    \"business_consultant::banner_title\": {\n        \"value\": \"<h1>We Provide<span style=\\\"color: #ed8e22;\\\">Best Solutions</span>For Your Online Business</h1>\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 09:56:03\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '449cb761-cc1f-4c64-9082-bd43dda4b703', '', '', '2019-03-20 09:56:03', '2019-03-20 09:56:03', '', 0, 'http://landing.wizworks.io/business-consultant/449cb761-cc1f-4c64-9082-bd43dda4b703/', 0, 'customize_changeset', '', 0),
(158, 1, '2019-03-20 10:04:13', '0000-00-00 00:00:00', '{\n    \"business_consultant::contact_map\": {\n        \"value\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14900.820008282744!2d105.78794855!3d20.984417999999998!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1svi!2s!4v1552961000328\\\" width=\\\"600\\\" height=\\\"650\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen=\\\"\\\"></iframe>\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-20 10:04:13\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '8a3e1998-3265-4e59-9085-aca72eec24a0', '', '', '2019-03-20 10:04:13', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=158', 0, 'customize_changeset', '', 0),
(159, 1, '2019-03-20 11:05:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-20 11:05:47', '0000-00-00 00:00:00', '', 0, 'http://landing.wizworks.io/business-consultant/?p=159', 0, 'post', '', 0),
(160, 1, '2019-03-22 07:16:33', '2019-03-22 07:16:33', '{\n    \"business_consultant::banner_title\": {\n        \"value\": \"Delivering Sustainable Growth & Profitability For Businesses\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 07:16:33\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '36271bc4-3281-4bbc-8834-f49b99bd8bd3', '', '', '2019-03-22 07:16:33', '2019-03-22 07:16:33', '', 0, 'http://localhost/wordpress/?p=160', 0, 'customize_changeset', '', 0),
(161, 1, '2019-03-22 07:36:15', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-22 07:36:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=161', 0, 'post', '', 0),
(166, 1, '2019-03-22 07:38:21', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-22 07:38:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=166', 0, 'post', '', 0),
(171, 1, '2019-03-22 07:53:29', '2019-03-22 07:53:29', '{\n    \"business_consultant::service_number_post\": {\n        \"value\": \"4\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 07:53:29\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '63134f20-f237-4c03-8731-2add32e2bfbc', '', '', '2019-03-22 07:53:29', '2019-03-22 07:53:29', '', 0, 'http://localhost/wordpress/?p=171', 0, 'customize_changeset', '', 0),
(172, 1, '2019-03-22 07:53:56', '2019-03-22 07:53:56', '{\n    \"business_consultant::Service_category\": {\n        \"value\": \"5\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 07:53:56\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '25ee28e2-2254-4dd4-bc47-ba1222e0e130', '', '', '2019-03-22 07:53:56', '2019-03-22 07:53:56', '', 0, 'http://localhost/wordpress/?p=172', 0, 'customize_changeset', '', 0),
(173, 1, '2019-03-22 08:44:40', '0000-00-00 00:00:00', '{\n    \"business_consultant::so_Skil_3\": {\n        \"value\": \"85%\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 08:44:40\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '4794d890-4aed-49ed-bc5c-886139521be1', '', '', '2019-03-22 08:44:40', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=173', 0, 'customize_changeset', '', 0),
(174, 1, '2019-03-22 08:52:39', '2019-03-22 08:52:39', '', 'logo1', '', 'publish', 'open', 'closed', '', 'logo1', '', '', '2019-03-22 08:52:39', '2019-03-22 08:52:39', '', 0, 'http://localhost/wordpress/?post_type=gallery&#038;p=174', 0, 'gallery', '', 0),
(176, 1, '2019-03-22 08:52:44', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2019-03-22 08:52:44', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=gallery&p=176', 0, 'gallery', '', 0),
(182, 1, '2019-03-22 09:07:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-22 09:07:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=182', 0, 'post', '', 0),
(189, 1, '2019-03-22 09:17:56', '2019-03-22 09:17:56', '{\n    \"business_consultant::blog_category\": {\n        \"value\": \"6\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:17:56\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'a8382a19-3381-4c80-8504-0def6d3a8a1f', '', '', '2019-03-22 09:17:56', '2019-03-22 09:17:56', '', 0, 'http://localhost/wordpress/?p=189', 0, 'customize_changeset', '', 0),
(190, 1, '2019-03-22 09:49:06', '2019-03-22 09:49:06', '<div class=\"header-form\">\r\n                <h3>Contact us for further information</h3>\r\n</div>\r\n<div class=\"form-group row\">\r\n	<div class=\"col-xl-4\">\r\n		[text* your-name  class:form-control form-control-lg placeholder\"Enter Your Name\"]\r\n	</div>\r\n	<div class=\"col-lg-4\">      \r\n		[email* your-email class:form-control form-control-lg placeholder \"Enter Your Email Address\"]\r\n	</div>\r\n</div>\r\n\r\n<div class=\"form-group\">\r\n	<div class=\"\">      \r\n		[textarea* your-message x3 class:form-control form-control-lg placeholder\"Type Your Message\"]\r\n	</div>\r\n</div>\r\n<div class=\"form-group\">\r\n\r\n		[submit class:btn \"Contact Us for free quotation\"]\r\n	</div>\n1\nAccountancy Firm \"[your-subject]\"\nAccountancy Firm <example@example.com>\nexample@example.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Accountancy Firm (http://localhost/wordpress)\nReply-To: [your-email]\n\n\n\n\nAccountancy Firm \"[your-subject]\"\nAccountancy Firm <example@example.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Accountancy Firm (http://localhost/wordpress)\nReply-To: example@example.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'contact2', '', 'publish', 'closed', 'closed', '', 'untitled', '', '', '2019-03-25 06:18:48', '2019-03-25 06:18:48', '', 0, 'http://localhost/wordpress/?post_type=wpcf7_contact_form&#038;p=190', 0, 'wpcf7_contact_form', '', 0),
(191, 1, '2019-03-22 09:53:25', '2019-03-22 09:53:25', '{\n    \"business_consultant::contact_shortcode\": {\n        \"value\": \"[contact-form-7 id=\\\"190\\\" title=\\\"contact2\\\"]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:53:25\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'fdf4507c-cf98-4df7-8450-1d7d02c57db5', '', '', '2019-03-22 09:53:25', '2019-03-22 09:53:25', '', 0, 'http://localhost/wordpress/?p=191', 0, 'customize_changeset', '', 0),
(192, 1, '2019-03-22 10:57:22', '2019-03-22 10:57:22', '{\n    \"business_consultant::banner_shortcode\": {\n        \"value\": \"[contact-form-7 id=\\\"5\\\" title=\\\"Contact\\\"]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:57:22\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4b32aeee-2eec-493f-bd8c-ee7784343f95', '', '', '2019-03-22 10:57:22', '2019-03-22 10:57:22', '', 0, 'http://localhost/wordpress/?p=192', 0, 'customize_changeset', '', 0),
(193, 1, '2019-03-22 11:13:30', '2019-03-22 11:13:30', '{\n    \"business_consultant::Service_category\": {\n        \"value\": \"5\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 11:13:30\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'a4641183-6443-4f65-9b2a-bd6c12b8da7f', '', '', '2019-03-22 11:13:30', '2019-03-22 11:13:30', '', 0, 'http://localhost/wordpress/?p=193', 0, 'customize_changeset', '', 0),
(194, 1, '2019-03-22 11:19:53', '2019-03-22 11:19:53', '{\n    \"business_consultant::Service_category\": {\n        \"value\": \"5\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 11:19:53\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '24779376-7458-4797-a71e-34d161ee57fc', '', '', '2019-03-22 11:19:53', '2019-03-22 11:19:53', '', 0, 'http://localhost/wordpress/?p=194', 0, 'customize_changeset', '', 0),
(195, 1, '2019-03-22 11:44:28', '2019-03-22 11:44:28', '{\n    \"business_consultant::customizer_repeater_whychose\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"test\\\",\\\"subtitle\\\":\\\"123\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c94ca7c6392b\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 11:44:28\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'e85d43c3-f381-439d-81b3-1c401b551ecd', '', '', '2019-03-22 11:44:28', '2019-03-22 11:44:28', '', 0, 'http://localhost/wordpress/?p=195', 0, 'customize_changeset', '', 0),
(196, 1, '2019-03-25 06:39:09', '0000-00-00 00:00:00', '{\n    \"business_consultant::customizer_repeater_whychose\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"jbsdjkbkas\\\",\\\"subtitle\\\":\\\"90\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c94ca7c6392b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"\\\",\\\"subtitle\\\":\\\"\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c98775aa6ac5\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"\\\",\\\"subtitle\\\":\\\"\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c98776edb759\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 06:39:09\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '68ca9940-7d25-4a96-a240-3b453a64a753', '', '', '2019-03-25 06:39:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=196', 0, 'customize_changeset', '', 0),
(197, 1, '2019-03-25 06:53:26', '2019-03-25 06:53:26', '{\n    \"business_consultant::customizer_repeater_whychose\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"01)   FINANCIAL ANALYSIS\\\",\\\"subtitle\\\":\\\"90\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c94ca7c6392b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"02)   INVESTMENTS\\\",\\\"subtitle\\\":\\\"80\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c9879da291ef\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"03)   FINANCIAL PLANNER\\\",\\\"subtitle\\\":\\\"95\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c987aba32d89\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"04)   BUSINESS SUPPORT\\\",\\\"subtitle\\\":\\\"85\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c987ad209b48\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 06:53:24\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '694686d7-67d9-46c4-afa6-ab526d107df4', '', '', '2019-03-25 06:53:26', '2019-03-25 06:53:26', '', 0, 'http://localhost/wordpress/?p=197', 0, 'customize_changeset', '', 0),
(198, 1, '2019-03-25 06:59:17', '2019-03-25 06:59:17', '{\n    \"business_consultant::Service_category\": {\n        \"value\": \"5\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 06:59:17\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '456557a5-d0db-4d15-b74c-0688d660b53e', '', '', '2019-03-25 06:59:17', '2019-03-25 06:59:17', '', 0, 'http://localhost/wordpress/?p=198', 0, 'customize_changeset', '', 0),
(199, 1, '2019-03-25 10:06:02', '2019-03-25 10:06:02', '', 'blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2019-03-25 10:06:02', '2019-03-25 10:06:02', '', 0, 'http://localhost/wordpress/?page_id=199', 0, 'page', '', 0),
(200, 1, '2019-03-25 10:06:02', '2019-03-25 10:06:02', '', 'contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2019-03-25 10:06:02', '2019-03-25 10:06:02', '', 0, 'http://localhost/wordpress/?page_id=200', 0, 'page', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(201, 1, '2019-03-25 10:06:01', '2019-03-25 10:06:01', '{\n    \"nav_menu_item[10]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 10,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"the pub\",\n            \"url\": \"#services\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 3,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 10:06:01\"\n    },\n    \"nav_menu_item[12]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 12,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"our brewery\",\n            \"url\": \"#blog\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 4,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 10:06:01\"\n    },\n    \"nav_menu_item[13]\": {\n        \"value\": {\n            \"menu_item_parent\": 0,\n            \"object_id\": 13,\n            \"object\": \"custom\",\n            \"type\": \"custom\",\n            \"type_label\": \"Custom Link\",\n            \"title\": \"rates\",\n            \"url\": \"#contact\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"nav_menu_term_id\": 2,\n            \"position\": 5,\n            \"status\": \"publish\",\n            \"original_title\": \"\",\n            \"_invalid\": false\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 10:06:01\"\n    },\n    \"nav_menus_created_posts\": {\n        \"value\": [\n            199,\n            200\n        ],\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 10:06:01\"\n    },\n    \"nav_menu_item[-8085867261368852000]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 10:06:01\"\n    },\n    \"nav_menu_item[-507975809158656000]\": {\n        \"value\": false,\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 10:06:01\"\n    },\n    \"nav_menu_item[-8752086291758137000]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 6,\n            \"type\": \"custom\",\n            \"title\": \"blog\",\n            \"url\": \"#blog\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"blog\",\n            \"nav_menu_term_id\": 2,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 10:06:01\"\n    },\n    \"nav_menu_item[-2552092631928246300]\": {\n        \"value\": {\n            \"object_id\": 0,\n            \"object\": \"custom\",\n            \"menu_item_parent\": 0,\n            \"position\": 7,\n            \"type\": \"custom\",\n            \"title\": \"contact\",\n            \"url\": \"#contact\",\n            \"target\": \"\",\n            \"attr_title\": \"\",\n            \"description\": \"\",\n            \"classes\": \"\",\n            \"xfn\": \"\",\n            \"status\": \"publish\",\n            \"original_title\": \"contact\",\n            \"nav_menu_term_id\": 2,\n            \"_invalid\": false,\n            \"type_label\": \"Custom Link\"\n        },\n        \"type\": \"nav_menu_item\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 10:06:01\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '689d5bf3-dc76-4656-9797-dc02a5902ccc', '', '', '2019-03-25 10:06:01', '2019-03-25 10:06:01', '', 0, 'http://localhost/wordpress/?p=201', 0, 'customize_changeset', '', 0),
(202, 1, '2019-03-25 10:06:02', '2019-03-25 10:06:02', '', 'blog', '', 'inherit', 'closed', 'closed', '', '199-revision-v1', '', '', '2019-03-25 10:06:02', '2019-03-25 10:06:02', '', 199, 'http://localhost/wordpress/?p=202', 0, 'revision', '', 0),
(203, 1, '2019-03-25 10:06:02', '2019-03-25 10:06:02', '', 'contact', '', 'inherit', 'closed', 'closed', '', '200-revision-v1', '', '', '2019-03-25 10:06:02', '2019-03-25 10:06:02', '', 200, 'http://localhost/wordpress/?p=203', 0, 'revision', '', 0),
(204, 1, '2019-03-25 10:06:03', '2019-03-25 10:06:03', '', 'blog', '', 'publish', 'closed', 'closed', '', 'blog-2', '', '', '2019-03-25 10:07:42', '2019-03-25 10:07:42', '', 0, 'http://localhost/wordpress/?p=204', 6, 'nav_menu_item', '', 0),
(205, 1, '2019-03-25 10:06:04', '2019-03-25 10:06:04', '', 'contact', '', 'publish', 'closed', 'closed', '', 'contact-2', '', '', '2019-03-25 10:07:42', '2019-03-25 10:07:42', '', 0, 'http://localhost/wordpress/?p=205', 7, 'nav_menu_item', '', 0),
(206, 1, '2019-03-25 10:39:35', '2019-03-25 10:39:35', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n<!-- /wp:paragraph -->', 'Drewed Beers', '', 'publish', 'open', 'open', '', 'drewed-beers', '', '', '2019-03-25 10:39:35', '2019-03-25 10:39:35', '', 0, 'http://localhost/wordpress/?p=206', 0, 'post', '', 0),
(207, 1, '2019-03-25 10:39:25', '2019-03-25 10:39:25', '', 'the-pub-img-3', '', 'inherit', 'open', 'closed', '', 'the-pub-img-3', '', '', '2019-03-25 10:39:25', '2019-03-25 10:39:25', '', 206, 'http://localhost/wordpress/wp-content/uploads/2019/03/the-pub-img-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(208, 1, '2019-03-25 10:39:35', '2019-03-25 10:39:35', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n<!-- /wp:paragraph -->', 'Drewed Beers', '', 'inherit', 'closed', 'closed', '', '206-revision-v1', '', '', '2019-03-25 10:39:35', '2019-03-25 10:39:35', '', 206, 'http://localhost/wordpress/?p=208', 0, 'revision', '', 0),
(209, 1, '2019-03-25 10:40:26', '2019-03-25 10:40:26', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n<!-- /wp:paragraph -->', 'Spacious Pub', '', 'publish', 'open', 'open', '', 'spacious-pub', '', '', '2019-03-25 10:40:26', '2019-03-25 10:40:26', '', 0, 'http://localhost/wordpress/?p=209', 0, 'post', '', 0),
(210, 1, '2019-03-25 10:40:19', '2019-03-25 10:40:19', '', 'the-pub-img-2', '', 'inherit', 'open', 'closed', '', 'the-pub-img-2', '', '', '2019-03-25 10:40:19', '2019-03-25 10:40:19', '', 209, 'http://localhost/wordpress/wp-content/uploads/2019/03/the-pub-img-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(211, 1, '2019-03-25 10:40:26', '2019-03-25 10:40:26', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n<!-- /wp:paragraph -->', 'Spacious Pub', '', 'inherit', 'closed', 'closed', '', '209-revision-v1', '', '', '2019-03-25 10:40:26', '2019-03-25 10:40:26', '', 209, 'http://localhost/wordpress/?p=211', 0, 'revision', '', 0),
(212, 1, '2019-03-25 10:42:11', '2019-03-25 10:42:11', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n<!-- /wp:paragraph -->', 'Friendly Staff', '', 'publish', 'open', 'open', '', 'friendly-staff', '', '', '2019-03-25 10:42:11', '2019-03-25 10:42:11', '', 0, 'http://localhost/wordpress/?p=212', 0, 'post', '', 0),
(213, 1, '2019-03-25 10:41:55', '2019-03-25 10:41:55', '', 'the-pub-img-1', '', 'inherit', 'open', 'closed', '', 'the-pub-img-1', '', '', '2019-03-25 10:41:55', '2019-03-25 10:41:55', '', 212, 'http://localhost/wordpress/wp-content/uploads/2019/03/the-pub-img-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(214, 1, '2019-03-25 10:42:11', '2019-03-25 10:42:11', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>\n<!-- /wp:paragraph -->', 'Friendly Staff', '', 'inherit', 'closed', 'closed', '', '212-revision-v1', '', '', '2019-03-25 10:42:11', '2019-03-25 10:42:11', '', 212, 'http://localhost/wordpress/?p=214', 0, 'revision', '', 0),
(215, 1, '2019-03-25 11:03:40', '2019-03-25 11:03:40', '<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n\n</p>\n<!-- /wp:paragraph -->', 'traditional beer', '', 'publish', 'open', 'open', '', 'traditional-beer', '', '', '2019-03-25 11:03:40', '2019-03-25 11:03:40', '', 0, 'http://localhost/wordpress/?p=215', 0, 'post', '', 0),
(216, 1, '2019-03-25 11:03:34', '2019-03-25 11:03:34', '', 'our-brewery-img-1', '', 'inherit', 'open', 'closed', '', 'our-brewery-img-1', '', '', '2019-03-25 11:03:34', '2019-03-25 11:03:34', '', 215, 'http://localhost/wordpress/wp-content/uploads/2019/03/our-brewery-img-1.png', 0, 'attachment', 'image/png', 0),
(217, 1, '2019-03-25 11:03:40', '2019-03-25 11:03:40', '<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n\n</p>\n<!-- /wp:paragraph -->', 'traditional beer', '', 'inherit', 'closed', 'closed', '', '215-revision-v1', '', '', '2019-03-25 11:03:40', '2019-03-25 11:03:40', '', 215, 'http://localhost/wordpress/?p=217', 0, 'revision', '', 0),
(218, 1, '2019-03-25 11:03:59', '2019-03-25 11:03:59', '{\n    \"beer_pub::The_pub_category\": {\n        \"value\": \"6\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-25 11:03:59\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '9547a215-f930-4d6d-91f1-20c055017bd5', '', '', '2019-03-25 11:03:59', '2019-03-25 11:03:59', '', 0, 'http://localhost/wordpress/?p=218', 0, 'customize_changeset', '', 0),
(219, 1, '2019-03-25 11:04:55', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-25 11:04:55', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=219', 0, 'post', '', 0),
(220, 1, '2019-03-25 11:29:55', '2019-03-25 11:29:55', '<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n\n</p>\n<!-- /wp:paragraph -->', 'best craft beer', '', 'publish', 'open', 'open', '', 'best-craft-beer', '', '', '2019-03-25 11:29:55', '2019-03-25 11:29:55', '', 0, 'http://localhost/wordpress/?p=220', 0, 'post', '', 0),
(221, 1, '2019-03-25 11:29:48', '2019-03-25 11:29:48', '', 'our-brewery-img-2', '', 'inherit', 'open', 'closed', '', 'our-brewery-img-2', '', '', '2019-03-25 11:29:48', '2019-03-25 11:29:48', '', 220, 'http://localhost/wordpress/wp-content/uploads/2019/03/our-brewery-img-2.png', 0, 'attachment', 'image/png', 0),
(222, 1, '2019-03-25 11:29:55', '2019-03-25 11:29:55', '<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n\n</p>\n<!-- /wp:paragraph -->', 'best craft beer', '', 'inherit', 'closed', 'closed', '', '220-revision-v1', '', '', '2019-03-25 11:29:55', '2019-03-25 11:29:55', '', 220, 'http://localhost/wordpress/?p=222', 0, 'revision', '', 0),
(223, 1, '2019-03-25 11:30:41', '2019-03-25 11:30:41', '<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n\n</p>\n<!-- /wp:paragraph -->', 'world beer', '', 'publish', 'open', 'open', '', 'world-beer', '', '', '2019-03-25 11:30:41', '2019-03-25 11:30:41', '', 0, 'http://localhost/wordpress/?p=223', 0, 'post', '', 0),
(224, 1, '2019-03-25 11:30:30', '2019-03-25 11:30:30', '', 'our-brewery-img-3', '', 'inherit', 'open', 'closed', '', 'our-brewery-img-3', '', '', '2019-03-25 11:30:30', '2019-03-25 11:30:30', '', 223, 'http://localhost/wordpress/wp-content/uploads/2019/03/our-brewery-img-3.png', 0, 'attachment', 'image/png', 0),
(225, 1, '2019-03-25 11:30:41', '2019-03-25 11:30:41', '<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n\n</p>\n<!-- /wp:paragraph -->', 'world beer', '', 'inherit', 'closed', 'closed', '', '223-revision-v1', '', '', '2019-03-25 11:30:41', '2019-03-25 11:30:41', '', 223, 'http://localhost/wordpress/?p=225', 0, 'revision', '', 0),
(226, 1, '2019-03-25 11:31:19', '2019-03-25 11:31:19', '<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n\n</p>\n<!-- /wp:paragraph -->', 'wheat variety', '', 'publish', 'open', 'open', '', 'wheat-variety', '', '', '2019-03-25 11:31:19', '2019-03-25 11:31:19', '', 0, 'http://localhost/wordpress/?p=226', 0, 'post', '', 0),
(227, 1, '2019-03-25 11:31:14', '2019-03-25 11:31:14', '', 'our-brewery-img-4', '', 'inherit', 'open', 'closed', '', 'our-brewery-img-4', '', '', '2019-03-25 11:31:14', '2019-03-25 11:31:14', '', 226, 'http://localhost/wordpress/wp-content/uploads/2019/03/our-brewery-img-4.png', 0, 'attachment', 'image/png', 0),
(228, 1, '2019-03-25 11:31:19', '2019-03-25 11:31:19', '<!-- wp:paragraph -->\n<p>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n\n</p>\n<!-- /wp:paragraph -->', 'wheat variety', '', 'inherit', 'closed', 'closed', '', '226-revision-v1', '', '', '2019-03-25 11:31:19', '2019-03-25 11:31:19', '', 226, 'http://localhost/wordpress/?p=228', 0, 'revision', '', 0),
(229, 1, '2019-03-26 01:58:54', '2019-03-26 01:58:54', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)', '', 'publish', 'closed', 'closed', '', 'mathews-samanthastudent', '', '', '2019-03-26 01:58:54', '2019-03-26 01:58:54', '', 0, 'http://localhost/wordpress/?post_type=testimonial&#038;p=229', 0, 'testimonial', '', 0),
(230, 1, '2019-03-26 01:58:44', '2019-03-26 01:58:44', '', 'avatar', '', 'inherit', 'open', 'closed', '', 'avatar', '', '', '2019-03-26 01:58:44', '2019-03-26 01:58:44', '', 229, 'http://localhost/wordpress/wp-content/uploads/2019/03/avatar.png', 0, 'attachment', 'image/png', 0),
(231, 1, '2019-03-26 01:58:54', '2019-03-26 01:58:54', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)', '', 'inherit', 'closed', 'closed', '', '229-revision-v1', '', '', '2019-03-26 01:58:54', '2019-03-26 01:58:54', '', 229, 'http://localhost/wordpress/?p=231', 0, 'revision', '', 0),
(232, 1, '2019-03-26 01:59:14', '2019-03-26 01:59:14', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)', '', 'publish', 'closed', 'closed', '', 'mathews-samanthastudent-2', '', '', '2019-03-26 01:59:14', '2019-03-26 01:59:14', '', 0, 'http://localhost/wordpress/?post_type=testimonial&#038;p=232', 0, 'testimonial', '', 0),
(233, 1, '2019-03-26 01:59:14', '2019-03-26 01:59:14', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)', '', 'inherit', 'closed', 'closed', '', '232-revision-v1', '', '', '2019-03-26 01:59:14', '2019-03-26 01:59:14', '', 232, 'http://localhost/wordpress/?p=233', 0, 'revision', '', 0),
(234, 1, '2019-03-26 01:59:17', '2019-03-26 01:59:17', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)', '', 'inherit', 'closed', 'closed', '', '232-autosave-v1', '', '', '2019-03-26 01:59:17', '2019-03-26 01:59:17', '', 232, 'http://localhost/wordpress/?p=234', 0, 'revision', '', 0),
(235, 1, '2019-03-26 01:59:32', '2019-03-26 01:59:32', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)', '', 'publish', 'closed', 'closed', '', 'mathews-samanthastudent-3', '', '', '2019-03-26 02:07:14', '2019-03-26 02:07:14', '', 0, 'http://localhost/wordpress/?post_type=testimonial&#038;p=235', 0, 'testimonial', '', 0),
(236, 1, '2019-03-26 01:59:32', '2019-03-26 01:59:32', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)', '', 'inherit', 'closed', 'closed', '', '235-revision-v1', '', '', '2019-03-26 01:59:32', '2019-03-26 01:59:32', '', 235, 'http://localhost/wordpress/?p=236', 0, 'revision', '', 0),
(237, 1, '2019-03-26 02:05:31', '2019-03-26 02:05:31', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)3', '', 'inherit', 'closed', 'closed', '', '235-revision-v1', '', '', '2019-03-26 02:05:31', '2019-03-26 02:05:31', '', 235, 'http://localhost/wordpress/?p=237', 0, 'revision', '', 0),
(238, 1, '2019-03-26 02:07:14', '2019-03-26 02:07:14', '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.\"', 'Mathews & Samantha(Student)', '', 'inherit', 'closed', 'closed', '', '235-revision-v1', '', '', '2019-03-26 02:07:14', '2019-03-26 02:07:14', '', 235, 'http://localhost/wordpress/?p=238', 0, 'revision', '', 0),
(239, 1, '2019-03-26 02:08:00', '2019-03-26 02:08:00', '{\n    \"beer_pub::our_brewery_category\": {\n        \"value\": \"5\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 02:08:00\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cfbec003-c65e-4a2d-bac5-95d506d1de73', '', '', '2019-03-26 02:08:00', '2019-03-26 02:08:00', '', 0, 'http://localhost/wordpress/?p=239', 0, 'customize_changeset', '', 0),
(240, 1, '2019-03-26 02:36:01', '2019-03-26 02:36:01', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 02:36:01\"\n    },\n    \"beer_pub::customizer_repeater_whychose\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"\\\",\\\"subtitle\\\":\\\"\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3aec9b6\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 02:33:26\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '65a0eb40-1290-464f-81c6-8657e1313986', '', '', '2019-03-26 02:36:01', '2019-03-26 02:36:01', '', 0, 'http://localhost/wordpress/?p=240', 0, 'customize_changeset', '', 0),
(241, 1, '2019-03-26 02:45:40', '2019-03-26 02:45:40', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 02:45:40\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '7b996788-e324-4df9-b789-0ab7b1ad719e', '', '', '2019-03-26 02:45:40', '2019-03-26 02:45:40', '', 0, 'http://localhost/wordpress/?p=241', 0, 'customize_changeset', '', 0),
(242, 1, '2019-03-26 02:50:10', '2019-03-26 02:50:10', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 02:50:10\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'c577f10b-e97a-4cf4-9910-f129de56d651', '', '', '2019-03-26 02:50:10', '2019-03-26 02:50:10', '', 0, 'http://localhost/wordpress/?p=242', 0, 'customize_changeset', '', 0),
(243, 1, '2019-03-26 02:50:47', '2019-03-26 02:50:47', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"3\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 02:50:47\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '070af7eb-04b2-4bde-89bc-1902dea76249', '', '', '2019-03-26 02:50:47', '2019-03-26 02:50:47', '', 0, 'http://localhost/wordpress/?p=243', 0, 'customize_changeset', '', 0),
(244, 1, '2019-03-26 02:52:42', '2019-03-26 02:52:42', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"3\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 02:52:42\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4a0ca388-20eb-4a09-a6b8-626a536d32f2', '', '', '2019-03-26 02:52:42', '2019-03-26 02:52:42', '', 0, 'http://localhost/wordpress/?p=244', 0, 'customize_changeset', '', 0),
(245, 1, '2019-03-26 03:05:07', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-03-26 03:05:07', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=245', 0, 'post', '', 0),
(246, 1, '2019-03-26 03:05:39', '2019-03-26 03:05:39', '', 'Allow us to host your next dinner party, or simply meet your friends in the pub', '', 'publish', 'open', 'open', '', 'allow-us-to-host-your-next-dinner-party-or-simply-meet-your-friends-in-the-pub', '', '', '2019-03-26 03:05:39', '2019-03-26 03:05:39', '', 0, 'http://localhost/wordpress/?p=246', 0, 'post', '', 0),
(247, 1, '2019-03-26 03:05:39', '2019-03-26 03:05:39', '', 'Allow us to host your next dinner party, or simply meet your friends in the pub', '', 'inherit', 'closed', 'closed', '', '246-revision-v1', '', '', '2019-03-26 03:05:39', '2019-03-26 03:05:39', '', 246, 'http://localhost/wordpress/?p=247', 0, 'revision', '', 0),
(248, 1, '2019-03-26 03:05:56', '2019-03-26 03:05:56', '', 'Allow us to host your next dinner party, or simply meet your friends in the pub', '', 'publish', 'open', 'open', '', 'allow-us-to-host-your-next-dinner-party-or-simply-meet-your-friends-in-the-pub-2', '', '', '2019-03-26 03:05:56', '2019-03-26 03:05:56', '', 0, 'http://localhost/wordpress/?p=248', 0, 'post', '', 0),
(249, 1, '2019-03-26 03:05:56', '2019-03-26 03:05:56', '', 'Allow us to host your next dinner party, or simply meet your friends in the pub', '', 'inherit', 'closed', 'closed', '', '248-revision-v1', '', '', '2019-03-26 03:05:56', '2019-03-26 03:05:56', '', 248, 'http://localhost/wordpress/?p=249', 0, 'revision', '', 0),
(250, 1, '2019-03-26 03:06:46', '2019-03-26 03:06:46', '', 'Allow us to host your next dinner party, or simply meet your friends in the pub', '', 'publish', 'open', 'open', '', 'allow-us-to-host-your-next-dinner-party-or-simply-meet-your-friends-in-the-pub-3', '', '', '2019-03-26 03:06:46', '2019-03-26 03:06:46', '', 0, 'http://localhost/wordpress/?p=250', 0, 'post', '', 0),
(251, 1, '2019-03-26 03:06:46', '2019-03-26 03:06:46', '', 'Allow us to host your next dinner party, or simply meet your friends in the pub', '', 'inherit', 'closed', 'closed', '', '250-revision-v1', '', '', '2019-03-26 03:06:46', '2019-03-26 03:06:46', '', 250, 'http://localhost/wordpress/?p=251', 0, 'revision', '', 0),
(252, 1, '2019-03-26 03:14:52', '2019-03-26 03:14:52', '{\n    \"beer_pub::blog_category\": {\n        \"value\": \"7\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 03:14:52\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '431a7a21-5249-4907-acd6-0fa2e56f0449', '', '', '2019-03-26 03:14:52', '2019-03-26 03:14:52', '', 0, 'http://localhost/wordpress/?p=252', 0, 'customize_changeset', '', 0),
(253, 1, '2019-03-26 04:43:03', '0000-00-00 00:00:00', '{\n    \"beer_pub::google_font_header\": {\n        \"value\": \"Life+Savers:400,700\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 04:43:03\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'd1f0bfe9-d9cb-46cf-9b14-f7ffaff02cb9', '', '', '2019-03-26 04:43:03', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=253', 0, 'customize_changeset', '', 0),
(254, 1, '2019-03-26 04:55:13', '0000-00-00 00:00:00', '{\n    \"beer_pub::google_font_p\": {\n        \"value\": \"Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 04:55:13\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'ebd49f24-4220-416c-ae3b-3300459fa3da', '', '', '2019-03-26 04:55:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=254', 0, 'customize_changeset', '', 0),
(255, 1, '2019-03-26 06:39:19', '0000-00-00 00:00:00', '{\n    \"beer_pub::google_font_header\": {\n        \"value\": \"Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 06:39:19\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'e7378e83-db0b-470c-acb1-6f16c05052f6', '', '', '2019-03-26 06:39:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=255', 0, 'customize_changeset', '', 0),
(256, 1, '2019-03-26 06:39:49', '0000-00-00 00:00:00', '{\n    \"beer_pub::google_font_header\": {\n        \"value\": \"Old Standard TT:400,400i,700\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 06:39:49\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '5d10a7bd-cfb5-473d-a613-77ddf41d2547', '', '', '2019-03-26 06:39:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=256', 0, 'customize_changeset', '', 0),
(257, 1, '2019-03-26 06:42:48', '0000-00-00 00:00:00', '{\n    \"beer_pub::google_font_header\": {\n        \"value\": \"Life Savers:400,700\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 06:42:48\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', 'c9c2ce7f-5906-45e8-8885-19f0c6a3c096', '', '', '2019-03-26 06:42:48', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=257', 0, 'customize_changeset', '', 0),
(258, 1, '2019-03-26 06:52:53', '2019-03-26 06:52:53', '{\n    \"beer_pub::google_font_h\": {\n        \"value\": \"Lily+Script+One:400\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 06:52:53\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'edfc4ec5-e44c-411c-8956-08019a92f82c', '', '', '2019-03-26 06:52:53', '2019-03-26 06:52:53', '', 0, 'http://localhost/wordpress/?p=258', 0, 'customize_changeset', '', 0),
(259, 1, '2019-03-26 06:53:46', '0000-00-00 00:00:00', '{\n    \"beer_pub::google_font_header\": {\n        \"value\": \"Life Savers:400,700\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 06:53:46\"\n    },\n    \"beer_pub::google_font_p\": {\n        \"value\": \"Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 06:53:46\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '64afed87-45c4-4168-8387-840842fb54a6', '', '', '2019-03-26 06:53:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=259', 0, 'customize_changeset', '', 0),
(260, 1, '2019-03-26 06:56:15', '0000-00-00 00:00:00', '{\n    \"beer_pub::google_font_h\": {\n        \"value\": \"Lily+Script+One:400\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 06:56:15\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '8a10336e-7d1e-4544-9d70-5f34fec146c5', '', '', '2019-03-26 06:56:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=260', 0, 'customize_changeset', '', 0),
(261, 1, '2019-03-26 06:59:30', '2019-03-26 06:59:30', '{\n    \"beer_pub::google_font_h\": {\n        \"value\": \"Lily Script One:400\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 06:59:30\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '3508f5df-afcd-4e1f-956a-8bcaa9983845', '', '', '2019-03-26 06:59:30', '2019-03-26 06:59:30', '', 0, 'http://localhost/wordpress/?p=261', 0, 'customize_changeset', '', 0),
(262, 1, '2019-03-26 07:01:24', '2019-03-26 07:01:24', '{\n    \"beer_pub::google_font_header\": {\n        \"value\": \"Life Savers:400,700\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 07:00:23\"\n    },\n    \"beer_pub::google_font_p\": {\n        \"value\": \"Libre Baskerville:400,400i,700\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 07:00:23\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '8b2e399e-7ad4-43e6-b17c-c41e5ed0f8a5', '', '', '2019-03-26 07:01:24', '2019-03-26 07:01:24', '', 0, 'http://localhost/wordpress/?p=262', 0, 'customize_changeset', '', 0),
(263, 1, '2019-03-26 07:47:23', '2019-03-26 07:47:23', '{\n    \"beer_pub::map_contact\": {\n        \"value\": \"<iframe src=\\\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14900.820008282744!2d105.78794855!3d20.984417999999998!3m2!1i1024!2i768!4f13.1!4m3!3e6!4m0!4m0!5e0!3m2!1svi!2s!4v1553584360760\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0\\\" allowfullscreen></iframe>\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 07:47:23\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '7ed1c37a-ac3a-4d7c-957d-3124801227f7', '', '', '2019-03-26 07:47:23', '2019-03-26 07:47:23', '', 0, 'http://localhost/wordpress/?p=263', 0, 'customize_changeset', '', 0),
(264, 1, '2019-03-26 08:14:13', '2019-03-26 08:14:13', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 08:13:52\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '6a188c8b-a3b5-43c6-a303-a2f0f5e518a5', '', '', '2019-03-26 08:14:13', '2019-03-26 08:14:13', '', 0, 'http://localhost/wordpress/?p=264', 0, 'customize_changeset', '', 0),
(265, 1, '2019-03-26 08:21:52', '2019-03-26 08:21:52', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"undefined\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 08:21:52\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4e35587d-477e-49de-a294-4371240bc570', '', '', '2019-03-26 08:21:52', '2019-03-26 08:21:52', '', 0, 'http://localhost/wordpress/?p=265', 0, 'customize_changeset', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(266, 1, '2019-03-26 09:02:29', '2019-03-26 09:02:29', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$25&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 09:02:29\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b29c9211-061e-4d2f-bdbe-90e75b949aee', '', '', '2019-03-26 09:02:29', '2019-03-26 09:02:29', '', 0, 'http://localhost/wordpress/?p=266', 0, 'customize_changeset', '', 0),
(267, 1, '2019-03-26 09:03:32', '0000-00-00 00:00:00', '{\n    \"beer_pub::customizer_repeater_rates\": {\n        \"value\": \"[{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Red Cow Craft Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"social-repeater-5c998f3ae2b3e\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Narragansett Lager\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f6a90975\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$25&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Schofferhofer Grapefruit\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998f93a38a9\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Guinness Draught\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fb75d20b\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"The Perfect Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fd004669\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"Awesome Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998fe614e44\\\",\\\"shortcode\\\":\\\"undefined\\\"},{\\\"text\\\":\\\"$15&#x2F;-\\\",\\\"link\\\":\\\"#\\\",\\\"text2\\\":\\\"undefined\\\",\\\"title\\\":\\\"That Kind Of Beer\\\",\\\"subtitle\\\":\\\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\\\",\\\"social_repeater\\\":\\\"\\\",\\\"id\\\":\\\"customizer-repeater-5c998ff70d377\\\",\\\"shortcode\\\":\\\"undefined\\\"}]\",\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-26 09:03:32\"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '1c8dac53-c05d-4562-b87f-3fac8dfb36c8', '', '', '2019-03-26 09:03:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=267', 0, 'customize_changeset', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main', 'main', 0),
(3, 'menu-footer', 'menu-footer', 0),
(5, 'Our Brewery', 'our-brewery', 0),
(6, 'The Pub', 'the-pub', 0),
(7, 'blog', 'blog', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(8, 2, 0),
(9, 2, 0),
(10, 2, 0),
(12, 2, 0),
(13, 2, 0),
(74, 3, 0),
(76, 3, 0),
(77, 3, 0),
(78, 3, 0),
(79, 3, 0),
(80, 3, 0),
(204, 2, 0),
(205, 2, 0),
(206, 6, 0),
(209, 6, 0),
(212, 6, 0),
(215, 5, 0),
(220, 5, 0),
(223, 5, 0),
(226, 5, 0),
(246, 7, 0),
(248, 7, 0),
(250, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 7),
(3, 3, 'nav_menu', '', 0, 6),
(5, 5, 'category', '', 0, 4),
(6, 6, 'category', '', 0, 3),
(7, 7, 'category', '', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:5:{s:64:\"d67db611462067e989507eb93829552026d7f14996d760a6d9cec9c3f6546bdd\";a:4:{s:10:\"expiration\";i:1553667480;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1553494680;}s:64:\"be036b8d5517599d6f2ca3ab390f3de85080d159455bf91aeff545ad7bce0619\";a:4:{s:10:\"expiration\";i:1553676911;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1553504111;}s:64:\"4359891c4c1aad898fb87a85db15eb56d7ba6653a99265173e90112373f4e955\";a:4:{s:10:\"expiration\";i:1553676973;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1553504173;}s:64:\"c645d0605ec8f1424b17c5f2c18be7d864a01d84bface6ff61945d445eca6355\";a:4:{s:10:\"expiration\";i:1553676981;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1553504181;}s:64:\"6f3626d641a83dcce5c50b4c322022465c8367526c2af967a40ad2400bd8e76a\";a:4:{s:10:\"expiration\";i:1553737866;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1553565066;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '159'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:21:\"add-post-type-gallery\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-post_format\";i:3;s:15:\"add-gallery_cat\";}'),
(20, 1, 'nav_menu_recently_edited', '2'),
(21, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce&hidetb=1'),
(22, 1, 'wp_user-settings-time', '1553240185'),
(24, 1, 'AkeebaSession_6a4fc5daadc30f8b3eeb46bfe6e014d5', 'a:6:{s:9:\"insideCMS\";b:1;s:22:\"platformNameForUpdates\";s:9:\"wordpress\";s:25:\"platformVersionForUpdates\";s:5:\"5.0.3\";s:7:\"user_id\";i:1;s:13:\"newSecretWord\";N;s:7:\"profile\";i:1;}'),
(26, 1, 'AkeebaSession_ba19a440619a16bbc773c5f1ed30bf68', 'a:1:{s:5:\"value\";s:128:\"dcc57c6e57371b7977de1a19ffd39eabace09e81414388c05b781caf2c65998c84562368639b07d51576c6e7d490150f84dfd0026154de2761f392ba96f8b372\";}'),
(27, 1, 'AkeebaSession_id', '8cb599fcc81b4be6e467611656e9106d'),
(28, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:12:\"118.70.182.0\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL,
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BeyPDC3rXQXTSgkBi9B8KjqaBPgNbr0', 'admin', 'example@example.com', '', '2019-02-13 04:36:13', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_akeeba_common`
--
ALTER TABLE `wp_akeeba_common`
  ADD PRIMARY KEY (`key`) USING BTREE;

--
-- Indexes for table `wp_ak_params`
--
ALTER TABLE `wp_ak_params`
  ADD PRIMARY KEY (`tag`) USING BTREE;

--
-- Indexes for table `wp_ak_profiles`
--
ALTER TABLE `wp_ak_profiles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `wp_ak_stats`
--
ALTER TABLE `wp_ak_stats`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `idx_fullstatus` (`filesexist`,`status`) USING BTREE,
  ADD KEY `idx_stale` (`status`,`origin`) USING BTREE;

--
-- Indexes for table `wp_ak_storage`
--
ALTER TABLE `wp_ak_storage`
  ADD PRIMARY KEY (`tag`) USING BTREE;

--
-- Indexes for table `wp_ak_users`
--
ALTER TABLE `wp_ak_users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`) USING BTREE,
  ADD KEY `comment_id` (`comment_id`) USING BTREE,
  ADD KEY `meta_key` (`meta_key`(191)) USING BTREE;

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`) USING BTREE,
  ADD KEY `comment_post_ID` (`comment_post_ID`) USING BTREE,
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`) USING BTREE,
  ADD KEY `comment_date_gmt` (`comment_date_gmt`) USING BTREE,
  ADD KEY `comment_parent` (`comment_parent`) USING BTREE,
  ADD KEY `comment_author_email` (`comment_author_email`(10)) USING BTREE;

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`) USING BTREE,
  ADD KEY `link_visible` (`link_visible`) USING BTREE;

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`) USING BTREE,
  ADD UNIQUE KEY `option_name` (`option_name`) USING BTREE;

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`) USING BTREE,
  ADD KEY `post_id` (`post_id`) USING BTREE,
  ADD KEY `meta_key` (`meta_key`(191)) USING BTREE;

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`) USING BTREE,
  ADD KEY `post_name` (`post_name`(191)) USING BTREE,
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`) USING BTREE,
  ADD KEY `post_parent` (`post_parent`) USING BTREE,
  ADD KEY `post_author` (`post_author`) USING BTREE;

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`) USING BTREE,
  ADD KEY `term_id` (`term_id`) USING BTREE,
  ADD KEY `meta_key` (`meta_key`(191)) USING BTREE;

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`) USING BTREE,
  ADD KEY `slug` (`slug`(191)) USING BTREE,
  ADD KEY `name` (`name`(191)) USING BTREE;

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`) USING BTREE,
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`) USING BTREE;

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`) USING BTREE,
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`) USING BTREE,
  ADD KEY `taxonomy` (`taxonomy`) USING BTREE;

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`) USING BTREE,
  ADD KEY `user_id` (`user_id`) USING BTREE,
  ADD KEY `meta_key` (`meta_key`(191)) USING BTREE;

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`) USING BTREE,
  ADD KEY `user_login_key` (`user_login`) USING BTREE,
  ADD KEY `user_nicename` (`user_nicename`) USING BTREE,
  ADD KEY `user_email` (`user_email`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_ak_profiles`
--
ALTER TABLE `wp_ak_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_ak_stats`
--
ALTER TABLE `wp_ak_stats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_ak_users`
--
ALTER TABLE `wp_ak_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=424;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=735;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
