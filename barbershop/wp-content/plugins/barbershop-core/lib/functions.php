<?php
require_once(BARBERSHOP_SHORTCODES_LIB . '/widgets/barbershop_recent_posts.php'); 
require_once(BARBERSHOP_SHORTCODES_LIB . '/widgets/barbershop_recent_comments.php');
require_once(BARBERSHOP_SHORTCODES_LIB . '/widgets/barbershop_about.php');

remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


function barbershop_remove_cssjs_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
    $src = remove_query_arg( 'ver', $src );
    return $src;
}

add_filter( 'style_loader_src', 'barbershop_remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'barbershop_remove_cssjs_ver', 10, 2 );

// Build the string of values in an Array
function barbershop_getFontsData( $fontsString ) {   
 
    // Font data Extraction
    $googleFontsParam = new Vc_Google_Fonts();      
    $fieldSettings = array();
    $fontsData = strlen( $fontsString ) > 0 ? $googleFontsParam->_vc_google_fonts_parse_attributes( $fieldSettings, $fontsString ) : '';
    return $fontsData;
     
}

// Build the inline style starting from the data
function barbershop_googleFontsStyles( $fontsData ) {
     
    $styles[]='';
    // Inline styles
    if(isset( $fontsData['values']['font_family'] ) ){
        $fontFamily = explode( ':', $fontsData['values']['font_family'] );
        $styles[] = 'font-family:' . $fontFamily[0];            
    }
    if(isset( $fontsData['values']['font_style'] ) ){
        $fontStyles = explode( ':', $fontsData['values']['font_style'] );
        $styles[] = 'font-weight:' . $fontStyles[1];
        $styles[] = 'font-style:' . $fontStyles[2];
    }
    $inline_style = '';    
    if (count($styles) > 0 && (is_array($styles) || is_object($styles))){ 
        foreach( $styles as $attribute ){
            if($attribute!=''){           
                $inline_style .= $attribute.'; '; 
            }      
        }   
    }
     
    return $inline_style;
     
}

function barbershop_shortcode_extract_class( $el_class ) {
    $output = '';
    if ( $el_class != '' ) {
        $output = " " . str_replace( ".", "", $el_class );
    }

    return $output;
}

function barbershop_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
    return $mime_types;
}
add_filter('upload_mimes', 'barbershop_myme_types', 1, 1);
