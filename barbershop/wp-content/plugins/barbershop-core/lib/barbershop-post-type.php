<?php
/**
 * Register "Testimonials" post type
 * @return [type] [description]
 */
function register_testimonial_post_type() {

	$labels = array(
	    'name' => __('Testimonials', 'barbershop-core'),
	    'singular_name' => __('Testimonials', 'barbershop-core'),
	    'add_new' => __('Add New', 'barbershop-core'),
	    'add_new_item' => __('Add New Testimonial', 'barbershop-core'),
	    'edit_item' => __('Edit Testimonial', 'barbershop-core'),
	    'new_item' => __('New Testimonial', 'barbershop-core'),
	    'all_items' => __('All Testimonial', 'barbershop-core'),
	    'view_item' => __('View Testimonial', 'barbershop-core'),
	    'search_items' => __('Search Testimonials', 'barbershop-core'),
	    'not_found' =>  __('No Testimonials found', 'barbershop-core'),
	    'not_found_in_trash' => __('No Testimonials found in Trash', 'barbershop-core'),
	    'parent_item_colon' => '',
	    'menu_name' => __('Testimonials', 'barbershop-core')
	  );

  	$args = array(
	    'labels' => $labels,
	    'public' => false,
	    'exclude_from_search' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'show_in_menu' => true, 
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'testimonial' ),
	    'capability_type' => 'post',
	    'has_archive' => false, 
	    'hierarchical' => false,
	    'menu_position' => 50,
	    'menu_icon' => 'dashicons-groups',
	    'supports' => array( 'title','editor', 'thumbnail', 'revisions','page-attributes')
  	); 

	register_post_type( 'testimonial', $args);

}
add_action( 'init', 'register_testimonial_post_type');

/**
 * Customize Rug table overview tables
 */

function add_new_testimonial_table_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['image'] = __('Image', 'barbershop-core');    
    $new_columns['title'] = _x('Name', 'column name', 'barbershop-core');
    $new_columns['date'] = _x('Date', 'column name', 'barbershop-core');
 
    return $new_columns;
}
// Add to admin_init function
add_filter('manage_testimonial_posts_columns', 'add_new_testimonial_table_columns');
function manage_testimonial_table_columns($column_name, $id) {
    global $wpdb;
	$featured_img_url = get_the_post_thumbnail_url($id, 'thumbnail');
    switch ($column_name) {
	    case 'image':
			if(!empty($featured_img_url)){
				echo '<img src="'. $featured_img_url .'" width="80" >';
			}else{
				echo 'No image';
			}
	        break;
	    default:
	        break;
    } // end switch
}   
// Add to admin_init function
add_action('manage_testimonial_posts_custom_column', 'manage_testimonial_table_columns', 10,2);

/**
 * Register meta box testimonial
 */
function testimonial_meta_box(){
	add_meta_box( 'meta-box-testimonial', 'Testimonial info', 'testimonial_meta_display', 'testimonial', 'side', 'low' );
}
add_action( 'add_meta_boxes', 'testimonial_meta_box' );
/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
 
function testimonial_meta_display(){
    global $post ;
    $testimonial_name = get_post_meta( $post->ID, 'testimonial_name', true );
    $testimonial_position = get_post_meta( $post->ID, 'testimonial_position', true );
	wp_nonce_field( 'save_testimonial', 'testimonial_nonce' );
	?>
    <div class="testimonial-info-wrapper">
        <div class="single-field-wrap">
            <h4><span class="section-title"><?php _e( 'Position', 'barbershop-core' );?></span></h4>
            <span class="section-inputfield"><input style="width:100%;" type="text" name="testimonial_position" value="<?php if( !empty( $testimonial_position ) ){ echo $testimonial_position ; }?>" /></span>
        </div>
    </div>
<?php
}
/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function testimonial_save_meta_box( $post_id ) {

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['testimonial_nonce'] ) || !wp_verify_nonce( $_POST['testimonial_nonce'], 'save_testimonial' ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    if( isset( $_POST['testimonial_position'] ) ){
        update_post_meta( $post_id, 'testimonial_position',  $_POST['testimonial_position'] );
    }else{
        delete_post_meta( $post_id, 'testimonial_position' );
    }
}
add_action( 'save_post', 'testimonial_save_meta_box' );

function create_shortcode_testimonial($atts) {

		extract( shortcode_atts( array (
		  "id"    => ''
		), $atts ) );

        $the_query = new WP_Query(array(
			'post_type' => 'testimonial',
			'posts_per_page' => 1,
			'p' => $id,
			'post_status' => 'publish',
			'orderby'	=> 'date',
			'order'	=> 'DESC',
        ));

        ob_start();
        if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php $testimonial_fields = get_post_meta( get_the_ID(), 'testimonial_fields', true); if ( $testimonial_fields ) : ?>
					<div class="list-testimonial">
						<?php foreach ( $testimonial_fields as $field ) { ?>
							<div class="item-testimonial">
								<div class="wrap-item">
									<?php if($field['content'] != ''){ ?>
										<div class="content"><?php echo wpautop( $field['content'] ); ?></div>
									<?php } ?>
									<?php if($field['name'] != ''){ ?>
										<p class="name"><?php echo esc_attr( $field['name'] ); ?></p>
									<?php } ?>
									<?php if($field['address'] != ''){ ?>
										<p class="name"><?php echo esc_attr( $field['address'] ); ?></p>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				<?php endif; ?>
			<?php
			endwhile;
        endif;
		wp_reset_postdata();
        $list_post = ob_get_contents();

        ob_end_clean();

        return $list_post;
}
add_shortcode('testimonial', 'create_shortcode_testimonial');

/**
 * Register "Features" post type
 * @return [type] [description]
 */
function register_feature_post_type() {

	$labels = array(
	    'name' => __('Features', 'barbershop-core'),
	    'singular_name' => __('Features', 'barbershop-core'),
	    'add_new' => __('Add New', 'barbershop-core'),
	    'add_new_item' => __('Add New Feature', 'barbershop-core'),
	    'edit_item' => __('Edit Feature', 'barbershop-core'),
	    'new_item' => __('New Feature', 'barbershop-core'),
	    'all_items' => __('All Features', 'barbershop-core'),
	    'view_item' => __('View Features', 'barbershop-core'),
	    'search_items' => __('Search Features', 'barbershop-core'),
	    'not_found' =>  __('No Features found', 'barbershop-core'),
	    'not_found_in_trash' => __('No Features found in Trash', 'barbershop-core'),
	    'parent_item_colon' => '',
	    'menu_name' => __('Features', 'barbershop-core')
	  );

  	$args = array(
	    'labels' => $labels,
	    'public' => false,
	    'exclude_from_search' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'show_in_menu' => true, 
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'feature' ),
	    'capability_type' => 'post',
	    'has_archive' => false, 
	    'hierarchical' => false,
	    'menu_position' => 50,
	    'menu_icon' => 'dashicons-format-gallery',
	    'supports' => array( 'title')
  	); 

	register_post_type( 'feature', $args);

}
//add_action( 'init', 'register_feature_post_type');

/**
 * Customize feature overview tables ("feature" -> "All features")
 * Add / modify columns at feature edit overview
 * @param  [type] $gallery_columns [description]
 * @return [type]                  [description]
 */
function add_new_feature_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['title'] = _x('Feature Name', 'column name', 'barbershop-core');    
    $new_columns['shortcode'] = __('Shortcode', 'barbershop-core');
    $new_columns['date'] = _x('Date', 'column name', 'barbershop-core');
 
    return $new_columns;
}
// Add to admin_init function
add_filter('manage_feature_posts_columns', 'add_new_feature_columns');
function manage_feature_columns($column_name, $id) {
    global $wpdb;

    switch ($column_name) {
	    case 'shortcode':
	        echo '<input type="text" style="width: 300px;" readonly="readonly" onclick="this.select()" value="[feature id=&quot;'. $id . '&quot;]"/>';
	            break;
	 
	    default:
	        break;
    } // end switch
}   
// Add to admin_init function
add_action('manage_feature_posts_custom_column', 'manage_feature_columns', 10,2);

add_action('admin_init', 'feature_add_meta_boxes', 1);
function feature_add_meta_boxes() {
	add_meta_box( 'feature-fields', 'Features', 'feature_meta_box_display', 'feature', 'normal', 'high');
}
function feature_meta_box_display() {
	global $post;
	$feature_fields = get_post_meta($post->ID, 'feature_fields', true);
	wp_nonce_field( 'feature_meta_box_nonce', 'feature_meta_box_nonce' );
	?>
	<script type="text/javascript">
	jQuery(document).ready(function( $ ){
		$( '#add-row' ).on('click', function() {			
			var row = $( '.empty-row.screen-reader-text' ).clone(true);
			row.removeClass( 'empty-row screen-reader-text' );
			row.insertBefore( '#repeatable-fieldset-one .list-item:last-child' );
			$(row).find(".select2-container").remove();
			var select2 = $(row).find(".select-icon");
			select2.select2({
				width: "100%",
				formatResult: format,
				formatSelection: format,
				escapeMarkup: function(m) { return m; }
			});				 
			return false;
		});
  	
		$( '.remove-row' ).on('click', function() {
			$(this).parents('.list-item').remove();
			return false;
		});
		function format(icon) {
			var originalOption = icon.element;
			return '<i class="fa ' + $(originalOption).val() + '"></i>  ' + icon.text;
		}

		$('.select-icon').select2({
			width: "100%",
			formatResult: format,
			formatSelection: format,
			escapeMarkup: function(m) { return m; }
		});		
	});
	</script>
  
	<div id="repeatable-fieldset-one" width="100%">

		<?php
		
		if ( $feature_fields ) :
		
		foreach ( $feature_fields as $field ) {
		?>
		<div class="list-item">
			<p><label><?php echo _e('Title','barbershop-core'); ?></label><input type="text" class="widefat" name="titleFeature[]" value="<?php if ($field['titleFeature'] != '') echo esc_attr( $field['titleFeature'] ); ?>" /></p>
			<p><label><?php echo _e('Content','barbershop-core'); ?></label><textarea class="widefat" name="content[]"  rows="4"><?php if($field['content'] != '') echo esc_attr( $field['content'] ); ?></textarea></p>
			<p><label><?php echo _e('URL','barbershop-core'); ?></label><input type="text" class="widefat" name="url[]" value="<?php if ($field['url'] != '') echo esc_attr( $field['url'] ); ?>" /></p>
		
			<p><a class="button remove-row" href="#"><?php echo _e('Remove','barbershop-core'); ?></a></p>
		</div>
		<?php
		}
		else :
		// show a blank one
		?>
		<div class="list-item">
			<p><label><?php echo _e('Title','barbershop-core'); ?></label><input type="text" class="widefat" name="titleFeature[]" /></p>
			<p><label><?php echo _e('Content','barbershop-core'); ?></label><textarea class="widefat" name="content[]"  rows="4"></textarea></p>
			<p><label><?php echo _e('URL','barbershop-core'); ?></label><input type="text" class="widefat" name="url[]" value="" /></p>
		
			<p><a class="button remove-row" href="#"><?php echo _e('Remove','barbershop-core'); ?></a></p>
		</div>
		<?php endif; ?>
		
		<!-- empty hidden one for jQuery -->
		<div class="list-item empty-row screen-reader-text">
			<p><label><?php echo _e('Title','barbershop-core'); ?></label><input type="text" class="widefat" name="titleFeature[]" /></p>
			<p><label><?php echo _e('Content','barbershop-core'); ?></label><textarea class="widefat" name="content[]"  rows="4"></textarea></p>
			<p><label><?php echo _e('URL','barbershop-core'); ?></label><input type="text" class="widefat" name="url[]" value="" /></p>
		
			<p><a class="button remove-row" href="#"><?php echo _e('Remove','barbershop-core'); ?></a></p>
		</div>
	</div>
	<div class="clear"></div>
	
	<p><a id="add-row" class="button" href="#"><?php echo _e('New Feature','barbershop-core'); ?></a></p>
	<?php
}
add_action('save_post', 'feature_meta_box_save');
function feature_meta_box_save($post_id) {
	if ( ! isset( $_POST['feature_meta_box_nonce'] ) ||
	! wp_verify_nonce( $_POST['feature_meta_box_nonce'], 'feature_meta_box_nonce' ) )
		return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	
	if (!current_user_can('edit_post', $post_id))
		return;
	
	$old = get_post_meta($post_id, 'feature_fields', true);
	$new = array();

	$titleFeature = $_POST['titleFeature'];
	$content = $_POST['content'];
	$url = $_POST['url'];
	
	$count = count( $titleFeature );
	
	for ( $i = 0; $i < $count; $i++ ) {
		if ( $titleFeature[$i] != '' ) :
			$new[$i]['titleFeature'] = $titleFeature[$i];
			$new[$i]['content'] = stripslashes( strip_tags( $content[$i] ) );
			$new[$i]['url'] = stripslashes( strip_tags( $url[$i] ) );
		endif;
	}
	if ( !empty( $new ) && $new != $old )
		update_post_meta( $post_id, 'feature_fields', $new );
	elseif ( empty($new) && $old )
		delete_post_meta( $post_id, 'feature_fields', $old );
}

function create_shortcode_feature($atts) {
	
		extract( shortcode_atts( array (
		  "id"    => ''
		), $atts ) );
		
        $the_query = new WP_Query(array(
			'post_type' => 'feature',
			'posts_per_page' => 1,
			'p' => $id,
			'post_status' => 'publish',
			'orderby'	=> 'date',
			'order'	=> 'DESC',
        ));
 
        ob_start();
        if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php 
				$feature_fields = get_post_meta( get_the_ID(), 'feature_fields', true); 
				$layout_feature = get_post_meta( get_the_ID(), 'custom_layout_feature_meta_box', true);
				if ( $feature_fields ) : ?>
					<div class="list-feature">
						<div class="row">
							<?php foreach ( $feature_fields as $field ) { ?>
								<div class="item-feature col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="wrapp">
										<?php if($field['titleFeature'] != '' && $field['url'] != ''){ ?>
											<h3><a href="<?php echo esc_url($field['url']); ?>"><?php echo $field['titleFeature']; ?></a></h3>
										<?php }elseif($field['titleFeature'] != ''){ ?>
											<h3><?php echo $field['titleFeature']; ?></h3>
										<?php } ?>
										<?php if($field['content'] != ''){ ?>
											<div class="content"><?php echo wpautop( $field['content'] ); ?></div>
										<?php } ?>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				<?php endif; ?>				
			<?php
			endwhile;
        endif;
		wp_reset_postdata();
        $list_post = ob_get_contents();
 
        ob_end_clean();
 
        return $list_post;
}
add_shortcode('feature', 'create_shortcode_feature');


/**
 * Register "Teams" post type
 * @return [type] [description]
 */
function register_team_post_type() {

    $labels = array(
        'name' => __('Teams', 'barbershop-core'),
        'singular_name' => __('Teams', 'barbershop-core'),
        'add_new' => __('Add New', 'barbershop-core'),
        'add_new_item' => __('Add New Team', 'barbershop-core'),
        'edit_item' => __('Edit Team', 'barbershop-core'),
        'new_item' => __('New Team', 'barbershop-core'),
        'all_items' => __('All Teams', 'barbershop-core'),
        'view_item' => __('View Teams', 'barbershop-core'),
        'search_items' => __('Search Teams', 'barbershop-core'),
        'not_found' =>  __('No Teams found', 'barbershop-core'),
        'not_found_in_trash' => __('No Teams found in Trash', 'barbershop-core'),
        'parent_item_colon' => '',
        'menu_name' => __('Teams', 'barbershop-core')
      );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array( 'slug' => 'team' ),
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => false,
        'menu_position' => 50,
        'menu_icon' => 'dashicons-format-gallery',
        'supports' => array( 'title','editor', 'thumbnail', 'revisions','page-attributes')
    ); 

    register_post_type( 'team', $args);

}
add_action( 'init', 'register_team_post_type');

/**
 * Customize Rug table overview tables
 */
function add_new_team_table_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['image'] = __('Image', 'barbershop-core');    
    $new_columns['title'] = _x('Name', 'column name', 'barbershop-core');
    $new_columns['date'] = _x('Date', 'column name', 'barbershop-core');
 
    return $new_columns;
}
// Add to admin_init function
add_filter('manage_team_posts_columns', 'add_new_team_table_columns');
function manage_team_table_columns($column_name, $id) {
    global $wpdb;
    $featured_img_url = get_the_post_thumbnail_url($id, 'thumbnail');
    switch ($column_name) {
        case 'image':
            if(!empty($featured_img_url)){
                echo '<img src="'. $featured_img_url .'" width="80" >';
            }else{
                echo 'No image';
            }
            break;
        default:
            break;
    } // end switch
}   
// Add to admin_init function
add_action('manage_team_posts_custom_column', 'manage_team_table_columns', 10,2);

/**
 * Register meta box team
 */
function team_meta_box(){
    add_meta_box( 'meta-box-team', 'Team info', 'team_meta_display', 'team', 'side', 'low' );
}
add_action( 'add_meta_boxes', 'team_meta_box' );
/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
 
function team_meta_display(){
    global $post ;
    $team_name = get_post_meta( $post->ID, 'team_name', true );
    $team_position = get_post_meta( $post->ID, 'team_position', true );
    wp_nonce_field( 'save_team', 'team_nonce' );
    ?>
    <div class="team-info-wrapper">
        <div class="single-field-wrap">
            <h4><span class="section-title"><?php _e( 'Position', 'barbershop-core' );?></span></h4>
            <span class="section-inputfield"><input style="width:100%;" type="text" name="team_position" value="<?php if( !empty( $team_position ) ){ echo $team_position ; }?>" /></span>
        </div>
    </div>
<?php
}
/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function team_save_meta_box( $post_id ) {

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['team_nonce'] ) || !wp_verify_nonce( $_POST['team_nonce'], 'save_team' ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    if( isset( $_POST['team_position'] ) ){
        update_post_meta( $post_id, 'team_position',  $_POST['team_position'] );
    }else{
        delete_post_meta( $post_id, 'team_position' );
    }
}
add_action( 'save_post', 'team_save_meta_box' );