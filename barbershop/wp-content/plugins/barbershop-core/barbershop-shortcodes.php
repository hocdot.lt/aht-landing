<?php

/*
  Plugin Name: Barbershop Core
  Plugin URI:
  Description: Core for Barbershop Theme.
  Version: 1.0.0
  Author: Barbershop
  Author URI:
  Text-domain: barbershop-core
 */

// don't load directly
if (!defined('ABSPATH'))
    die('-1');

require_once dirname(__FILE__) . '/social-widget/functions.php'; 
define('BARBERSHOP_SHORTCODES_URL', plugin_dir_url(__FILE__));
define('BARBERSHOP_SHORTCODES_LIB', dirname(__FILE__) . '/lib/');
class BarbershopShortcodesClass {

	function __construct() {

        // Load text domain
        add_action('plugins_loaded', array($this, 'loadTextDomain'));
        // Init plugins
        add_action('init', array($this, 'initPlugin'));

        $this->addShortcodes();

    }

    // Init plugins
    function initPlugin() {
        $this->addTinyMCEButtons();
    }

    // load plugin text domain
    function loadTextDomain() {
        load_plugin_textdomain('barbershop-core', false, dirname(__FILE__) . '/languages/');
    }

    // Add buttons to tinyMCE
    function addTinyMCEButtons() {
        if (!current_user_can('edit_posts') && !current_user_can('edit_pages'))
            return;

        if (get_user_option('rich_editing') == 'true') {
            add_filter('mce_buttons', array(&$this, 'registerTinyMCEButtons'));
        }
    }

    function registerTinyMCEButtons($buttons) {
        array_push($buttons, "barbershop_shortcodes_button");
        return $buttons;
    }

    // Add shortcodes
    function addShortcodes() {
        require_once(BARBERSHOP_SHORTCODES_LIB . 'functions.php');
        require_once(BARBERSHOP_SHORTCODES_LIB . 'barbershop-post-type.php');
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }

}

// Finally initialize code
new BarbershopShortcodesClass();
