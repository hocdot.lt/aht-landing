(function ($) {
    $('.testimonial-slider').slick({
        infinite: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                }
            }
        ],

    });

    $(".open-menu-mobile").click(function(e) {
        $('.overlay').addClass('overlay-menu');
        e.preventDefault();
        $(this).closest(".main-menu").find(".menu-container").toggleClass("open");
    });
    $('.overlay').on('click', function(){
        $(this).removeClass('overlay-menu');
        $(".close-menu-mobile").click()
    });
    $(".close-menu-mobile").click(function(e) {
        e.preventDefault();
        $('.overlay').removeClass('overlay-menu');
        $(this).closest(".menu-container").removeClass("open");
    });
    $(".hide-menu").click(function(e) {
        e.preventDefault();
        $('.overlay').removeClass('overlay-menu');
        $(this).closest(".menu-container").removeClass("open");
    });
    if($('body.home.page-template-homepage').length){
        30 <= $(window).scrollTop() ? $("header.header").addClass("sticky-header") : $("header.header").removeClass("sticky-header");
        $(window).scroll(function() {
            30 <= $(window).scrollTop() ? $("header.header").addClass("sticky-header") : $("header.header").removeClass("sticky-header")
        });
    }
    $('.menu-container .mega-menu > li.menu-item.current-menu-item > a,.menu-footer li a').click(function(e){
        e.preventDefault();
        if($('body').hasClass('home')){
            var href=$(this).attr('href');
            href=href.split('#');
            if(href.length>1){
                var top=0;
                if(href[1]==''){
                    top=0;
                }else{top=$('#session-'+href[1]).offset().top-$('header.header').outerHeight();}
                $('body,html').animate({scrollTop:top},'300',function(){
                    window.location.hash = '#'+href[1];
                });
            }else{
                location.href=$(this).attr('href');
            }
        }else{
            location.href=$(this).attr('href');
        }
    });
    menuScroll();
    $(window).scroll(function(){
        menuScroll();
    });

    function menuScroll(){
        if($('body').hasClass('home')){
            var sections = {};
            $('#main .section').each(function( index , el) {
                var key = $(el).attr('id');
                key = key.split('-')[1]
                if(key){
                    sections[key] = el.offsetTop;
                }
            });
            //console.log(sections);
            for (var i in sections) {
                if (sections[i] < $(window).scrollTop() + $('header.header').outerHeight() + 10) {
                    $('.mega-menu li').removeClass('active');
                    if($('.mega-menu li a[href*=#' + i + ']').length){
                        $('.mega-menu li a[href*=#' + i + ']').closest('li').addClass('active');
                    }else{
                        $('.mega-menu li a[href$=#]').closest('li').addClass('active');
                    }
                }
            }
        }
    }
})(jQuery);