<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
$barbershop_sidebar_left = barbershop_get_sidebar_left();
$barbershop_sidebar_right = barbershop_get_sidebar_right();
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site ">
        <header class="header fixed-top sticky-header">
            <div class="header-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-2 justify-content-center bg-header hc-left d-flex align-items-center">
                            <?php
                                $header_info = get_theme_mod('header_info','<p>800 345 678</p><p>linfo@website.com</p>');
                            ?>
                            <?php if ( $header_info !='') :?>
                            <div class="header-info">
                                <?php echo apply_filters( 'the_content', $header_info); ?>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6 justify-content-center px-2">
                            <?php if (is_front_page()) : ?>
                                <h1 class="logo text-md-center">
                            <?php else: ?>
                                <h2 class="logo text-md-center">
                                    <?php endif; ?>
                                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                        <?php
                                        $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                        if ($logo_header && $logo_header!=''):
                                            echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                        else:
                                            echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                        endif;
                                        ?>
                                    </a>
                            <?php if (is_front_page()) : ?>
                                </h1>
                            <?php else: ?>
                                </h2>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-3 col-md-2 col-2 justify-content-center bg-header hc-right d-flex align-items-center">
                            <?php
							    if(function_exists('barbershop_header_menu')){
                                    barbershop_header_menu();
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="main" class="wrapper">
				
            

                    
        
       
