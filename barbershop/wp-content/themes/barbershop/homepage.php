<?php 
/**
Template Name: Home
*/
get_header();

get_template_part('part-templates/home-banner');
get_template_part('part-templates/home-about');
get_template_part('part-templates/home-team');
get_template_part('part-templates/home-pricing');
get_template_part('part-templates/home-testimonials');
get_template_part('part-templates/home-blog');
get_template_part('part-templates/home-contact');

get_footer(); ?> 