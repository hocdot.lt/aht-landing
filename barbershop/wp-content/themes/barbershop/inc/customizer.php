<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'barbershop_customize_register' );
function barbershop_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'barbershop_Customize_Sidebar_Control' ) ) {
	    class barbershop_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',			
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',			
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',
                'Rozha One' => 'Rozha One',
                '' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'barbershop') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','barbershop'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','barbershop' ),
	      'description' => esc_html__( 'Upload Logo','barbershop' ),
	      'section' => 'general_settings',
	   )
	) );
    $wp_customize->add_setting( 'header_info',
        array(
            'default' => '<p>800 345 678</p><p>linfo@website.com</p>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'header_info',
        array(
            'label' => esc_html__( 'Header info','barbershop' ),
            'section' => 'general_settings',
        )
    ) );

    $wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'RozhaOne:400',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => '',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Main Google Font',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'barbershop'),
        'description'    => __('Several settings pertaining my theme', 'barbershop'),
    ) );
	
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','barbershop'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
	// title banner
	$wp_customize->add_setting( 'banner_title',
	   array(
	      'default' => 'Finest Men Haircuts In Dubai',
	   )
	);	
	$wp_customize->add_control( 'banner_title',
	   array(
	      'label' => esc_html__( 'Title Banner','barbershop' ),
	      'section' => 'home_banner_settings',
	      'type' => 'text',
	   )
	);	
	// sub title banner
	$wp_customize->add_setting( 'banner_sub_title',
	   array(
	      'default' => esc_html__('Looking for the best barber shop Dubai has to offer? From beard trims to haircuts, check out these top barbershop experiences.','barbershop'),
	   )
	);	
	$wp_customize->add_control( 'banner_sub_title',
	   array(
	      'label' => esc_html__( 'Sub Title Banner','barbershop' ),
	      'section' => 'home_banner_settings',
	      'type' => 'textarea',
	   )
	);	

	// button banner
	$wp_customize->add_setting( 'button_banner',
	   array(
	      'default' => esc_html__('Check Pricing','barbershop'),

	   )
	);	
	$wp_customize->add_control('button_banner',
		array(
	      'label' => esc_html__( 'Button banner','barbershop' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',	
	   )
	);
	$wp_customize->add_setting( 'link_banner',
	   array(
	      'default' => esc_html__('#','barbershop'),

	   )
	);	
	$wp_customize->add_control('link_banner',
		array(
	      'label' => esc_html__( 'Button URL','barbershop' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',	
	   )
	);
	// background banner
	$wp_customize->add_setting( 'background_banner',
		array(
			'default' => get_template_directory_uri() . '/images/banner.jpg',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_banner',
	   array(
	      'label' => esc_html__( 'Background Banner','barbershop' ),
	      'description' => esc_html__( 'Upload Image','barbershop' ),
	      'section' => 'home_banner_settings',
	   )
	) );

    // service 1
    $wp_customize->add_setting( 'service_icon_1',
        array(
            'default' => get_template_directory_uri() . '/images/service-1.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_icon_1',
        array(
            'label' => esc_html__( 'Service icon 1','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'home_banner_settings',
        )
    ) );
    $wp_customize->add_setting( 'service_title_1',
        array(
            'default' => 'HAIR CUT',
        )
    );
    $wp_customize->add_control( 'service_title_1',
        array(
            'label' => esc_html__( 'Title service 1','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_content_1',
        array(
            'default' => esc_html__('Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','barbershop'),
        )
    );
    $wp_customize->add_control( 'service_content_1',
        array(
            'label' => esc_html__( 'Content service 1','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'service_link_1',
        array(
            'default' => esc_html__('#','barbershop'),

        )
    );
    $wp_customize->add_control('service_link_1',
        array(
            'label' => esc_html__( 'Service 1 URL','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // service 2
    $wp_customize->add_setting( 'service_icon_2',
        array(
            'default' => get_template_directory_uri() . '/images/service-2.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_icon_2',
        array(
            'label' => esc_html__( 'Service icon 2','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'home_banner_settings',
        )
    ) );
    $wp_customize->add_setting( 'service_title_2',
        array(
            'default' => 'SHAVING',
        )
    );
    $wp_customize->add_control( 'service_title_2',
        array(
            'label' => esc_html__( 'Title service 2','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_content_2',
        array(
            'default' => esc_html__('Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','barbershop'),
        )
    );
    $wp_customize->add_control( 'service_content_2',
        array(
            'label' => esc_html__( 'Content service 2','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'service_link_2',
        array(
            'default' => esc_html__('#','barbershop'),

        )
    );
    $wp_customize->add_control('service_link_2',
        array(
            'label' => esc_html__( 'Service 2 URL','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // service 3
    $wp_customize->add_setting( 'service_icon_3',
        array(
            'default' => get_template_directory_uri() . '/images/service-3.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_icon_3',
        array(
            'label' => esc_html__( 'Service icon 3','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'home_banner_settings',
        )
    ) );
    $wp_customize->add_setting( 'service_title_3',
        array(
            'default' => 'STYLING',
        )
    );
    $wp_customize->add_control( 'service_title_3',
        array(
            'label' => esc_html__( 'Title service 3','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_content_3',
        array(
            'default' => esc_html__('Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','barbershop'),
        )
    );
    $wp_customize->add_control( 'service_content_3',
        array(
            'label' => esc_html__( 'Content service 3','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'service_link_3',
        array(
            'default' => esc_html__('#','barbershop'),

        )
    );
    $wp_customize->add_control('service_link_3',
        array(
            'label' => esc_html__( 'Service 3 URL','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // service 4
    $wp_customize->add_setting( 'service_icon_4',
        array(
            'default' => get_template_directory_uri() . '/images/service-4.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_icon_4',
        array(
            'label' => esc_html__( 'Service icon 4','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'home_banner_settings',
        )
    ) );
    $wp_customize->add_setting( 'service_title_4',
        array(
            'default' => 'TRIMMING',
        )
    );
    $wp_customize->add_control( 'service_title_4',
        array(
            'label' => esc_html__( 'Title service 4','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_content_4',
        array(
            'default' => esc_html__('Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','barbershop'),
        )
    );
    $wp_customize->add_control( 'service_content_4',
        array(
            'label' => esc_html__( 'Content service 4','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'service_link_4',
        array(
            'default' => esc_html__('#','barbershop'),

        )
    );
    $wp_customize->add_control('service_link_4',
        array(
            'label' => esc_html__( 'Service 4 URL','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','barbershop' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barbershop' ),
	         'yes' => esc_html__( 'Yes','barbershop' ),
            )
	   )
	);
	// End banner section
	
	// Section about
	
	$wp_customize->add_section( 'about_settings',
	   array(
	      'title' => esc_html__( 'About section','barbershop'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

	
	$wp_customize->add_setting( 'title_about',
	   array(
	      'default' => esc_html__('Best Haircut and Shaving studio in Town','barbershop'),

	   )
	);	
	$wp_customize->add_control('title_about',
		array(
	      'label' => esc_html__( 'Title About','barbershop' ),
	      'section' => 'about_settings',
	       'type' => 'text',	
	   )
	);
	
	$wp_customize->add_setting( 'sub_title_about',
	   array(
	      'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.','barbershop'),

	   )
	);	
	$wp_customize->add_control('sub_title_about',
		array(
	      'label' => esc_html__( 'Sub Title About','barbershop' ),
	      'section' => 'about_settings',
	       'type' => 'textarea',
	   )
	);

    $wp_customize->add_setting( 'about_image_1',
        array(
            'default' => get_template_directory_uri() . '/images/about-1.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_image_1',
        array(
            'label' => esc_html__( 'Image About 1','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'about_settings',
        )
    ) );

    $wp_customize->add_setting( 'about_image_2',
        array(
            'default' => get_template_directory_uri() . '/images/about-2.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_image_2',
        array(
            'label' => esc_html__( 'Image About 2','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'about_settings',
        )
    ) );

	// content
	$wp_customize->add_setting( 'about_content_1',
		array(
			'default' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Tempor incididunt ut labore et dolore magna aliqua...</p>",
		)
	);	
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'about_content_1',
	   array(
	      'label' => esc_html__( 'About content 1','barbershop' ),
	      'description' => esc_html__( 'About content','barbershop' ),
	      'section' => 'about_settings',
	   )
	) );
    $wp_customize->add_setting( 'about_content_2',
        array(
            'default' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Tempor incididunt ut labore et dolore magna aliqua...</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'about_content_2',
        array(
            'label' => esc_html__( 'About content 2','barbershop' ),
            'description' => esc_html__( 'About content','barbershop' ),
            'section' => 'about_settings',
        )
    ) );
    $wp_customize->add_setting( 'about_content_3',
        array(
            'default' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Tempor incididunt ut labore et dolore magna aliqua...</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'about_content_3',
        array(
            'label' => esc_html__( 'About content 3','barbershop' ),
            'description' => esc_html__( 'About content','barbershop' ),
            'section' => 'about_settings',
        )
    ) );
	
	$wp_customize->add_setting( 'about_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'about_show',
	   array(
            'label' => esc_html__( 'Show Section','barbershop' ),
            'section' => 'about_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barbershop' ),
	         'yes' => esc_html__( 'Yes','barbershop' ),
            )
	   )
	);
	// End about


	// start team
	$wp_customize->add_section( 'team_settings',
	   array(
	      'title' => esc_html__( 'Team Section','barbershop'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

    $wp_customize->add_setting( 'icon_title_team',
        array(
            'default' => get_template_directory_uri() . '/images/title-icon-1.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'icon_title_team',
        array(
            'label' => esc_html__( 'Title icon','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'team_settings',
        )
    ) );
    $wp_customize->add_setting( 'title_team',
        array(
            'default' => esc_html__('Our Expert Team of Trimming & Dressing','barbershop'),
        )
    );
    $wp_customize->add_control('title_team',
        array(
            'label' => esc_html__( 'Title Section','barbershop' ),
            'section' => 'team_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_team',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.','barbershop'),
        )
    );
    $wp_customize->add_control('sub_title_team',
        array(
            'label' => esc_html__( 'Sub title Section','barbershop' ),
            'section' => 'team_settings',
            'type' => 'textarea',
        )
    );

	$wp_customize->add_setting( 'on_team_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'on_team_show',
	   array(
            'label' => esc_html__( 'Show Section','barbershop' ),
            'section' => 'team_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barbershop' ),
	         'yes' => esc_html__( 'Yes','barbershop' ),
            )
	   )
	);
	// End work

	// start Testimonials
	$wp_customize->add_section( 'testimonials_settings',
	   array(
	      'title' => esc_html__( 'Testimonials Section','barbershop'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

	// start Pricing
	$wp_customize->add_section( 'pricing_settings',
	   array(
	      'title' => esc_html__( 'Pricing Section','barbershop'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

	$wp_customize->add_setting( 'pricing_title',
	   array(
	      'default' => 'Check out our <br> Price List <br> New year offer,<br> 30% on Haircut and Trimming',
	   )
	);	
	$wp_customize->add_control( 'pricing_title',
	   array(
	      'label' => esc_html__( 'Pricing Title','barbershop' ),
	      'section' => 'pricing_settings',
	      'type' => 'text',
	   )
	);
	$wp_customize->add_setting( 'pricing_sub_title',
	   array(
	      'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore...',
	   )
	);	
	$wp_customize->add_control( 'pricing_sub_title',
	   array(
	      'label' => esc_html__( 'Sub Title','barbershop' ),
	      'section' => 'pricing_settings',
	      'type' => 'textarea',
	   )
	);
	$wp_customize->add_setting( 'img_pricing',
		array(
			'default' => get_template_directory_uri() . '/images/price.jpg',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_pricing',
	   array(
	      'label' => esc_html__( 'Image','barbershop' ),
	      'description' => esc_html__( 'Upload image','barbershop' ),
	      'section' => 'pricing_settings',
	   )
	) );
    $wp_customize->add_setting( 'customizer_repeater_pricing', array(
        'sanitize_callback' => 'customizer_repeater_sanitize'
    ));
    $wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'customizer_repeater_pricing', array(
        'label'   => esc_html__('Pricing Details','customizer-repeater'),
        'section' => 'pricing_settings',
        'customizer_repeater_title_control' => true,
        'customizer_repeater_subtitle_control' => true,
        'customizer_repeater_text_control' => true,
        'customizer_repeater_image_control' => true,
        'customizer_repeater_repeater_control' => true
    ) ) );


	$wp_customize->add_setting( 'pricing_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'pricing_show',
	   array(
            'label' => esc_html__( 'Show Section','barbershop' ),
            'section' => 'pricing_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barbershop' ),
	         'yes' => esc_html__( 'Yes','barbershop' ),
            )
	   )
	);
	// End price

    // testimonials
    $wp_customize->add_setting( 'testimonials_title',
        array(
            'default' => 'Check Out What Our Customers Say',
        )
    );
    $wp_customize->add_control( 'testimonials_title',
        array(
            'label' => esc_html__( 'Testimonials Title','barbershop' ),
            'section' => 'testimonials_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'testimonials_sub_title',
        array(
            'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.',
        )
    );
    $wp_customize->add_control( 'testimonials_sub_title',
        array(
            'label' => esc_html__( 'Sub Title','barbershop' ),
            'section' => 'testimonials_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'testimonials_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'testimonials_show',
        array(
            'label' => esc_html__( 'Show Section','barbershop' ),
            'section' => 'testimonials_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','barbershop' ),
                'yes' => esc_html__( 'Yes','barbershop' ),
            )
        )
    );
    // End Testimonials

    // start Blog
    $wp_customize->add_section( 'blog_settings',
        array(
            'title' => esc_html__( 'Blog Section','barbershop'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    $wp_customize->add_setting( 'icon_title_blog',
        array(
            'default' => get_template_directory_uri() . '/images/title-icon-2.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'icon_title_blog',
        array(
            'label' => esc_html__( 'Title icon','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'blog_settings',
        )
    ) );
    $wp_customize->add_setting( 'title_blog',
        array(
            'default' => esc_html__('Some Beautiful Stories from our Clients','barbershop'),
        )
    );
    $wp_customize->add_control('title_blog',
        array(
            'label' => esc_html__( 'Title Section','barbershop' ),
            'section' => 'blog_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_blog',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.','barbershop'),
        )
    );
    $wp_customize->add_control('sub_title_blog',
        array(
            'label' => esc_html__( 'Sub title Section','barbershop' ),
            'section' => 'blog_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting( 'number_post_blog',
        array(
            'default' => esc_html__('3','barbershop'),
        )
    );
    $wp_customize->add_control('number_post_blog',
        array(
            'label' => esc_html__( 'Number Post','barbershop' ),
            'section' => 'blog_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'blog_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'blog_show',
        array(
            'label' => esc_html__( 'Show Section','barbershop' ),
            'section' => 'blog_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','barbershop' ),
                'yes' => esc_html__( 'Yes','barbershop' ),
            )
        )
    );
    // End work

	// start Contact
	$wp_customize->add_section( 'contact_settings',
	   array(
	      'title' => esc_html__( 'Contact Section','barbershop'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

	$wp_customize->add_setting( 'contact_shortcode',
	   array(
	      'default' => '[contact-form-7 id="49" title="Contact form home"]',
	   )
	);	
	$wp_customize->add_control( 'contact_shortcode',
	   array(
	      'label' => esc_html__( 'Contact Shortcode','barbershop' ),
	      'section' => 'contact_settings',
	      'type' => 'text',
	   )
	);
    $wp_customize->add_setting( 'contact_info',
        array(
            'default' => "<h3>BarnerShop<span>Mens Club</span></h3><p class=\"address\">145 Amands Street, Beverly Hill Sights, NYC 2345<br> E-Mail: barbershop@email.com</p><p class=\"phone\">Call : (800) 0123 4567 890</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'contact_info',
        array(
            'label' => esc_html__( 'Contact info','barbershop' ),
            'description' => esc_html__( 'About content','barbershop' ),
            'section' => 'contact_settings',
        )
    ) );
    $wp_customize->add_setting( 'bg_contact',
        array(
            'default' => get_template_directory_uri() . '/images/bg-contact.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_contact',
        array(
            'label' => esc_html__( 'Background','barbershop' ),
            'description' => esc_html__( 'Upload Image','barbershop' ),
            'section' => 'contact_settings',
        )
    ) );
	$wp_customize->add_setting( 'contact_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'contact_show',
	   array(
            'label' => esc_html__( 'Show Section','barbershop' ),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','barbershop' ),
	         'yes' => esc_html__( 'Yes','barbershop' ),
            )
	   )
	);
	// End Contact
		
	// End Home Options
	
	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','barbershop'  ),
	      'priority' => 35, 
	   )
	);
    $wp_customize->add_setting( 'logo_footer',
        array(
            'default' => get_template_directory_uri() . '/images/logo-footer.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_footer',
        array(
            'label' => esc_html__( 'Logo footer','barbershop' ),
            'description' => esc_html__( 'Upload Logo','barbershop' ),
            'section' => 'footer_settings',
        )
    ) );
	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '<p class="text-center m-0">(C) All Rights Reserved BaberShop. Designed and Developed by <a href="#">Template.net</a></p>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','barbershop' ),
	      'section' => 'footer_settings',
	   )
	) );	
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','barbershop'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','barbershop' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','barbershop' ),
	         'yes' => esc_html__( 'Yes','barbershop' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','barbershop'),
		'instagram'=> esc_html__('Instagram','barbershop'),
		'twitter'=> esc_html__('Twitter','barbershop'),
		'google'=> esc_html__('Google','barbershop'),
		'linkedin'=> esc_html__('Linkedin','barbershop')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','barbershop' ),
				 'yes' => esc_html__( 'Yes','barbershop' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','barbershop'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','barbershop'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','barbershop' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','barbershop' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','barbershop' ),
	         'yes' => esc_html__( 'Yes','barbershop' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','barbershop' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','barbershop' ),
	         'yes' => esc_html__( 'Yes','barbershop' ),
	      )
	   )
	);
}
/**
 * Theme options in the Customizer js
 * @package barbershop
 */
 
function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'barbershop_customize_admin_menu_hide', 999 );
function barbershop_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="barbershop-theme-options"><span>'.esc_html__( 'barbershop','barbershop' ).'</span>'.esc_html__( 'Theme Options','barbershop' ).'</div>',
         '<div class="barbershop-theme-options"><span>'.esc_html__( 'barbershop','barbershop' ).'</span>'.esc_html__( 'Theme Options','barbershop' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}