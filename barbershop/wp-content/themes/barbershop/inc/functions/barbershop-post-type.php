<?php
/**
 * Register "Testimonials" post type
 * @return [type] [description]
 */
function register_testimonial_post_type() {

	$labels = array(
	    'name' => __('Testimonials', 'barbershop-core'),
	    'singular_name' => __('Testimonials', 'barbershop-core'),
	    'add_new' => __('Add New', 'barbershop-core'),
	    'add_new_item' => __('Add New Testimonial', 'barbershop-core'),
	    'edit_item' => __('Edit Testimonial', 'barbershop-core'),
	    'new_item' => __('New Testimonial', 'barbershop-core'),
	    'all_items' => __('All Testimonial', 'barbershop-core'),
	    'view_item' => __('View Testimonial', 'barbershop-core'),
	    'search_items' => __('Search Testimonials', 'barbershop-core'),
	    'not_found' =>  __('No Testimonials found', 'barbershop-core'),
	    'not_found_in_trash' => __('No Testimonials found in Trash', 'barbershop-core'),
	    'parent_item_colon' => '',
	    'menu_name' => __('Testimonials', 'barbershop-core')
	  );

  	$args = array(
	    'labels' => $labels,
	    'public' => false,
	    'exclude_from_search' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true, 
	    'show_in_menu' => true, 
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'testimonial' ),
	    'capability_type' => 'post',
	    'has_archive' => false, 
	    'hierarchical' => false,
	    'menu_position' => 50,
	    'menu_icon' => 'dashicons-groups',
	    'supports' => array( 'title','editor', 'thumbnail', 'revisions','page-attributes')
  	); 

	register_post_type( 'testimonial', $args);

}
add_action( 'init', 'register_testimonial_post_type');

/**
 * Customize Rug table overview tables
 */

function add_new_testimonial_table_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['image'] = __('Image', 'barbershop-core');    
    $new_columns['title'] = _x('Name', 'column name', 'barbershop-core');
    $new_columns['date'] = _x('Date', 'column name', 'barbershop-core');
 
    return $new_columns;
}
// Add to admin_init function
add_filter('manage_testimonial_posts_columns', 'add_new_testimonial_table_columns');
function manage_testimonial_table_columns($column_name, $id) {
    global $wpdb;
	$featured_img_url = get_the_post_thumbnail_url($id, 'thumbnail');
    switch ($column_name) {
	    case 'image':
			if(!empty($featured_img_url)){
				echo '<img src="'. $featured_img_url .'" width="80" >';
			}else{
				echo 'No image';
			}
	        break;
	    default:
	        break;
    } // end switch
}   
// Add to admin_init function
add_action('manage_testimonial_posts_custom_column', 'manage_testimonial_table_columns', 10,2);


function create_shortcode_testimonial($atts) {
	
		extract( shortcode_atts( array (
		  "id"    => ''
		), $atts ) );
		
        $the_query = new WP_Query(array(
			'post_type' => 'testimonial',
			'posts_per_page' => 1,
			'p' => $id,
			'post_status' => 'publish',
			'orderby'	=> 'date',
			'order'	=> 'DESC',
        ));
 
        ob_start();
        if ( $the_query->have_posts() ) : 
			while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				<?php $testimonial_fields = get_post_meta( get_the_ID(), 'testimonial_fields', true); if ( $testimonial_fields ) : ?>
					<div class="list-testimonial">
						<?php foreach ( $testimonial_fields as $field ) { ?>
							<div class="item-testimonial">
								<div class="wrap-item">
									<?php if($field['content'] != ''){ ?>
										<div class="content"><?php echo wpautop( $field['content'] ); ?></div>
									<?php } ?>
									<?php if($field['name'] != ''){ ?>
										<p class="name"><?php echo esc_attr( $field['name'] ); ?></p>
									<?php } ?>
									<?php if($field['address'] != ''){ ?>
										<p class="name"><?php echo esc_attr( $field['address'] ); ?></p>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				<?php endif; ?>				
			<?php
			endwhile;
        endif;
		wp_reset_postdata();
        $list_post = ob_get_contents();
 
        ob_end_clean();
 
        return $list_post;
}
add_shortcode('testimonial', 'create_shortcode_testimonial');

/**
 * Register "Teams" post type
 * @return [type] [description]
 */
function register_team_post_type() {

    $labels = array(
        'name' => __('Teams', 'barbershop-core'),
        'singular_name' => __('Teams', 'barbershop-core'),
        'add_new' => __('Add New', 'barbershop-core'),
        'add_new_item' => __('Add New Team', 'barbershop-core'),
        'edit_item' => __('Edit Team', 'barbershop-core'),
        'new_item' => __('New Team', 'barbershop-core'),
        'all_items' => __('All Teams', 'barbershop-core'),
        'view_item' => __('View Teams', 'barbershop-core'),
        'search_items' => __('Search Teams', 'barbershop-core'),
        'not_found' =>  __('No Teams found', 'barbershop-core'),
        'not_found_in_trash' => __('No Teams found in Trash', 'barbershop-core'),
        'parent_item_colon' => '',
        'menu_name' => __('Teams', 'barbershop-core')
      );

    $args = array(
        'labels' => $labels,
        'public' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'show_in_menu' => true, 
        'query_var' => true,
        'rewrite' => array( 'slug' => 'team' ),
        'capability_type' => 'post',
        'has_archive' => false, 
        'hierarchical' => false,
        'menu_position' => 50,
        'menu_icon' => 'dashicons-format-gallery',
        'supports' => array( 'title','editor', 'thumbnail', 'revisions','page-attributes')
    ); 

    register_post_type( 'team', $args);

}
add_action( 'init', 'register_team_post_type');

/**
 * Customize Rug table overview tables
 */
function add_new_team_table_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['image'] = __('Image', 'barbershop-core');    
    $new_columns['title'] = _x('Name', 'column name', 'barbershop-core');
    $new_columns['date'] = _x('Date', 'column name', 'barbershop-core');
 
    return $new_columns;
}
// Add to admin_init function
add_filter('manage_team_posts_columns', 'add_new_team_table_columns');
function manage_team_table_columns($column_name, $id) {
    global $wpdb;
    $featured_img_url = get_the_post_thumbnail_url($id, 'thumbnail');
    switch ($column_name) {
        case 'image':
            if(!empty($featured_img_url)){
                echo '<img src="'. $featured_img_url .'" width="80" >';
            }else{
                echo 'No image';
            }
            break;
        default:
            break;
    } // end switch
}   
// Add to admin_init function
add_action('manage_team_posts_custom_column', 'manage_team_table_columns', 10,2);

/**
 * Register meta box team
 */
function team_meta_box(){
    add_meta_box( 'meta-box-team', 'Team info', 'team_meta_display', 'team', 'side', 'low' );
}
add_action( 'add_meta_boxes', 'team_meta_box' );
/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
 
function team_meta_display(){
    global $post ;
    $team_name = get_post_meta( $post->ID, 'team_name', true );
    $team_position = get_post_meta( $post->ID, 'team_position', true );
    wp_nonce_field( 'save_team', 'team_nonce' );
    ?>
    <div class="team-info-wrapper">
        <div class="single-field-wrap">
            <h4><span class="section-title"><?php _e( 'Position', 'barbershop-core' );?></span></h4>
            <span class="section-inputfield"><input style="width:100%;" type="text" name="team_position" value="<?php if( !empty( $team_position ) ){ echo $team_position ; }?>" /></span>
        </div>
    </div>
<?php
}
/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function team_save_meta_box( $post_id ) {

    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['team_nonce'] ) || !wp_verify_nonce( $_POST['team_nonce'], 'save_team' ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    if( isset( $_POST['team_position'] ) ){
        update_post_meta( $post_id, 'team_position',  $_POST['team_position'] );
    }else{
        delete_post_meta( $post_id, 'team_position' );
    }
}
add_action( 'save_post', 'team_save_meta_box' );