<?php
/**
 * Render contact in home page
 */

//Get config
$contact_show =  get_theme_mod('contact_show','yes');
$bg_contact = get_theme_mod('bg_contact',get_template_directory_uri() . '/images/bg-contact.jpg');
$contact_info = get_theme_mod('contact_info','<h3>BarnerShop<span>Mens Club</span></h3><p class="address">145 Amands Street, Beverly Hill Sights, NYC 2345<br> E-Mail: barbershop@email.com</p><p class="phone">Call : (800) 0123 4567 890</p>');
$contact_shortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="49" title="Contact form home"]');
?>
<?php if($contact_show === 'yes'): ?>
<div id="session-contact" class="section">
    <div class="bg-cover" <?php if(!empty($bg_contact)){ ?> style="background-image:url(<?php echo $bg_contact; ?>);" <?php } ?>></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-1">
                <?php if($contact_shortcode != ''): ?>
                    <?php echo do_shortcode($contact_shortcode); ?>
                <?php endif; ?>
                <?php if($contact_info != ''): ?>
                <div class="contact-info text-center text-white">
                    <?php echo apply_filters( 'the_content', $contact_info); ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>