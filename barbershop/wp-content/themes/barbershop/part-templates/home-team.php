<?php
/**
 * Render team in home page
 */

//Get config
$on_team_show =  get_theme_mod('on_team_show','yes');
$icon_title_team = get_theme_mod('icon_title_team',get_template_directory_uri() . '/images/title-icon-1.png');
$title_team = get_theme_mod('title_team','Our Expert Team of Trimming & Dressing');
$sub_title_team = get_theme_mod('sub_title_team','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.');
?>
<?php if($on_team_show === 'yes'): ?>
<div id="session-team" class="section">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-1 text-center">
                <?php if(!empty($icon_title_team)){ ?>
                    <div class="session-icon"><img src="<?php echo $icon_title_team; ?>" alt="<?php echo $title_team; ?>"></div>
                <?php } ?>
                <?php  if($title_team !=''){ ?>
                    <h2 class="session-title"><?php echo $title_team; ?></h2>
                <?php } ?>
                <?php  if($sub_title_team !=''){ ?>
                    <p class="session-desc"><?php echo $sub_title_team; ?></p>
                <?php } ?>
                <?php
                $the_query = new WP_Query(array(
                    'post_type' => 'team',
                    'posts_per_page' => 3,
                    'post_status' => 'publish',
                    'orderby'	=> 'date',
                    'order'	=> 'DESC',
                ));
                if ( $the_query->have_posts() ) :  $i = 1;?>
                <div class="row">
                    <?php
                        while ( $the_query->have_posts() ) : $the_query->the_post();
                        $class = '';
                        switch ($i%3) {
                            case "1":
                                $class = 'pr-xl-5';
                                break;
                            case "2":
                                $class = 'px-xl-5';
                                break;
                            case "0":
                                $class = 'pl-xl-5 pr-xl-0';
                                break;
                            default:
                                $class = '';
                        }
                    ?>
                    <div class="col-lg <?php echo $class; ?>">
                        <div class="team my-sm-5 my-3">
                            <?php if ( has_post_thumbnail() ) : ?>
                            <div class="team-image">
                                <?php the_post_thumbnail('team'); ?>
                            </div>
                            <?php endif; ?>
                            <div class="team-info">
                                <h3><?php the_title() ?></h3>
                                <?php
                                    $positon = get_post_meta(get_the_ID(),'team_position', true);
                                    if(! empty( $positon)):
                                ?>
                                <h5>(<?php echo $positon ;?>)</h5>
                                <?php endif; ?>
                                <p><?php echo wp_trim_words( get_the_excerpt(), $num_words = 23, '' ) ?></p>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Read More...</a>
                            </div>
                        </div>
                    </div>
                    <?php $i++;endwhile; ?>
                </div>
                <?php
                endif;
                wp_reset_postdata();?>
            </div>
        </div>
    </div>
</div>
<?php endif;