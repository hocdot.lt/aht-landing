<?php
/**
 * Render blog in home page
 */

//Get config
$blog_show =  get_theme_mod('blog_show','yes');
$number_post_blog = get_theme_mod('number_post_blog','3');
$title_blog = get_theme_mod('title_blog','Some Beautiful Stories from our Clients');
$sub_title_blog = get_theme_mod("sub_title_blog","Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.");
$icon_title_blog = get_theme_mod('icon_title_blog',get_template_directory_uri() . '/images/title-icon-2.png');
?>
<?php if($blog_show === 'yes'): ?>
<div id="session-blog" class="section">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-1 text-center">
                <?php if(!empty($icon_title_blog)){ ?>
                <div class="session-icon"><img src="<?php echo $icon_title_blog; ?>" alt="<?php echo $title_blog; ?>"></div>
                <?php } ?>
                <?php  if($title_blog !=''){ ?>
                <h2 class="session-title"><?php echo $title_blog; ?></h2>
                <?php } ?>
                <?php  if($sub_title_blog !=''){ ?>
                <p class="session-desc"><?php echo $sub_title_blog; ?></p>
                <?php } ?>
            </div>
        </div>
        <?php if(!empty($number_post_blog)){ ?>
        <div class="row">
            <?php
            $the_query = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => $number_post_blog,
                'post_status' => 'publish',
                'orderby'	=> 'date',
                'order'	=> 'DESC',
            ));
            ?>
            <?php if ( $the_query->have_posts() ) : $i = 1;
                while ( $the_query->have_posts() ) : $the_query->the_post();
                    $class = '';
                    switch ($i%3) {
                        case "1":
                            $class = 'pr-xl-5';
                            break;
                        case "2":
                            $class = 'px-xl-5';
                            break;
                        case "0":
                            $class = 'pl-xl-5';
                            break;
                        default:
                            $class = '';
                    }
            ?>
            <div class="col-lg <?php echo $class; ?>">
                <div class="post mb-4 text-center">
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="post-image">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute('blog'); ?>">
                            <?php the_post_thumbnail('blog'); ?>
                        </a>
                    </div>
                    <?php endif; ?>
                    <div class="post-content">
                        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                        <div class="post-meta">
                            <p>
                                <u><?php echo get_the_date('F j, Y'); ?></u>
                            </p>
                            <p>Writtn by <?php the_author(); ?></p>
                        </div>
                    </div>
                </div>
            </div>
                <?php $i++;endwhile;
            endif;
            wp_reset_postdata(); ?>
        </div>
        <?php } ?>
    </div>
</div>
<?php endif; ?>