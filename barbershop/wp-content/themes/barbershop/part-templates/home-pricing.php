<?php
/**
 * Render prices in home page
 */

//Get config
$pricing_show =  get_theme_mod('pricing_show','yes');
$pricing_title = get_theme_mod('pricing_title','Check out our <br> Price List <br> New year offer,<br> 30% on Haircut and Trimming');
$pricing_sub_title = get_theme_mod('pricing_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore...');
$img_pricing = get_theme_mod('img_pricing',get_template_directory_uri() . '/images/price.jpg');
$customizer_repeater_pricing = get_theme_mod("customizer_repeater_pricing");
?>
<?php if($pricing_show === 'yes'): ?>
<div id="session-pricing" class="section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-left d-flex align-self-stretch align-items-center" <?php if(!empty($img_pricing)){ ?> style="background-image:url(<?php echo $img_pricing; ?>);" <?php } ?>>
                <div class="content-wrap">
                    <?php  if($pricing_title !=''){ ?>
                    <h2 class="session-title"><?php echo $pricing_title; ?></h2>
                    <?php } ?>
                    <?php  if($pricing_sub_title !=''){ ?>
                    <p class="session-desc"><?php echo $pricing_sub_title; ?></p>
                    <?php } ?>
                </div>
            </div>
            <div class="col-right">
                <div class="pricing-wrap">
                    <?php if(!empty($customizer_repeater_pricing)): ?>
                    <ul class="pricing-list">
                        <?php
                        $customizer_repeater_example_decoded = json_decode($customizer_repeater_pricing);
                        foreach( $customizer_repeater_example_decoded as $item ):
                        ?>
                        <li class="d-flex justify-content-between">
                            <div class="title">
                                <h4><?php echo $item->title;?></h4><span><?php echo $item->text;?></span>
                            </div>
                            <div class="price"><?php echo $item->subtitle;?></div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>