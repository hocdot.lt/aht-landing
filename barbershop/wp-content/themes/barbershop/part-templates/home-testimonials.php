<?php
/**
 * Render testimonials in home page
 */

//Get config
$testimonials_show =  get_theme_mod('testimonials_show','yes');
$testimonials_title = get_theme_mod('testimonials_title','Check Out What Our Customers Say');
$testimonials_sub_title = get_theme_mod('testimonials_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.');
?>
<?php if($testimonials_show === 'yes'): ?>
<div id="section-testimonials" class="section">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-1 text-center">
                <?php  if($testimonials_title !=''){ ?>
                <h2 class="session-title"><?php echo $testimonials_title; ?></h2>
                <?php } ?>
                <?php  if($testimonials_sub_title !=''){ ?>
                <p class="session-desc"><?php echo $testimonials_sub_title; ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-8">
                <?php
                $the_query = new WP_Query(array(
                    'post_type' => 'testimonial',
                    'posts_per_page' => 5,
                    'post_status' => 'publish',
                    'orderby'	=> 'date',
                    'order'	=> 'DESC',
                ));
                if ( $the_query->have_posts() ) : ?>
                <div class="testimonial-slider">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="item-testimonial">
                        <div class="avatar rounded-circle d-flex mx-auto mb-4">
                            <?php the_post_thumbnail('thumbnail'); ?>
                        </div>
                        <div class="desc">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php
                endif;
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>