<?php
/**
 * Render about-us in home page
 */

//Get config
$aboutShow =  get_theme_mod('about_show','yes');
$title_about = get_theme_mod('title_about','Best Haircut and Shaving studio in Town');
$sub_title_about = get_theme_mod('sub_title_about','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.');
$about_image_1 = get_theme_mod('about_image_1',get_template_directory_uri() . '/images/about-1.jpg');
$about_image_2 = get_theme_mod('about_image_2',get_template_directory_uri() . '/images/about-2.jpg');

$about_content_1 = get_theme_mod('about_content_1','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Tempor incididunt ut labore et dolore magna aliqua...</p>');
$about_content_2 = get_theme_mod('about_content_2','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Tempor incididunt ut labore et dolore magna aliqua...</p>');
$about_content_3 = get_theme_mod('about_content_3','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod Tempor incididunt ut labore et dolore magna aliqua...</p>');
?>
<?php if($aboutShow === 'yes'): ?>
<div id="session-about" class="section">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-1 text-center">
                <?php if(!empty($title_about)){ ?>
                <h2 class="session-title"><?php echo $title_about; ?></h2>
                <?php } ?>
                 <?php if(!empty($sub_title_about)){ ?>
                <p class="session-desc"><?php echo $sub_title_about; ?></p>
                 <?php } ?>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php if(!empty($about_image_1)){ ?>
            <div class="col-xl-3 col-md-4 mt-4 mb-5 pl-xl-1">
                <img class="mx-xl-0 mx-auto d-block" src="<?php echo $about_image_1; ?>" alt="<?php echo $title_about; ?>" >
            </div>
            <?php } ?>
            <?php if(!empty($about_image_2)){ ?>
            <div class="col-xl-3 col-md-4 mt-4 mb-5 pr-xl-1">
                <img class="mx-xl-0 mx-auto d-block float-md-right" src="<?php echo $about_image_2; ?>" alt="<?php echo $title_about; ?>" >
            </div>
            <?php } ?>
        </div>
        <div class="row">
            <?php if(!empty($about_content_1)){ ?>
            <div class="col-lg my-lg-5 pr-xl-4rem">
                <?php echo apply_filters( 'the_content', $about_content_1); ?>
            </div>
            <?php } ?>
            <?php if(!empty($about_content_1)){ ?>
            <div class="col-lg my-lg-5 px-xl-4rem">
                <?php echo apply_filters( 'the_content', $about_content_2); ?>
            </div>
            <?php } ?>
            <?php if(!empty($about_content_3)){ ?>
            <div class="col-lg my-lg-5 pl-xl-4rem">
                <?php echo apply_filters( 'the_content', $about_content_3); ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php endif; ?>