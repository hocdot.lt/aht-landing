<?php
/**
 * Render banner in home page
 */

//Get config
$banner_show =  get_theme_mod('banner_show','yes');
$banner_title = get_theme_mod('banner_title','Finest Men Haircuts In Dubai');
$banner_sub_title = get_theme_mod('banner_sub_title','Looking for the best barber shop Dubai has to offer? From beard trims to haircuts, check out these top barbershop experiences.');
$button_banner = get_theme_mod('button_banner','Check Pricing');
$link_banner = get_theme_mod('link_banner','#');
$background_banner = get_theme_mod('background_banner',get_template_directory_uri() . '/images/banner.jpg');

$service_title_1 = get_theme_mod('service_title_1','Hair Cut');
$service_content_1 = get_theme_mod('service_content_1','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
$service_link_1 = get_theme_mod('service_link_1','#');
$service_icon_1 = get_theme_mod('service_icon_1',get_template_directory_uri() . '/images/service-1.png');

$service_title_2 = get_theme_mod('service_title_2','Shaving');
$service_content_2 = get_theme_mod('service_content_2','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
$service_link_2 = get_theme_mod('service_link_2','#');
$service_icon_2 = get_theme_mod('service_icon_2',get_template_directory_uri() . '/images/service-2.png');

$service_title_3 = get_theme_mod('service_title_3','Styling');
$service_content_3 = get_theme_mod('service_content_3','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
$service_link_3 = get_theme_mod('service_link_3','#');
$service_icon_3 = get_theme_mod('service_icon_3',get_template_directory_uri() . '/images/service-3.png');

$service_title_4 = get_theme_mod('service_title_4','Trimming');
$service_content_4 = get_theme_mod('service_content_4','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
$service_link_4 = get_theme_mod('service_link_4','#');
$service_icon_4 = get_theme_mod('service_icon_4',get_template_directory_uri() . '/images/service-4.png');
?>
<?php if($banner_show === 'yes'): ?>
<div id="session-banner" class="section">
    <div class="bg-cover" <?php if(!empty($background_banner)){ ?> style="background-image:url(<?php echo $background_banner; ?>);" <?php } ?>></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-1 text-center caption-banner">
                <?php if(!empty($banner_title)){ ?>
                    <h2><?php echo $banner_title; ?></h2>
                <?php } ?>
                <?php if(!empty($banner_sub_title)){ ?>
                    <p class="sub-title"><?php echo $banner_sub_title; ?></p>
                <?php } ?>
                <?php if(!empty($link_banner) || !empty($button_banner)){ ?>
                    <a class="btn btn-banner my-xl-5 my-3" href="<?php echo $link_banner; ?>"><?php echo $button_banner; ?></a>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <?php if(!empty($service_title_1) || !empty($service_content_1) || !empty($service_link_1) || !empty($service_icon_1)){ ?>
            <div class="col-lg-2 col-4">
                <div class="service text-center">
                    <div class="thumb bg-white rounded-circle d-flex mx-auto">
                        <?php if(!empty($service_icon_1)){ ?>
                            <img class="align-self-center m-auto" src="<?php echo $service_icon_1; ?>" alt="<?php echo $service_title_1; ?>">
                        <?php } ?>
                    </div>
                    <div class="wrapp-content">
                        <?php if(!empty($service_title_1)){ ?>
                            <h3><?php echo $service_title_1; ?></h3>
                        <?php } ?>
                        <?php if(!empty($service_content_1)){ ?>
                            <p><?php echo $service_content_1; ?></p>
                        <?php } ?>
                        <?php if(!empty($service_link_1)){ ?>
                            <a class="btn btn-service my-lg-4 mb-5" href="<?php echo $service_link_1; ?>">Read More</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if(!empty($service_title_2) || !empty($service_content_2) || !empty($service_link_2) || !empty($service_icon_2)){ ?>
                <div class="col-lg-2 col-4">
                    <div class="service text-center">
                        <div class="thumb bg-white rounded-circle d-flex mx-auto">
                            <?php if(!empty($service_icon_2)){ ?>
                                <img class="align-self-center m-auto" src="<?php echo $service_icon_2; ?>" alt="<?php echo $service_title_2; ?>">
                            <?php } ?>
                        </div>
                        <div class="wrapp-content">
                            <?php if(!empty($service_title_2)){ ?>
                                <h3><?php echo $service_title_2; ?></h3>
                            <?php } ?>
                            <?php if(!empty($service_content_2)){ ?>
                                <p><?php echo $service_content_2; ?></p>
                            <?php } ?>
                            <?php if(!empty($service_link_2)){ ?>
                                <a class="btn btn-service my-lg-4 mb-5" href="<?php echo $service_link_2; ?>">Read More</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if(!empty($service_title_3) || !empty($service_content_3) || !empty($service_link_3) || !empty($service_icon_3)){ ?>
                <div class="col-lg-2 col-4">
                    <div class="service text-center">
                        <div class="thumb bg-white rounded-circle d-flex mx-auto">
                            <?php if(!empty($service_icon_3)){ ?>
                                <img class="align-self-center m-auto" src="<?php echo $service_icon_3; ?>" alt="<?php echo $service_title_3; ?>">
                            <?php } ?>
                        </div>
                        <div class="wrapp-content">
                            <?php if(!empty($service_title_3)){ ?>
                                <h3><?php echo $service_title_3; ?></h3>
                            <?php } ?>
                            <?php if(!empty($service_content_3)){ ?>
                                <p><?php echo $service_content_3; ?></p>
                            <?php } ?>
                            <?php if(!empty($service_link_3)){ ?>
                                <a class="btn btn-service my-lg-4 mb-5" href="<?php echo $service_link_3; ?>">Read More</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if(!empty($service_title_4) || !empty($service_content_4) || !empty($service_link_4) || !empty($service_icon_4)){ ?>
                <div class="col-lg-2 col-4">
                    <div class="service text-center">
                        <div class="thumb bg-white rounded-circle d-flex mx-auto">
                            <?php if(!empty($service_icon_4)){ ?>
                                <img class="align-self-center m-auto" src="<?php echo $service_icon_4; ?>" alt="<?php echo $service_title_4; ?>">
                            <?php } ?>
                        </div>
                        <div class="wrapp-content">
                            <?php if(!empty($service_title_4)){ ?>
                                <h3><?php echo $service_title_4; ?></h3>
                            <?php } ?>
                            <?php if(!empty($service_content_4)){ ?>
                                <p><?php echo $service_content_4; ?></p>
                            <?php } ?>
                            <?php if(!empty($service_link_4)){ ?>
                                <a class="btn btn-service my-lg-4 mb-5" href="<?php echo $service_link_4; ?>">Read More</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php endif; ?>