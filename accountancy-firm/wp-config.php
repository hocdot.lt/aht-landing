<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'accountancy_firm' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!@i6txR*KfmW6.Qb9C<3)sPk/n29KnMA0ZlPG^VG$K1MMP?/9Ty&pJmg4?#;DY{3' );
define( 'SECURE_AUTH_KEY',  'eZgF2<e(41+ fQW<ts%Evw>-HWZbz0re(X]%&g9nl#A<}WK-u~dBTVn)S{4~kPUe' );
define( 'LOGGED_IN_KEY',    'S-+E.P ZVB7(t{*vWT$~%1z7vo[/[:BzQlynP[J>jzs3uLa4Jq.o&?b]v0pE:G}A' );
define( 'NONCE_KEY',        '7d1J3{Gb%l!=c7er2f+b~Y-oc_aX-XL4h_EGoQsH!+ni]4<*c=FW*jzu==h_`uVX' );
define( 'AUTH_SALT',        '9-2ht8-V$ 0FsqZ,3aHQ~[4`h cI}b.8.Re;fy}2FR_tKTtEs3FN7Bb{Log&=bWP' );
define( 'SECURE_AUTH_SALT', '*{>Xk}iS$Or|750&w$Yg%%FBP9OAah*Pr~EFD,<s&(/.%!W;z&53a73U*51mGK&h' );
define( 'LOGGED_IN_SALT',   'B*E^dku^b|}G_?Wh_^2d<. R&x,&x:`q]iC3]&ls3~DG}#%JG[s!YU!a9cJj:o #' );
define( 'NONCE_SALT',       '1Sb0M21)|_i.3ICs:9t%*)bv]?IDf%i~lWvMQgQ8bf1Uo/V;@~q eAW4mN~!S5zY' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
