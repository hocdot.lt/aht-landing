<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'accountancy_firm_customize_register' );
function accountancy_firm_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'accountancy_firm_Customize_Sidebar_Control' ) ) {
	    class accountancy_firm_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Baloo Tamma' => 'Baloo Tamma',
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',
                'Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Poppins',
                'Arial:100,200,300,400' => 'Arial',
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',			
				'' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'accountancy_firm') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','accountancy_firm'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','accountancy_firm' ),
	      'description' => esc_html__( 'Upload Logo','accountancy_firm' ),
	      'section' => 'general_settings',
	   )
	) );
	
	$wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Arial:100,200,300,400',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Font Main',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'accountancy_firm'),
        'description'    => __('Several settings pertaining my theme', 'accountancy_firm'),
    ) );
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','accountancy_firm'  ),
	      'priority' => 20,
		  'panel'  => 'home_page_setting',
	   )
	);
	// title banner
    $wp_customize->add_setting( 'banner_title',
        array(
            'default' => esc_html__('Delivering Sustainable Growth & Profitability For Businesses'),
        )
    );
    $wp_customize->add_control( 'banner_title',
        array(
            'label' => esc_html__( 'Title Banner','accountancy_firm' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
	// sub title banner
	$wp_customize->add_setting( 'banner_sub_title',
	   array(
	      'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
	   )
	);
	$wp_customize->add_control( 'banner_sub_title',
	   array(
	      'label' => esc_html__( 'Sub Title Banner','accountancy_firm' ),
	      'section' => 'home_banner_settings',
	      'type' => 'textarea',
	   )
	);
    // desc title banner
    $wp_customize->add_setting( 'banner_desc_title',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
        )
    );
    $wp_customize->add_control( 'banner_desc_title',
        array(
            'label' => esc_html__( 'Desc Title Banner','accountancy_firm' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );
	// background banner
	$wp_customize->add_setting( 'background_banner',
		array(
			'default' => get_template_directory_uri() . '/images/img-banner.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_banner',
	   array(
	      'label' => esc_html__( 'Background Banner Right','accountancy_firm' ),
	      'description' => esc_html__( 'Upload Image','accountancy_firm' ),
	      'section' => 'home_banner_settings',
	   )
	) );
    //banner shortcode
    $wp_customize->add_setting( 'banner_shortcode',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control( 'banner_shortcode',
        array(
            'label' => esc_html__( 'Banner shortcode','accountancy_firm' ),
            'section' => 'home_banner_settings',
            'type' => 'text'
        )
    );

	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','accountancy_firm' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array(
	         'no' => esc_html__( 'No','accountancy_firm' ),
	         'yes' => esc_html__( 'Yes','accountancy_firm' ),
            )
	   )
	);
	// End banner section




    //services
    $wp_customize->add_section( 'services_settings',
        array(
            'title' => esc_html__( 'Services section','accountancy_firm'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    //subTitle
    $wp_customize->add_setting( 'service_sub_title',
        array(
            'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
        )
    );
    $wp_customize->add_control( 'service_sub_title',
        array(
            'label' => esc_html__( 'Title Sub','accountancy_firm' ),
            'section' => 'services_settings',
            'type' => 'text'
        )
    );
    //Title
    $wp_customize->add_setting( 'service_title',
        array(
            'default' => 'Low Cost Accountancy Services or All Your Business Needs',
        )
    );
    $wp_customize->add_control( 'service_title',
        array(
            'label' => esc_html__( 'Title','accountancy_firm' ),
            'section' => 'services_settings',
            'type' => 'text'
        )
    );
    //link
    $wp_customize->add_setting( 'link_service',
        array(
            'default' => '#',
        )
    );
    $wp_customize->add_control( 'link_service',
        array(
            'label' => esc_html__( 'Link All Services','accountancy_firm' ),
            'section' => 'services_settings',
            'type' => 'text'
        )
    );
    //  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $cats[""] = "Select a category";
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

    $wp_customize->add_setting( 'Service_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('Service_category',
        array(
            'label' => esc_html__( 'Category Post','accountancy_firm' ),
            'section' => 'services_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

    //Number post
    $wp_customize->add_setting( 'service_number_post',
        array(
            'default' => esc_html__('4','accountancy_firm'),
        )
    );
    $wp_customize->add_control('service_number_post',
        array(
            'label' => esc_html__( 'Number Post','accountancy_firm' ),
            'section' => 'services_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'show_hide_post',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'show_hide_post',
        array(
            'label' => esc_html__( 'Show hide post','accountancy_firm' ),
            'section' => 'services_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','accountancy_firm' ),
                'yes' => esc_html__( 'Yes','accountancy_firm' ),
            )
        )
    );
    //Show/Hide session
    $wp_customize->add_setting( 'services_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'services_show',
        array(
            'label' => esc_html__( 'Show Section','accountancy_firm' ),
            'section' => 'services_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','accountancy_firm' ),
                'yes' => esc_html__( 'Yes','accountancy_firm' ),
            )
        )
    );

    //End serice
    //whychose
    $wp_customize->add_section( 'whychose_settings',
        array(
            'title' => esc_html__( 'whychose section','accountancy_firm'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    //img whychose
    $wp_customize->add_setting( 'img_whychose',
        array(
            'default' => get_template_directory_uri() . '/images/images1.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_whychose',
        array(
            'label' => esc_html__( 'Images Whychose','accountancy_firm' ),
            'description' => esc_html__( 'Upload Images','accountancy_firm' ),
            'section' => 'whychose_settings',
        )
    ) );
    //sub Tile
    $wp_customize->add_setting( 'whychose_sub_title',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt et dolore magna aliqua.aliqua.','accountancy_firm'),
        )
    );
    $wp_customize->add_control('whychose_sub_title',
        array(
            'label' => esc_html__( 'Sub Title Section','accountancy_firm' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    );
    //Tile
    $wp_customize->add_setting( 'whychose_title',
        array(
            'default' => esc_html__('Why Chose Us?','accountancy_firm'),
        )
    );
    $wp_customize->add_control('whychose_title',
        array(
            'label' => esc_html__( 'Title Section','accountancy_firm' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    );

    //logo thuong hieu
    //logo 1
    $wp_customize->add_setting( 'logo1',
        array(
            'default' => get_template_directory_uri() . '/images/logo1.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo1',
        array(
            'label' => esc_html__( 'Logo 1','accountancy_firm' ),
            'description' => esc_html__( 'Upload Images','accountancy_firm' ),
            'section' => 'whychose_settings',
        )
    ) );
    //link logo 1
    $wp_customize->add_setting( 'link_1',
        array(
            'default' => esc_html__('#','accountancy_firm'),
        )
    );
    $wp_customize->add_control('link_1',
        array(
            'label' => esc_html__( 'Link logo 1','accountancy_firm' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    ); //logo 1
    $wp_customize->add_setting( 'logo2',
        array(
            'default' => get_template_directory_uri() . '/images/logo2.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo2',
        array(
            'label' => esc_html__( 'Logo 2','accountancy_firm' ),
            'description' => esc_html__( 'Upload Images','accountancy_firm' ),
            'section' => 'whychose_settings',
        )
    ) );
    //link logo 1
    $wp_customize->add_setting( 'link_2',
        array(
            'default' => esc_html__('#','accountancy_firm'),
        )
    );
    $wp_customize->add_control('link_2',
        array(
            'label' => esc_html__( 'link logo 2','accountancy_firm' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    ); //logo 1
    $wp_customize->add_setting( 'logo3',
        array(
            'default' => get_template_directory_uri() . '/images/logo3.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo3',
        array(
            'label' => esc_html__( 'Logo 3','accountancy_firm' ),
            'description' => esc_html__( 'Upload Images','accountancy_firm' ),
            'section' => 'whychose_settings',
        )
    ) );
    //link logo 1
    $wp_customize->add_setting( 'link_3',
        array(
            'default' => esc_html__('#','accountancy_firm'),
        )
    );
    $wp_customize->add_control('link_3',
        array(
            'label' => esc_html__( 'link logo 3','accountancy_firm' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    ); //logo 1
    $wp_customize->add_setting( 'logo4',
        array(
            'default' => get_template_directory_uri() . '/images/logo4.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo4',
        array(
            'label' => esc_html__( 'Logo 4','accountancy_firm' ),
            'description' => esc_html__( 'Upload Images','accountancy_firm' ),
            'section' => 'whychose_settings',
        )
    ) );
    //link logo 1
    $wp_customize->add_setting( 'link_4',
        array(
            'default' => esc_html__('#','accountancy_firm'),
        )
    );
    $wp_customize->add_control('link_4',
        array(
            'label' => esc_html__( 'Link logo 5','accountancy_firm' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    );
    //logo 5
    $wp_customize->add_setting( 'logo5',
        array(
            'default' => get_template_directory_uri() . '/images/logo5.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo1',
        array(
            'label' => esc_html__( 'Logo 5','accountancy_firm' ),
            'description' => esc_html__( 'Upload Images','accountancy_firm' ),
            'section' => 'whychose_settings',
        )
    ) );
    //link logo 1
    $wp_customize->add_setting( 'link_5',
        array(
            'default' => esc_html__('#','accountancy_firm'),
        )
    );
    $wp_customize->add_control('link_5',
        array(
            'label' => esc_html__( 'Link logo 5','accountancy_firm' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'customizer_repeater_whychose', array(
        'sanitize_callback' => 'customizer_repeater_sanitize'
    ));
    $wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'customizer_repeater_whychose', array(
        'label'   => esc_html__('Why Chose Details','customizer-repeater'),
        'section' => 'whychose_settings',
        'customizer_repeater_title_control' => true,
        'customizer_repeater_subtitle_control' => true,
        'customizer_repeater_repeater_control' => true
    ) ) );
    //Show/Hide session
    $wp_customize->add_setting( 'whychose_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'whychose_show',
        array(
            'label' => esc_html__( 'Show Section','accountancy_firm' ),
            'section' => 'whychose_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','accountancy_firm' ),
                'yes' => esc_html__( 'Yes','accountancy_firm' ),
            )
        )
    );
    //end whychose
    //blog
    $wp_customize->add_section( 'blog_settings',
        array(
            'title' => esc_html__( 'Blog section','accountancy_firm'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    //img blog
    $wp_customize->add_setting( 'bg_blog',
        array(
            'default' => get_template_directory_uri() . '/images/banner_post.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_blog',
        array(
            'label' => esc_html__( 'Background Blog','accountancy_firm' ),
            'description' => esc_html__( 'Upload Images','accountancy_firm' ),
            'section' => 'blog_settings',
        )
    ) );

    //Tile
    $wp_customize->add_setting( 'blog_title',
        array(
            'default' => esc_html__('Read Latest Blog Posts','accountancy_firm'),
        )
    );
    $wp_customize->add_control('blog_title',
        array(
            'label' => esc_html__( 'Title Section','accountancy_firm' ),
            'section' => 'blog_settings',
            'type' => 'text',
        )
    );

    //  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

    $wp_customize->add_setting( 'blog_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('blog_category',
        array(
            'label' => esc_html__( 'Category Post','accountancy_firm' ),
            'section' => 'blog_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );

    //Number post
    $wp_customize->add_setting( 'blog_number_post',
        array(
            'default' => esc_html__('4','accountancy_firm'),
        )
    );
    $wp_customize->add_control('blog_number_post',
        array(
            'label' => esc_html__( 'Number Post','accountancy_firm' ),
            'section' => 'blog_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'show_hide_post',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'show_hide_post',
        array(
            'label' => esc_html__( 'Show hide post','accountancy_firm' ),
            'section' => 'blog_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','accountancy_firm' ),
                'yes' => esc_html__( 'Yes','accountancy_firm' ),
            )
        )
    );

    //Show/Hide session
    $wp_customize->add_setting( 'blog_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'blog_show',
        array(
            'label' => esc_html__( 'Show Section','accountancy_firm' ),
            'section' => 'blog_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','accountancy_firm' ),
                'yes' => esc_html__( 'Yes','accountancy_firm' ),
            )
        )
    );

    //End blog


    //contact Session
    $wp_customize->add_section( 'contact_settings',
        array(
            'title' => esc_html__( 'Contact section','accountancy_firm'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    //img whychose
    $wp_customize->add_setting( 'logo_contact',
        array(
            'default' => get_template_directory_uri() . '/images/logo-contact.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_contact',
        array(
            'label' => esc_html__( 'LOgo Contact','accountancy_firm' ),
            'description' => esc_html__( 'Upload Images','accountancy_firm' ),
            'section' => 'contact_settings',
        )
    ) );

//Contact shortcode
    $wp_customize->add_setting( 'contact_shortcode',
        array(
            'default' => '',
        )
    );
    $wp_customize->add_control( 'contact_shortcode',
        array(
            'label' => esc_html__( 'Contact shortcode','accountancy_firm' ),
            'section' => 'contact_settings',
            'type' => 'text'
        )
    );
    $wp_customize->add_setting( 'desc_contact',
        array(
            'default' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vitae semper quis lectus nulla at. Ipsum a arcu cursus vitae. penatibus et magnis dis parturient montes nascetur. Lorem ipsum dolor sit amet elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#">info@accountancyfirm.com</a>
                            <p>Accountancy FIrm LLC</p>
                            <p>85 Broad Street, 28th Floor</p>
                            <p>New York, NY 10004</p>
                            <p>800 - 123 - 4567</p>',
            'sanitize_callback' => 'wp_kses_post',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'desc_contact',
        array(
            'label' => esc_html__( 'info contact','accountancy_firm' ),
            'section' => 'contact_settings',
        )
    ) );
    //Show/Hide session
    $wp_customize->add_setting( 'contact_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'contact_show',
        array(
            'label' => esc_html__( 'Show Section','accountancy_firm' ),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','accountancy_firm' ),
                'yes' => esc_html__( 'Yes','accountancy_firm' ),
            )
        )
    );

    //End Donate


	// End Home Options
	
	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','accountancy_firm'  ),
	      'priority' => 35, 
	   )
	);

	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '(C) 2018. AllRights Reserved. Divine Makeup Artist. Designed by <a href="#">Template.net</a>',
			'sanitize_callback' => 'wp_kses_post',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','accountancy_firm' ),
	      'section' => 'footer_settings',
	   )
	) );

	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','accountancy_firm'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','accountancy_firm' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','accountancy_firm' ),
	         'yes' => esc_html__( 'Yes','accountancy_firm' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','accountancy_firm'),
		'instagram'=> esc_html__('Instagram','accountancy_firm'),
		'twitter'=> esc_html__('Twitter','accountancy_firm'),
		'google'=> esc_html__('Google','accountancy_firm'),
		'linkedin'=> esc_html__('Linkedin','accountancy_firm')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','accountancy_firm' ),
				 'yes' => esc_html__( 'Yes','accountancy_firm' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','accountancy_firm'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','accountancy_firm'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','accountancy_firm' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','accountancy_firm' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','accountancy_firm' ),
	         'yes' => esc_html__( 'Yes','accountancy_firm' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','accountancy_firm' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','accountancy_firm' ),
	         'yes' => esc_html__( 'Yes','accountancy_firm' ),
	      )
	   )
	);
}




















/**
 * Theme options in the Customizer js
 * @package accountancy_firm
 */

function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'accountancy_firm_customize_admin_menu_hide', 999 );
function accountancy_firm_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="accountancy_firm-theme-options"><span>'.esc_html__( 'accountancy_firm','accountancy_firm' ).'</span>'.esc_html__( 'Theme Options','accountancy_firm' ).'</div>',
         '<div class="accountancy_firm-theme-options"><span>'.esc_html__( 'accountancy_firm','accountancy_firm' ).'</span>'.esc_html__( 'Theme Options','accountancy_firm' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}