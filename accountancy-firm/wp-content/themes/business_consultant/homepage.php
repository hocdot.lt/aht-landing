<?php 
/**
Template Name: Home
*/
get_header();

get_template_part('part-templates/home-banner');
get_template_part('part-templates/home-services');
get_template_part('part-templates/home-whychose');
get_template_part('part-templates/home-blog');
get_template_part('part-templates/home-contact');

get_footer(); ?> 