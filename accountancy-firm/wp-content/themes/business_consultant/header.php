<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
    $classAdminBar = is_admin_bar_showing() ? 'adminbar':'';
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
        <header class="header">
            <div class="header-wrapper">
                <div class="container">
                    <div class="header-main">
                        <div class="row">
                            <div class="col-lg-2 col-6">
                                <h1 class="logo">
                                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                        <?php
                                        $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                        if ($logo_header && $logo_header!=''):
                                            echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                        else:
                                            //bloginfo('name');
                                        endif;
                                        ?>
                                    </a>
                                </h1>
                            </div>
                            <div class="col-lg-6 d-flex flex-column justify-content-end col-2">
                                <div class="main-menu">
                                    <div class="open-menu-mobile d-xl-none">
                                        <div class="d-flex">
                                            <div class="fa fab fa-bars"></div>
                                        </div>
                                    </div>
                                    <div class="menu-container">
                                        <nav class="main-navigation hide-menu" id="site-navigation">
                                            <div class="close-menu-mobile d-xl-none"><i></i></div>
                                            <div class="menu-main-container">
                                                <?php
                                                if (has_nav_menu('primary')) {
                                                    wp_nav_menu(array(
                                                            'theme_location' => 'primary',
                                                            'menu_class' => 'mega-menu',
                                                            'items_wrap' =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                        )
                                                    );
                                                }
                                                ?>
                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <div id="main" class="wrapper">
				
            

                    
        
       
