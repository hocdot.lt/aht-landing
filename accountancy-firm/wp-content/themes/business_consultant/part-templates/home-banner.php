<?php
$Bannershow = get_theme_mod('banner_show', 'yes');
$Titlebanner = get_theme_mod('banner_title','Delivering Sustainable Growth & Profitability For Businesses');
$SubtitleBanner = get_theme_mod('banner_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$DescBanner = get_theme_mod('banner_desc_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$bgRightBanner = get_theme_mod('background_banner',get_template_directory_uri() . '/images/img-banner.png');
$shortcodeBanner = get_theme_mod('banner_shortcode','[contact-form-7 id="5" title="Contact"]');

?>
<?php if($Bannershow === 'yes'):?>
<div id="bannerSession">
    <div class="content-banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 ">
                        <div class="row justify-content-end ct-left">
                            <div class="col-8 col-lg-8 col-xl-6">
                                <div class="inner">
                                    <h2><?php echo $Titlebanner?></h2>
                                    <h4><?php echo $SubtitleBanner?></h4>
                                    <h6><?php echo $DescBanner?></h6>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-xl-4">
                    <div class="content-right-banner">
                        <div class="img-right-banner"><img src="<?php echo $bgRightBanner?>"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-bottom-banner">
        <div class="container">
            <?php echo do_shortcode($shortcodeBanner)?>
        </div>
    </div>
</div>
<?php endif ?>