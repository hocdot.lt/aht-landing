<?php
$postShow = get_theme_mod('show_hide_post','yes');
$servicesShow = get_theme_mod('services_show','yes');
$servicesTitle = get_theme_mod('service_title','Low Cost Accountancy Services or All Your Business Needs');
$servicesSubtitle = get_theme_mod('service_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit,');
$servicesLink = get_theme_mod('link_service','#');

$serviceCategory = get_theme_mod('Service_category',5;
if(!$serviceCategory) $serviceCategory =5;
$serviceLimit = get_theme_mod('service_number_post',4);
$args_servies = array(
    'posts_per_page'   => $serviceLimit,
    'offset'           => 0,
    'cat'         => $serviceCategory,
    'order' => 'ASC',
    'post_type'        => 'post',
);
$posts_array = get_posts( $args_servies );
$diver = ceil(count($posts_array)/2);
$service_1 = array_slice($posts_array,0,$diver);
$service_2 = array_slice($posts_array,$diver);
$i = 1;
?>
<?php if ($servicesShow === 'yes'):?>
<div id="services">
    <div class="content-services">
        <div class="container">
            <div class="row justify-content-center">
                <?php if ($postShow === 'yes'):?>
                <div class="col-lg-2">
                    <?php foreach ($service_1 as $_post): setup_postdata( $_post );?>
                    <div class="box-services">
                        <a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                        <p><?php echo get_the_excerpt($_post->ID)?></p>
                    </div>
                    <?php endforeach; wp_reset_postdata();?>

                </div>
                <?php endif?>
                <div class="col-lg-4">
                    <div class="content-between-services">
                        <h3><?php echo $servicesTitle?></h3>
                        <h5><?php echo $servicesSubtitle?></h5>
                        <a href="<?php echo $servicesLink?>">View all services</a>
                    </div>
                </div>
                <?php if ($postShow === 'yes'):?>
                <div class="col-lg-2">
                    <?php foreach ($service_2 as $_post): setup_postdata( $_post );?>
                    <div class="box-services">
                        <a href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                        <p><?php echo get_the_excerpt($_post->ID) ?></p>
                    </div>
                    <?php endforeach; wp_reset_postdata();?>
                </div>
                <?php endif?>
            </div>
        </div>
    </div>
</div>
<?php endif?>