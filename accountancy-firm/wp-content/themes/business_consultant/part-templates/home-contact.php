<?php
$contactShow = get_theme_mod('contact_show','yes');
$contactshortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="190" title="contact2"]');
$contactInfo = get_theme_mod('desc_contact','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Vitae semper quis lectus nulla at. Ipsum a arcu cursus vitae. penatibus et magnis dis parturient montes nascetur. Lorem ipsum dolor sit amet elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#">info@accountancyfirm.com</a>
                            <p>Accountancy FIrm LLC</p>
                            <p>85 Broad Street, 28th Floor</p>
                            <p>New York, NY 10004</p>
                            <p>800 - 123 - 4567</p>');

?>
<div id="contact">
    <div class="coontent-contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="content-left-contect">
                        <div class="logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <?php
                            $logo_header = get_theme_mod('logo_contact',get_template_directory_uri() . '/images/logo-contact.png');
                            if ($logo_header && $logo_header!=''):
                                echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                            else:
                                //bloginfo('name');
                            endif;
                            ?>
                        </a>
                        </div>
                        <div class="desc-info-contact">
                            <?php echo $contactInfo?>
                        </div>
                        <div class="mxh">
                            <?php
                            if(function_exists('accountancy_firm_social_link')){
                                accountancy_firm_social_link();
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="content-right-contact">
                        <?php echo do_shortcode($contactshortcode)?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>