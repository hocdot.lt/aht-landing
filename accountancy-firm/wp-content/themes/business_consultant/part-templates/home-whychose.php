<?php


$whychoseShow = get_theme_mod('whychose_show','yes');
$whychosetitle = get_theme_mod('whychose_title','Why Chose Us?');
$whychoseSubtitle = get_theme_mod('whychose_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt et dolore magna aliqua.aliqua.');
$whychoseImageLeft = get_theme_mod('img_whychose',get_template_directory_uri() . '/images/images1.jpg');
//skill
$customizer_repeater_whychose = get_theme_mod("customizer_repeater_whychose");
$customizer_repeater_example_decoded = json_decode($customizer_repeater_whychose);

//logo
$logo1 = get_theme_mod('logo1',get_template_directory_uri() . '/images/logo1.jpg');
$linklogo1 = get_theme_mod('link_1','#');
$logo2 = get_theme_mod('logo2',get_template_directory_uri() . '/images/logo2.jpg');
$linklogo2 = get_theme_mod('link_2','#');
$logo3 = get_theme_mod('logo3',get_template_directory_uri() . '/images/logo3.jpg');
$linklogo3 = get_theme_mod('link_3','#');
$logo4 = get_theme_mod('logo4',get_template_directory_uri() . '/images/logo4.jpg');
$linklogo4 = get_theme_mod('link_4','#');
$logo5 = get_theme_mod('logo5',get_template_directory_uri() . '/images/logo5.jpg');
$linklogo5 = get_theme_mod('link_5','#');

?>
<?php if($whychoseShow === 'yes'):?>
<div id="whychose">
    <div class="content-whychose">
        <div class="container-fluid nopd">
            <div class="row">
                <div class="col-lg-4">
                    <div class="bg-content-whychose-left"><img src="<?php echo $whychoseImageLeft?>"></div>
                </div>
                <div class="col-lg-4">
                    <div class="row justify-content-lg-start">
                        <div class="col-lg-6">
                            <div class="content-right-whychose">
                                <h3><?php echo $whychosetitle?></h3>
                                <h6><?php echo $whychoseSubtitle ?></h6>
                                <div class="content-bt-right">
                    <?php foreach ($customizer_repeater_example_decoded as $item): ?>
                                    <div class="bar-1">
                                        <div class="title">
                                            <h5><?php echo $item->title?></h5>
                                        </div>
                                        <div class="bar" data-width="<?php echo $item->subtitle?>%">
                                            <div class="bar-inner"></div>
                                            <div class="bar-percent "><span class="Count"><?php echo $item->subtitle?></span><span>%</span></div>
                                        </div>
                                    </div>
                    <?php endforeach; wp_reset_postdata();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-bottom-whychose">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="img-bottom"><a href="<?php echo $linklogo1?>"><img src="<?php echo $logo1?>"></a></div>
                    </div>
                    <div class="col">
                        <div class="img-bottom"><a href="<?php echo $linklogo2?>"><img src="<?php echo $logo2?>"></a></div>
                    </div>
                    <div class="col">
                        <div class="img-bottom"><a href="<?php echo $linklogo3?>"><img src="<?php echo $logo3?>"></a></div>
                    </div>
                    <div class="col">
                        <div class="img-bottom"><a href="<?php echo $linklogo4?>"><img src="<?php echo $logo4?>"></a></div>
                    </div>
                    <div class="col">
                        <div class="img-bottom"><a href="<?php echo $linklogo5?>"><img src="<?php echo $logo5?>"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif?>