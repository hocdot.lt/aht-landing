<?php
$blogShow = get_theme_mod('blog_show','yes');
$blogShowpost = get_theme_mod('show_hide_post','yes');
$blogTitle = get_theme_mod('blog_title','Read Latest Blog Posts');
$blogBackground = get_theme_mod('bg_blog',get_template_directory_uri() . '/images/banner_post.jpg');
$blogCategory = get_theme_mod('blog_category','');
$BlogLimit = get_theme_mod('blog_number_post',4);
$args = array(
    'posts_per_page'   => $BlogLimit,
    'offset'           => 0,
    'cat'         => $blogCategory,
    'order' => 'ASC',
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );
?>
<?php if ($blogShow === 'yes'):?>
<div id="blog">
    <div class="content-blog" style="background-image: url(<?php echo $blogBackground ?>);">
        <div class="headet-blog">
            <h3> <?php echo $blogTitle?></h3>
        </div>
    <?php if ($blogShowpost === 'yes'):?>
        <div class="content-slider-post">
            <div class="container">
                <div class="gallery gallery-responsive portfolio_slider">
                    <?php
                    // the query
                    $the_query = new WP_Query( $args ); ?>
                    <?php if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="col">
                        <div class="img-slider">
                            <a href="<?php echo the_permalink()?>">
                                <img src="<?php echo the_post_thumbnail_url()?>"/>
                            </a>
                            <div class="share">
                                <?php
                                if(function_exists('accountancy_firm_social_link_2')){
                                    accountancy_firm_social_link_2();
                                }
                                ?>
                            </div>
                        </div>
                        <h4><?php echo get_the_date( 'j/M/Y' ); ?></h4>
                        <a href="<?php echo the_permalink()?>"><?php the_title(); ?></a>
                    </div>
                        <?php endwhile; ?>
                    <?php endif;?>

                </div>
            </div>
        </div>
    <?php endif?>
    </div>
</div>
<?php endif?>