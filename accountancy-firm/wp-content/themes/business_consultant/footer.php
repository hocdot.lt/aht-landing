
</div> <!-- End main-->
<footer class="footer">
    <div class="content-footer">
        <div class="container">
            <div class="footer-top">
                <div class="menu-footer">
                    <?php
                    if (has_nav_menu('footer')) {
                        wp_nav_menu(array(
                                'theme_location' => 'footer',
                                'menu_class' => 'mega-menu',
                                'items_wrap' =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            )
                        );
                    }
                    ?>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="copyright">
                                <?php
                                $copyright =  get_theme_mod('copyright','<h6 class="text-center m-0">(C) All Rights Reserved. Charity Theme, Designed & Developed by<a href="#">Template.net</a></h6>');;
                                if ( $copyright !='') : ?>
                                    <?php echo wp_kses_post( $copyright); ?>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>