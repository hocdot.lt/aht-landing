<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
$car_wash_sidebar_left = car_wash_get_sidebar_left();
$car_wash_sidebar_right = car_wash_get_sidebar_right();
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site ">
        <header class="header <?php echo is_front_page()?'fixed-top':'sticky-top'; ?>">
            <div class="header-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col col-lg-2 col-xl-3 d-flex">
                            <?php if (is_front_page()) : ?>
                                <h1 class="logo">
                                <?php else: ?>
                                <h2 class="logo">
                                    <?php endif; ?>
                                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                        <?php
                                        $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo.png');
                                        if ($logo_header && $logo_header!=''):
                                            echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                        else:
                                            echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                        endif;
                                        ?>
                                    </a>
                                    <?php if (is_front_page()) : ?>
                                </h1>
                            <?php else: ?>
                                </h2>
                            <?php endif; ?>
                            <?php
                            $logo_subtitle = get_theme_mod('logo_subtitle','<h3>Mister Car</h3><p>CAR WASH</p>');
                            ?>
                            <?php if ( $logo_subtitle !='') :?>
                            <div class="desclogo">
                                <?php echo apply_filters( 'the_content', $logo_subtitle); ?>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-5 d-flex flex-column justify-content-center col-2">
                            <?php
                                if(function_exists('car_wash_header_menu')){
                                    car_wash_header_menu();
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="main" class="wrapper">
				
            

                    
        
       
