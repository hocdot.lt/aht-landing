<?php 
/**
Template Name: Home
*/
get_header();?>
<?php
get_template_part('part-templates/home-section1');
get_template_part('part-templates/home-section2');
get_template_part('part-templates/home-section3');
get_template_part('part-templates/home-section4');
get_template_part('part-templates/home-section5');
get_template_part('part-templates/home-section6');
get_template_part('part-templates/home-section7');

?>

<?php get_footer(); ?>