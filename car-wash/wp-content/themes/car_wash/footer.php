
</div> <!-- End main-->
<?php
    $logo_footer = get_theme_mod('logo_footer',get_template_directory_uri() . '/images/logo-footer.png');
    $copyright = get_theme_mod('copyright','<p class="text-center m-0">(C) 2019, All Rights Reserved. Mistercarswash. Designed and Developed by <a href="#">Template.net</a></p>');
?>
<footer class="footer">
    <div class=".footer-wrapper">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 d-flex">
                        <div class="logo">
                            <a href="#" rel="home">
                                <?php
                                if ($logo_foooter && $logo_foooter!=''):
                                    echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_footer)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                else:
                                    echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo-footer.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                endif;
                                ?>
                            </a>
                        </div>
                        <?php
                        $logo_footer_subtitle = get_theme_mod('logo_footer_subtitle','<h3>Mister Car</h3><p>CAR WASH</p>');
                        if ( $logo_footer_subtitle !='') :
                        ?>
                            <div class="desclogo">
                                <?php echo apply_filters( 'the_content', $logo_footer_subtitle); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-5 d-flex flex-column justify-content-center">
                        <?php if (has_nav_menu('footer')) : ?>
                        <div class="main-menu">
                            <div class="menu-container">
                                <nav class="main-navigation">
                                    <div class="menu-main-container">
                                        <?php wp_nav_menu(array('theme_location' => 'footer','menu_class' => 'mega-menu',)); ?>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if ($copyright): ?>
                        <div class="copyright">
                            <?php echo apply_filters( 'the_content', $copyright); ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>