<?php
/**
 * Render whychose in home page
 */

//Get config
$on_whychose_show =  get_theme_mod('on_whychose_show','yes');
$img_whychose = get_theme_mod('img_whychose',get_template_directory_uri() . '/images/images2.png');
$title_whychose = get_theme_mod('title_whychose','Our Expert Team of Trimming & Dressing');
$whychose_title = get_theme_mod('whychose_title','Our Expert Team of Trimming & Dressing');
$whychose_content = get_theme_mod('whychose_content','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio eu feugiat pretium nibh ipsum consequat nisl vel.');
$button_whychose = get_theme_mod('button_whychose','Book Appointment');
$link_whychose = get_theme_mod('link_whychose','#');
?>
<?php if($on_whychose_show === 'yes'): ?>
    <div id="whychose">
        <div class="content-top">
            <div class="container">
                <div class="nd-content-top">
                    <div class="row">
                        <div class="col-lg-4">
                            <?php  if($title_whychose !=''): ?>
                                <div class="header-whychose">
                                    <h3><?php echo $title_whychose; ?></h3>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                             <?php if(!empty($img_whychose)): ?>
                             <div class="img-left-why"><img src="<?php echo $img_whychose; ?>" alt="<?php echo $title_whychose; ?>"></div>
                             <?php endif; ?>
                        </div>
                        <div class="col-lg-4">
                            <div class="content-right-why">
                                <?php if(!empty($whychose_title)): ?>
                                <div class="title">
                                    <h3><?php echo $whychose_title; ?></h3>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($whychose_content)): ?>
                                <div class="desc">
                                    <?php echo apply_filters( 'the_content', $whychose_content); ?>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($button_whychose) || !empty($link_whychose)): ?>
                                <div class="btn-bottom">
                                    <a href="<?php echo $link_whychose; ?>" class="btn"><?php echo $button_whychose; ?></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-bootom"></div>
    </div>
<?php endif;