<?php
/**
 * Render testimonials in home page
 */

//Get config
$testimonials_show =  get_theme_mod('testimonials_show','yes');
$testimonials_title = get_theme_mod('testimonials_title','Read words from our customers');
$testimonials_sub_title = get_theme_mod('testimonials_sub_title','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore. Nisi porta lorem mollis aliquam ut porttitor leo.');
$testimonials_img = get_theme_mod('testimonials_img',get_template_directory_uri() . '/images/images7.png');
?>
<?php if($testimonials_show === 'yes'): ?>
    <div id="read-words-from">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="row justify-content-end">
                        <div class="col-md-8 col-lg-6">
                            <div class="header-read-word">
                                <?php  if($testimonials_title !=''){ ?>
                                    <h3><?php echo $testimonials_title; ?></h3>
                                <?php } ?>
                                <?php  if($testimonials_sub_title !=''){ ?>
                                    <p><?php echo $testimonials_sub_title; ?></p>
                                <?php } ?>
                            </div>
                            <?php
                            $the_query = new WP_Query(array(
                                'post_type' => 'testimonial',
                                'posts_per_page' => 3,
                                'post_status' => 'publish',
                                'orderby'	=> 'date',
                                'order'	=> 'DESC',
                            ));
                            if ( $the_query->have_posts() ) : ?>
                            <div class="silder-avart">
                                <div class="row">
                                    <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10,'dotsData':true,'dotsEach':true,'loop':true}">
                                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                        <div data-dot="&lt;img src='<?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail'); ?>'&gt;">
                                            <div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark mb-0">
                                                <blockquote>
                                                    <p class="text-5 line-height-5 mb-0">
                                                        <?php echo get_the_content(); ?>
                                                    </p>
                                                </blockquote>
                                            </div>
                                        </div>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            endif;
                            wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 content-right-read">
                    <?php if(!empty($testimonials_img)){ ?>
                    <div class="img-right-read"><img src="<?php echo $testimonials_img; ?>" alt="<?php echo $testimonials_title; ?><"></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>