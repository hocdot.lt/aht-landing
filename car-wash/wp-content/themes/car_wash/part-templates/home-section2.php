<?php
/**
 * Render about-us in home page
 */

//Get config
$aboutShow =  get_theme_mod('about_show','yes');
$title_about = get_theme_mod('title_about','About Mister Car Wash');
$sub_title_about = get_theme_mod('sub_title_about','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisi porta lorem mollis aliquam ut porttitor leo.Amet est placerat in egestas erat imperdiet sed euismod nisi. Tellus rutrum tellus pellentesque eu tincidunt tortor. Leo vel fringilla est ullamcorper eget.');
$about_image = get_theme_mod('about_image',get_template_directory_uri() . '/images/images1.png');

$customizer_repeater_about = get_theme_mod("customizer_repeater_about");
?>
<?php if($aboutShow === 'yes'): ?>
    <div id="About">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="header-about">
                        <?php if(!empty($title_about)){ ?>
                        <div class="title-header">
                            <h3><?php echo $title_about; ?></h3>
                        </div>
                        <?php } ?>
                        <?php if(!empty($sub_title_about)){ ?>
                        <div class="desc-about">
                            <p ><?php echo $sub_title_about; ?></p>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-about">
            <div class="container-fluid content-main-about">
                <div class="row">
                    <div class="col-lg-4">
                        <?php if(!empty($about_image)){ ?>
                            <div class="bd-img"><img src="<?php echo $about_image; ?>"></div>
                        <?php } ?>
                    </div>
                    <div class="col-lg-4">
                        <div class="row justify-content-start content-right-about">
                            <div class="col-md-8 col-lg-8 col-xl-6">
                                <?php if(!empty($customizer_repeater_about)): ?>
								<div class="toggle toggle-minimal toggle-primary" data-plugin-toggle data-plugin-options="{ 'isAccordion': true }">
                                
                                    <?php
                                    $customizer_repeater_example_decoded = json_decode($customizer_repeater_about);
                                    foreach( $customizer_repeater_example_decoded as $key => $item ):
                                    ?>
                                    <section class="toggle <?php if($key == 0)  echo 'active';?>">
                                        <label><?php echo $item->title;?></label>
                                        <div class="toggle-content">
                                            <p><?php echo htmlspecialchars_decode($item->text); ?></p>
                                        </div>
                                    </section>
                                    <?php endforeach; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
