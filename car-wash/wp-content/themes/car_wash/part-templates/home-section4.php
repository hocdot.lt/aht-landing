<?php
/**
 * Render services in home page
 */

//Get config
$service_show =  get_theme_mod('service_show','yes');
$img_service = get_theme_mod('img_service',get_template_directory_uri() . '/images/images3.jpg');

$service_title_1 = get_theme_mod('service_title_1','Best & Easy Convinence');
$service_content_1 = get_theme_mod('service_content_1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$service_icon_1 = get_theme_mod('service_icon_1',get_template_directory_uri() . '/images/icon1.png');
$service_link_1 = get_theme_mod('service_link_1','#');

$service_title_2 = get_theme_mod('service_title_2','Full Organic Products');
$service_content_2 = get_theme_mod('service_content_2','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$service_icon_2 = get_theme_mod('service_icon_2',get_template_directory_uri() . '/images/icon2.png');
$service_link_2 = get_theme_mod('service_link_2','#');

$service_title_3 = get_theme_mod('service_title_3','Fuly Experience Team');
$service_content_3 = get_theme_mod('service_content_3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$service_icon_3 = get_theme_mod('service_icon_3',get_template_directory_uri() . '/images/icon3.png');
$service_link_3 = get_theme_mod('service_link_3','#');

$service_title_4 = get_theme_mod('service_title_4','Full Organic Products');
$service_content_4 = get_theme_mod('service_content_4','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$service_icon_4 = get_theme_mod('service_icon_4',get_template_directory_uri() . '/images/icon4.png');
$service_link_4 = get_theme_mod('service_link_4','#');
?>
<?php if($service_show === 'yes'): ?>
    <div id="service">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="row justify-content-end">
                        <div class="col-md-8 col-lg-8 col-xl-6">
                            <div class="row">
                                <?php if(!empty($service_title_1) || !empty($service_content_1) || !empty($service_link_1) || !empty($service_icon_1)): ?>
                                <div class="col-lg-4">
                                    <div class="featured-box featured-box-primary featured-box-effect-4 appear-animation featured1" data-appear-animation="fadeIn">
                                        <div class="box-content px-4">
                                            <?php if(!empty($service_icon_1)){ ?>
                                                <div class="icon-featured"><img src="<?php echo $service_icon_1; ?>"></div>
                                            <?php } ?>
                                            <?php if(!empty($service_title_1) || !empty($service_link_1)){ ?>
                                                <h4>
                                                    <a class="mb-2" href="<?php echo service_link_1;?>"><?php echo $service_title_1; ?></a>
                                                </h4>
                                            <?php } ?>
                                            <?php if(!empty($service_content_1)){ ?>
                                                <p class="mb-0"><?php echo $service_content_1; ?></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <?php if(!empty($service_title_2) || !empty($service_content_2) || !empty($service_link_2) || !empty($service_icon_2)): ?>
                                    <div class="col-lg-4">
                                        <div class="featured-box featured-box-primary featured-box-effect-4 appear-animation featured2" data-appear-animation="fadeIn">
                                            <div class="box-content px-4">
                                                <?php if(!empty($service_icon_2)){ ?>
                                                    <div class="icon-featured"><img src="<?php echo $service_icon_2; ?>"></div>
                                                <?php } ?>
                                                <?php if(!empty($service_title_2) || !empty($service_link_2)){ ?>
                                                    <h4>
                                                        <a class="mb-2" href="<?php echo service_link_2;?>"><?php echo $service_title_2; ?></a>
                                                    </h4>
                                                <?php } ?>
                                                <?php if(!empty($service_content_2)){ ?>
                                                    <p class="mb-0"><?php echo $service_content_2; ?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(!empty($service_title_3) || !empty($service_content_3) || !empty($service_link_3) || !empty($service_icon_3)): ?>
                                    <div class="col-lg-4">
                                        <div class="featured-box featured-box-primary featured-box-effect-4 appear-animation featured3" data-appear-animation="fadeIn">
                                            <div class="box-content px-4">
                                                <?php if(!empty($service_icon_3)){ ?>
                                                    <div class="icon-featured"><img src="<?php echo $service_icon_3; ?>"></div>
                                                <?php } ?>
                                                <?php if(!empty($service_title_3) || !empty($service_link_3)){ ?>
                                                    <h4>
                                                        <a class="mb-3" href="<?php echo service_link_3;?>"><?php echo $service_title_3; ?></a>
                                                    </h4>
                                                <?php } ?>
                                                <?php if(!empty($service_content_3)){ ?>
                                                    <p class="mb-0"><?php echo $service_content_3; ?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if(!empty($service_title_4) || !empty($service_content_4) || !empty($service_link_4) || !empty($service_icon_4)): ?>
                                    <div class="col-lg-4">
                                        <div class="featured-box featured-box-primary featured-box-effect-4 appear-animation featured4" data-appear-animation="fadeIn">
                                            <div class="box-content px-4">
                                                <?php if(!empty($service_icon_4)){ ?>
                                                    <div class="icon-featured"><img src="<?php echo $service_icon_4; ?>"></div>
                                                <?php } ?>
                                                <?php if(!empty($service_title_4) || !empty($service_link_4)){ ?>
                                                    <h4>
                                                        <a class="mb-4" href="<?php echo service_link_4;?>"><?php echo $service_title_4; ?></a>
                                                    </h4>
                                                <?php } ?>
                                                <?php if(!empty($service_content_4)){ ?>
                                                    <p class="mb-0"><?php echo $service_content_4; ?></p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 content-right">
                    <?php if(!empty($img_service)): ?>
                    <div class="bg-right-service"><img src="<?php echo $img_service; ?>" alt="Services"></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>