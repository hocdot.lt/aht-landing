<?php
/**
 * Render contact in home page
 */

//Get config
$contact_show =  get_theme_mod('contact_show','yes');
$bg_contact = get_theme_mod('bg_contact',get_template_directory_uri() . '/images/banner-form.jpg');
$title_contact = get_theme_mod('title_contact','Make an Appointment for Your Car Wash');
$call_info = get_theme_mod('call_info','Or Call 800 200 100');
$sub_title_contact = get_theme_mod('sub_title_contact','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Nisi porta lorem mollis aliquam ut porttitor leo.');
$contact_info = get_theme_mod('contact_info','<p>123 Street, City, State 45678</p><p>Phone: 1234 56 7890</p><div class="mail"><a href="#">info@mistercarwashs.com</a></div><div class="mail"><a href="#">mistercarwashs.com</a></div>');
$contact_shortcode = get_theme_mod('contact_shortcode','[contact-form-7 id="49" title="Contact form home"]');
?>
<?php if($contact_show === 'yes'): ?>
<div id="make-an-appointment" <?php if(!empty($bg_contact)){ ?> style="background-image:url(<?php echo $bg_contact; ?>);" <?php } ?>>
    <div class="background-make">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="content-left-make">
                        <?php  if($title_contact !=''){ ?>
                            <div class="title">
                                <h3><?php echo $title_contact; ?></h3>
                            </div>
                        <?php } ?>
                        <?php  if($sub_title_contact !=''){ ?>
                            <div class="desc">
                                <p><?php echo $sub_title_contact; ?></p>
                            </div>
                        <?php } ?>
                        <?php if($contact_info != ''): ?>
                        <div class="info">
                            <?php echo apply_filters( 'the_content', $contact_info); ?>
                            <div class="icon-mxh">
                                <?php
                                if(function_exists('car_wash_social_link')){
                                    car_wash_social_link();
                                }
                                ?>
                            </div>
                        </div>
                         <?php endif; ?>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="content-form-right justify-content-end">
                        <?php if($contact_shortcode != ''): ?>
                            <?php echo do_shortcode($contact_shortcode); ?>
                        <?php endif; ?>
                        <?php  if($call_info !=''){ ?>
                            <div class="phone">
                                <h3><?php echo $call_info; ?></h3>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>