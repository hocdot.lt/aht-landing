<?php
/**
 * Render banner in home page
 */

//Get config
$banner_show =  get_theme_mod('banner_show','yes');
$slider_type =  get_theme_mod('slider_type','default');
$revolution_slider_shortcode = get_theme_mod('revolution_slider_shortcode','[rev_slider alias="home-slider"]');

$slider_title_1 = get_theme_mod('title_slider_1','Eco-friendly, Hand Car Wash and Detailing Services');
$bellow_button_slider_1 = get_theme_mod('bellow_button_slider_1','Or Call : 800 120 300');
$sub_title_slider_1 = get_theme_mod('sub_title_slider_1','Our goal is to provide our customers with the friendliest, most convenient hand car wash experience possible.');
$button_slider_1 = get_theme_mod('button_slider_1','Book Appointment');
$link_slider_1 = get_theme_mod('link_slider_1','#');
$img_slider_1 = get_theme_mod('img_slider_1',get_template_directory_uri() . '/images/banner.jpg');

$slider_title_2 = get_theme_mod('title_slider_2','Eco-friendly, Hand Car Wash and Detailing Services');
$bellow_button_slider_2 = get_theme_mod('bellow_button_slider_2','Or Call : 800 120 300');
$sub_title_slider_2 = get_theme_mod('sub_title_slider_2','Our goal is to provide our customers with the friendliest, most convenient hand car wash experience possible.');
$button_slider_2 = get_theme_mod('button_slider_2','Book Appointment');
$link_slider_2 = get_theme_mod('link_slider_2','#');
$img_slider_2 = get_theme_mod('img_slider_2',get_template_directory_uri() . '/images/banner.jpg');

$slider_title_3 = get_theme_mod('title_slider_3','Eco-friendly, Hand Car Wash and Detailing Services');
$bellow_button_slider_3 = get_theme_mod('bellow_button_slider_3','Or Call : 800 120 300');
$sub_title_slider_3 = get_theme_mod('sub_title_slider_3','Our goal is to provide our customers with the friendliest, most convenient hand car wash experience possible.');
$button_slider_3 = get_theme_mod('button_slider_3','Book Appointment');
$link_slider_3 = get_theme_mod('link_slider_3','#');
$img_slider_3 = get_theme_mod('img_slider_3',get_template_directory_uri() . '/images/banner.jpg');

?>
<?php if($banner_show === 'yes'): ?>
    <div id="bannerSession">
        <?php if($slider_type === 'default'): ?>
        <div class="owl-carousel owl-theme nav-style-1 mb-0" data-plugin-options="{'items': 1, 'margin': 0, 'loop': false, 'nav': true, 'dots': false, 'stagePadding': 0, 'autoHeight': true}">
            <?php if(!empty($slider_title_1) || !empty($sub_title_slider_1) || !empty($link_slider_1) || !empty($img_slider_1)): ?>
            <div>
                <?php if(!empty($img_slider_1)): ?>
                    <div class="media">
                        <img class="img-fluid rounded-0 mb-4" src="<?php echo $img_slider_1; ?>" alt="<?php echo $slider_title_1;?>">
                    </div>
                <?php endif;?>
                <div class="cap">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="inner">
                                    <?php if(!empty($slider_title_1)){ ?>
                                        <h3><?php echo $slider_title_1; ?></h3>
                                    <?php } ?>
                                    <?php if(!empty($sub_title_slider_1)){ ?>
                                        <p><?php echo $sub_title_slider_1; ?></p>
                                    <?php } ?>
                                    <?php if(!empty($link_slider_1) || !empty($button_slider_1)){ ?>
                                        <a class="btn" href="<?php echo $link_slider_1; ?>"><?php echo $button_slider_1; ?></a>
                                    <?php } ?>
                                    <?php if(!empty($bellow_button_slider_1)){ ?>
                                    <h3 class="phone"><?php echo $bellow_button_slider_1; ?></h3>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if(!empty($slider_title_2) || !empty($sub_title_slider_2) || !empty($link_slider_2) || !empty($img_slider_2)): ?>
                <div>
                    <?php if(!empty($img_slider_2)): ?>
                        <div class="media">
                            <img class="img-fluid rounded-0 mb-4" src="<?php echo $img_slider_2; ?>" alt="<?php echo $slider_title_2;?>">
                        </div>
                    <?php endif;?>
                    <div class="cap">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5">
                                    <div class="inner">
                                        <?php if(!empty($slider_title_2)){ ?>
                                            <h3><?php echo $slider_title_2; ?></h3>
                                        <?php } ?>
                                        <?php if(!empty($sub_title_slider_2)){ ?>
                                            <p><?php echo $sub_title_slider_2; ?></p>
                                        <?php } ?>
                                        <?php if(!empty($link_slider_2) || !empty($button_slider_2)){ ?>
                                            <a class="btn" href="<?php echo $link_slider_2; ?>"><?php echo $button_slider_2; ?></a>
                                        <?php } ?>
                                        <?php if(!empty($bellow_button_slider_2)){ ?>
                                            <h3 class="phone"><?php echo $bellow_button_slider_2; ?></h3>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if(!empty($slider_title_3) || !empty($sub_title_slider_3) || !empty($link_slider_3) || !empty($img_slider_3)): ?>
                <div>
                    <?php if(!empty($img_slider_3)): ?>
                        <div class="media">
                            <img class="img-fluid rounded-0 mb-4" src="<?php echo $img_slider_3; ?>" alt="<?php echo $slider_title_3;?>">
                        </div>
                    <?php endif;?>
                    <div class="cap">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-5">
                                    <div class="inner">
                                        <?php if(!empty($slider_title_3)){ ?>
                                            <h3><?php echo $slider_title_3; ?></h3>
                                        <?php } ?>
                                        <?php if(!empty($sub_title_slider_3)){ ?>
                                            <p><?php echo $sub_title_slider_3; ?></p>
                                        <?php } ?>
                                        <?php if(!empty($link_slider_3) || !empty($button_slider_3)){ ?>
                                            <a class="btn" href="<?php echo $link_slider_3; ?>"><?php echo $button_slider_3; ?></a>
                                        <?php } ?>
                                        <?php if(!empty($bellow_button_slider_3)){ ?>
                                            <h3 class="phone"><?php echo $bellow_button_slider_3; ?></h3>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        <?php if($slider_type === 'rs'): ?>
            <?php if($revolution_slider_shortcode != ''): ?>
                <?php echo do_shortcode($revolution_slider_shortcode); ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
<?php endif; ?>