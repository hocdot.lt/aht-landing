<?php
/**
 * Render blog in home page
 */

//Get config
$blog_show =  get_theme_mod('blog_show','yes');
$number_post_blog = get_theme_mod('number_post_blog','3');
$title_blog = get_theme_mod('title_blog','Recent Blog Posts');
?>
<?php if($blog_show === 'yes'): ?>
    <div id="blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header-blog">
                        <?php  if($title_blog !=''){ ?>
                            <h3><?php echo $title_blog; ?></h3>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                $the_query = new WP_Query(array(
                    'post_type' => 'post',
                    'posts_per_page' => $number_post_blog,
                    'post_status' => 'publish',
                    'orderby'	=> 'date',
                    'order'	=> 'DESC',
                ));
                ?>
                <?php
                    if ( $the_query->have_posts() ) : $i = 0;
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>
                <?php if($i%3 == 0): ?>
                    <div class="col-lg-4">
                        <div class="portfolio-item">
                            <a href="#">
                                <span class="thumb-info thumb-info-lighten">
                                    <span class="thumb-info-wrapper">
                                        <?php if ( has_post_thumbnail() ) : ?>
                                            <?php the_post_thumbnail('blog', ['class' => 'img-fluid border-radius-0']); ?>
                                        <?php endif; ?>
                                        <span class="thumb-info-title">
                                            <span class="thumb-info-inner"><?php echo get_the_date('M j Y'); ?></span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                            <div class="title">
                                <h3><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                            </div>
                        </div>
                    </div>
                    <?php if( $i+1 < $the_query->post_count ): ?>
                        <div class="col-lg-4">
                            <div class="content-right">
                    <?php endif; ?>
                <?php else: ?>
                    <div class="row item<?php echo $i%3;?> <?php if($i%3 == 2) echo 'flex-row-reverse';?>">
                        <div class="col-md-4">
                            <div class="portfolio-item">
                                <a href="#">
                                    <span class="thumb-info thumb-info-lighten">
                                        <span class="thumb-info-wrapper">
                                            <?php if ( has_post_thumbnail() ) : ?>
                                                <?php the_post_thumbnail('blog_2', ['class' => 'img-fluid border-radius-0']); ?>
                                            <?php endif; ?>
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner"><?php echo get_the_date('M j Y'); ?></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="content-post">
                                <div class="title">
                                    <h3><?php the_title(); ?></h3>
                                </div>
                                <div class="desc">
                                    <p><?php echo wp_trim_words( get_the_excerpt(), $num_words = 23, '' ) ?></p>
                                </div>
                                <div class="btnbb">
                                    <a class="read-more btn" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Continue Reading</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if( $i+1 == $the_query->post_count || $i%3 == 2) echo "</div></div>"; ?>
                <?php endif;?>
                <?php
                    $i++;endwhile;
                    endif;wp_reset_postdata();
                ?>
             </div>
        </div>
    </div>
<?php endif; ?>