<?php
    $car_wash_sidebar_left = car_wash_get_sidebar_left();
?>
<?php if ($car_wash_sidebar_left && $car_wash_sidebar_left != "none" && is_active_sidebar($car_wash_sidebar_left)) : ?>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 left-sidebar active-sidebar"><!-- main sidebar -->
        <?php dynamic_sidebar($car_wash_sidebar_left); ?>
    </div>
<?php endif; ?>


