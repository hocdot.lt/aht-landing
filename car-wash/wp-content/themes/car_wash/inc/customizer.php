<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'car_wash_customize_register' );
function car_wash_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'car_wash_Customize_Sidebar_Control' ) ) {
	    class car_wash_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',			
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',			
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Playfair Display:400,700,400i' => 'Playfair Display', 
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',
                'Rozha One' => 'Rozha One',
                'Arial,Helvetica Neue,Helvetica,sans-serif:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'car_wash') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','car_wash'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','car_wash' ),
	      'description' => esc_html__( 'Upload Logo','car_wash' ),
	      'section' => 'general_settings',
	   )
	) );
    $wp_customize->add_setting( 'header_info',
        array(
            'default' => '<p>800 345 678</p><p>linfo@website.com</p>',
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'header_info',
        array(
            'label' => esc_html__( 'Header info','car_wash' ),
            'section' => 'general_settings',
        )
    ) );

    $wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => '',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Main Google Font',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options
    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'car_wash'),
        'description'    => __('Several settings pertaining my theme', 'car_wash'),
    ) );
	
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','car_wash'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
    $wp_customize->add_setting( 'slider_type',
        array(
            'default' => 'default',
        )
    );
    $wp_customize->add_control( 'slider_type',
        array(
            'label' => esc_html__( 'Slider','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array(
                'default' => esc_html__( 'Default','car_wash' ),
                'rs' => esc_html__( 'Revolution Slider','car_wash' ),
            )
        )
    );
    $wp_customize->add_setting( 'revolution_slider_shortcode',
        array(
            'default' => '[rev_slider alias="home-slider"]',
        )
    );
    $wp_customize->add_control( 'revolution_slider_shortcode',
        array(
            'label' => esc_html__( 'Revolution Slider Shortcode','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // start slider 1
    $wp_customize->add_setting( 'img_slider_1',
        array(
            'default' => get_template_directory_uri() . '/images/banner.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_slider_1',
        array(
            'label' => esc_html__( 'Image 1','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'home_banner_settings',
        )
    ) );
    $wp_customize->add_setting( 'title_slider_1',
        array(
            'default' => esc_html__('Eco-friendly, Hand Car Wash and Detailing Services','car_wash'),
        )
    );
    $wp_customize->add_control('title_slider_1',
        array(
            'label' => esc_html__( 'Title Slider 1','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_slider_1',
        array(
            'default' => esc_html__('Our goal is to provide our customers with the friendliest, most convenient hand car wash experience possible.','car_wash'),

        )
    );
    $wp_customize->add_control('sub_title_slider_1',
        array(
            'label' => esc_html__( 'Sub Title Slider 1','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'button_slider_1',
        array(
            'default' => esc_html__('Book Appointment','car_wash'),

        )
    );
    $wp_customize->add_control('button_slider_1',
        array(
            'label' => esc_html__( 'Button Slider 1','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_slider_1',
        array(
            'default' => esc_html__('#','car_wash'),
        )
    );
    $wp_customize->add_control('link_slider_1',
        array(
            'label' => esc_html__( 'Button URL Slider 1','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'bellow_button_slider_1',
        array(
            'default' => esc_html__('Or Call : 800 120 300','car_wash'),

        )
    );
    $wp_customize->add_control('bellow_button_slider_1',
        array(
            'label' => esc_html__( 'Title 2 Slider 1','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // End slider 1
    // start slider 2
    $wp_customize->add_setting( 'img_slider_2',
        array(
            'default' => get_template_directory_uri() . '/images/banner.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_slider_2',
        array(
            'label' => esc_html__( 'Image 2','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'home_banner_settings',
        )
    ) );
    $wp_customize->add_setting( 'title_slider_2',
        array(
            'default' => esc_html__('Eco-friendly, Hand Car Wash and Detailing Services','car_wash'),
        )
    );
    $wp_customize->add_control('title_slider_2',
        array(
            'label' => esc_html__( 'Title Slider 2','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_slider_2',
        array(
            'default' => esc_html__('Our goal is to provide our customers with the friendliest, most convenient hand car wash experience possible.','car_wash'),

        )
    );
    $wp_customize->add_control('sub_title_slider_2',
        array(
            'label' => esc_html__( 'Sub Title Slider 2','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'button_slider_2',
        array(
            'default' => esc_html__('Book Appointment','car_wash'),

        )
    );
    $wp_customize->add_control('button_slider_2',
        array(
            'label' => esc_html__( 'Button Slider 2','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_slider_2',
        array(
            'default' => esc_html__('#','car_wash'),
        )
    );
    $wp_customize->add_control('link_slider_2',
        array(
            'label' => esc_html__( 'Button URL Slider 2','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'bellow_button_slider_2',
        array(
            'default' => esc_html__('Or Call : 800 220 300','car_wash'),

        )
    );
    $wp_customize->add_control('bellow_button_slider_2',
        array(
            'label' => esc_html__( 'Title 2 Slider 2','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // End slider 2
    // start slider 3
    $wp_customize->add_setting( 'img_slider_3',
        array(
            'default' => get_template_directory_uri() . '/images/banner.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_slider_3',
        array(
            'label' => esc_html__( 'Image 3','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'home_banner_settings',
        )
    ) );
    $wp_customize->add_setting( 'title_slider_3',
        array(
            'default' => esc_html__('Eco-friendly, Hand Car Wash and Detailing Services','car_wash'),
        )
    );
    $wp_customize->add_control('title_slider_3',
        array(
            'label' => esc_html__( 'Title Slider 3','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_slider_3',
        array(
            'default' => esc_html__('Our goal is to provide our customers with the friendliest, most convenient hand car wash experience possible.','car_wash'),

        )
    );
    $wp_customize->add_control('sub_title_slider_3',
        array(
            'label' => esc_html__( 'Sub Title Slider 3','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'button_slider_3',
        array(
            'default' => esc_html__('Book Appointment','car_wash'),

        )
    );
    $wp_customize->add_control('button_slider_3',
        array(
            'label' => esc_html__( 'Button Slider 3','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_slider_3',
        array(
            'default' => esc_html__('#','car_wash'),
        )
    );
    $wp_customize->add_control('link_slider_3',
        array(
            'label' => esc_html__( 'Button URL Slider 3','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'bellow_button_slider_3',
        array(
            'default' => esc_html__('Or Call : 800 330 300','car_wash'),

        )
    );
    $wp_customize->add_control('bellow_button_slider_3',
        array(
            'label' => esc_html__( 'Title 2 Slider 3','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'text',
        )
    );
    // End slider 3

	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','car_wash' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','car_wash' ),
	         'yes' => esc_html__( 'Yes','car_wash' ),
            )
	   )
	);
	// End banner section
	
	// Section about
	
	$wp_customize->add_section( 'about_settings',
	   array(
	      'title' => esc_html__( 'About section','car_wash'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);

	
	$wp_customize->add_setting( 'title_about',
	   array(
	      'default' => esc_html__('About Mister Car Wash','car_wash'),

	   )
	);	
	$wp_customize->add_control('title_about',
		array(
	      'label' => esc_html__( 'Title About','car_wash' ),
	      'section' => 'about_settings',
	       'type' => 'text',	
	   )
	);
	
	$wp_customize->add_setting( 'sub_title_about',
	   array(
	      'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisi porta lorem mollis aliquam ut porttitor leo.Amet est placerat in egestas erat imperdiet sed euismod nisi. Tellus rutrum tellus pellentesque eu tincidunt tortor. Leo vel fringilla est ullamcorper eget.','car_wash'),

	   )
	);	
	$wp_customize->add_control('sub_title_about',
		array(
	      'label' => esc_html__( 'Sub Title About','car_wash' ),
	      'section' => 'about_settings',
	       'type' => 'textarea',
	   )
	);

    $wp_customize->add_setting( 'about_image',
        array(
            'default' => get_template_directory_uri() . '/images/images1.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'about_image',
        array(
            'label' => esc_html__( 'Image About','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'about_settings',
        )
    ) );

    $wp_customize->add_setting( 'customizer_repeater_about', array(
        'sanitize_callback' => 'customizer_repeater_sanitize'
    ));
    $wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'customizer_repeater_about', array(
        'label'   => esc_html__('About Details','customizer-repeater'),
        'section' => 'about_settings',
        'customizer_repeater_title_control' => true,
        'customizer_repeater_text_control' => true,
        'customizer_repeater_repeater_control' => true
    ) ) );
	
	$wp_customize->add_setting( 'about_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'about_show',
	   array(
            'label' => esc_html__( 'Show Section','car_wash' ),
            'section' => 'about_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','car_wash' ),
	         'yes' => esc_html__( 'Yes','car_wash' ),
            )
	   )
	);
	// End about

    // start whychose
    $wp_customize->add_section( 'whychose_settings',
        array(
            'title' => esc_html__( 'Why chose Section','car_wash'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    $wp_customize->add_setting( 'img_whychose',
        array(
            'default' => get_template_directory_uri() . '/images/images2.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_whychose',
        array(
            'label' => esc_html__( 'Image','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'whychose_settings',
        )
    ) );
    $wp_customize->add_setting( 'title_whychose',
        array(
            'default' => esc_html__('Why Chose Mister Car Wash?','car_wash'),
        )
    );
    $wp_customize->add_control('title_whychose',
        array(
            'label' => esc_html__( 'Title Section','car_wash' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'whychose_title',
        array(
            'default' => esc_html__('We care much about environment and your health, take pride in being a Bio Car Wash.','car_wash'),
        )
    );
    $wp_customize->add_control('whychose_title',
        array(
            'label' => esc_html__( 'Title Section','car_wash' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'whychose_content',
        array(
            'default' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nisi porta lorem mollis erat imperdiet sed euismod nisi. Tellus rutrum tellus pellentesque eu tincidunt tortor.Leo vel fringilla est ullamcorper eget.</p>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'whychose_content',
        array(
            'label' => esc_html__( 'Whychose content','car_wash' ),
            'description' => esc_html__( 'Whychose content','car_wash' ),
            'section' => 'whychose_settings',
        )
    ) );

    $wp_customize->add_setting( 'button_whychose',
        array(
            'default' => esc_html__('Book Appointment','car_wash'),

        )
    );
    $wp_customize->add_control('button_whychose',
        array(
            'label' => esc_html__( 'Button banner','car_wash' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'link_whychose',
        array(
            'default' => esc_html__('#','car_wash'),
        )
    );
    $wp_customize->add_control('link_whychose',
        array(
            'label' => esc_html__( 'Button URL','car_wash' ),
            'section' => 'whychose_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'on_whychose_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'on_whychose_show',
        array(
            'label' => esc_html__( 'Show Section','car_wash' ),
            'section' => 'whychose_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','car_wash' ),
                'yes' => esc_html__( 'Yes','car_wash' ),
            )
        )
    );
    // End whychose

    // start Services
    $wp_customize->add_section( 'service_settings',
        array(
            'title' => esc_html__( 'Services Section','car_wash'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    $wp_customize->add_setting( 'img_service',
        array(
            'default' => get_template_directory_uri() . '/images/images3.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_service',
        array(
            'label' => esc_html__( 'Image','car_wash' ),
            'description' => esc_html__( 'Upload image','car_wash' ),
            'section' => 'service_settings',
        )
    ) );

    $wp_customize->add_setting( 'service_icon_1',
        array(
            'default' => get_template_directory_uri() . '/images/icon1.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_icon_1',
        array(
            'label' => esc_html__( 'Service icon 1','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'service_settings',
        )
    ) );
    $wp_customize->add_setting( 'service_title_1',
        array(
            'default' => 'Best & Easy Convinence',
        )
    );
    $wp_customize->add_control( 'service_title_1',
        array(
            'label' => esc_html__( 'Title service 1','car_wash' ),
            'section' => 'service_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_content_1',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','car_wash'),
        )
    );
    $wp_customize->add_control( 'service_content_1',
        array(
            'label' => esc_html__( 'Content service 1','car_wash' ),
            'section' => 'service_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting( 'service_link_1',
        array(
            'default' => esc_html__('#','car_wash'),

        )
    );
    $wp_customize->add_control('service_link_1',
        array(
            'label' => esc_html__( 'Service 1 URL','car_wash' ),
            'section' => 'service_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_icon_2',
        array(
            'default' => get_template_directory_uri() . '/images/icon2.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_icon_2',
        array(
            'label' => esc_html__( 'Service icon 2','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'service_settings',
        )
    ) );
    $wp_customize->add_setting( 'service_title_2',
        array(
            'default' => 'Full Organic Products',
        )
    );
    $wp_customize->add_control( 'service_title_2',
        array(
            'label' => esc_html__( 'Title service 2','car_wash' ),
            'section' => 'service_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_content_2',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','car_wash'),
        )
    );
    $wp_customize->add_control( 'service_content_2',
        array(
            'label' => esc_html__( 'Content service 2','car_wash' ),
            'section' => 'service_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting( 'service_link_2',
        array(
            'default' => esc_html__('#','car_wash'),

        )
    );
    $wp_customize->add_control('service_link_2',
        array(
            'label' => esc_html__( 'Service 2 URL','car_wash' ),
            'section' => 'service_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_icon_3',
        array(
            'default' => get_template_directory_uri() . '/images/icon3.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_icon_3',
        array(
            'label' => esc_html__( 'Service icon 3','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'service_settings',
        )
    ) );
    $wp_customize->add_setting( 'service_title_3',
        array(
            'default' => 'Fuly Experience Team',
        )
    );
    $wp_customize->add_control( 'service_title_3',
        array(
            'label' => esc_html__( 'Title service 3','car_wash' ),
            'section' => 'service_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_content_3',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','car_wash'),
        )
    );
    $wp_customize->add_control( 'service_content_3',
        array(
            'label' => esc_html__( 'Content service 3','car_wash' ),
            'section' => 'service_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting( 'service_link_3',
        array(
            'default' => esc_html__('#','car_wash'),

        )
    );
    $wp_customize->add_control('service_link_3',
        array(
            'label' => esc_html__( 'Service 3 URL','car_wash' ),
            'section' => 'service_settings',
            'type' => 'text',
        )
    );


    $wp_customize->add_setting( 'service_icon_4',
        array(
            'default' => get_template_directory_uri() . '/images/icon4.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'service_icon_4',
        array(
            'label' => esc_html__( 'Service icon 4','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'service_settings',
        )
    ) );
    $wp_customize->add_setting( 'service_title_4',
        array(
            'default' => 'Fuly Experience Team',
        )
    );
    $wp_customize->add_control( 'service_title_4',
        array(
            'label' => esc_html__( 'Title service 4','car_wash' ),
            'section' => 'service_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_content_4',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','car_wash'),
        )
    );
    $wp_customize->add_control( 'service_content_4',
        array(
            'label' => esc_html__( 'Content service 4','car_wash' ),
            'section' => 'service_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting( 'service_link_4',
        array(
            'default' => esc_html__('#','car_wash'),

        )
    );
    $wp_customize->add_control('service_link_4',
        array(
            'label' => esc_html__( 'Service 4 URL','car_wash' ),
            'section' => 'service_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'service_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'service_show',
        array(
            'label' => esc_html__( 'Show Section','car_wash' ),
            'section' => 'service_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','car_wash' ),
                'yes' => esc_html__( 'Yes','car_wash' ),
            )
        )
    );
    // End price

    // start Blog
    $wp_customize->add_section( 'blog_settings',
        array(
            'title' => esc_html__( 'Blog Section','car_wash'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );
    $wp_customize->add_setting( 'title_blog',
        array(
            'default' => esc_html__('Recent Blog Posts','car_wash'),
        )
    );
    $wp_customize->add_control('title_blog',
        array(
            'label' => esc_html__( 'Title Section','car_wash' ),
            'section' => 'blog_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'number_post_blog',
        array(
            'default' => esc_html__('3','car_wash'),
        )
    );
    $wp_customize->add_control('number_post_blog',
        array(
            'label' => esc_html__( 'Number Post','car_wash' ),
            'section' => 'blog_settings',
            'type' => 'text',
        )
    );

    $wp_customize->add_setting( 'blog_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'blog_show',
        array(
            'label' => esc_html__( 'Show Section','car_wash' ),
            'section' => 'blog_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','car_wash' ),
                'yes' => esc_html__( 'Yes','car_wash' ),
            )
        )
    );
    // End work

    // start Testimonials
    $wp_customize->add_section( 'testimonials_settings',
        array(
            'title' => esc_html__( 'Testimonials Section','car_wash'  ),
            'priority' => 20,
            'panel'  => 'home_page_setting',
        )
    );

    $wp_customize->add_setting( 'testimonials_title',
        array(
            'default' => 'Read words from our customers',
        )
    );
    $wp_customize->add_control( 'testimonials_title',
        array(
            'label' => esc_html__( 'Testimonials Title','car_wash' ),
            'section' => 'testimonials_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'testimonials_sub_title',
        array(
            'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore. Nisi porta lorem mollis aliquam ut porttitor leo.',
        )
    );
    $wp_customize->add_control( 'testimonials_sub_title',
        array(
            'label' => esc_html__( 'Sub Title','car_wash' ),
            'section' => 'testimonials_settings',
            'type' => 'textarea',
        )
    );

    $wp_customize->add_setting( 'testimonials_img',
        array(
            'default' => get_template_directory_uri() . '/images/images7.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'testimonials_img',
        array(
            'label' => esc_html__( 'Image','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'testimonials_settings',
        )
    ) );

    $wp_customize->add_setting( 'testimonials_show',
        array(
            'default' => 'yes',
        )
    );
    $wp_customize->add_control( 'testimonials_show',
        array(
            'label' => esc_html__( 'Show Section','car_wash' ),
            'section' => 'testimonials_settings',
            'type' => 'select',
            'choices' => array(
                'no' => esc_html__( 'No','car_wash' ),
                'yes' => esc_html__( 'Yes','car_wash' ),
            )
        )
    );
    // End Testimonials

	// start Contact
	$wp_customize->add_section( 'contact_settings',
	   array(
	      'title' => esc_html__( 'Contact Section','car_wash'  ),
	      'priority' => 20, 
		  'panel'  => 'home_page_setting',
	   )
	);
    $wp_customize->add_setting( 'title_contact',
        array(
            'default' => esc_html__('Make an Appointment for Your Car Wash','car_wash'),
        )
    );
    $wp_customize->add_control('title_contact',
        array(
            'label' => esc_html__( 'Title Section','car_wash' ),
            'section' => 'contact_settings',
            'type' => 'text',
        )
    );
    $wp_customize->add_setting( 'sub_title_contact',
        array(
            'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.Nisi porta lorem mollis aliquam ut porttitor leo.','car_wash'),
        )
    );
    $wp_customize->add_control('sub_title_contact',
        array(
            'label' => esc_html__( 'Sub title Section','car_wash' ),
            'section' => 'contact_settings',
            'type' => 'textarea',
        )
    );
	$wp_customize->add_setting( 'contact_shortcode',
	   array(
	      'default' => '[contact-form-7 id="49" title="Contact form home"]',
	   )
	);	
	$wp_customize->add_control( 'contact_shortcode',
	   array(
	      'label' => esc_html__( 'Contact Shortcode','car_wash' ),
	      'section' => 'contact_settings',
	      'type' => 'text',
	   )
	);
    $wp_customize->add_setting( 'call_info',
        array(
            'default' => esc_html__('Or Call 800 200 100','car_wash'),
        )
    );
    $wp_customize->add_control('call_info',
        array(
            'label' => esc_html__( 'Call','car_wash' ),
            'section' => 'contact_settings',
            'type' => 'textarea',
        )
    );
    $wp_customize->add_setting( 'contact_info',
        array(
            'default' => "<p>123 Street, City, State 45678</p><p>Phone: 1234 56 7890</p><div class=\"mail\"><a href=\"#\">info@mistercarwashs.com</a></div><div class=\"mail\"><a href=\"#\">mistercarwashs.com</a></div>",
        )
    );
    $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'contact_info',
        array(
            'label' => esc_html__( 'Contact info','car_wash' ),
            'description' => esc_html__( 'About content','car_wash' ),
            'section' => 'contact_settings',
        )
    ) );
    $wp_customize->add_setting( 'bg_contact',
        array(
            'default' => get_template_directory_uri() . '/images/banner-form.jpg',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_contact',
        array(
            'label' => esc_html__( 'Background','car_wash' ),
            'description' => esc_html__( 'Upload Image','car_wash' ),
            'section' => 'contact_settings',
        )
    ) );
	$wp_customize->add_setting( 'contact_show',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'contact_show',
	   array(
            'label' => esc_html__( 'Show Section','car_wash' ),
            'section' => 'contact_settings',
            'type' => 'select',
            'choices' => array( 
	         'no' => esc_html__( 'No','car_wash' ),
	         'yes' => esc_html__( 'Yes','car_wash' ),
            )
	   )
	);
	// End Contact
		
	// End Home Options
	
	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','car_wash'  ),
	      'priority' => 35, 
	   )
	);
    $wp_customize->add_setting( 'logo_footer',
        array(
            'default' => get_template_directory_uri() . '/images/logo-footer.png',
        )
    );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_footer',
        array(
            'label' => esc_html__( 'Logo footer','car_wash' ),
            'description' => esc_html__( 'Upload Logo','car_wash' ),
            'section' => 'footer_settings',
        )
    ) );
	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '<p class="text-center m-0">(C) 2019, All Rights Reserved. Mistercarswash. Designed and Developed by <a href="#">Template.net</a></p>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','car_wash' ),
	      'section' => 'footer_settings',
	   )
	) );	
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','car_wash'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','car_wash' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','car_wash' ),
	         'yes' => esc_html__( 'Yes','car_wash' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','car_wash'),
		'instagram'=> esc_html__('Instagram','car_wash'),
		'twitter'=> esc_html__('Twitter','car_wash'),
		'google'=> esc_html__('Google','car_wash'),
        'pinterest'=> esc_html__('Pinterest','car_wash'),
		'linkedin'=> esc_html__('Linkedin','car_wash')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','car_wash' ),
				 'yes' => esc_html__( 'Yes','car_wash' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','car_wash'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','car_wash'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','car_wash' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','car_wash' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','car_wash' ),
	         'yes' => esc_html__( 'Yes','car_wash' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','car_wash' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','car_wash' ),
	         'yes' => esc_html__( 'Yes','car_wash' ),
	      )
	   )
	);
}
/**
 * Theme options in the Customizer js
 * @package car_wash
 */
 
function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'car_wash_customize_admin_menu_hide', 999 );
function car_wash_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="car_wash-theme-options"><span>'.esc_html__( 'car_wash','car_wash' ).'</span>'.esc_html__( 'Theme Options','car_wash' ).'</div>',
         '<div class="car_wash-theme-options"><span>'.esc_html__( 'car_wash','car_wash' ).'</span>'.esc_html__( 'Theme Options','car_wash' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){
                $input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );
			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}