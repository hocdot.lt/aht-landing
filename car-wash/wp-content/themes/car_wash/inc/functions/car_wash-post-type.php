<?php
/**
 * Register "Testimonials" post type
 * @return [type] [description]
 */
function register_testimonial_post_type() {

	$labels = array(
	    'name' => __('Testimonials', 'car_wash'),
	    'singular_name' => __('Testimonials', 'car_wash'),
	    'add_new' => __('Add New', 'car_wash'),
	    'add_new_item' => __('Add New Testimonial', 'car_wash'),
	    'edit_item' => __('Edit Testimonial', 'car_wash'),
	    'new_item' => __('New Testimonial', 'car_wash'),
	    'all_items' => __('All Testimonial', 'car_wash'),
	    'view_item' => __('View Testimonial', 'car_wash'),
	    'search_items' => __('Search Testimonials', 'car_wash'),
	    'not_found' =>  __('No Testimonials found', 'car_wash'),
	    'not_found_in_trash' => __('No Testimonials found in Trash', 'car_wash'),
	    'parent_item_colon' => '',
	    'menu_name' => __('Testimonials', 'car_wash')
	  );

  	$args = array(
	    'labels' => $labels,
	    'public' => false,
	    'exclude_from_search' => true,
	    'publicly_queryable' => true,
	    'show_ui' => true,
	    'show_in_menu' => true,
	    'query_var' => true,
	    'rewrite' => array( 'slug' => 'testimonial' ),
	    'capability_type' => 'post',
	    'has_archive' => false,
	    'hierarchical' => false,
	    'menu_position' => 50,
	    'menu_icon' => 'dashicons-groups',
	    'supports' => array( 'title','editor', 'thumbnail', 'revisions','page-attributes')
  	);

	register_post_type( 'testimonial', $args);

}
add_action( 'init', 'register_testimonial_post_type');

/**
 * Customize Rug table overview tables
 */

function add_new_testimonial_table_columns($gallery_columns) {
    $new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['image'] = __('Image', 'car_wash');
    $new_columns['title'] = _x('Name', 'column name', 'car_wash');
    $new_columns['date'] = _x('Date', 'column name', 'car_wash');

    return $new_columns;
}
// Add to admin_init function
add_filter('manage_testimonial_posts_columns', 'add_new_testimonial_table_columns');
function manage_testimonial_table_columns($column_name, $id) {
    global $wpdb;
	$featured_img_url = get_the_post_thumbnail_url($id, 'thumbnail');
    switch ($column_name) {
	    case 'image':
			if(!empty($featured_img_url)){
				echo '<img src="'. $featured_img_url .'" width="80" >';
			}else{
				echo 'No image';
			}
	        break;
	    default:
	        break;
    } // end switch
}
// Add to admin_init function
add_action('manage_testimonial_posts_custom_column', 'manage_testimonial_table_columns', 10,2);

