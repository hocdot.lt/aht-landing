<?php 
/**
Template Name: Home
*/

get_header();

get_template_part('part-templates/home-banner');
get_template_part('part-templates/home-section-one');
get_template_part('part-templates/home-section-two');
get_template_part('part-templates/hone-section-three');
get_template_part('part-templates/home-section-for');
get_template_part('part-templates/home-section-five');
get_template_part('part-templates/home-section-zoom');


get_footer(); ?>