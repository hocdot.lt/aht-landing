</div> <!-- End main-->
<?php
/**
 * Render banner in home page
 */
//GET config
$Contactshow = get_theme_mod('contact_show','yes');
$LogoContact = get_theme_mod('logo_contact',get_template_directory_uri() . '/images/icon-footer.png');
$bgContact = get_theme_mod('Background_contact',get_template_directory_uri() . '/images/banner-footer.jpg');
$TitleLogo = get_theme_mod('title_conatct','Amanda Fit');
$SubTitleLogo = get_theme_mod('sub_title_contact','Personal Trainer');
$desclogo = get_theme_mod('desc_title_contact','Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus lacus odio, at convallis dui molestie et. Nunc luctus porttitor mauris ut luctus. sapien sit amet, pharetra dolor. In tempus lacus odio,at convallis dui molestie et. Nunc luctus porttitor...');
$Linklogo = get_theme_mod('link_title_contact','#');
$titleMenu = get_theme_mod('title_menu_contact','Site Navigation');
$titletabContact = get_theme_mod('title_tab_contact','Contacts');
$linktab = get_theme_mod('link_tab_contact','#');
$titleLink = get_theme_mod('link_title_tab_contact','Amanda Fit');
$ndInfo = get_theme_mod('info_tab_contact','123 Street, City, State 45678');
$Phone = get_theme_mod('Phone_tab_contact','1234 56 7890');
$Mail1 = get_theme_mod('Mail_1_tab_contact','info@amandafit.com');
$Mail2 = get_theme_mod('Mail_2_tab_contact','amandsfitstudion.com');
?>
 <footer id="contact">
      <div class="banner-footer" style="background-image: url(<?php echo $bgContact?>);">
        <div class="container">
          <div class="row py-5">
            <div class="col-lg-4">
              <div class="logo-footer">
                <div class="logo-ft"><img src="<?php echo $LogoContact?>"></div>
                <div class="desc-logo">
                  <h5><?php echo $TitleLogo ?></h5>
                  <p><?php echo $SubTitleLogo ?></p>
                </div>
              </div>
              <div class="desc-footer">
                <p>
                <?php echo $desclogo?>
                </p>
                <div class="link-xemthem"><a href="<?php echo $Linklogo?>">Read More</a></div>
              </div>
            </div>
            <div class="col-lg-1">
              <div class="nav-footer">
                <h5><?php echo $titleMenu?></h5>
                <div class="nav">
                 <?php
                     if (has_nav_menu('footer')) {
                       wp_nav_menu(array(
                         'theme_location' => 'footer',
                         'menu_class' => 'mega-menu',
                         'items_wrap' =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
                             )
                            );
                         }
                     ?>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <div class="info-footer">
                <h5><?php echo $titletabContact?></h5>
                <div class="content-info"><a class="linktop" href="<?php echo $linktab?>"><?php echo $titleLink?></a>
                  <p><?php echo $ndInfo?></p>
                  <p><?php echo $Phone?></p>
                  <div class="mail"><a href="#"><?php echo $Mail1?></a></div>
                  <div class="mail mail-bb"><a href="#"><?php echo $Mail2?></a></div>
                </div>
              </div>
              <div class="icon-mxh clear-fix">
            <?php
            if(function_exists('personal_trainer_social_link')){
                     personal_trainer_social_link();
                  }
            ?>
            </div>
            </div>
          </div>
        </div>
      </div>
      <div class="banquyen">
      <?php
                 $copyright =  get_theme_mod('copyright','<p class="text-center m-0">(C) All Rights Reserved. Charity Theme, Designed & Developed by<a href="#">Template.net</a></p>');;
                 if ( $copyright !='') : ?>
                 <?php echo wp_kses_post( $copyright); ?>
            <?php endif;?>


      </div>
    </footer>
</div> <!-- End page-->
<?php wp_footer(); ?>
</body>
</html>
