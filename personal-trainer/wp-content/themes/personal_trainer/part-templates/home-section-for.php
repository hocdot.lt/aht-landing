<?php
/**
 * Render section-two in home page
 */
//GET config
$sectionforShow = get_theme_mod('section_for_show','yes');
$Titlesectionfor = get_theme_mod('title_section_for','My Articles on Professional Fitness Training');
$subtitleSectionFor = get_theme_mod('sub_title_section_for','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$ForCategory = get_theme_mod('section_for_category','');
$ForLimit = get_theme_mod('section_for_number_post',3);

$args = array(
	'posts_per_page'   => $ForLimit,
	'offset'           => 0,
	'cat'         => $ForCategory,
	'post_type'        => 'post',
  'post_status'    => 'publish',
  'order'         => 'DESC',
);
$posts_array = get_posts( $args );
$i = 0;
?>
<?php if($sectionforShow ==='yes'): ?>
 <section id="section-for">
      <div class="container">
        <div class="row">
          <div class="col-lg-5">
            <div class="header-content-for">
              <h3><?php echo $Titlesectionfor ?></h3>
              <div class="desc-header-content-for">
                <p> <?php echo $subtitleSectionFor?></p>
              </div>
            </div>
          </div>
        </div>
        <?php foreach ($posts_array as $key => $_post): setup_postdata( $_post );?>
          <?php if($i == 0): ?>  
            <div class="row">
              <div class="col-lg-4">
                <div class="img-content"><?php echo get_the_post_thumbnail($_post->ID,'blog'); ?></div>
              </div>
              <div class="col-lg-4">
                <div class="text-content">
                  <h3 class="hd-text"><?php echo get_the_title($_post->ID) ?></h3>
                  <p class="text-body">
                    <?php echo wp_trim_words( get_the_excerpt($_post->ID), $num_words = 23, '' ) ?>
                  </p>
                  <a class="btn btn-section-for" href="<?php echo get_post_permalink($_post->ID) ?>">Continue Reading</a>
                </div>
              </div>
            </div>
            <div class="row post">
          <?php else: ?>
            <div class="col-lg-4" id="<?php echo ($i%2)?"col-main-left":"col-main-right";?>">
              <div class="row">
                <div class="col-lg-4">
                  <div class="img-post">
                  <?php echo get_the_post_thumbnail($_post->ID,'blog-2'); ?>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="text-post">
                    <a class="link-post" href="<?php echo get_post_permalink($_post->ID) ?>"><?php echo get_the_title($_post->ID) ?></a>
                    <p class="desc-post"><?php echo wp_trim_words( get_the_excerpt($_post->ID), $num_words = 20, '' ) ?></p>
                    <a class="btn-post" href="<?php echo get_post_permalink($_post->ID) ?>">Continue...</a>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?> 
        <?php 
            $i++;
            endforeach;wp_reset_postdata();
            if($posts_array) echo "</div>";
        ?>   
      </div>
    </section>
    <?php endif ?>