<?php
/**
 * Render section-two in home page
 */
//GET config
$sectionTwoShow = get_theme_mod('section_two_show','yes');
$titlesectionTwoShow = get_theme_mod('title_section_two','My Professional Training Courses');
$SubtitlesectionTwoShow = get_theme_mod('sub_title_section_two','Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
//post1
$imgPost1 = get_theme_mod('images_1',get_template_directory_uri() . '/images/img-1.jpg');
$TitlePost1 = get_theme_mod('title_images_1','YOGA BASICS');
$linkPost1 = get_theme_mod('link_title','#');
$PricePost1 = get_theme_mod('price_1','49');
$SubPost1 = get_theme_mod('sub_title_1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.');
//post2
$imgPost2 = get_theme_mod('images_2',get_template_directory_uri() . '/images/img-2.jpg');
$TitlePost2 = get_theme_mod('title_images_2','CARDIO TRAINING');
$linkPost2 = get_theme_mod('link_title_2','#');
$PricePost2 = get_theme_mod('price_2','49');
$SubPost2 = get_theme_mod('sub_title_2','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.');
//post3
$imgPost3 = get_theme_mod('images_3',get_template_directory_uri() . '/images/img-3.jpg');
$TitlePost3 = get_theme_mod('title_images_3','CROSS FIT TRAINING');
$linkPost3 = get_theme_mod('link_title_3','#');
$PricePost3 = get_theme_mod('price_3','99');
$SubPost3 = get_theme_mod('sub_title_3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.');
$SectionTwoLimit = get_theme_mod('section_two_number_post',4);
$args = array(
	'posts_per_page'   => $SectionTwoLimit,
	'offset'           => 0,
	'post_type'        => 'testimonial',
);
$testimonial_array = get_posts( $args );
?>
<?php if($sectionTwoShow === 'yes'): ?>
 <section class="section section-no-background section-no-border section-footer m-0" id="section-two">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="header-content-two">
          <h3><?php echo $titlesectionTwoShow?></h3>
          <div class="desc-header-content-two">
            <p>  <?php echo $SubtitlesectionTwoShow?></p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md">
        <div class="item-section-two"><img class="img-fluid" src="<?php echo $imgPost1 ?>" alt="<?php echo $TitlePost1; ?>">
          <div class="price">
            <div class="ct-price">
              <p><?php echo $PricePost1?><span>$</span></p>
            </div>
          </div>
          <div class="recent-posts">
            <article class="post">
              <h5><a href="<?php echo $linkPost1?>"><?php echo $TitlePost1?></a></h5>
              <p><?php echo $SubPost1?></p>
            </article>
          </div>
        </div>
      </div>
      <div class="col-md">
        <div class="item-section-two"><img class="img-fluid" src="<?php echo $imgPost2 ?>" alt="<?php echo $TitlePost2; ?>">
          <div class="price">
            <div class="ct-price">
              <p><?php echo $PricePost2?><span>$</span></p>
            </div>
          </div>
          <div class="recent-posts">
            <article class="post">
              <h5><a href="<?php echo $linkPost2?>"><?php echo $TitlePost2?></a></h5>
              <p><?php echo $SubPost2?></p>
            </article>
          </div>
        </div>
      </div>
      <div class="col-md">
        <div class="item-section-two"><img class="img-fluid" src="<?php echo $imgPost3 ?>" alt="<?php echo $TitlePost3?>">
          <div class="price">
            <div class="ct-price">
              <p><?php echo $PricePost3?><span>$</span></p>
            </div>
          </div>
          <div class="recent-posts">
            <article class="post">
              <h5><a href="<?php echo $linkPost3?>"><?php echo $TitlePost3?></a></h5>
              <p><?php echo $SubPost3?></p>
            </article>
          </div>
        </div>
      </div>
    </div>
    <div class="row info-sective-two">
      <?php foreach ($testimonial_array as $_post): setup_postdata( $_post ); ?>
        <div class="col-lg-2 appear-animation animated fadeInRightShorter appear-animation-visible" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200" style="animation-delay: 200ms;">
          <div class="featured-box featured-box-effect-4">
            <div class="box-content">
              <div class="img-box"><img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($_post->ID,'thumbnail'); ?>"></div>
              <div class="title-box">
                <h4 class="font-weight-bold"><?php echo get_the_title($_post->ID);?></h4>
                <p class="desc-box"><?php echo get_post_meta($_post->ID,'testimonial_position',true);?></p>
              </div>
              <p><?php echo wp_trim_words( get_the_excerpt($_post->ID), $num_words = 20, '' ) ?></p>
            </div>
          </div>
        </div>
      <?php endforeach; wp_reset_postdata();?>
    </div>
  </div>
</section>

<?php endif ?>