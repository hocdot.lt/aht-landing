<?php
/**
 * Render section-five in home page
 */
//GET config
$sectionFiveShow = get_theme_mod('section_five_show','yes');
$BanneresectionFive = get_theme_mod('background_section_five',get_template_directory_uri() . '/images/banner2.jpg');
$TitlesectionFive = get_theme_mod('title_section_five','“See what my clients have to say aboutmy training and activities”');
$SubTitlesectionFive = get_theme_mod('sub_title_section_five','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

$newsletter_title = get_theme_mod('newsletter_title','Subscribe To My Weekly Newsletter');
$newsletter_desc = get_theme_mod('newsletter_desc','Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus lacus odio, at convallis dui molestie et. Nunc luctus porttitor mauris ut luctus. sapien sit amet, pharetra dolor.');
$contactShortcode = get_theme_mod('form_shortcode','[mc4wp_form id="6"]');
//slider1
$descslider1 = get_theme_mod('desc_1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$Namelider1 = get_theme_mod('name_1','Amanda Smith');
$Imglider1 = get_theme_mod('avatar_1',get_template_directory_uri() . '/images/click3.png');
//slider2
$descslider2 = get_theme_mod('desc_2','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$Namelider2 = get_theme_mod('name_2','Amanda Smith');
$Imglider2 = get_theme_mod('avatar_2',get_template_directory_uri() . '/images/click2.png');
//slider3
$descslider3 = get_theme_mod('desc_3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
$Namelider3 = get_theme_mod('name_3','Amanda Smith');
$Imglider3 = get_theme_mod('avatar_3',get_template_directory_uri() . '/images/click1.png');
?>
<?php if ($sectionFiveShow === 'yes'): ?>
 <section id="section-five">
      <div class="banner-section-five" style="background-image: url(<?php echo $BanneresectionFive ?>);">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">
              <div class="content-five-left">
                <div class="header-left">
                  <h3><?php echo $TitlesectionFive ?></h3>
                </div>
                <div class="text-left">
                  <p>
                   <?php echo $SubTitlesectionFive ?>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
                <div class="content-five-right">
                  <div class="container">
                    <div class="row">
                      <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10,'dotsData':true,'dotsEach':true,'loop':true}">
                        <div data-dot="&lt;img src='<?php echo $Imglider1?>'&gt;">
                          <div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark mb-0">
                            <blockquote>
                              <p class="text-5 line-height-5 mb-0">
                               <?php echo $descslider1?>
                              </p>
                            </blockquote>
                            <div class="testimonial-author">
                              <p><strong class="font-weight-extra-bold text-2"> <?php echo $Namelider1?></strong></p>
                            </div>
                          </div>
                        </div>
                        <div data-dot="&lt;img src='<?php echo $Imglider2?>'&gt;">
                          <div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark mb-0">
                            <blockquote>
                              <p class="text-5 line-height-5 mb-0">
                                  <?php echo $descslider2?>
                              </p>
                            </blockquote>
                            <div class="testimonial-author">
                              <p><strong class="font-weight-extra-bold text-2"><?php echo $Namelider2?></strong></p>
                            </div>
                          </div>
                        </div>
                        <div data-dot="&lt;img src='<?php echo $Imglider3?>'&gt;">
                          <div class="testimonial testimonial-style-2 testimonial-with-quotes testimonial-quotes-dark mb-0">
                            <blockquote>
                              <p class="text-5 line-height-5 mb-0">
                               <?php echo $descslider3?>
                              </p>
                            </blockquote>
                            <div class="testimonial-author">
                              <p><strong class="font-weight-extra-bold text-2"><?php echo $Namelider3?></strong></p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="form-section-five">
        <div class="container">
          <div class="row justify-content-md-center">
            <div class="col-lg-6">
              <div class="content-form-section-five">
                <h3 class="td-section-five"><?php echo $newsletter_title ?></h3>
                 <p class="desc-section-five"><?php echo $newsletter_desc ?></p>
                 <?php echo do_shortcode($contactShortcode)?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php endif ?>