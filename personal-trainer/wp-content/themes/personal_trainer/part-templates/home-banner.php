<?php
/**
 * Render banner in home page
 */
//GET config
$bannerShow = get_theme_mod('home_banner_settings','yes');
$bgBanner = get_theme_mod('background_banner',get_template_directory_uri() . '/images/banner.jpg');
$bgBannerVideo = get_theme_mod('background_banner_right',get_template_directory_uri() . '/images/img-video.jpg');
$titleBanner = get_theme_mod('banner_title','<h1>GET IN, GETFIT.</h1>');
$titleBannerVideo = get_theme_mod('title_video','Amandas 5-Minute Fat-Blasting Workout');
$subtitleBanner = get_theme_mod('banner_sub_title','Personalized Training by Experienced Tutors and Institutes');
$buttonBanner = get_theme_mod('button_banner','Try Free Class');
$buttonBannerLink = get_theme_mod('link_banner','#');
$VideoBannerLink = get_theme_mod('link_video','https://vimeo.com/45830194');
?>
<?php if($bannerShow === 'yes'): ?>
<div class="forcefullwidth_wrapper_tp_banner" id="revolutionSlider_forcefullwidth" style="position:relative;width:100%;height:auto;margin-top:0px;margin-bottom:0px;background-image: url(<?php echo $bgBanner ?>);">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-5">
            <div class="row justify-content-end">
              <div class="col-lg-6">
                <div class="content-left-banner">
                <?php if($titleBanner):?>
                  <?php echo $titleBanner ?>
                  <?php endif ?>
                  <?php if($subtitleBanner): ?>
                  <p><?php echo $subtitleBanner ?></p>
                  <?php endif ?>
                  <div class="btn-header">
                  <?php if($buttonBanner): ?>
                    <h4><a href="<?php echo $buttonBannerLink ?>"><?php echo $buttonBanner ?></a></h4>
                    <?php endif ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 content-right-main">
            <div class="content-right-banner">
              <div class="featured-boxes featured-boxes-modern-style-1">
                <div class="featured-box overlay overlay-show overlay-op-7 py-3">
                  <div class="featured-box-background" style="background-image: url(<?php echo $bgBannerVideo ?>); background-size: cover; background-position: center;"></div>
                  <div class="box-content"><a class="text-decoration-none lightbox" href="<?php echo $VideoBannerLink ?>" data-plugin-options="{'type':'iframe'}"><i class="fas fa-play featured-icon featured-icon-style-2 featured-icon-hover-effect-1 right-4 top-0 m-0"></i></a>
                    <?php if($titleBannerVideo): ?>
                    <p class="pt-1"><?php echo $titleBannerVideo ?></p>
                    <?php endif ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
 <?php endif ?>