<?php
/**
 * Render banner in home page
 */
//GET config
$sectionOneShow = get_theme_mod('section_one_settings','yes');
$titleSectionOne = get_theme_mod('title_section_one','<h2>Achieve Your FitnesGoal EffectivelyNow! Get The Best Fitness Trainer</h2>');
$linkSectionOne = get_theme_mod('link_main_section_one','#');
$ButtonSectionOne = get_theme_mod('button_section_one','Make an appointment with personal trainer');
$Imagesbottom = get_theme_mod('images_bottom',get_template_directory_uri() . '/images/bg-content.jpg');
$Titlepost = get_theme_mod('title_content_bottom','Little About Myself!');
$Linkpost = get_theme_mod('link_title_content_bottom','#');
$Descpost = get_theme_mod('Desc_title_content_bottom','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.');
$sub1post = get_theme_mod('sub1_title_content_bottom','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor siLorem ipsum dolor sit amet, consectetur dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit,');
$sub2post = get_theme_mod('sub2_title_content_bottom','Sd do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut ');
//box1
$Icon1 = get_theme_mod('icon_1',get_template_directory_uri() . '/images/gym.png');
$TitleIcon1 = get_theme_mod('title_icon_1','Make your body fit');
$SubTitleIcon1 = get_theme_mod('sub_title_icon_1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.');
//box2
$Icon2 = get_theme_mod('icon_2',get_template_directory_uri() . '/images/runner.png');
$TitleIcon2 = get_theme_mod('title_icon_2','Make your body fit');
$SubTitleIcon2 = get_theme_mod('sub_title_icon_2','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.');
//box3
$Icon3 = get_theme_mod('icon_3',get_template_directory_uri() . '/images/woman.png');
$TitleIcon3 = get_theme_mod('title_icon_3','Make your body fit');
$SubTitleIcon3 = get_theme_mod('sub_title_icon_3','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.');
//box3
$Icon4 = get_theme_mod('icon_4',get_template_directory_uri() . '/images/gym2.png');
$TitleIcon4 = get_theme_mod('title_icon_4','Make your body fit');
$SubTitleIcon4 = get_theme_mod('sub_title_icon_4','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.');
?>
<?php if($sectionOneShow === 'yes'): ?>
<section id="section-one">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
            <div class="row justify-content-md-center header-content">
              <div class="col-lg-4">
              <?php echo $titleSectionOne?>
               </div>
            </div>
            <div class="row">
              <div class="col-lg-2">
                <div class="feature-box feature-box-style-2">
                  <div class="feature-box-icon"><i class="icons"><img src="<?php echo $Icon1; ?>"></i></div>
                  <div class="feature-box-color">
                    <div class="content-box-color box-color1"></div>
                  </div>
                  <div class="feature-box-info">
                    <h4 class="font-weight-bold text-4 mb-0"><?php echo $TitleIcon1?></h4>
                    <p class="mb-4">
                    <?php echo $SubTitleIcon1 ?>
                </p>
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="feature-box feature-box-style-2">
                  <div class="feature-box-icon"><i class="icons"><img src="<?php echo $Icon2; ?>"></i></div>
                  <div class="feature-box-color">
                    <div class="content-box-color box-color2"></div>
                  </div>
                  <div class="feature-box-info">
                    <h4 class="font-weight-bold text-4 mb-0"><?php echo $TitleIcon2 ?></h4>
                    <p class="mb-4">
                    <?php echo $SubTitleIcon2 ?>
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="feature-box feature-box-style-2">
                  <div class="feature-box-icon"><i class="icons"><img src="<?php echo $Icon3;?>"></i></div>
                  <div class="feature-box-color">
                    <div class="content-box-color box-color3"></div>
                  </div>
                  <div class="feature-box-info">
                   <h4 class="font-weight-bold text-4 mb-0"><?php echo $TitleIcon3?></h4>
                    <p class="mb-4">
                    <?php echo $SubTitleIcon3 ?>
                    </p>
                </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="feature-box feature-box-style-2">
                  <div class="feature-box-icon"><i class="icons"><img src="<?php echo $Icon4;?>"></i></div>
                  <div class="feature-box-color">
                    <div class="content-box-color box-color4"></div>
                  </div>
                  <div class="feature-box-info">
                    <h4 class="font-weight-bold text-4 mb-0"> <?php echo $TitleIcon4 ?></h4>
                    <p class="mb-4">
                     <?php echo $SubTitleIcon4 ?>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="btn-main-section1">
          <div class="row justify-content-md-center">
            <div class="col-lg-4">
              <div class="btn-section">
                <a class="btn" href=" <?php echo $linkSectionOne?>"><?php echo $ButtonSectionOne ?></a>
              </div>
            </div>
          </div>
        </div>
        <div class="content-botom-section1">
          <div class="row">
            <div class="col-lg-4">
              <div class="img-content"><img src="<?php echo $Imagesbottom?>"></div>
            </div>
            <div class="col-lg-4">
              <div class="text-content">
                <a href="<?php echo $Linkpost?>" class="hd-text"><?php echo $Titlepost?></a>
                <p class="desc-content">
                <?php echo $Descpost?>
                </p>
                <p class="text-body">
                <?php echo $sub1post?>
                </p>
                <p class="text-body">
                <?php echo $sub2post?>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php endif ?>