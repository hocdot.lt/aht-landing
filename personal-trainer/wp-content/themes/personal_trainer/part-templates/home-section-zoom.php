<?php
/**
 * Render section-two in home page
 */
//GET config
$sectionZoomShow = get_theme_mod('section_zoom_show','yes');

 //item1
 $Thumb1 = get_theme_mod('thumb_img',get_template_directory_uri() . '/images/zoom5.jpg');
 $Zoom1 = get_theme_mod('zoom_img',get_template_directory_uri() . '/images/bg-content.jpg');
 //item2
 $Thumb2 = get_theme_mod('thumb_2_img',get_template_directory_uri() . '/images/zoom6.jpg');
 $Zoom2 = get_theme_mod('zoom_2_img',get_template_directory_uri() . '/images/bg-content.jpg');
 //item3
 $Thumb3 = get_theme_mod('thumb_3_img',get_template_directory_uri() . '/images/zoom4.jpg');
 $Zoom3 = get_theme_mod('zoom_3_img',get_template_directory_uri() . '/images/bg-content.jpg');
 //item4
 $Thumb4 = get_theme_mod('thumb_4_img',get_template_directory_uri() . '/images/zoom3.jpg');
 $Zoom4 = get_theme_mod('zoom_4_img',get_template_directory_uri() . '/images/bg-content.jpg');
 //item5
 $Thumb5 = get_theme_mod('thumb_5_img',get_template_directory_uri() . '/images/zoom2.jpg');
 $Zoom5 = get_theme_mod('zoom_5_img',get_template_directory_uri() . '/images/bg-content.jpg');
 //item16
 $Thumb6 = get_theme_mod('thumb_6_img',get_template_directory_uri() . '/images/zoom1.jpg');
 $Zoom6 = get_theme_mod('zoom_6_img',get_template_directory_uri() . '/images/bg-content.jpg');
 //item7
 $Thumb7 = get_theme_mod('thumb_7_img',get_template_directory_uri() . '/images/zoom7.jpg');
 $Zoom7 = get_theme_mod('zoom_7_img',get_template_directory_uri() . '/images/bg-content.jpg');
 ?>
 <?php if ($sectionZoomShow === 'yes'):?>
 <section id="section-zoom">
      <article class="post post-large">
        <div class="post-image">
          <div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}, 'mainClass': 'mfp-with-zoom', 'zoom': {'enabled': true, 'duration': 300}}">
            <div class="row mx-0">
              <div class="col-md"><a href="<?php echo $Zoom1?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo $Thumb1 ?>" alt=""><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search-plus"></i></span></span></span></span></a></div>
              <div class="col-md"><a href="<?php echo $Zoom2?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo $Thumb2?>" alt=""><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search-plus"></i></span></span></span></span></a></div>
              <div class="col-md"><a href="<?php echo $Zoom3?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo $Thumb3?>" alt=""><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search-plus"></i></span></span></span></span></a></div>
              <div class="col-md"><a href="<?php echo $Zoom4?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo $Thumb4?>" alt=""><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search-plus"></i></span></span></span></span></a></div>
              <div class="col-md"><a href="<?php echo $Zoom5?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo $Thumb5?>" alt=""><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search-plus"></i></span></span></span></span></a></div>
              <div class="col-md"><a href="<?php echo $Zoom6?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo $Thumb6?>" alt=""><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search-plus"></i></span></span></span></span></a></div>
              <div class="col-md"><a href="<?php echo $Zoom7?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo $Thumb7?>" alt=""><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search-plus"></i></span></span></span></span></a></div>
            </div>
          </div>
        </div>
      </article>
    </section>
<?php endif ?>