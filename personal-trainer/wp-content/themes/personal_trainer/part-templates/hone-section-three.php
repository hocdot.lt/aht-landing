<?php
/**
 * Render section-two in home page
 */
//GET config
$SectionThreeShow = get_theme_mod('section_three_show','yes');
$BannerSectionThree = get_theme_mod('background_three',get_template_directory_uri() . '/images/banner-ss3.jpg');
$TitleSectionThree = get_theme_mod('title_section_three','<h2>GetInGetFit</h2>');
$SubTitleSectionThree = get_theme_mod('sub_title_section_three','Join us now, to start training with a team of professionals!');
$LinkSectionThree = get_theme_mod('link_section_three','#');
$BtnSectionThree = get_theme_mod('btn_section_three','Make An Appointment Now');
$phone = get_theme_mod('phone_section_three','800 123 45678');
?>
<?php if ($SectionThreeShow === 'yes'): ?>
<section id="section-three" style="background-image: url(<?php echo $BannerSectionThree ?>);">
  <div class="banner-section-three">
    <div class="bg-banner">
      <div class="content-banner">
        <?php echo $TitleSectionThree ?>
        <p class="desc-banner"><?php echo $SubTitleSectionThree ?></p>
        <a href ="<?php echo $LinkSectionThree ?>" class="btn btn-banner" ><?php echo $BtnSectionThree?></a>
        <h4 class="call">  OR Call : <?php echo $phone?></h4>
      </div>
    </div>
  </div>
</section>
<?php endif ?>