<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'personal_trainer_customize_register' );
function personal_trainer_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'personal_trainer_Customize_Sidebar_Control' ) ) {
		class personal_trainer_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

	        	$output = '
	        	<select>';
	        	foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
	        		$output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
	        		ucwords( $sidebar['name']).'</option>';
	        	}
	        	$output .= '</select>';

	        	$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

	        	printf(
	        		'<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
	        		$this->label,
	        		$output
	        	);
	        	?>
	        	<?php if ( '' != $this->description ) : ?>
	        		<div class="description customize-control-description">
	        			<?php echo esc_html( $this->description ); ?>
	        		</div>
	        	<?php endif; 
	        }
	    }
	}
	if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
		class My_Dropdown_Category_Control extends WP_Customize_Control {

			public $type = 'dropdown-category';

			public $dropdown_args = false;

			public function render_content() {
				$dropdown_args = wp_parse_args( $this->dropdown_args, array(
					'taxonomy'          => 'category',
					'show_option_none'  => ' ',
					'selected'          => $this->value(),
					'show_option_all'   => '',
					'orderby'           => 'id',
					'order'             => 'ASC',
					'show_count'        => 1,
					'hide_empty'        => 1,
					'child_of'          => 0,
					'exclude'           => '',
					'hierarchical'      => 1,
					'depth'             => 0,
					'tab_index'         => 0,
					'hide_if_empty'     => false,
					'option_none_value' => 0,
					'value_field'       => 'term_id',
				) );

				$dropdown_args['echo'] = false;

				$dropdown = wp_dropdown_categories( $dropdown_args );
				$dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
				$dropdown1 = str_replace( 'cat', '', $dropdown1);
				printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					$this->label, $dropdown1);
			}
		}
	}
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
				<label>
					<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
					<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
						foreach ( $this->posts as $post )
						{
							printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
						}
						?>
					</select>
				</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */

	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

			private $fonts = false;

			public function __construct( $manager, $id, $args = array(), $options = array() ) {
				$this->fonts = $this->get_fonts();
				parent::__construct( $manager, $id, $args );
			}

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
		  	if ( ! empty( $this->fonts ) ) { ?>
		  		<label>
		  			<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
		  			<select class="select-fonts" <?php $this->link(); ?>>
		  				<?php
		  				foreach ( $this->fonts as $k=>$v ) {
		  					printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
		  				}
		  				?>
		  			</select>
		  		</label>
		  	<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
		  	$fonts = array(
		  		'Baloo Tamma' => 'Baloo Tamma',
		  		'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',
		  		'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',
		  		'Old Standard TT:400,400i,700' => 'Old Standard TT',
		  		'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
		  		'Jura:300,400,500,600,700' => 'Jura',
		  		'Playfair Display:400,700,400i' => 'Playfair Display',
		  		'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
		  		'Oswald:200,300,400,500,600,700' => 'Oswald',
		  		'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
		  		'Droid Sans:400,700' => 'Droid Sans',
		  		'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
		  		'Arvo:400,700,400i,700i' => 'Arvo',
		  		'Lora:400,700,400i,700i' => 'Lora',
		  		'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
		  		'Oxygen:400,300,700' => 'Oxygen',
		  		'PT Serif:400,700' => 'PT Serif', 
		  		'PT Sans:400,700,400i,700i' => 'PT Sans',
		  		'PT Sans Narrow:400,700' => 'PT Sans Narrow',
		  		'Cabin:400,700,400i' => 'Cabin',
		  		'Fjalla One:400' => 'Fjalla One',
		  		'Francois One:400' => 'Francois One',
		  		'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
		  		'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
		  		'Arimo:400,700,400i,700i' => 'Arimo',
		  		'Ubuntu:400,700,400i,700i' => 'Ubuntu',
		  		'Bitter:400,700,400i' => 'Bitter',
		  		'Droid Serif:400,700,400i,700i' => 'Droid Serif',
		  		'Lobster:400' => 'Lobster',
		  		'Roboto:400,400i,700,700i' => 'Roboto',
		  		'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
		  		'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
		  		'Roboto Slab:100,300,400,700' => 'Roboto Slab',
		  		'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
		  		'Mandali:400' => 'Mandali',
		  		'Vesper Libre:400,700' => 'Vesper Libre',
		  		'NTR:400' => 'NTR',
		  		'Dhurjati:400' => 'Dhurjati',
		  		'Faster One:400' => 'Faster One',
		  		'Mallanna:400' => 'Mallanna',
		  		'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
		  		'Galindo:400' => 'Galindo',
		  		'Titan One:400' => 'Titan One',
		  		'Abel:400' => 'Abel',
		  		'Nunito:400,300,700' => 'Nunito',
		  		'Poiret One:400' => 'Poiret One',
		  		'Signika:400,300,600,700' => 'Signika',
		  		'Muli:400,400i,300i,300' => 'Muli',
		  		'Play:400,700' => 'Play',
		  		'Bree Serif:400' => 'Bree Serif',
		  		'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
		  		'Cuprum:400,400i,700,700i' => 'Cuprum',
		  		'Noto Serif:400,400i,700,700i' => 'Noto Serif',
		  		'Pacifico:400' => 'Pacifico',
		  		'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
		  		'Asap:400,400i,700,700i' => 'Asap',
		  		'Maven Pro:400,500,700' => 'Maven Pro',
		  		'Dancing Script:400,700' => 'Dancing Script',
		  		'Karla:400,700,400i,700i' => 'Karla',
		  		'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
		  		'Exo:400,300,400i,700,700i' => 'Exo',
		  		'Varela Round:400' => 'Varela Round',
		  		'Cabin Condensed:400,600,700' => 'Cabin Condensed',
		  		'PT Sans Caption:400,700' => 'PT Sans Caption',
		  		'Cinzel:400,700' => 'Cinzel',
		  		'News Cycle:400,700' => 'News Cycle',
		  		'Inconsolata:400,700' => 'Inconsolata',
		  		'Architects Daughter:400' => 'Architects Daughter',
		  		'Quicksand:400,700,300' => 'Quicksand',
		  		'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
		  		'Quicksand:400,700,300' => 'Quicksand',
		  		'Monda:400,700' => 'Monda',
		  		'Didact Gothic:400' => 'Didact Gothic',
		  		'Coming Soon:400' => 'Coming Soon',
		  		'Ropa Sans:400,400i' => 'Ropa Sans',
		  		'Tinos:400,400i,700,700i' => 'Tinos',
		  		'Glegoo:400,700' => 'Glegoo',
		  		'Pontano Sans:400' => 'Pontano Sans',
		  		'Fredoka One:400' => 'Fredoka One',
		  		'Lobster Two:400,400i,700,700i' => 'Lobster Two',
		  		'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
		  		'Covered By Your Grace:400' => 'Covered By Your Grace',
		  		'Changa One:400,400i' => 'Changa One',
		  		'Marvel:400,400i,700,700i' => 'Marvel',
		  		'BenchNine:400,700,300' => 'BenchNine',
		  		'Orbitron:400,700,500' => 'Orbitron',
		  		'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
		  		'Bangers:400' => 'Bangers',
		  		'Courgette:400' => 'Courgette',			
		  		'' => 'None',
		  	);
return $fonts;
}		  
}
}

	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
					<?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
					?>
				</label>
				<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */

	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'personal_trainer') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
		array(
			'title' => esc_html__( 'General Settings','personal_trainer'  ),
			'priority' => 20, 
		)
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
		array(
			'label' => esc_html__( 'Logo','personal_trainer' ),
			'description' => esc_html__( 'Upload Logo','personal_trainer' ),
			'section' => 'general_settings',
		)
	) );
	// Info header
	$wp_customize->add_setting( 'text_logo',
		array(
			'default' => esc_html__('Amanda Fit'),
		)
	);
	$wp_customize->add_control( 'text_logo',
		array(
			'label' => esc_html__( 'Text Logo','personal_trainer' ),
			'section' => 'general_settings',
			'type' => 'text',
		)
    	);// Info header
	$wp_customize->add_setting( 'desc_logo',
		array(
			'default' => esc_html__('PERSONAL TRAINER'),
		)
	);
	$wp_customize->add_control( 'desc_logo',
		array(
			'label' => esc_html__( 'Desc Logo','personal_trainer' ),
			'section' => 'general_settings',
			'type' => 'text',
		)
    	);// Info header
	$wp_customize->add_setting( 'text_1',
		array(
			'default' => esc_html__('Phone 1234 56 7890'),
		)
	);
	$wp_customize->add_control( 'text_1',
		array(
			'label' => esc_html__( 'Text Info 1','personal_trainer' ),
			'section' => 'general_settings',
			'type' => 'text',
		)
    	);	// Info header
	$wp_customize->add_setting( 'text_2',
		array(
			'default' => esc_html__('123 Street, City, State 45678'),
		)
	);
	$wp_customize->add_control( 'text_2',
		array(
			'label' => esc_html__( 'Text Info 2','personal_trainer' ),
			'section' => 'general_settings',
			'type' => 'text',
		)
	);
	
	$wp_customize->add_setting( 'google_font_h',
		array(
			'default' => '',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
		array(
			'label' => 'Font Family Tab H',
			'section' => 'general_settings',
		)
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
		array(
			'label' => 'Main Google Font',
			'section' => 'general_settings',
		)
	) );

	// End Header
	
	// Panel Home Options

	$wp_customize->add_panel( 'home_page_setting', array(
		'priority'       => 30,
		'capability'     => 'edit_theme_options',
		'title'          => __('Home Options', 'personal_trainer'),
		'description'    => __('Several settings pertaining my theme', 'personal_trainer'),
	) );
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
		array(
			'title' => esc_html__( 'Banner Section','personal_trainer'  ),
			'priority' => 20,
			'panel'  => 'home_page_setting',
		)
	);
	// background  banner
	$wp_customize->add_setting( 'background_banner',
		array(
			'default' => get_template_directory_uri() . '/images/banner.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_banner',
		array(
			'label' => esc_html__( 'Background Banner','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'home_banner_settings',
		)
	) );
	// title banner
	$wp_customize->add_setting( 'banner_title',
		array(
			'default' => '<h1>GET IN, GETFIT.</h1>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'banner_title',
		array(
			'label' => esc_html__( 'Title Banner','personal_trainer' ),
			'description' => esc_html__( 'Title Banner','personal_trainer' ),
			'section' => 'home_banner_settings',
		)
	));
	// sub title banner
	$wp_customize->add_setting( 'banner_sub_title',
		array(
			'default' => esc_html__('Personalized Training by Experienced Tutors and Institutes'),
		)
	);
	$wp_customize->add_control( 'banner_sub_title',
		array(
			'label' => esc_html__( 'Sub Title Banner','personal_trainer' ),
			'section' => 'home_banner_settings',
			'type' => 'textarea',
		)
	);
	// button banner
	$wp_customize->add_setting( 'button_banner',
		array(
			'default' => esc_html__('Try Free Class','personal_trainer'),

		)
	);
	$wp_customize->add_control('button_banner',
		array(
			'label' => esc_html__( 'Button banner','personal_trainer' ),
			'section' => 'home_banner_settings',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting( 'link_banner',
		array(
			'default' => esc_html__('#','personal_trainer'),

		)
	);
	$wp_customize->add_control('link_banner',
		array(
			'label' => esc_html__( 'Button URL','personal_trainer' ),
			'section' => 'home_banner_settings',
			'type' => 'text',
		)
	);
	// background right banner
	$wp_customize->add_setting( 'background_banner_right',
		array(
			'default' => get_template_directory_uri() . '/images/img-video.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_banner_right',
		array(
			'label' => esc_html__( 'Background Video','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'home_banner_settings',
		)
	) );
	//linkvideo
	$wp_customize->add_setting( 'link_video',
		array(
			'default' => esc_html__('https://vimeo.com/45830194','personal_trainer'),

		)
	);
	$wp_customize->add_control('link_video',
		array(
			'label' => esc_html__( '','personal_trainer' ),
			'section' => 'home_banner_settings',
			'type' => 'text',
		)
	);
    //title video
	$wp_customize->add_setting( 'title_video',
		array(
			'default' => esc_html__('Amandas 5-Minute Fat-Blasting Workout'),
		)
	);
	$wp_customize->add_control( 'title_video',
		array(
			'label' => esc_html__( 'Title Video','personal_trainer' ),
			'section' => 'home_banner_settings',
			'type' => 'textarea',
		)
	);
	 //Show/Hide banner
	$wp_customize->add_setting( 'banner_show',
		array(
			'default' => 'yes',
		)
	);
	$wp_customize->add_control( 'banner_show',
		array(
			'label' => esc_html__( 'Show Section','personal_trainer' ),
			'section' => 'home_banner_settings',
			'type' => 'select',
			'choices' => array(
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);
	// End banner section
    // Start section-one
	$wp_customize->add_section( 'section_one_settings',
		array(
			'title' => esc_html__( 'section one','personal_trainer' ),
			'priority' => 20,
			'panel' => 'home_page_setting',
		)
	);
    //title section one
	$wp_customize->add_setting( 'title_section_one',
		array(
			'default' => '
			<h2>Achieve Your FitnesGoal EffectivelyNow! Get The Best Fitness Trainer</h2>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'title_section_one',
		array(
			'label' => esc_html__( 'Title ','personal_trainer' ),
			'description' => esc_html__( 'Title','personal_trainer' ),
			'section' => 'section_one_settings',
		)
	));
    // icon1
	$wp_customize->add_setting( 'icon_1',
		array(
			'default' => get_template_directory_uri() . '/images/gym.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'icon_1',
		array(
			'label' => esc_html__( 'Icon 1','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_one_settings',
		)
	) );
    //title Icon1
	$wp_customize->add_setting( 'title_icon_1',
		array(
			'default' => esc_html__('Make your body fit'),
		)
	);
	$wp_customize->add_control( 'title_icon_1',
		array(
			'label' => esc_html__( 'Title Icon 1','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'text',
		)
	);
    //sub title Icon1
	$wp_customize->add_setting( 'sub_title_icon_1',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_icon_1',
		array(
			'label' => esc_html__( 'Sub Title Icon 1','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'textarea',
		)
	);
    // icon2
	$wp_customize->add_setting( 'icon_2',
		array(
			'default' => get_template_directory_uri() . '/images/runner.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'icon_2',
		array(
			'label' => esc_html__( 'Icon 2','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_one_settings',
		)
	) );
    //title Icon1
	$wp_customize->add_setting( 'title_icon_2',
		array(
			'default' => esc_html__('Power overwhelming'),
		)
	);
	$wp_customize->add_control( 'title_icon_2',
		array(
			'label' => esc_html__( 'Title Icon 2','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'text',
		)
	);
    //sub title Icon2
	$wp_customize->add_setting( 'sub_title_icon_2',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_icon_2',
		array(
			'label' => esc_html__( 'Sub Title Icon 2','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'textarea',
		)
	);
        // icon3
	$wp_customize->add_setting( 'icon_3',
		array(
			'default' => get_template_directory_uri() . '/images/woman.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'icon_3',
		array(
			'label' => esc_html__( 'Icon 3','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_one_settings',
		)
	) );
    //title Icon3
	$wp_customize->add_setting( 'title_icon_3',
		array(
			'default' => esc_html__('Personal Training'),
		)
	);
	$wp_customize->add_control( 'title_icon_3',
		array(
			'label' => esc_html__( 'Title Icon 3','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'text',
		)
	);
    //sub title Icon3
	$wp_customize->add_setting( 'sub_title_icon_3',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_icon_3',
		array(
			'label' => esc_html__( 'Sub Title Icon 3','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'textarea',
		)
	);
    // icon1
	$wp_customize->add_setting( 'icon_4',
		array(
			'default' => get_template_directory_uri() . '/images/gym2.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'icon_4',
		array(
			'label' => esc_html__( 'Icon 4','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_one_settings',
		)
	) );
    //title Icon4
	$wp_customize->add_setting( 'title_icon_4',
		array(
			'default' => esc_html__('Individual Training'),
		)
	);
	$wp_customize->add_control( 'title_icon_4',
		array(
			'label' => esc_html__( 'Title Icon 4','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'text',
		)
	);
    //sub title Icon4
	$wp_customize->add_setting( 'sub_title_icon_4',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_icon_4',
		array(
			'label' => esc_html__( 'Sub Title Icon 4','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'textarea',
		)
	);
     // button section one
	$wp_customize->add_setting( 'button_section_one',
		array(
			'default' => esc_html__('Make an appointment with personal trainer','personal_trainer'),
		)
	);
	$wp_customize->add_control('button_section_one',
		array(
			'label' => esc_html__( 'Link Section One','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'text',
		)
	);
    //linkmain section one
	$wp_customize->add_setting( 'link_main_section_one',
		array(
			'default' => esc_html__('#','personal_trainer'),
		)
	);
	$wp_customize->add_control('link_main_section_one',
		array(
			'label' => esc_html__( 'Link Section One','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'text',
		)
	);
    // content-one
    //images content one
	$wp_customize->add_setting( 'images_bottom',
		array(
			'default' => get_template_directory_uri() . '/images/bg-content.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'images_bottom',
		array(
			'label' => esc_html__( 'Images Conetnt Bottom','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_one_settings',
		)
	) );
    //title Icon4
	$wp_customize->add_setting( 'title_content_bottom',
		array(
			'default' => esc_html__('Little About Myself!'),
		)
	);
	$wp_customize->add_control( 'title_content_bottom',
		array(
			'label' => esc_html__( 'Title Content Bottom','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'text',
		)
	);
        //link
	$wp_customize->add_setting( 'link_title_content_bottom',
		array(
			'default' => esc_html__('#'),
		)
	);
	$wp_customize->add_control( 'link_title_content_bottom',
		array(
			'label' => esc_html__( 'Link','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'text',
		)
	);
    //Desc title Icon4
	$wp_customize->add_setting( 'Desc_title_content_bottom',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'Desc_title_content_bottom',
		array(
			'label' => esc_html__( 'Desc Title','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'textarea',
		)
	);
    //Sub title

	$wp_customize->add_setting( 'sub1_title_content_bottom',
		array(
			'default' => '
			<p >Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor siLorem ipsum dolor sit amet, consectetur dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'sub1_title_content_bottom',
		array(
			'label' => esc_html__( 'Sub Title ','personal_trainer' ),
			'description' => esc_html__( 'The left content','personal_trainer' ),
			'section' => 'section_one_settings',
		)
	));
     //Sub title

	$wp_customize->add_setting( 'sub2_title_content_bottom',
		array(
			'default' => '
			<p >Sd do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut </p>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'sub2_title_content_bottom',
		array(
			'label' => esc_html__( 'Sub 2 Title ','personal_trainer' ),
			'description' => esc_html__( 'The left content','personal_trainer' ),
			'section' => 'section_one_settings',
		)
	));

    //show/hide section one
	$wp_customize->add_setting( 'section_one_show',
		array(
			'default' => 'yes',
		)
	);
	$wp_customize->add_control( 'section_one_show',
		array(
			'label' => esc_html__( 'Show Section One','personal_trainer' ),
			'section' => 'section_one_settings',
			'type' => 'select',
			'choices' => array(
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);

        // End  section one
         // Start section two
	$wp_customize->add_section( 'Section_two_settings',
		array(
			'title' => esc_html__( 'section two','personal_trainer'  ),
			'priority' => 20,
			'panel'  => 'home_page_setting',
		)
	);
         // title section two
	$wp_customize->add_setting( 'title_section_two',
		array(
			'default' => 'My Professional Training Courses',
		)
	);
	$wp_customize->add_control( 'title_section_two',
		array(
			'label' => esc_html__( 'Title','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
               // sub title
	$wp_customize->add_setting( 'sub_title_section_two',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_section_two',
		array(
			'label' => esc_html__( 'Sub Title','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'textarea',
		)
	);

         //content top section two

         // images 1
	$wp_customize->add_setting( 'images_1',
		array(
			'default' => get_template_directory_uri() . '/images/img-1.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'images_1',
		array(
			'label' => esc_html__( 'Images 1','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'Section_two_settings',
		)
	) );
             //title Images 1
	$wp_customize->add_setting( 'title_images_1',
		array(
			'default' => esc_html__('YOGA BASICS'),
		)
	);
	$wp_customize->add_control( 'title_images_1',
		array(
			'label' => esc_html__( 'Title Images 1','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
             //link title Icon1
	$wp_customize->add_setting( 'link_title',
		array(
			'default' => esc_html__('#'),
		)
	);
	$wp_customize->add_control( 'link_title',
		array(
			'label' => esc_html__( 'Link title 1 ','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
                 //price 1
	$wp_customize->add_setting( 'price_1',
		array(
			'default' => esc_html__('49'),
		)
	);
	$wp_customize->add_control( 'price_1',
		array(
			'label' => esc_html__( 'Price 1 ','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);

             //sub title 1
	$wp_customize->add_setting( 'sub_title_1',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_1',
		array(
			'label' => esc_html__( 'Sub Title 1','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'textarea',
		)
	);
             // images 2
	$wp_customize->add_setting( 'images_2',
		array(
			'default' => get_template_directory_uri() . '/images/img-2.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'images_2',
		array(
			'label' => esc_html__( 'Images 2','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'Section_two_settings',
		)
	) );
             //title Images 2
	$wp_customize->add_setting( 'title_images_2',
		array(
			'default' => esc_html__('CARDIO TRAINING'),
		)
	);
	$wp_customize->add_control( 'title_images_2',
		array(
			'label' => esc_html__( 'Title Images 2','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
             //link title 2
	$wp_customize->add_setting( 'link_title_2',
		array(
			'default' => esc_html__('#'),
		)
	);
	$wp_customize->add_control( 'link_title_2',
		array(
			'label' => esc_html__( 'Link title 2','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
                    //price 2
	$wp_customize->add_setting( 'price_2',
		array(
			'default' => esc_html__('99'),
		)
	);
	$wp_customize->add_control( 'price_2',
		array(
			'label' => esc_html__( 'Price 2 ','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
             //sub title 2
	$wp_customize->add_setting( 'sub_title_2',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_2',
		array(
			'label' => esc_html__( 'Sub Title 2','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'textarea',
		)
	);
             // images 3
	$wp_customize->add_setting( 'images_3',
		array(
			'default' => get_template_directory_uri() . '/images/img-3.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'images_3',
		array(
			'label' => esc_html__( 'Images 3','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'Section_two_settings',
		)
	) );
             //title Images 3
	$wp_customize->add_setting( 'title_images_3',
		array(
			'default' => esc_html__('CROSS FIT TRAINING'),
		)
	);
	$wp_customize->add_control( 'title_images_3',
		array(
			'label' => esc_html__( 'Title Images 3','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
             //link title 3
	$wp_customize->add_setting( 'link_title_3',
		array(
			'default' => esc_html__('#'),
		)
	);
	$wp_customize->add_control( 'link_title_3',
		array(
			'label' => esc_html__( 'Link title 3','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
            //price 3
	$wp_customize->add_setting( 'price_3',
		array(
			'default' => esc_html__('99'),
		)
	);
	$wp_customize->add_control( 'price_3',
		array(
			'label' => esc_html__( 'Price 3 ','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
             //sub title 3
	$wp_customize->add_setting( 'sub_title_3',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_3',
		array(
			'label' => esc_html__( 'Sub Title 3','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'textarea',
		)
	);

                //Number post
	$wp_customize->add_setting( 'section_two_number_post',
		array(
			'default' => esc_html__('4','personal_trainer'),
		)
	);
	$wp_customize->add_control('section_two_number_post',
		array(
			'label' => esc_html__( 'Number Post','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'text',
		)
	);
         //show/hide Section two
	$wp_customize->add_setting( 'section_two_show',
		array(
			'default' => 'yes',
		)
	);
	$wp_customize->add_control( 'section_two_show',
		array(
			'label' => esc_html__( 'Show Section Info','personal_trainer' ),
			'section' => 'Section_two_settings',
			'type' => 'select',
			'choices' => array(
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);
         // End  section two
         // Start section three
	$wp_customize->add_section( 'section_three_settings',
		array(
			'title' => esc_html__( 'Section Three','personal_trainer'  ),
			'priority' => 20,
			'panel'  => 'home_page_setting',
		)
	);
	$wp_customize->add_setting( 'background_three',
		array(
			'default' => get_template_directory_uri() . '/images/banner-ss3.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_three',
		array(
			'label' => esc_html__( 'Background Three','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_three_settings',
		)
	) );
        //title section three
	$wp_customize->add_setting( 'title_section_three',
		array(
			'default' => '
			<h2>Get In Get Fit</h2>',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'title_section_three',
		array(
			'label' => esc_html__( 'Title ','personal_trainer' ),
			'description' => esc_html__( 'The left content','personal_trainer' ),
			'section' => 'section_three_settings',
		)
	));
        //sub title three
	$wp_customize->add_setting( 'sub_title_section_three',
		array(
			'default' => esc_html__('Join us now, to start training with a team of professionals!'),
		)
	);
	$wp_customize->add_control( 'sub_title_section_three',
		array(
			'label' => esc_html__( 'Sub Title','personal_trainer' ),
			'section' => 'section_three_settings',
			'type' => 'textarea',
		)
	);

        //linksection three
	$wp_customize->add_setting( 'link_section_three',
		array(
			'default' => esc_html__('#','personal_trainer'),

		)
	);
	$wp_customize->add_control('link_section_three',
		array(
			'label' => esc_html__( 'Link ','personal_trainer' ),
			'section' => 'section_three_settings',
			'type' => 'text',
		)
	);
    	//btn setion three
	$wp_customize->add_setting( 'btn_section_three',
		array(
			'default' => esc_html__('Make An Appointment Now'),
		)
	);
	$wp_customize->add_control( 'btn_section_three',
		array(
			'label' => esc_html__( 'Text Button','personal_trainer' ),
			'section' => 'section_three_settings',
			'type' => 'textarea',
		)
	);
         //Phone
	$wp_customize->add_setting( 'phone_section_three',
		array(
			'default' => esc_html__('800 123 45678'),
		)
	);
	$wp_customize->add_control( 'phone_section_three',
		array(
			'label' => esc_html__( 'Phone','personal_trainer' ),
			'section' => 'section_three_settings',
			'type' => 'textarea',
		)
	);
          //show/hide section three
	$wp_customize->add_setting( 'section_three_show',
		array(
			'default' => 'yes',
		)
	);
	$wp_customize->add_control( 'section_three_show',
		array(
			'label' => esc_html__( 'Show Section Info','personal_trainer' ),
			'section' => 'section_three_settings',
			'type' => 'select',
			'choices' => array(
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);
         // End  section three
            // Start section for
	$wp_customize->add_section( 'section_for_settings',
		array(
			'title' => esc_html__( 'Section For','personal_trainer'  ),
			'priority' => 20,
			'panel'  => 'home_page_setting',
		)
	);
         //title section for
	$wp_customize->add_setting( 'title_section_for',
		array(
			'default' => esc_html__('My Articles on Professional Fitness Training'),
		)
	);
	$wp_customize->add_control( 'title_section_for',
		array(
			'label' => esc_html__( 'Title','personal_trainer' ),
			'section' => 'section_for_settings',
			'type' => 'text',
		)
	);
         //sub title for
	$wp_customize->add_setting( 'sub_title_section_for',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_section_for',
		array(
			'label' => esc_html__( 'Sub Title','personal_trainer' ),
			'section' => 'section_for_settings',
			'type' => 'textarea',
		)
	);

    //Number post
	$wp_customize->add_setting( 'section_for_number_post',
		array(
			'default' => esc_html__('3','personal_trainer'),
		)
	);
	$wp_customize->add_control('section_for_number_post',
		array(
			'label' => esc_html__( 'Number Post','personal_trainer' ),
			'section' => 'section_for_settings',
			'type' => 'text',
		)
	);
            //show/hide section for
	$wp_customize->add_setting( 'section_for_show',
		array(
			'default' => 'yes',
		)
	);
	$wp_customize->add_control( 'section_for_show',
		array(
			'label' => esc_html__( 'Show Section for','personal_trainer' ),
			'section' => 'section_for_settings',
			'type' => 'select',
			'choices' => array(
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);
              //section five
	$wp_customize->add_section( 'section_five_settings',
		array(
			'title' => esc_html__( 'Section Five','personal_trainer'  ),
			'priority' => 20,
			'panel'  => 'home_page_setting',
		)
	);
         //background section_five
	$wp_customize->add_setting( 'background_section_five',
		array(
			'default' => get_template_directory_uri() . '/images/banner2.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_section_five',
		array(
			'label' => esc_html__( 'Background Section Five','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_five_settings',
		)
	) );
            //title section five
	$wp_customize->add_setting( 'title_section_five',
		array(
			'default' => esc_html__('See what my clients have to say aboutmy training and activities'),
		)
	);
	$wp_customize->add_control( 'title_section_five',
		array(
			'label' => esc_html__( 'Title','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'text',
		)
	);
        //slider
	$wp_customize->add_setting( 'desc_1',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'desc_1',
		array(
			'label' => esc_html__( 'Desc 1','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'textarea',
		)
	);
        //desc slider
	$wp_customize->add_setting( 'name_1',
		array(
			'default' => esc_html__('Amanda Smith'),
		)
	);
	$wp_customize->add_control( 'name_1',
		array(
			'label' => esc_html__( 'Name 1','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'text',
		)
	);
        //name
	$wp_customize->add_setting( 'avatar_1',
		array(
			'default' => get_template_directory_uri() . '/images/click3.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'avatar_1',
		array(
			'label' => esc_html__( 'Avatar 1','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_five_settings',
		)
	) );
             //images
              //slider2
	$wp_customize->add_setting( 'desc_2',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'desc_2',
		array(
			'label' => esc_html__( 'Desc 2','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'textarea',
		)
	);
        //desc slider
	$wp_customize->add_setting( 'name_2',
		array(
			'default' => esc_html__('Amanda Smith'),
		)
	);
	$wp_customize->add_control( 'name_2',
		array(
			'label' => esc_html__( 'Name 2','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'text',
		)
	);
        //name
	$wp_customize->add_setting( 'avatar_2',
		array(
			'default' => get_template_directory_uri() . '/images/click2.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'avatar_2',
		array(
			'label' => esc_html__( 'Avatar 2','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_five_settings',
		)
	) );
             //images
             //slider3
	$wp_customize->add_setting( 'desc_3',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'desc_3',
		array(
			'label' => esc_html__( 'Desc 3','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'textarea',
		)
	);
        //desc slider
	$wp_customize->add_setting( 'name_3',
		array(
			'default' => esc_html__('Amanda Smith'),
		)
	);
	$wp_customize->add_control( 'name_3',
		array(
			'label' => esc_html__( 'Name 3','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'text',
		)
	);
        //name
	$wp_customize->add_setting( 'avatar_3',
		array(
			'default' => get_template_directory_uri() . '/images/click1.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'avatar_3',
		array(
			'label' => esc_html__( 'Avatar 3','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_five_settings',
		)
	) );
     //images
    //sub title five
	$wp_customize->add_setting( 'sub_title_section_five',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
		)
	);
	$wp_customize->add_control( 'sub_title_section_five',
		array(
			'label' => esc_html__( 'Sub Title','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'textarea',
		)
	);
    //Contact shortcode
    $wp_customize->add_setting( 'newsletter_title',
		array(
			'default' => esc_html__('Subscribe To My Weekly Newsletter'),
		)
	);
	$wp_customize->add_control( 'newsletter_title',
		array(
			'label' => esc_html__( 'Newsletter Title','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'text',
		)
	);
	$wp_customize->add_setting( 'newsletter_desc',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus lacus odio, at convallis dui molestie et. Nunc luctus porttitor mauris ut luctus. sapien sit amet, pharetra dolor.'),
		)
	);
	$wp_customize->add_control( 'newsletter_desc',
		array(
			'label' => esc_html__( 'Newsletter Title','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'textarea',
		)
	);
	$wp_customize->add_setting( 'form_shortcode',
		array(
			'default' => '[mc4wp_form id="6"]',
		)
	);
	$wp_customize->add_control( 'form_shortcode',
		array(
			'label' => esc_html__( 'Form shortcode','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'text'
		)
	);
               //shortcode
             //show/hide section five
	$wp_customize->add_setting( 'section_five_show',
		array(
			'default' => 'yes',
		)
	);
	$wp_customize->add_control( 'section_five_show',
		array(
			'label' => esc_html__( 'Show Section Five','personal_trainer' ),
			'section' => 'section_five_settings',
			'type' => 'select',
			'choices' => array(
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);
        //section zoom
	$wp_customize->add_section( 'section_zoom_settings',
		array(
			'title' => esc_html__( 'Section Zoom','personal_trainer'  ),
			'priority' => 20,
			'panel'  => 'home_page_setting',
		)
	);
         //zoom1 section_zoom
         //thumb1
	$wp_customize->add_setting( 'thumb_img',
		array(
			'default' => get_template_directory_uri() . '/images/zoom6.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'thumb_img',
		array(
			'label' => esc_html__( 'Thumb 1','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
         //zoom1
	$wp_customize->add_setting( 'zoom_img',
		array(
			'default' => get_template_directory_uri() . '/images/bg-content.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'zoom_img',
		array(
			'label' => esc_html__( 'Zoom 1','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
                   //thumb2
	$wp_customize->add_setting( 'thumb_2_img',
		array(
			'default' => get_template_directory_uri() . '/images/zoom5.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'thumb_2_img',
		array(
			'label' => esc_html__( 'Thumb 2','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
         //zoom2
	$wp_customize->add_setting( 'zoom_2_img',
		array(
			'default' => get_template_directory_uri() . '/images/bg-content.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'zoom_2_img',
		array(
			'label' => esc_html__( 'Zoom 1','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
                   ) );//thumb1
	$wp_customize->add_setting( 'thumb_3_img',
		array(
			'default' => get_template_directory_uri() . '/images/zoom4.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'thumb_3_img',
		array(
			'label' => esc_html__( 'Thumb 3','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
         //zoom3
	$wp_customize->add_setting( 'zoom_3_img',
		array(
			'default' => get_template_directory_uri() . '/images/bg-content.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'zoom_3_img',
		array(
			'label' => esc_html__( 'Zoom 3','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
                   //thumb4
	$wp_customize->add_setting( 'thumb_4_img',
		array(
			'default' => get_template_directory_uri() . '/images/zoom3.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'thumb_4_img',
		array(
			'label' => esc_html__( 'Thumb 4','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
         //zoom4
	$wp_customize->add_setting( 'zoom_4_img',
		array(
			'default' => get_template_directory_uri() . '/images/bg-content.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'zoom_4_img',
		array(
			'label' => esc_html__( 'Zoom 4','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
                   //thumb5
	$wp_customize->add_setting( 'thumb_5_img',
		array(
			'default' => get_template_directory_uri() . '/images/zoom2.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'thumb_5_img',
		array(
			'label' => esc_html__( 'Thumb 5','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
         //zoom5
	$wp_customize->add_setting( 'zoom_5_img',
		array(
			'default' => get_template_directory_uri() . '/images/bg-content.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'zoom_5_img',
		array(
			'label' => esc_html__( 'Zoom 5','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
               //thumb6
	$wp_customize->add_setting( 'thumb_6_img',
		array(
			'default' => get_template_directory_uri() . '/images/zoom1.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'thumb_6_img',
		array(
			'label' => esc_html__( 'Thumb 6','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
         //zoom6
	$wp_customize->add_setting( 'zoom_6_img',
		array(
			'default' => get_template_directory_uri() . '/images/bg-content.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'zoom_6_img',
		array(
			'label' => esc_html__( 'Zoom 6','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
               //thumb7
	$wp_customize->add_setting( 'thumb_7_img',
		array(
			'default' => get_template_directory_uri() . '/images/zoom7.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'thumb_7_img',
		array(
			'label' => esc_html__( 'Thumb 7','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
         //zoom7
	$wp_customize->add_setting( 'zoom_7_img',
		array(
			'default' => get_template_directory_uri() . '/images/bg-content.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'zoom_7_img',
		array(
			'label' => esc_html__( 'Zoom 7','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'section_zoom_settings',
		)
	) );
                //show/hide section five
	$wp_customize->add_setting( 'section_zoom_show',
		array(
			'default' => 'yes',
		)
	);
	$wp_customize->add_control( 'section_zoom_show',
		array(
			'label' => esc_html__( 'Show Section Five','personal_trainer' ),
			'section' => 'section_zoom_settings',
			'type' => 'select',
			'choices' => array(
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);

            //contact
	$wp_customize->add_section( 'Contact_settings',
		array(
			'title' => esc_html__( 'Contact','personal_trainer'  ),
			'priority' => 20,
			'panel'  => 'home_page_setting',
		)
	);
	$wp_customize->add_setting( 'logo_contact',
		array(
			'default' => get_template_directory_uri() . '/images/icon-footer.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_contact',
		array(
			'label' => esc_html__( 'Logo Contact','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'Contact_settings',
		)
	) );
	$wp_customize->add_setting( 'Background_contact',
		array(
			'default' => get_template_directory_uri() . '/images/banner-footer.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'Background_contact',
		array(
			'label' => esc_html__( 'Background Contact','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'Contact_settings',
		)
	) );
                //title section contact
	$wp_customize->add_setting( 'title_conatct',
		array(
			'default' => esc_html__('Amanda Fit'),
		)
	);
	$wp_customize->add_control( 'title_conatct',
		array(
			'label' => esc_html__( 'Title','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
	);
               //sub title contact
	$wp_customize->add_setting( 'sub_title_contact',
		array(
			'default' => esc_html__('Personal Trainer'),
		)
	);
	$wp_customize->add_control( 'sub_title_contact',
		array(
			'label' => esc_html__( 'Sub Title','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
	);
                 //desc title five
	$wp_customize->add_setting( 'desc_title_contact',
		array(
			'default' => esc_html__('Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus lacus odio, at convallis dui molestie et. Nunc luctus porttitor mauris ut luctus. sapien sit amet, pharetra dolor. In tempus lacus odio,at convallis dui molestie et. Nunc luctus porttitor...'),
		)
	);
	$wp_customize->add_control( 'desc_title_contact',
		array(
			'label' => esc_html__( 'Desc','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'textarea',
		)
	);
                       //link
	$wp_customize->add_setting( 'link_title_contact',
		array(
			'default' => esc_html__('#'),
		)
	);
	$wp_customize->add_control( 'link_title_contact',
		array(
			'label' => esc_html__( 'Link','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
	);
                       //title menu
	$wp_customize->add_setting( 'title_menu_contact',
		array(
			'default' => esc_html__('Site Navigation'),
		)
	);
	$wp_customize->add_control( 'title_menu_contact',
		array(
			'label' => esc_html__( 'Title menu','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
	);
                     //title tab contact
	$wp_customize->add_setting( 'title_tab_contact',
		array(
			'default' => esc_html__('Contacts'),
		)
	);
	$wp_customize->add_control( 'title_tab_contact',
		array(
			'label' => esc_html__( 'Title Tab Contact','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
	);
                       //link tab contact
	$wp_customize->add_setting( 'link_tab_contact',
		array(
			'default' => esc_html__('#'),
		)
	);
	$wp_customize->add_control( 'link_tab_contact',
		array(
			'label' => esc_html__( 'Link Tab Contact','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
	);
                       //Title link tab contact
	$wp_customize->add_setting( 'link_title_tab_contact',
		array(
			'default' => esc_html__('Amanda Fit'),
		)
	);
	$wp_customize->add_control( 'link_title_tab_contact',
		array(
			'label' => esc_html__( 'Title Link','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
                       ); // info tab contact
	$wp_customize->add_setting( 'info_tab_contact',
		array(
			'default' => esc_html__('123 Street, City, State 45678'),
		)
	);
	$wp_customize->add_control( 'info_tab_contact',
		array(
			'label' => esc_html__( 'Info','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
                       ); //Phone tab contact
	$wp_customize->add_setting( 'Phone_tab_contact',
		array(
			'default' => esc_html__('1234 56 7890'),
		)
	);
	$wp_customize->add_control( 'Phone_tab_contact',
		array(
			'label' => esc_html__( 'Phone','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
	);
                       //mail1 tab contact
	$wp_customize->add_setting( 'Mail_1_tab_contact',
		array(
			'default' => esc_html__('info@amandafit.com'),
		)
	);
	$wp_customize->add_control( 'Mail_1_tab_contact',
		array(
			'label' => esc_html__( 'Mail 1','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
                       );//mail2 tab contact
	$wp_customize->add_setting( 'Mail_2_tab_contact',
		array(
			'default' => esc_html__('amandsfitstudion.com'),
		)
	);
	$wp_customize->add_control( 'Mail_2_tab_contact',
		array(
			'label' => esc_html__( 'Mail 2','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'text',
		)
	);

                        //show/hide section contact
	$wp_customize->add_setting( 'contact_show',
		array(
			'default' => 'yes',
		)
	);
	$wp_customize->add_control( 'contact_show',
		array(
			'label' => esc_html__( 'Show Contact','personal_trainer' ),
			'section' => 'Contact_settings',
			'type' => 'select',
			'choices' => array(
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);

       //

	// Footer
	$wp_customize->add_section( 'footer_settings',
		array(
			'title' => esc_html__( 'Footer Settings','personal_trainer'  ),
			'priority' => 35, 
		)
	);

	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '<p>(C) 2019, All Rights Reserved, AmandaFit. Theme Developed by<a href="#">Template.net</a></p>',
			'sanitize_callback' => 'wp_kses_post',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
		array(
			'label' => esc_html__( 'Copyright text','personal_trainer' ),
			'section' => 'footer_settings',
		)
	) );
	$wp_customize->add_setting( 'img_left_contact',
		array(
			'default' => get_template_directory_uri() . '/images/checkin.png',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img_left_contact',
		array(
			'label' => esc_html__( 'Images left: Image','personal_trainer' ),
			'description' => esc_html__( 'Upload Image','personal_trainer' ),
			'section' => 'contact_settings',
		)
	) );
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
		array(
			'title' => esc_html__( 'Social','personal_trainer'  ),
			'priority' => 35, 
		)
	);
	$wp_customize->add_setting( 'show_social',
		array(
			'default' => 'yes',
		)
	);	
	$wp_customize->add_control( 'show_social',
		array(
			'label' => esc_html__( 'Show Social','personal_trainer' ),
			'section' => 'social_settings',
			'type' => 'select',
	      'choices' => array( // Optional.
	      	'no' => esc_html__( 'No','personal_trainer' ),
	      	'yes' => esc_html__( 'Yes','personal_trainer' ),
	      )
	  )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','personal_trainer'),
		'instagram'=> esc_html__('Instagram','personal_trainer'),
		'twitter'=> esc_html__('Twitter','personal_trainer'),
		'google'=> esc_html__('Google','personal_trainer'),
		'linkedin'=> esc_html__('Linkedin','personal_trainer')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
			array(
				'default' => '#',
			)
		);	
		$wp_customize->add_control( $key,
			array(
				'label' => esc_html( $value),
				'section' => 'social_settings',
				'type' => 'text',
			)
		);	
	}	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( 'share_'.$key,
			array(
				'default' => 'yes',
			)
		);		
		$wp_customize->add_control( 'share_'.$key,
			array(
				'label' => esc_html( 'Share '.$value),
				'section' => 'social_settings',
				'type' => 'select',
			  'choices' => array( // Optional.
			  	'no' => esc_html__( 'No','personal_trainer' ),
			  	'yes' => esc_html__( 'Yes','personal_trainer' ),
			  )
			)
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
		array(
			'title' => esc_html__( 'Blog Settings','personal_trainer'  ),
			'priority' => 35, 
		)
	);	 
	$wp_customize->add_setting('blog_page_title',
		array(
			'default' => esc_html__('Blog Articles','personal_trainer'),
		)
	);    
	$wp_customize->add_control( 'blog_page_title',
		array(
			'label' => esc_html__( 'Blog page Title','personal_trainer' ),
			'section' => 'blog_page_settings',
			'type' => 'text',
		)
	);
	
	$wp_customize->add_setting( 'blog_single_related',
		array(
			'default' => 'yes',
		)
	);

	$wp_customize->add_control( 'blog_single_related',
		array(
			'label' => esc_html__( 'Related Posts','personal_trainer' ),
			'section' => 'blog_page_settings',
			'type' => 'select',
			'choices' => array( 
				'no' => esc_html__( 'No','personal_trainer' ),
				'yes' => esc_html__( 'Yes','personal_trainer' ),
			)
		)
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
		array(
			'default' => 'no',
		)
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
		array(
			'label' => esc_html__( 'Show Loadmore','personal_trainer' ),
			'section' => 'blog_page_settings',
			'type' => 'select',
	      'choices' => array( // Optional.
	      	'no' => esc_html__( 'No','personal_trainer' ),
	      	'yes' => esc_html__( 'Yes','personal_trainer' ),
	      )
	  )
	);
}




















/**
 * Theme options in the Customizer js
 * @package personal_trainer
 */

function editor_customizer_script() {
	wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'personal_trainer_customize_admin_menu_hide', 999 );
function personal_trainer_customize_admin_menu_hide(){
	global $submenu;

    // Remove Appearance - Customize Menu
	unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
	$customize_url = add_query_arg(
		'return',
		urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
		'customize.php'
	);
    // Add sub menu page to the Dashboard menu.
	add_dashboard_page(
		'<div class="personal_trainer-theme-options"><span>'.esc_html__( 'personal_trainer','personal_trainer' ).'</span>'.esc_html__( 'Theme Options','personal_trainer' ).'</div>',
		'<div class="personal_trainer-theme-options"><span>'.esc_html__( 'personal_trainer','personal_trainer' ).'</span>'.esc_html__( 'Theme Options','personal_trainer' ).'</div>',
		'customize',
		esc_url( $customize_url ),
		''
	);
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

				$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}