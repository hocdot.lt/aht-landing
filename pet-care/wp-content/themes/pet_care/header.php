<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) :?>
        <?php if (!empty(get_theme_mod('site_icon'))): ?>
            <link rel="shortcut icon" href="<?php echo esc_url(str_replace(array('http:', 'https:'), '', get_theme_mod('site_icon'))); ?>" type="image/x-icon" />
        <?php endif; ?>
    <?php endif;?>  
    <?php wp_head(); ?>
</head>
<?php
    $classAdminBar = is_admin_bar_showing() ? 'adminbar':'';
?>
<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
        <header class="header-effect-shrink fixed-top <?php if(is_front_page()) echo 'header-transparent'; ?> " id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30}">
            <div class="header-body border-bottom-0">
                <div class="header-container container">
                    <div class="header-row row">
                        <div class="header-column column2">
                            <div class="header-row">
                                <div class="header-logo1">
                                    <div class="logo">
                                        <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                            <?php
                                            $logo_header = get_theme_mod('logo',get_template_directory_uri() . '/images/logo-header.png');
                                            if ($logo_header && $logo_header!=''):
                                                echo '<img class="logo-img"  src="' . esc_url(str_replace(array('http:', 'https:'), '', $logo_header)) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                            else:
                                                echo '<img class="logo-img"  src="' . get_template_directory_uri() . '/images/logo.png' . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '" />';
                                            endif;
                                            ?>
                                        </a>
                                    </div>
                                    <div class="desc-logo">
                                        <?php
                                        $title_logo_header = get_theme_mod('text','SALON');
                                        ?>
                                        <p><?php echo $title_logo_header?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-column column3">
                            <div class="header-row">
                                <div class="header-nav pt-1">
                                    <div class="header-nav-main">
                                        <nav class="collapse">
                                            <?php
                                            if (has_nav_menu('primary')) {
                                                wp_nav_menu(array(
                                                        'theme_location' => 'primary',
                                                        'menu_class' => 'mega-menu',
                                                        'items_wrap' =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                    )
                                                );
                                            }
                                            ?>
                                        </nav>
                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav" aria-expanded="true"><i class="fas fa-bars"></i></button>
                                        <!-- reponsive-->
                                        <div class="sidenav" id="mySidenav" onclick="closeNav()"><a class="closebtn" href="javascript:void(0)" onclick="closeNav()">×</a>
                                            <div class="showmenu">
                                                <?php
                                                if (has_nav_menu('primary')) {
                                                    wp_nav_menu(array(
                                                            'theme_location' => 'primary',
                                                            'menu_class' => 'mega-menu',
                                                            'items_wrap' =>  '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                                        )
                                                    );
                                                }
                                                ?>                                            </div>
                                        </div><span class="onclick" style="font-size:30px;cursor:pointer" onclick="openNav()">
                      <div class="span"></div>
                      <div class="span"></div>
                      <div class="span"></div></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
<!-- include components/menu-->
<!-- include components/page-title-->
        <div id="main" class="wrapper">
				
            

                    
        
       
