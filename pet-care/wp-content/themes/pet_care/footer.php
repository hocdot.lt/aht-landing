</div>
<footer class="mt-0" id="footer">
    <div class="footer-copyright">
        <div class="container py-2">
            <div class="nav-footer">
                <?php
                if(function_exists('render_header_menu')){
                    render_header_menu();
                }
                ?>
            </div>

            <div class="desc-footer">
                <?php
                $copyright =  get_theme_mod('copyright','<p class="text-center m-0">(C) All Rights Reserved. Charity Theme, Designed & Developed by<a href="#">Template.net</a></p>');;
                if ( $copyright !='') : ?>
                    <?php echo wp_kses_post( $copyright); ?>
                <?php endif;?>            </div>
            <div class="logo-footer">
                <div class="content-logo">
                    <?php
                    $logo = get_theme_mod('logo_footer',get_template_directory_uri() . '/images/logo.png');
                    ?>
                    <img src="<?php echo  $logo?>">
                </div>
            </div>
            <div class="icon-mxh clear-fix">
                <?php
                if(function_exists('pet_care_social_link')){
                    pet_care_social_link();
                }
                ?>
                </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</div> <!-- End page-->
</body>
</html>
