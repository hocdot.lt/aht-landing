<?php

/**
 * Implement Customizer additions and adjustments.
 *
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'pet_care_customize_register' );
function pet_care_customize_register( $wp_customize ) {

	remove_theme_support('custom-header');
	
	require_once( get_template_directory().'/inc/class/customizer-repeater-control.php' );

	if ( ! class_exists( 'pet_care_Customize_Sidebar_Control' ) ) {
	    class pet_care_Customize_Sidebar_Control extends WP_Customize_Control {
	        /**
	         * Render the control's content.
	         *
	         * @since 3.4.0
	         */
	        public function render_content() {

				$output = '
			        <select>';
						foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
						     $output .= '<option value="'.esc_attr( $sidebar['id'] ).'"'.'>'.
						              ucwords( $sidebar['name']).'</option>';
						}
					$output .= '</select>';

					$output = str_replace( '<select', '<select ' . $this->get_link(), $output );

					printf(
					    '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label,
					    $output
					);
					?>
					<?php if ( '' != $this->description ) : ?>
						<div class="description customize-control-description">
							<?php echo esc_html( $this->description ); ?>
						</div>
					<?php endif; 
	        }
	    }
	}
    if ( ! class_exists( 'My_Dropdown_Category_Control' ) ) {
        class My_Dropdown_Category_Control extends WP_Customize_Control {

            public $type = 'dropdown-category';

            public $dropdown_args = false;

            public function render_content() {
                $dropdown_args = wp_parse_args( $this->dropdown_args, array(
                    'taxonomy'          => 'category',
                    'show_option_none'  => ' ',
                    'selected'          => $this->value(),
                    'show_option_all'   => '',
                    'orderby'           => 'id',
                    'order'             => 'ASC',
                    'show_count'        => 1,
                    'hide_empty'        => 1,
                    'child_of'          => 0,
                    'exclude'           => '',
                    'hierarchical'      => 1,
                    'depth'             => 0,
                    'tab_index'         => 0,
                    'hide_if_empty'     => false,
                    'option_none_value' => 0,
                    'value_field'       => 'term_id',
                ) );

                $dropdown_args['echo'] = false;

                $dropdown = wp_dropdown_categories( $dropdown_args );
                $dropdown1 = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
                $dropdown1 = str_replace( 'cat', '', $dropdown1);
                printf('<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
					    $this->label, $dropdown1);
            }
        }
    }
	
	if ( ! class_exists( 'WP_Customize_Control' ) )
		return NULL;
	/**
	 * Class to create a custom post type control
	 */
	class Post_Type_Dropdown_Custom_Control extends WP_Customize_Control
	{
		private $posts = false;
		public function __construct($manager, $id, $args = array(), $options = array())
		{
			$postargs = array(
				'post_type' => 'service',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'orderby'	=> 'date',
				'order'	=> 'DESC',
			);
			$this->posts = get_posts($postargs);
			parent::__construct( $manager, $id, $args );
		}
		/**
		* Render the content on the theme customizer page
		*/
		public function render_content()
		{
			if(!empty($this->posts))
			{
				?>
					<label>
						<span class="customize-service-dropdown"><?php echo esc_html( $this->label ); ?></span>
						<select name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
						<?php
							foreach ( $this->posts as $post )
							{
								printf('<option value="%s" %s>%s</option>', $post->ID, selected($this->value(), $post->ID, false), $post->post_title);
							}
						?>
						</select>
					</label>
				<?php
			}
		}
	}
	
	/**
	 * Googe Font Select Custom Control
	 */
	 
	if ( ! class_exists( 'Google_font_Dropdown_Custom_Control' ) ) {
		class Google_font_Dropdown_Custom_Control extends WP_Customize_Control
		{

		  private $fonts = false;

		  public function __construct( $manager, $id, $args = array(), $options = array() ) {
			$this->fonts = $this->get_fonts();
			parent::__construct( $manager, $id, $args );
		  }

		  /**
		   * Render the content of the dropdown
		   *
		   * Adding the font-family styling to the select so that the font renders 
		   * @return HTML
		   */
		  public function render_content() {
			if ( ! empty( $this->fonts ) ) { ?>
			  <label>
				<span class="customize-category-select-control"><?php echo esc_html( $this->label ); ?></span>
				<select class="select-fonts" <?php $this->link(); ?>>
				  <?php
					foreach ( $this->fonts as $k=>$v ) {
					  printf('<option value="%s" %s>%s</option>', $k, selected($this->value(), $k, false), $v);
					}
				  ?>
				</select>
			  </label>
			<?php }
		  }
		  
		  /** 
		   * Get the list of fonts 
		   *
		   * @return string
		   */
		  function get_fonts() {
			$fonts = array(
				'Baloo Tamma' => 'Baloo Tamma',
				'Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Roboto',
				'Arial:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' => 'Arial',
				'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i' => 'Montserrat',
				'Old Standard TT:400,400i,700' => 'Old Standard TT',
				'Source Sans Pro:400,700,400i,700i' => 'Source Sans Pro',
				'Jura:300,400,500,600,700' => 'Jura',
				'Playfair Display:400,700,400i' => 'Playfair Display',
				'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i' => 'Open Sans',
				'Oswald:200,300,400,500,600,700' => 'Oswald',
				'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900i' => 'Raleway',
				'Droid Sans:400,700' => 'Droid Sans',
				'Lato:100,100i,300,300i,400,400i,700,700i,900,900i' => 'Lato',
				'Arvo:400,700,400i,700i' => 'Arvo',
				'Lora:400,700,400i,700i' => 'Lora',
				'Merriweather:400,300i,300,400i,700,700i' => 'Merriweather',
				'Oxygen:400,300,700' => 'Oxygen',
				'PT Serif:400,700' => 'PT Serif', 
				'PT Sans:400,700,400i,700i' => 'PT Sans',
				'PT Sans Narrow:400,700' => 'PT Sans Narrow',
				'Cabin:400,700,400i' => 'Cabin',
				'Fjalla One:400' => 'Fjalla One',
				'Francois One:400' => 'Francois One',
				'Josefin Sans:400,300,600,700' => 'Josefin Sans',  
				'Libre Baskerville:400,400i,700' => 'Libre Baskerville',
				'Arimo:400,700,400i,700i' => 'Arimo',
				'Ubuntu:400,700,400i,700i' => 'Ubuntu',
				'Bitter:400,700,400i' => 'Bitter',
				'Droid Serif:400,700,400i,700i' => 'Droid Serif',
				'Lobster:400' => 'Lobster',
				'Roboto:400,400i,700,700i' => 'Roboto',
				'Open Sans Condensed:700,300i,300' => 'Open Sans Condensed',
				'Roboto Condensed:400i,700i,400,700' => 'Roboto Condensed',
				'Roboto Slab:100,300,400,700' => 'Roboto Slab',
				'Yanone Kaffeesatz:400,700' => 'Yanone Kaffeesatz',
				'Mandali:400' => 'Mandali',
				'Vesper Libre:400,700' => 'Vesper Libre',
				'NTR:400' => 'NTR',
				'Dhurjati:400' => 'Dhurjati',
				'Faster One:400' => 'Faster One',
				'Mallanna:400' => 'Mallanna',
				'Averia Libre:400,300,700,400i,700i' => 'Averia Libre',
				'Galindo:400' => 'Galindo',
				'Titan One:400' => 'Titan One',
				'Abel:400' => 'Abel',
				'Nunito:400,300,700' => 'Nunito',
				'Poiret One:400' => 'Poiret One',
				'Signika:400,300,600,700' => 'Signika',
				'Muli:400,400i,300i,300' => 'Muli',
				'Play:400,700' => 'Play',
				'Bree Serif:400' => 'Bree Serif',
				'Archivo Narrow:400,400i,700,700i' => 'Archivo Narrow',
				'Cuprum:400,400i,700,700i' => 'Cuprum',
				'Noto Serif:400,400i,700,700i' => 'Noto Serif',
				'Pacifico:400' => 'Pacifico',
				'Alegreya:400,400i,700i,700,900,900i' => 'Alegreya',
				'Asap:400,400i,700,700i' => 'Asap',
				'Maven Pro:400,500,700' => 'Maven Pro',
				'Dancing Script:400,700' => 'Dancing Script',
				'Karla:400,700,400i,700i' => 'Karla',
				'Merriweather Sans:400,300,700,400i,700i' => 'Merriweather Sans',
				'Exo:400,300,400i,700,700i' => 'Exo',
				'Varela Round:400' => 'Varela Round',
				'Cabin Condensed:400,600,700' => 'Cabin Condensed',
				'PT Sans Caption:400,700' => 'PT Sans Caption',
				'Cinzel:400,700' => 'Cinzel',
				'News Cycle:400,700' => 'News Cycle',
				'Inconsolata:400,700' => 'Inconsolata',
				'Architects Daughter:400' => 'Architects Daughter',
				'Quicksand:400,700,300' => 'Quicksand',
				'Titillium Web:400,300,400i,700,700i' => 'Titillium Web',
				'Quicksand:400,700,300' => 'Quicksand',
				'Monda:400,700' => 'Monda',
				'Didact Gothic:400' => 'Didact Gothic',
				'Coming Soon:400' => 'Coming Soon',
				'Ropa Sans:400,400i' => 'Ropa Sans',
				'Tinos:400,400i,700,700i' => 'Tinos',
				'Glegoo:400,700' => 'Glegoo',
				'Pontano Sans:400' => 'Pontano Sans',
				'Fredoka One:400' => 'Fredoka One',
				'Lobster Two:400,400i,700,700i' => 'Lobster Two',
				'Quattrocento Sans:400,700,400i,700i' => 'Quattrocento Sans',
				'Covered By Your Grace:400' => 'Covered By Your Grace',
				'Changa One:400,400i' => 'Changa One',
				'Marvel:400,400i,700,700i' => 'Marvel',
				'BenchNine:400,700,300' => 'BenchNine',
				'Orbitron:400,700,500' => 'Orbitron',
				'Crimson Text:400,400i,600,700,700i' => 'Crimson Text',
				'Bangers:400' => 'Bangers',
				'Courgette:400' => 'Courgette',			
				'' => 'None',
			);
			return $fonts;
		  }		  
		}
	}
	
	/**
	 * TinyMCE Custom Control
	 *
	 */
	if ( ! class_exists( 'WP_TinyMCE_Custom_control' ) ) {
		class WP_TinyMCE_Custom_control extends WP_Customize_Control{
			public $type = 'textarea';
			/**
			** Render the content on the theme customizer page
			*/
			public function render_content() { ?>
				<label>
				  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				  <?php
					$settings = array(
						'media_buttons' => true,
						'quicktags' => true,
						'textarea_rows' => 5
					);
					$this->filter_editor_setting_link();
					wp_editor($this->value(), $this->id, $settings );
				  ?>
				</label>
			<?php
				do_action('admin_footer');
				do_action('admin_print_footer_scripts');
			}

			private function filter_editor_setting_link() {
				add_filter( 'the_editor', function( $output ) { return preg_replace( '/<textarea/', '<textarea ' . $this->get_link(), $output, 1 ); } );
			}
		}
	}
	
	/* Multi Input field */
	 
	class Multi_Input_Custom_control extends WP_Customize_Control{
		public $type = 'multi_input';
		public function render_content(){
			?>
			<label class="customize_multi_input">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<p><?php echo wp_kses_post($this->description); ?></p>
				<input type="hidden" id="<?php echo esc_attr($this->id); ?>" name="<?php echo esc_attr($this->id); ?>" value="<?php echo esc_attr($this->value()); ?>" class="customize_multi_value_field" data-customize-setting-link="<?php echo esc_attr($this->id); ?>"/>
				<div class="customize_multi_fields">
					<div class="set">
						<input type="text" value="" placeholder="Title" class="customize_multi_single_field"/>
						<a href="#" class="customize_multi_remove_field">X</a>
					</div>
				</div>
				<a href="#" class="button button-primary customize_multi_add_field"><?php esc_attr_e('Add More', 'pet_care') ?></a>
			</label>
			<?php
		}
	}

	/**
	 * Add Option Panel
	 */

	// General
	$wp_customize->add_section( 'general_settings',
	   array(
	      'title' => esc_html__( 'General Settings','pet_care'  ),
	      'priority' => 20, 
	   )
	);
	$wp_customize->add_setting( 'logo',
		array(
			'default' => get_template_directory_uri() . '/images/logo-header.png',
		)
	);	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo',
	   array(
	      'label' => esc_html__( 'Logo','pet_care' ),
	      'description' => esc_html__( 'Upload Logo','pet_care' ),
	      'section' => 'general_settings',
	   )
	) );
	/// Info header
    	$wp_customize->add_setting( 'text',
    	   array(
    	      'default' => esc_html__('SALON'),
    	   )
    	);
    	$wp_customize->add_control( 'text',
    	   array(
    	      'label' => esc_html__( 'Text','pet_care' ),
    	      'section' => 'general_settings',
    	      'type' => 'text',
    	   )
    	);
	
	$wp_customize->add_setting( 'google_font_h',
		array(
			'default' => 'Montserrat:100,100i,200i,300,400,500,500i,600,600i,700,700i,800,800i,900,900i',
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font_h',
	   array(
	      'label' => 'Font Family Tab H',
	      'section' => 'general_settings',
	   )
	) );
	$wp_customize->add_setting( 'google_font',
		array(
			'default' => 'Arial:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i' ,
		)
	);	
	$wp_customize->add_control( new Google_font_Dropdown_Custom_Control( $wp_customize, 'google_font',
	   array(
	      'label' => 'Main Google Font',
	      'section' => 'general_settings',
	   )
	) );

	// End Header
	
	// Panel Home Options

    $wp_customize->add_panel( 'home_page_setting', array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'title'          => __('Home Options', 'pet_care'),
        'description'    => __('Several settings pertaining my theme', 'pet_care'),
    ) );
	// Start Banner section
	$wp_customize->add_section( 'home_banner_settings',
	   array(
	      'title' => esc_html__( 'Banner Section','pet_care'  ),
	      'priority' => 20,
		  'panel'  => 'home_page_setting',
	   )
	);

	// title banner
	$wp_customize->add_setting( 'banner_title',
                array(
                    'default' => '<h1>Everything you need to keep your cat, dog, or small pet happy and healthy</h1>',
                )
            );
            $wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'banner_title',
                array(
                    'label' => esc_html__( 'Title Banner','pet_care' ),
                    'description' => esc_html__( 'Title Banner','pet_care' ),
                    'section' => 'home_banner_settings',
                )
            ));

	// button 1 banner
	$wp_customize->add_setting( 'button_1_banner',
	   array(
	      'default' => esc_html__('Join Us','pet_care'),

	   )
	);
	$wp_customize->add_control('button_1_banner',
		array(
	      'label' => esc_html__( 'Button 1 banner','pet_care' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',
	   )
	);// button 2 banner
	$wp_customize->add_setting( 'button_2_banner',
	   array(
	      'default' => esc_html__('Explore','pet_care'),

	   )
	);
	$wp_customize->add_control('button_2_banner',
		array(
	      'label' => esc_html__( 'Button 2 banner','pet_care' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',
	   )
	);
	//link1
	$wp_customize->add_setting( 'link_1_banner',
	   array(
	      'default' => esc_html__('#','pet_care'),

	   )
	);
	$wp_customize->add_control('link_1_banner',
		array(
	      'label' => esc_html__( 'Button URL 1','pet_care' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',
	   )
	);
	//link2
	$wp_customize->add_setting( 'link_2_banner',
	   array(
	      'default' => esc_html__('#','pet_care'),

	   )
	);
	//link2
	$wp_customize->add_control('link_2_banner',
		array(
	      'label' => esc_html__( 'Button URL 2','pet_care' ),
	      'section' => 'home_banner_settings',
	       'type' => 'text',
	   )
	);
	// img right banner
	$wp_customize->add_setting( 'img1_banner_right',
		array(
			'default' => get_template_directory_uri() . '/images/img-banner.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img1_banner_right',
	   array(
	      'label' => esc_html__( 'Images 1','pet_care' ),
	      'description' => esc_html__( 'Upload Image','pet_care' ),
	      'section' => 'home_banner_settings',
	   )
	) );// img right banner
	$wp_customize->add_setting( 'img2_banner_right',
		array(
			'default' => get_template_directory_uri() . '/images/zoom1.jpg',
		)
	);
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'img2_banner_right',
	   array(
	      'label' => esc_html__( 'Images 2','pet_care' ),
	      'description' => esc_html__( 'Upload Image','pet_care' ),
	      'section' => 'home_banner_settings',
	   )
	) );

	 //Show/Hide banner
	$wp_customize->add_setting( 'banner_show',
	   array(
	      'default' => 'yes',
	   )
	);
	$wp_customize->add_control( 'banner_show',
	   array(
            'label' => esc_html__( 'Show Section','pet_care' ),
            'section' => 'home_banner_settings',
            'type' => 'select',
            'choices' => array(
	         'no' => esc_html__( 'No','pet_care' ),
	         'yes' => esc_html__( 'Yes','pet_care' ),
            )
	   )
	);
	// End banner section
    // Start section-one
    $wp_customize->add_section( 'section_one_settings',
    array(
        'title' => esc_html__( 'section one','pet_care' ),
        'priority' => 20,
        'panel' => 'home_page_setting',
        )
        );
    //title section one
    $wp_customize->add_setting( 'title_section_one',
    	array(
        	'default' => 'We have been developing innovative health products for the pets you love.',
        	)
    );
    $wp_customize->add_control('title_section_one',
    	array(
        'label' => esc_html__( 'Title ','pet_care' ),
        'section' => 'section_one_settings',
        'type' => 'text',
    ));
    //  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

    $wp_customize->add_setting( 'section_one_category',
        array(
            'default' => $default,
        )
    );
    $wp_customize->add_control('section_one_category',
        array(
            'label' => esc_html__( 'Category Post','pet_care' ),
            'section' => 'section_one_settings',
            'type' => 'select',
            'choices' => $cats,
        )
    );
    //Number post
    $wp_customize->add_setting( 'section_one_number_post',
        array(
            'default' => esc_html__('3','pet_care'),
        )
    );
    $wp_customize->add_control('section_one_number_post',
        array(
            'label' => esc_html__( 'Number Post','pet_care' ),
            'section' => 'section_one_settings',
            'type' => 'text',
        )
    );
    //show/hide section one
    $wp_customize->add_setting( 'section_one_show',
    array(
        'default' => 'yes',
        )
        );
    $wp_customize->add_control( 'section_one_show',
    array(
        'label' => esc_html__( 'Show Section One','pet_care' ),
        'section' => 'section_one_settings',
        'type' => 'select',
        'choices' => array(
        'no' => esc_html__( 'No','pet_care' ),
        'yes' => esc_html__( 'Yes','pet_care' ),
        )
        )
        );

        // End  section one
         // Start section two
         $wp_customize->add_section( 'Section_two_settings',
              	   array(
              	      'title' => esc_html__( 'section two','pet_care'  ),
              	      'priority' => 20,
              		  'panel'  => 'home_page_setting',
              	   )
              	);
         // title section two
               $wp_customize->add_setting( 'title_section_two',
               array(
                   'default' => 'A fun place for dogs! PetCare provides primary and specialty veterinary care for your pet.',
                   )
                   );
               $wp_customize->add_control( 'title_section_two',
               array(
                   'label' => esc_html__( 'Title','pet_care' ),
                   'section' => 'Section_two_settings',
                   'type' => 'text',
                   )
                   );
            $wp_customize->add_setting( 'background_right_two',
                array(
                    'default' => get_template_directory_uri() . '/images/images1.jpg',
                )
            );
            $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_right_two',
                array(
                    'label' => esc_html__( 'Background Right','pet_care' ),
                    'description' => esc_html__( 'Upload Image','pet_care' ),
                    'section' => 'Section_two_settings',
                )
            ) );
         //content top section two
//  Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

            $wp_customize->add_setting( 'section_two_category',
                array(
                    'default' => $default,
                )
            );
            $wp_customize->add_control('section_two_category',
                array(
                    'label' => esc_html__( 'Category Post','pet_care' ),
                    'section' => 'Section_two_settings',
                    'type' => 'select',
                    'choices' => $cats,
                )
            );

                //Number post
                $wp_customize->add_setting( 'section_two_number_post',
                array(
                    'default' => esc_html__('6','pet_care'),
                    )
                    );
                $wp_customize->add_control('section_two_number_post',
                array(
                    'label' => esc_html__( 'Number Post','pet_care' ),
                    'section' => 'Section_two_settings',
                    'type' => 'text',
                    )
                    );
                //link post
                $wp_customize->add_setting( 'link_two',
                array(
                    'default' => esc_html__('#','pet_care'),
                    )
                    );
                $wp_customize->add_control('link_two',
                array(
                    'label' => esc_html__( 'Link All','pet_care' ),
                    'section' => 'Section_two_settings',
                    'type' => 'text',
                    )
                    );
         //show/hide Section two
         $wp_customize->add_setting( 'section_two_show',
         array(
             'default' => 'yes',
             )
             );
         $wp_customize->add_control( 'section_two_show',
         array(
             'label' => esc_html__( 'Show Section Info','pet_care' ),
             'section' => 'Section_two_settings',
             'type' => 'select',
             'choices' => array(
             'no' => esc_html__( 'No','pet_care' ),
             'yes' => esc_html__( 'Yes','pet_care' ),
             )
             )
             );
         // End  section two
         // Start section three
         $wp_customize->add_section( 'section_three_settings',
              	   array(
              	      'title' => esc_html__( 'Section Three','pet_care'  ),
              	      'priority' => 20,
              		  'panel'  => 'home_page_setting',
              	   )
              	);

        //title section three
            $wp_customize->add_setting( 'title_section_three',
            array(
                'default' => 'View Our Photo Gallery',
                )
                );
            $wp_customize->add_control( 'title_section_three',
            array(
                'label' => esc_html__( 'Title ','pet_care' ),
                'section' => 'section_three_settings',
                ));
        //sub title three
            $wp_customize->add_setting( 'sub_title_section_three',
            array(
                'default' => esc_html__('Dedicated to providing the highest quality veterinary care in a warm and friendly atmosphere.'),
                )
                );
            $wp_customize->add_control( 'sub_title_section_three',
            array(
                'label' => esc_html__( 'Sub Title','pet_care' ),
                'section' => 'section_three_settings',
                'type' => 'textarea',
                )
                );

            //Number post
                $wp_customize->add_setting( 'section_three_number_post',
                array(
                    'default' => esc_html__('15','pet_care'),
                    )
                    );
                $wp_customize->add_control('section_three_number_post',
                array(
                    'label' => esc_html__( 'Number Gallery','pet_care' ),
                    'section' => 'section_three_settings',
                    'type' => 'text',
                    )
                    );
        //linksection three
            $wp_customize->add_setting( 'link_section_three',
                   array(
                      'default' => esc_html__('#','pet_care'),

                   )
                );
                $wp_customize->add_control('link_section_three',
                    array(
                      'label' => esc_html__( 'Link Button','pet_care' ),
                      'section' => 'section_three_settings',
                       'type' => 'text',
                   )
    	);
          //show/hide section three
          $wp_customize->add_setting( 'section_three_show',
          array(
              'default' => 'yes',
              )
              );
          $wp_customize->add_control( 'section_three_show',
          array(
              'label' => esc_html__( 'Show Section Info','pet_care' ),
              'section' => 'section_three_settings',
              'type' => 'select',
              'choices' => array(
              'no' => esc_html__( 'No','pet_care' ),
              'yes' => esc_html__( 'Yes','pet_care' ),
              )
              )
              );
         // End  section three
            // Start section for
          $wp_customize->add_section( 'section_for_settings',
               	   array(
               	      'title' => esc_html__( 'Section For','pet_care'  ),
               	      'priority' => 20,
               		  'panel'  => 'home_page_setting',
               	   )
               	);
         //title section for
            $wp_customize->add_setting( 'title_section_for',
                array(
                    'default' => esc_html__('Read Interesting Pet Articles'),
                    )
                    );
                $wp_customize->add_control( 'title_section_for',
                array(
                    'label' => esc_html__( 'Title','pet_care' ),
                    'section' => 'section_for_settings',
                    'type' => 'text',
                    )
                    );
         //sub title for
             $wp_customize->add_setting( 'sub_title_section_for',
             array(
                 'default' => esc_html__('Dedicated to providing the highest quality veterinary care in a warm and friendly atmosphere.'),
                 )
                 );
             $wp_customize->add_control( 'sub_title_section_for',
             array(
                 'label' => esc_html__( 'Sub Title','pet_care' ),
                 'section' => 'section_for_settings',
                 'type' => 'textarea',
                 )
                 );
    //Category Dropdown
    $categories = get_categories();
    $cats = array();
    $i = 0;
    foreach($categories as $category){
        if($i==0){
            $default = $category->term_id;
            $i++;
        }
        $cats[$category->term_id] = $category->name;
    }

            $wp_customize->add_setting( 'section_for_category',
                array(
                    'default' => $default,
                )
            );
            $wp_customize->add_control('section_for_category',
                array(
                    'label' => esc_html__( 'Category Post','pet_care' ),
                    'section' => 'section_for_settings',
                    'type' => 'select',
                    'choices' => $cats,
                )
            );
                //Number post
                $wp_customize->add_setting( 'section_for_number_post',
                array(
                'default' => esc_html__('4','pet_care'),
                )
                );
                $wp_customize->add_control('section_for_number_post',
                array(
                'label' => esc_html__( 'Number Post','pet_care' ),
                'section' => 'section_for_settings',
                'type' => 'text',
                )
                );
            //show/hide section for
          $wp_customize->add_setting( 'section_for_show',
          array(
              'default' => 'yes',
              )
              );
          $wp_customize->add_control( 'section_for_show',
          array(
              'label' => esc_html__( 'Show Section for','pet_care' ),
              'section' => 'section_for_settings',
              'type' => 'select',
              'choices' => array(
              'no' => esc_html__( 'No','pet_care' ),
              'yes' => esc_html__( 'Yes','pet_care' ),
              )
              )
              );
              //section five
         $wp_customize->add_section( 'section_five_settings',
              	   array(
              	      'title' => esc_html__( 'Section Five','pet_care'  ),
              	      'priority' => 20,
              		  'panel'  => 'home_page_setting',
              	   )
              	);

            //title section five
              $wp_customize->add_setting( 'title_section_five',
                  array(
                      'default' => esc_html__('Clients Feedback'),
                      )
                      );
                  $wp_customize->add_control( 'title_section_five',
                  array(
                      'label' => esc_html__( 'Title','pet_care' ),
                      'section' => 'section_five_settings',
                      'type' => 'text',
                      )
                      );
                  //sub title five
               $wp_customize->add_setting( 'sub_title_section_five',
               array(
                   'default' => esc_html__('Dedicated to providing the highest quality veterinary care in a warm'),
                   )
                   );
               $wp_customize->add_control( 'sub_title_section_five',
               array(
                   'label' => esc_html__( 'Sub Title','pet_care' ),
                   'section' => 'section_five_settings',
                   'type' => 'textarea',
                   )
                   );

            //Number post
            $wp_customize->add_setting( 'section_five_number_five',
                array(
                    'default' => esc_html__('4','pet_care'),
                )
            );
            $wp_customize->add_control('section_five_number_five',
                array(
                    'label' => esc_html__( 'Number Five','pet_care' ),
                    'section' => 'section_five_settings',
                    'type' => 'text',
                )
            );
             //show/hide section five
               $wp_customize->add_setting( 'section_five_show',
               array(
                   'default' => 'yes',
                   )
                   );
               $wp_customize->add_control( 'section_five_show',
               array(
                   'label' => esc_html__( 'Show Section Five','pet_care' ),
                   'section' => 'section_five_settings',
                   'type' => 'select',
                   'choices' => array(
                   'no' => esc_html__( 'No','pet_care' ),
                   'yes' => esc_html__( 'Yes','pet_care' ),
                   )
                   )
                   );
            //contact
                 $wp_customize->add_section( 'Contact_settings',
              	   array(
              	      'title' => esc_html__( 'Contact','pet_care'  ),
              	      'priority' => 20,
              		  'panel'  => 'home_page_setting',
              	   )
              	);
                //zoom1 section_zoom
                //Contact shortcode
                $wp_customize->add_setting( 'form_shortcode',
                    array(
                        'default' => '',
                    )
                );
                $wp_customize->add_control( 'form_shortcode',
                    array(
                        'label' => esc_html__( 'Form shortcode','pet_care' ),
                        'section' => 'Contact_settings',
                        'type' => 'text'
                    )
                );
                   $wp_customize->add_setting( 'Background_contact',
               array(
                   'default' => get_template_directory_uri() . '/images/banner1.jpg',
                   )
                   );
               $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'Background_contact',
               array(
                   'label' => esc_html__( 'Background Contact','pet_care' ),
                   'description' => esc_html__( 'Upload Image','pet_care' ),
                   'section' => 'Contact_settings',
                   )
                   ) );
                //title section contact
                  $wp_customize->add_setting( 'title_conatct',
                      array(
                          'default' => esc_html__(' Make an Appointment'),
                          )
                          );
                      $wp_customize->add_control( 'title_conatct',
                      array(
                          'label' => esc_html__( 'Title','pet_care' ),
                          'section' => 'Contact_settings',
                          'type' => 'text',
                          )
                          );
               //sub title contact
                   $wp_customize->add_setting( 'sub_title_contact',
                   array(
                       'default' => esc_html__('“Everything you need to keep your cat, dog, or small pet happy and healthy”'),
                       )
                       );
                   $wp_customize->add_control( 'sub_title_contact',
                   array(
                       'label' => esc_html__( 'Sub Title','pet_care' ),
                       'section' => 'Contact_settings',
                       'type' => 'textarea',
                       )
                       );
                 //info
                   $wp_customize->add_setting( 'address_title_contact',
                   array(
                       'default' => esc_html__('Visit us at 123 Street, City, State 45678'),
                       )
                       );
                   $wp_customize->add_control( 'address_title_contact',
                   array(
                       'label' => esc_html__( 'address','pet_care' ),
                       'section' => 'Contact_settings',
                       'type' => 'textarea',
                       )
                       );
                       //mail1
                   $wp_customize->add_setting( 'Mail_1_contact',
                   array(
                       'default' => esc_html__('companyname.com'),
                       )
                       );
                   $wp_customize->add_control( 'Mail_1_contact',
                   array(
                       'label' => esc_html__( 'Mail 1','pet_care' ),
                       'section' => 'Contact_settings',
                       'type' => 'text',
                       )
                       );
                   //mail2
                   $wp_customize->add_setting( 'Mail_2_contact',
                   array(
                       'default' => esc_html__('info@companyname.com'),
                       )
                       );
                   $wp_customize->add_control( 'Mail_2_contact',
                   array(
                       'label' => esc_html__( 'Mail 2','pet_care' ),
                       'section' => 'Contact_settings',
                       'type' => 'text',
                       )
                       );
                       //phone
                   $wp_customize->add_setting( 'phone_contact',
                   array(
                       'default' => esc_html__('80 1234 56789'),
                       )
                       );
                   $wp_customize->add_control( 'phone_contact',
                   array(
                       'label' => esc_html__( 'Phone','pet_care' ),
                       'section' => 'Contact_settings',
                       'type' => 'text',
                       )
                       );

                        //show/hide section contact
                        $wp_customize->add_setting( 'contact_show',
                        array(
                            'default' => 'yes',
                            )
                            );
                        $wp_customize->add_control( 'contact_show',
                        array(
                            'label' => esc_html__( 'Show Contact','pet_care' ),
                            'section' => 'Contact_settings',
                            'type' => 'select',
                            'choices' => array(
                            'no' => esc_html__( 'No','pet_care' ),
                            'yes' => esc_html__( 'Yes','pet_care' ),
                            )
                            )
                        );

       //

	// Footer
	$wp_customize->add_section( 'footer_settings',
	   array(
	      'title' => esc_html__( 'Footer Settings','pet_care'  ),
	      'priority' => 35, 
	   )
	);

	$wp_customize->add_setting( 'copyright',
		array(
			'default' => '(C) 2018. AllRights Reserved. Divine Makeup Artist. Designed by <a href="#">Template.net</a>',
			'sanitize_callback' => 'wp_kses_post',
		)
	);
	$wp_customize->add_control( new WP_TinyMCE_Custom_control( $wp_customize, 'copyright',
	   array(
	      'label' => esc_html__( 'Copyright text','pet_care' ),
	      'section' => 'footer_settings',
	   )
	) );
	$wp_customize->add_setting( 'logo_footer',
        array(
            'default' => get_template_directory_uri() . '/images/logo.png',
            )
            );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_footer',
        array(
            'label' => esc_html__( 'logo Footer','pet_care' ),
            'description' => esc_html__( 'Upload Image','pet_care' ),
            'section' => 'footer_settings',
            )
            ) );
	//end footer

	// Social	
	$wp_customize->add_section( 'social_settings',
	   array(
	      'title' => esc_html__( 'Social','pet_care'  ),
	      'priority' => 35, 
	   )
	);
	$wp_customize->add_setting( 'show_social',
	   array(
	      'default' => 'yes',
	   )
	);	
	$wp_customize->add_control( 'show_social',
	   array(
	      'label' => esc_html__( 'Show Social','pet_care' ),
	      'section' => 'social_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','pet_care' ),
	         'yes' => esc_html__( 'Yes','pet_care' ),
	      )
	   )
	);		

	$social_links = array(
		'facebook' => esc_html__('Facebook','pet_care'),
		'instagram'=> esc_html__('Instagram','pet_care'),
		'twitter'=> esc_html__('Twitter','pet_care'),
		'google'=> esc_html__('Google','pet_care'),
		'linkedin'=> esc_html__('Linkedin','pet_care')
	);	
	foreach ($social_links as $key => $value) {
		$wp_customize->add_setting( $key,
		   array(
		      'default' => '#',
		   )
		);	
		$wp_customize->add_control( $key,
		   array(
		      'label' => esc_html( $value),
		      'section' => 'social_settings',
		      'type' => 'text',
		   )
		);	
	}	
	foreach ($social_links as $key => $value) {
			$wp_customize->add_setting( 'share_'.$key,
			   array(
				  'default' => 'yes',
			   )
			);		
		$wp_customize->add_control( 'share_'.$key,
		   array(
		      'label' => esc_html( 'Share '.$value),
		      'section' => 'social_settings',
			  'type' => 'select',
			  'choices' => array( // Optional.
				 'no' => esc_html__( 'No','pet_care' ),
				 'yes' => esc_html__( 'Yes','pet_care' ),
			  )
		   )
		);	
	}

	// Blog
	$wp_customize->add_section( 'blog_page_settings',
	   array(
	      'title' => esc_html__( 'Blog Settings','pet_care'  ),
	      'priority' => 35, 
	   )
	);	 
	$wp_customize->add_setting('blog_page_title',
	   array(
	      'default' => esc_html__('Blog Articles','pet_care'),
	   )
	);    
   	$wp_customize->add_control( 'blog_page_title',
	   array(
	      'label' => esc_html__( 'Blog page Title','pet_care' ),
	      'section' => 'blog_page_settings',
	      'type' => 'text',
	   )
	);
	
    $wp_customize->add_setting( 'blog_single_related',
	   array(
	      'default' => 'yes',
	   )
	);

	$wp_customize->add_control( 'blog_single_related',
	   	array(
	      'label' => esc_html__( 'Related Posts','pet_care' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( 
	         'no' => esc_html__( 'No','pet_care' ),
	         'yes' => esc_html__( 'Yes','pet_care' ),
	      )
	    )
	);
	
	$wp_customize->add_setting( 'show_loadmore_blog',
	   array(
	      'default' => 'no',
	   )
	);	
	$wp_customize->add_control( 'show_loadmore_blog',
	   array(
	      'label' => esc_html__( 'Show Loadmore','pet_care' ),
	      'section' => 'blog_page_settings',
	      'type' => 'select',
	      'choices' => array( // Optional.
	         'no' => esc_html__( 'No','pet_care' ),
	         'yes' => esc_html__( 'Yes','pet_care' ),
	      )
	   )
	);
}




















/**
 * Theme options in the Customizer js
 * @package pet_care
 */

function editor_customizer_script() {
    wp_enqueue_script( 'wp-editor-customizer', get_template_directory_uri() . '/inc/admin/js/customizer.js', array( 'jquery' ), false, true );
    wp_enqueue_script( 'customizer_repeater', get_template_directory_uri() . '/inc/class/customizer_repeater.js', array( 'jquery' ), false, true );
}
add_action( 'customize_controls_enqueue_scripts', 'editor_customizer_script' );

add_action( 'admin_menu', 'pet_care_customize_admin_menu_hide', 999 );
function pet_care_customize_admin_menu_hide(){
    global $submenu;

    // Remove Appearance - Customize Menu
    unset( $submenu[ 'themes.php' ][ 6 ] );

    // Create URL.
    $customize_url = add_query_arg(
        'return',
        urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ),
        'customize.php'
    );
    // Add sub menu page to the Dashboard menu.
    add_dashboard_page(
        '<div class="pet_care-theme-options"><span>'.esc_html__( 'pet_care','pet_care' ).'</span>'.esc_html__( 'Theme Options','pet_care' ).'</div>',
         '<div class="pet_care-theme-options"><span>'.esc_html__( 'pet_care','pet_care' ).'</span>'.esc_html__( 'Theme Options','pet_care' ).'</div>',
        'customize',
        esc_url( $customize_url ),
        ''
    );
}

function customizer_repeater_sanitize($input){
	$input_decoded = json_decode($input,true);

	if(!empty($input_decoded)) {
		foreach ($input_decoded as $boxk => $box ){
			foreach ($box as $key => $value){

					$input_decoded[$boxk][$key] = wp_kses_post( force_balance_tags( $value ) );

			}
		}
		return json_encode($input_decoded);
	}
	return $input;
}