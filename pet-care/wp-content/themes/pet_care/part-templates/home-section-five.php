<?php
/**
 * Render section one in home page
 */
//GET config
$sectionFiveShow = get_theme_mod('section_for_show','yes');
$TitleSectionfive = get_theme_mod('title_section_five','Clients Feedback');
$SubTitleSectionFor = get_theme_mod('sub_title_section_five','Dedicated to providing the highest quality veterinary care in a warm');
$FiveCategory = get_theme_mod('section_five_category','');
$FiveLimit = get_theme_mod('section_five_number_five',4);

$args = array(
    'posts_per_page'   => $FiveLimit,
    'offset'           => 0,
    'cat'         => $FiveCategory,
    'post_type'        => 'testimonial',
);
?>
<?php if($sectionFiveShow === 'yes'):?>
<section id="section-five">
    <div class="container">
        <div class="row justify-content-center">
            <div class="header-sective-five">
                <h2><?php echo $TitleSectionfive?></h2>
                <p class="text-desc"><?php echo $SubTitleSectionFor?></p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="owl-carousel owl-theme stage-margin mb-0" data-plugin-options="{'responsive': {'576': {'items': 1}, '768': {'items': 1}, '992': {'items': 1}, '1200': {'items': 2}}, 'margin': 25, 'loop': false, 'nav': false, 'dots': true, 'stagePadding': 40}">
                <?php
                // the query
                $the_query = new WP_Query( $args ); ?>
                <?php if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-lg-3"><img class="img-fluid mb-4" src="<?php echo the_post_thumbnail_url() ?>" alt=""></div>
                                <div class="col-lg-5">
                                    <p><?php echo get_the_excerpt()?></p>
                                    <strong><?php echo the_title()?></strong>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_postdata()?>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif ?>