<?php
/**
 * Render section three in home page
 */
//GET config
$sectionThreeShow = get_theme_mod('section_three_show', 'yes');
$TitleThree = get_theme_mod('title_section_three', 'View Our Photo Gallery');
$SubTitleThree = get_theme_mod('sub_title_section_three', 'Dedicated to providing the highest quality veterinary care in a warm and friendly atmosphere.');
$linkbtn = get_theme_mod('link_section_three', '#');
$ThreeLimit = get_theme_mod('section_three_number_post',15);
$args = array(
    'posts_per_page'   => $ThreeLimit,
    'offset'           => 0,
    'post_status' => 'publish',
    'post_type'        => 'gallery',
);
$i=1;

?>
<?php if($sectionThreeShow ==='yes'):?>
<section id="section-three">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="header-section3">
                    <h2><?php echo $TitleThree?></h2>
                    <p class="text-desc"><?php echo $SubTitleThree?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="zoom-fullwidth">
        <article class="post post-large">
            <div class="post-image">
                <div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}, 'mainClass': 'mfp-with-zoom', 'zoom': {'enabled': true, 'duration': 300}}">
                    <div class="row mx-0">
                        <?php
                        // the query
                        $the_query = new WP_Query( $args ); ?>
                        <?php if ( $the_query->have_posts() ) : ?>
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php if ( ($i%5==1 || $i%5==0) ) : ?>
                        <div class="col-md-1 p-0"><a href="<?php echo the_post_thumbnail_url()?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo the_post_thumbnail_url()?>" alt="<?php the_title(); ?>"><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search"></i></span></span></span></span></a>
                        </div>
                        <?php else :?>
                        <div class="col-md-2 p-0"><a href="<?php echo the_post_thumbnail_url()?>"><span class="thumb-info thumb-info-no-borders thumb-info-centered-icons"><span class="thumb-info-wrapper"><img class="img-fluid" src="<?php echo the_post_thumbnail_url()?>" alt="<?php the_title(); ?>"><span class="thumb-info-action"><span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-search"></i></span></span></span></span></a>
                        </div>
                        <?php endif;?>
                            <?php $i++;endwhile; ?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </article>
        <div class="btncenter text-center">
            <a class="btn" href="<?php echo $linkbtn?>">go to gallery</a>
        </div>
    </div>
</section>
<?php endif; ?>
<!---->