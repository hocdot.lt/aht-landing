<?php
/**
 * Render section one in home page
 */
//GET config
$sectionForShow = get_theme_mod('section_for_show','yes');
$TitleSectionFor = get_theme_mod('title_section_for','Read Interesting Pet Articles');
$SubTitleSectionFor = get_theme_mod('sub_title_section_for','Dedicated to providing the highest quality veterinary care in a warm and friendly atmosphere.');
$ForCategory = get_theme_mod('section_for_category','');
$ForLimit = get_theme_mod('section_for_number_post',4);

$args = array(
    'posts_per_page'   => $ForLimit,
    'offset'           => 0,
    'cat'         => $ForCategory,
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );

?>
<section id="section-for">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="header-section4">
                    <h2><?php echo $TitleSectionFor?></h2>
                    <p class="text-desc"><?php echo $SubTitleSectionFor?></p>
                </div>
            </div>
            <div class="col-md-8 nd-silder">
                <div class="owl-carousel owl-theme nav-style-1 nav-center-images-only stage-margin mb-0" data-plugin-options="{'responsive': {'576': {'items': 1}, '768': {'items': 1}, '992': {'items': 1}, '1200': {'items': 2}}, 'margin': 30, 'loop': false, 'nav': true, 'dots': false,'stagePadding': 0}">

                    <?php
                    // the query
                    $the_query = new WP_Query( $args ); ?>
                    <?php if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                        <div class="row">
                            <div class="col-lg-5"><img class="img-fluid rounded-0 mb-4" src="<?php echo the_post_thumbnail_url()?>" alt="">
                                <div class="desc-img">
                                    <div class="nd-desc">
                                        <p>21 - January</p>
                                        <p>Antony Markins</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <a href="<?php echo the_permalink()?>"><?php echo the_title()?></a>
                                <p><?php echo get_the_excerpt()?></p>
                            </div>
                        </div>


                        <?php endwhile; ?>
                    <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
</section>