<?php
/**
 * Render section one in home page
 */
//GET config
$sectionOneShow = get_theme_mod('section_one_show','yes');
$TitleSectionOne = get_theme_mod('title_section_one','We have been developing innovative health products for the pets you love.');
$oneCategory = get_theme_mod('section_one_category','');
$oneLimit = get_theme_mod('section_one_number_post',3);

$args = array(
    'posts_per_page'   => $oneLimit,
    'offset'           => 0,
    'cat'         => $oneCategory,
    'post_type'        => 'post',
);
$posts_array = get_posts( $args );

?>
<section id="section-one">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="header-section1">
                    <h3><?php echo $TitleSectionOne?></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="content-section1">
        <div class="container">
            <div class="row">
                <?php
                // the query
                $the_query = new WP_Query( $args ); ?>
                <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="col-md mb-4 mb-md-0 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200">
                    <div class="row justify-content-center justify-content-md-start">
                        <div class="col-2 no-pl col"><img class="img-fluid" src=" <?php echo the_post_thumbnail_url(); ?>"></div>
                        <div class="col-lg-6 col">
                            <div class="text-content1">
                                <a class="mb-1" href="<?php echo the_permalink() ?>"><?php echo the_title()?></a>
                                <p class="mb-0">
                                    <?php echo wp_trim_words( get_the_excerpt(), $num_words = 30, '' ) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile;wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>