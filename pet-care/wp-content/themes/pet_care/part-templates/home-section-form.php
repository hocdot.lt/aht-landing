
<?php
/**
 * Render banner in home page
 */
//GET config
$contactShow = get_theme_mod('contact_show','yes');
$BgContact = get_theme_mod('Background_contact',get_template_directory_uri() . '/images/banner1.jpg');
$TitleContact = get_theme_mod('title_conatct','Make an Appointment');
$SubtitleContact = get_theme_mod('sub_title_contact','“Everything you need to keep your cat, dog, or small pet happy and healthy”');
$address = get_theme_mod('address_title_contact','Visit us at 123 Street, City, State 45678');
$Mail1 = get_theme_mod('Mail_1_contact','companyname.com');
$Mail2 = get_theme_mod('Mail_2_contact','info@companyname.com');
$Phone = get_theme_mod('phone_contact','80 1234 56789');
$contactShortcode = get_theme_mod('form_shortcode','[contact-form-7 id="5" title="Contact"]');

?>
<?php if($contactShow ==='yes'): ?>
<section id="section-form" style="background-image: url(<?php echo $BgContact?>)">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="content-form-left">
                    <div class="title-ct-left">
                        <h3><?php echo $TitleContact ?></h3>
                    </div>
                    <div class="desc-form-left">
                        <p class="text-desc"><?php echo $SubtitleContact?></p>
                    </div>
                    <div class="info-form">
                        <p><?php echo $address?></p>
                        <div class="mail"><a href="#"><?php echo $Mail1?></a></div>
                        <div class="mail"><a href="#"><?php echo $Mail2?></a></div>
                        <div class="phone">
                            <p>Call : <?php echo $Phone?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="content-form-right">
                    <?php echo do_shortcode($contactShortcode)?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif ?>