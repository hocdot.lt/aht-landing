<?php
/**
 * Render section two in home page
 */
//GET config
$sectionTwoShow = get_theme_mod('section_two_show','yes');
$titleSectionTwo = get_theme_mod('title_section_two','A fun place for dogs! PetCare provides primary and specialty veterinary care for your pet.');
$linkSectionTwo = get_theme_mod('link_two','#');
$ImgSectionTwo = get_theme_mod('background_right_two', get_template_directory_uri() . '/images/images1.jpg');
$TwoCategory = get_theme_mod('section_two_category','');
$TwoLimit = get_theme_mod('section_two_number_post',6);

$args = array(
    'posts_per_page'   => $TwoLimit,
    'offset'           => 0,
    'cat'         => $TwoCategory,
    'post_type'        => 'post',
);
?>
<?php if ($sectionTwoShow === 'yes'):?>
<section id="section-two">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="row justify-content-end">
                    <div class="col-xl-6 col-lg-8 col-md-8">
                        <h2 class="font-weight-normal line-height-1 td-content-two"><?php echo $titleSectionTwo?></h2>
                        <div class="row content-two">
                            <div class="col-lg-8">
                                <div class="row">
                              <?php
                              // the query
                              $the_query = new WP_Query( $args ); ?>
                                    <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <div class="col-lg-4">
                                        <div class="feature-box feature-box-style-2 mb-4">
                                            <div class="feature-box-icon"><img src="<?php echo the_post_thumbnail_url()?>"></div>
                                            <div class="feature-box-info">
                                                <a class="font-weight-bold text-4 mb-0" href="<?php echo the_permalink()?>"><?php the_title(); ?></a>
                                                <p class="mb-4"><?php echo wp_trim_words( get_the_excerpt(), $num_words = 30, '' ) ?></p>
                                            </div>
                                        </div>
                                    </div>
                                     <?php endwhile;wp_reset_postdata(); ?>
                                <?php endif;?>
                                </div>
                            </div>
                        </div>
                        <div class="linkcenter text-center"><a href="<?php echo $linkSectionTwo?>">View All Services</a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 content-two-right">
                <div class="img-content-right-two"style="background-image: url(<?php echo $ImgSectionTwo?>)"><img src="<?php echo $ImgSectionTwo?>"></div>
            </div>
        </div>
    </div>
</section>
<!---->
<?php endif ?>