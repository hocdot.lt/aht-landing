
<?php
/**
 * Render banner in home page
 */
//GET config
$bannerShow = get_theme_mod('home_banner_settings','yes');
$bgBannerRight1 = get_theme_mod('img1_banner_right',get_template_directory_uri() . '/images/img-banner.jpg');
$bgBannerRight2 = get_theme_mod('img2_banner_right',get_template_directory_uri() . '/images/zoom1.jpg');
$titleBanner = get_theme_mod('banner_title','<h1>Everything you need to keep your cat, dog, or small pet happy and healthy</h1>');
$buttonBanner = get_theme_mod('button_1_banner','Join Us');
$buttonBannerLink = get_theme_mod('link_1_banner','#');
$button2Banner = get_theme_mod('button_2_banner','Explore');
$button2BannerLink = get_theme_mod('link_2_banner','#');

?>
<?php if($bannerShow === 'yes'): ?>
    <div class="forcefullwidth_wrapper_tp_banner" id="revolutionSlider_forcefullwidth" style="position:relative;width:100%;height:auto;margin-top:0px;margin-bottom:0px;">
        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="boxtop"></div>
                        <div class="content-left-banner">
                            <?php echo apply_filters( 'the_content', $titleBanner); ?>
                            <div class="btn-hd">
                                <div class="btn-header btn1">
                                    <a href="<?php echo $buttonBannerLink?>" ><?php echo $buttonBanner?></a>
                                </div>
                                <div class="btn-header btn2">
                                    <a href="<?php echo $button2BannerLink?>"><?php echo $button2Banner?></a>
                                </div>
                            </div>
                        </div>
                        <div class="boxbotom"></div>
                    </div>
                </div>
                <div class="content-right-banner">
                    <div class="boximg1"><img src="<?php echo $bgBannerRight1?>"></div>
                    <div class="boximg2"><img src="<?php echo $bgBannerRight2?>"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>